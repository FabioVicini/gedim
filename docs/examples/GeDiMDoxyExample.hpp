/*!
  \brief A test class demonstrating GeDiM documentation conventions.
  \details A more elaborate class description.
  \author List of authors.
  \copyright See top level LICENSE file for details.
*/
class GeDiMDoxyExample
{
protected:
  /// \brief A protected variable.
  /// \details Details.
  int protectedVar;

public:
  /// \brief An enum declaration.
  /// \details More detailed enum description.
  enum TEnum
  {
    TVal1, ///< Enum value TVal1.
    TVal2, ///< Enum value TVal2.
    TVal3  ///< Enum value TVal3.
  };

  /// \brief Enum pointer.
  /// \details Details.
  TEnum *enumPtr;

  TEnum enumVar; ///< Enum variable with brief description

  /// \brief A constructor.
  /// \details A more elaborate description of the constructor.
  /// \warning A warning about this object.
  /// \deprecated Example of deprecated entry.
  GeDiMDoxyExample();

  /// \brief A destructor.
  /// \details A more elaborate description of the destructor.
  /// \bug Example of bug entry.
  /// \todo Example of todo entry.
  ~GeDiMDoxyExample();

  /*!
    \brief A normal member taking two arguments and returning an integer value.
    \details A longer details comment, using the "/*!" syntax.  The
    "\sa" command is used to generate a "See also" section. \n LaTeX
    formulas can be added to descriptions: \f$f(x) = x^2+\delta\f$.
    \param a an integer argument.
    \param s a constant character pointer.
    \return The test results.
    \sa GeDiMDoxyExample(), ~GeDiMDoxyExample(),
    GeDiMDoxyExample::MethodWithCorrectDescription() and protectedVar.
  */
  int MethodWithCorrectDescription(int a, const char *s);

  /*!
    \brief A pure virtual member.
    \details The order in which you specify details is
    relevant. "Parameters" and "Returns" must always come first and
    the "See also" section must always be last.
    \sa MethodWithCorrectDescription().
    \param c1 the first argument.
    \param c2 the second argument.
  */
  virtual void MethodWithWrongDetailsOrder(char c1, char c2) = 0;

  /// \brief A function variable.
  /// \details Details.
  int (*handler)(int a,int b);
};
