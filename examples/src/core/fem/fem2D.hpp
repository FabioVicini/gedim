#ifndef __FEM2D_H
#define __FEM2D_H

namespace GeDiM
{
  namespace Examples
  {
    namespace Fem2D
    {
      class Main;
      class TwoDimensionExamples;

      class Main
      {
        public:
          static void RunAllExamples();
      };

      class TwoDimensionExamples
      {
        public:
          /// \brief 2D Elliptic Problem SingleDomain Triangle Primal Conforming Constant Lagrange Element Degree 1
          static void EP_SD_2D_Triangle_PCC_L1();
          /// \brief 2D Elliptic Problem SingleDomain Square Primal Conforming Constant Lagrange Element Degree 1
          static void EP_SD_2D_Square_PCC_L1();
      };
    }
  }
}

#endif // __FEM2D_H
