#include "fem2D.hpp"
#include "Output.hpp"
#include "Segment.hpp"
#include "Mesh.hpp"
#include "MeshCreator2DTriangle.hpp"
#include "Fem2DTriangleLagrangePCC.hpp"
#include "MeshDofHandler.hpp"
#include "FemValues.hpp"
#include "NewQuadrature.hpp"
#include "FemEllipticEquation.hpp"
#include "ConstantPhysicalParameter.hpp"
#include "VariablePhysicalParameter.hpp"
#include "ConstantBoundaryCondition.hpp"
#include "VariableBoundaryCondition.hpp"
#include "DomainAssembler.hpp"
#include "EigenCholeskySolver.hpp"
#include "CsvExporter.hpp"
#include "GeometryFactory.hpp"
#include "ShapeCreator.hpp"
#include "EigenSparseMatrix.hpp"
#include "EigenVector.hpp"
#include "Mapping1D2D.hpp"
#include "MeshCreator2DSquare.hpp"
#include "Fem2DSquareLagrangePCC.hpp"

using namespace MainApplication;

namespace GeDiM
{
  namespace Examples
  {
    namespace Fem2D
    {
      // ***************************************************************************
      void Main::RunAllExamples()
      {
        Output::PrintLine('-');
        Output::PrintGenericMessage("FEM Examples", true);
        Output::PrintLine('-');

        Output::PrintGenericMessage("FEM 2D Examples", true);
        TwoDimensionExamples::EP_SD_2D_Triangle_PCC_L1();
        TwoDimensionExamples::EP_SD_2D_Square_PCC_L1();
        Output::PrintLine('-');
      }
      // ***************************************************************************
      // Test 2D Forcing Term : 32.0*(y*(1.0-y)+ x*(1.0-x))
      Output::ExitCodes Test2DForcingTerm(const MatrixXd& points,
                                          VectorXd& results)
      {
        results = 32.0 * (points.row(1).array() * (1.0 - points.row(1).array()) +
                          points.row(0).array() * (1.0 - points.row(0).array()));
        return Output::Success;
      }
      // ***************************************************************************
      // Test 2D Neumann Condition : 16.0 * [ (1.0 - 2.0 * x) * y * (1.0 - y); (1.0 - 2.0 * y) * x * (1.0 - x) ]
      Output::ExitCodes Test2DNeumannCondition(const unsigned int& marker,
                                               const MatrixXd& points,
                                               VectorXd& results)
      {
        switch (marker)
        {
          case 2:
            results = - 16.0 * points.row(1).array() * (1.0 - points.row(1).array());
          return Output::Success;
          case 4:
            results = - 16.0 * points.row(0).array() * (1.0 - points.row(0).array());
          return Output::Success;
          default:
          return  Output::GenericError;
        }
      }
      // ***************************************************************************
      // Test 2D Real Solution : 16.0 * x*y*(1.0 - x)*(1.0 - y) + 1.1
      void Test2DSolution(const MatrixXd& points,
                          VectorXd& results)
      {
        results = 16.0 * (points.row(1).array() * (1.0 - points.row(1).array()) *
                          points.row(0).array() * (1.0 - points.row(0).array())) + 1.1;
      }
      // ***************************************************************************
      void TwoDimensionExamples::EP_SD_2D_Triangle_PCC_L1()
      {
        /// <ul>
        unsigned int solutionDimension = 1;
        unsigned int pointBoundaryCondition = 1;
        unsigned int leftBuoundaryCondition = 2, rightBoundaryCondition = 1;
        unsigned int upBuoundaryCondition = 1, downBoundaryCondition = 4;
        unsigned int femOrder = 1;

        size_t meshNumberCells = 40;
        bool symmetricProblem = true;

        /// <li> Create Domain
        GeometryFactory factory;
        ShapeCreator shapeCreator(factory);

        Polygon& geometry = shapeCreator.CreateSquare(Vector2d(0.0, 0.0),
                                                      1.0);

        geometry.Set2DPolygon();
        Output::Assert(geometry.ComputeRotatedVertices(), "%s: ComputeRotatedVertices", __func__);
        Output::Assert(geometry.ComputeMeasure(), "%s: ComputeMeasure", __func__);

        /// <li> Create Mesh
        Mesh mesh;
        MeshCreator2DTriangle meshCreator;

        meshCreator.SetMarkerDimension(solutionDimension);
        meshCreator.SetMinimumNumberOfCells(meshNumberCells);

        vector<unsigned int> boundaryConditionsVertices(4), boundaryConditionsEdges(4);
        for (unsigned int i = 0; i < solutionDimension; i++)
        {
          boundaryConditionsVertices[0] = pointBoundaryCondition;
          boundaryConditionsVertices[1] = pointBoundaryCondition;
          boundaryConditionsVertices[2] = pointBoundaryCondition;
          boundaryConditionsVertices[3] = pointBoundaryCondition;

          boundaryConditionsEdges[0] = downBoundaryCondition;
          boundaryConditionsEdges[1] = rightBoundaryCondition;
          boundaryConditionsEdges[2] = upBuoundaryCondition;
          boundaryConditionsEdges[3] = leftBuoundaryCondition;

          meshCreator.SetBoundaryConditions(boundaryConditionsVertices, boundaryConditionsEdges, i);
        }

        Output::Assert(meshCreator.CreateMesh(geometry, mesh), "%s: CreateMesh", __func__);


        Output::Assert(mesh.NumberOfMaximalCells() > 0, "%s: Check number mesh cells", __func__);
        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(0) != nullptr, "%s: Check first Cell", __func__);
        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(mesh.NumberOfMaximalCells() - 1) != nullptr, "%s: Check last Cell", __func__);
        /// <li> Create Reference Element
        Fem2DTriangleLagrangePCC referenceElement(femOrder);
        Output::Assert(referenceElement.Initialize(), "%s: Initialize Reference Element", __func__);

        /// <li> Create DofHandler
        MeshDofHandler dofHandler(referenceElement, mesh);

        dofHandler.SetSymmetricProblem(symmetricProblem);
        dofHandler.SetSolutionDimension(solutionDimension);

        Output::Assert(dofHandler.CreateDofs(), "%s: Create Dofs", __func__);
        Output::Assert(dofHandler.CreateDofPositions(), "%s: Create Dof Positions", __func__);
        Output::Assert(dofHandler.NumDofs() > 0 && dofHandler.NumDirichletDofs() > 0, "%s: Check Num Dofs", __func__);

        /// <li> Create Quadrature
        NewQuadrature internalQuadrature, neumannQuadrature;
        Output::Assert(internalQuadrature.Initialize(2, 3, IQuadrature::Type::Gauss), "%s: Create Internal Quadrature", __func__);
        Output::Assert(neumannQuadrature.Initialize(1, 3, IQuadrature::Type::Gauss), "%s: Create Neumann Quadrature", __func__);

        /// <li> Create values
        FemValues values;
        Output::Assert(values.Initialize(referenceElement,
                                         internalQuadrature), "%s: Initialize values", __func__);

        /// <li> Create Equation
        Mapping2D referenceElementMap;
        FemEllipticEquation equation(values,
                                     dofHandler,
                                     referenceElementMap);


        ConstantPhysicalParameter diffusionTerm(1.0);
        VariablePhysicalParameter forcingTerm(Test2DForcingTerm);

        ConstantBoundaryCondition dirichletConditions;
        dirichletConditions.SetValue(1, 1.1);

        Mapping1D2D neumannBorderMap;
        VariableBoundaryCondition neumannConditions(Test2DNeumannCondition);

        equation.SetForcingTerm(forcingTerm);
        equation.SetDiffusionTerm(diffusionTerm);
        equation.SetDirichletBoundaryConditions(dirichletConditions);
        equation.SetNeumannBoundaryConditions(neumannConditions,
                                              neumannQuadrature,
                                              neumannBorderMap);

        /// <li> Create Assembler
        DomainAssembler assembler;

        Map<const MatrixXd> dofPositions(NULL, 0, 0), dirichletPositions(NULL, 0, 0);

        Output::Assert(dofHandler.DofPositions(dofPositions), "%s: Get dofPositions", __func__);
        Output::Assert(dofHandler.DirichletPositions(dirichletPositions), "%s: Get dofPositions", __func__);

        EigenSparseMatrix<SparseMatrix<double>> globalMatrix, dirichletMatrix;
        EigenVector<VectorXd> rightHandSide, solutionDirichlet;

        Output::Assert(assembler.Initialize(equation,
                                            mesh,
                                            dofHandler,
                                            rightHandSide,
                                            solutionDirichlet,
                                            globalMatrix,
                                            dirichletMatrix), "%s: Initialize assembler", __func__);
        Output::Assert(assembler.AssembleDiscreteSystem(), "%s: AssembleDiscreteSystem", __func__);

        /// <li> Create Solver
        EigenVector<VectorXd> rightHandSideEquation = rightHandSide - dirichletMatrix * solutionDirichlet;
        EigenVector<VectorXd> solution;
        VectorXd& rSolution = solution;

        EigenCholeskySolverConfig solverConfig;
        solverConfig.SymmetricProblem = symmetricProblem;
        EigenCholeskySolver solver(solverConfig);
        Output::Assert(solver.Initialize(globalMatrix,
                                         rightHandSideEquation,
                                         solution), "%s: Initialize solver", __func__);

        Output::Assert(solver.Solve(), "%s: Solve system", __func__);


        /// <li> Check Solution
        const SparseMatrix<double>& rGlobalMatrix = globalMatrix;
        const VectorXd& rSolutionDirichlet = solutionDirichlet;
        unsigned int dimension = dofPositions.rows();
        unsigned int numDofs = dofPositions.cols(), numDirichlet = dirichletPositions.cols();
        unsigned int totalDimension = numDofs + numDirichlet;
        VectorXd realSolution, numericSolution(totalDimension);
        MatrixXd totalPoints(dimension, totalDimension);

        numericSolution<< rSolution, rSolutionDirichlet;
        totalPoints<< dofPositions, dirichletPositions;

        Test2DSolution(totalPoints, realSolution);

        double errorEnergyNorm = (realSolution.segment(0, dofPositions.cols()) - rSolution).transpose() *
                                 rGlobalMatrix * (realSolution.segment(0, dofPositions.cols()) - rSolution);

        Output::Assert(errorEnergyNorm < 1e-1, "%s: Check energy norm", __func__);

        VectorXd totalPointsX(totalDimension), totalPointsY(totalDimension);
        totalPointsX = totalPoints.row(0);
        totalPointsY = totalPoints.row(1);

        CsvExporter exportSolution;
        exportSolution.SetSeparator(',');
        exportSolution.AddColumn(totalPointsX.data(), totalPointsX.size(), "x");
        exportSolution.AddColumn(totalPointsY.data(), totalPointsY.size(), "y");
        exportSolution.AddColumn(numericSolution.data(), numericSolution.size(), "numeric");
        exportSolution.AddColumn(realSolution.data(), realSolution.size(), "real");

        exportSolution.Export("FEMTriangleTest2D.csv");

        /// </ul>
      }
      // ***************************************************************************
      void TwoDimensionExamples::EP_SD_2D_Square_PCC_L1()
      {
        /// <ul>
        unsigned int solutionDimension = 1;
        unsigned int pointBoundaryCondition = 1;
        unsigned int leftBuoundaryCondition = 2, rightBoundaryCondition = 1;
        unsigned int upBuoundaryCondition = 1, downBoundaryCondition = 4;
        unsigned int femOrder = 1;

        size_t meshNumberCells = 40;
        bool symmetricProblem = true;

        /// <li> Create Domain
        GeometryFactory factory;
        ShapeCreator shapeCreator(factory);

        Polygon& geometry = shapeCreator.CreateSquare(Vector2d(0.0, 0.0),
                                                      1.0);

        geometry.Set2DPolygon();
        Output::Assert(geometry.ComputeRotatedVertices(), "%s: ComputeRotatedVertices", __func__);
        Output::Assert(geometry.ComputeMeasure(), "%s: ComputeMeasure", __func__);

        /// <li> Create Mesh
        Mesh mesh;
        MeshCreator2DSquare meshCreator;

        meshCreator.SetMarkerDimension(solutionDimension);
        meshCreator.SetMinimumNumberOfCells(meshNumberCells);

        vector<unsigned int> boundaryConditionsVertices(4), boundaryConditionsEdges(4);
        for (unsigned int i = 0; i < solutionDimension; i++)
        {
          boundaryConditionsVertices[0] = pointBoundaryCondition;
          boundaryConditionsVertices[1] = pointBoundaryCondition;
          boundaryConditionsVertices[2] = pointBoundaryCondition;
          boundaryConditionsVertices[3] = pointBoundaryCondition;

          boundaryConditionsEdges[0] = downBoundaryCondition;
          boundaryConditionsEdges[1] = rightBoundaryCondition;
          boundaryConditionsEdges[2] = upBuoundaryCondition;
          boundaryConditionsEdges[3] = leftBuoundaryCondition;

          meshCreator.SetBoundaryConditions(boundaryConditionsVertices, boundaryConditionsEdges, i);
        }

        Output::Assert(meshCreator.CreateMesh(geometry, mesh), "%s: CreateMesh", __func__);

        Output::Assert(mesh.NumberOfMaximalCells() > 0, "%s: Check number mesh cells", __func__);
        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(0) != nullptr, "%s: Check first Cell", __func__);
        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(mesh.NumberOfMaximalCells() - 1) != nullptr, "%s: Check last Cell", __func__);
        /// <li> Create Reference Element
        Fem2DSquareLagrangePCC referenceElement(femOrder);
        Output::Assert(referenceElement.Initialize(), "%s: Initialize Reference Element", __func__);

        /// <li> Create DofHandler
        MeshDofHandler dofHandler(referenceElement, mesh);

        dofHandler.SetSymmetricProblem(symmetricProblem);
        dofHandler.SetSolutionDimension(solutionDimension);

        Output::Assert(dofHandler.CreateDofs(), "%s: Create Dofs", __func__);
        Output::Assert(dofHandler.CreateDofPositions(), "%s: Create Dof Positions", __func__);
        Output::Assert(dofHandler.NumDofs() > 0 && dofHandler.NumDirichletDofs() > 0, "%s: Check Num Dofs", __func__);

        /// <li> Create Quadrature
        NewQuadrature internalQuadrature, neumannQuadrature;
        Output::Assert(internalQuadrature.Initialize(2, 3, IQuadrature::Type::GaussSquare), "%s: Create Internal Quadrature", __func__);
        Output::Assert(neumannQuadrature.Initialize(1, 3, IQuadrature::Type::Gauss), "%s: Create Neumann Quadrature", __func__);

        /// <li> Create values
        FemValues values;
        Output::Assert(values.Initialize(referenceElement,
                                         internalQuadrature), "%s: Initialize values", __func__);

        /// <li> Create Equation
        Mapping2D referenceElementMap;
        FemEllipticEquation equation(values,
                                     dofHandler,
                                     referenceElementMap);


        ConstantPhysicalParameter diffusionTerm(1.0);
        VariablePhysicalParameter forcingTerm(Test2DForcingTerm);

        ConstantBoundaryCondition dirichletConditions;
        dirichletConditions.SetValue(1, 1.1);

        Mapping1D2D neumannBorderMap;
        VariableBoundaryCondition neumannConditions(Test2DNeumannCondition);

        equation.SetForcingTerm(forcingTerm);
        equation.SetDiffusionTerm(diffusionTerm);
        equation.SetDirichletBoundaryConditions(dirichletConditions);
        equation.SetNeumannBoundaryConditions(neumannConditions,
                                              neumannQuadrature,
                                              neumannBorderMap);

        /// <li> Create Assembler
        DomainAssembler assembler;

        Map<const MatrixXd> dofPositions(NULL, 0, 0), dirichletPositions(NULL, 0, 0);

        Output::Assert(dofHandler.DofPositions(dofPositions), "%s: Get dofPositions", __func__);
        Output::Assert(dofHandler.DirichletPositions(dirichletPositions), "%s: Get dofPositions", __func__);

        EigenSparseMatrix<SparseMatrix<double>> globalMatrix, dirichletMatrix;
        EigenVector<VectorXd> rightHandSide, solutionDirichlet;

        Output::Assert(assembler.Initialize(equation,
                                            mesh,
                                            dofHandler,
                                            rightHandSide,
                                            solutionDirichlet,
                                            globalMatrix,
                                            dirichletMatrix), "%s: Initialize assembler", __func__);
        Output::Assert(assembler.AssembleDiscreteSystem(), "%s: AssembleDiscreteSystem", __func__);

        /// <li> Create Solver
        EigenVector<VectorXd> rightHandSideEquation = rightHandSide - dirichletMatrix * solutionDirichlet;
        EigenVector<VectorXd> solution;
        VectorXd& rSolution = solution;

        EigenCholeskySolverConfig solverConfig;
        solverConfig.SymmetricProblem = symmetricProblem;
        EigenCholeskySolver solver(solverConfig);
        Output::Assert(solver.Initialize(globalMatrix,
                                         rightHandSideEquation,
                                         solution), "%s: Initialize solver", __func__);

        Output::Assert(solver.Solve(), "%s: Solve system", __func__);


        /// <li> Check Solution
        const SparseMatrix<double>& rGlobalMatrix = globalMatrix;
        const VectorXd& rSolutionDirichlet = solutionDirichlet;
        unsigned int dimension = dofPositions.rows();
        unsigned int numDofs = dofPositions.cols(), numDirichlet = dirichletPositions.cols();
        unsigned int totalDimension = numDofs + numDirichlet;
        VectorXd realSolution, numericSolution(totalDimension);
        MatrixXd totalPoints(dimension, totalDimension);

        numericSolution<< rSolution, rSolutionDirichlet;
        totalPoints<< dofPositions, dirichletPositions;

        Test2DSolution(totalPoints, realSolution);

        double errorEnergyNorm = (realSolution.segment(0, dofPositions.cols()) - rSolution).transpose() *
                                 rGlobalMatrix * (realSolution.segment(0, dofPositions.cols()) - rSolution);

        Output::Assert(errorEnergyNorm < 1e-1, "%s: Check energy norm", __func__);

        VectorXd totalPointsX(totalDimension), totalPointsY(totalDimension);
        totalPointsX = totalPoints.row(0);
        totalPointsY = totalPoints.row(1);

        CsvExporter exportSolution;
        exportSolution.SetSeparator(',');
        exportSolution.AddColumn(totalPointsX.data(), totalPointsX.size(), "x");
        exportSolution.AddColumn(totalPointsY.data(), totalPointsY.size(), "y");
        exportSolution.AddColumn(numericSolution.data(), numericSolution.size(), "numeric");
        exportSolution.AddColumn(realSolution.data(), realSolution.size(), "real");

        exportSolution.Export("FEMSquareTest2D.csv");

        /// </ul>
      }
      // ***************************************************************************
    }
  }
}
