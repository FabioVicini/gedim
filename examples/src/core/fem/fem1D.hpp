#ifndef __FEM1D_H
#define __FEM1D_H

namespace GeDiM
{
  namespace Examples
  {
    namespace Fem1D
    {
      class Main;
      class OneDimensionExamples;

      class Main
      {
        public:
          static void RunAllExamples();
      };

      class OneDimensionExamples
      {
        public:
          /// \brief 1D Elliptic Problem SingleDomain Primal Conforming Constant Lagrange Element Degree 1
          static void EP_SD_1D_PCC_L1();
      };
    }
  }
}

#endif // __FEM1D_H
