#include "fem1D.hpp"
#include "Output.hpp"
#include "Segment.hpp"
#include "Mesh.hpp"
#include "MeshCreator1DEquispaced.hpp"
#include "Fem1DLagrangePCC.hpp"
#include "MeshDofHandler.hpp"
#include "FemValues.hpp"
#include "NewQuadrature.hpp"
#include "FemEllipticEquation.hpp"
#include "ConstantPhysicalParameter.hpp"
#include "VariablePhysicalParameter.hpp"
#include "ConstantBoundaryCondition.hpp"
#include "VariableBoundaryCondition.hpp"
#include "DomainAssembler.hpp"
#include "EigenCholeskySolver.hpp"
#include "CsvExporter.hpp"
#include "GeometryFactory.hpp"
#include "ShapeCreator.hpp"
#include "EigenSparseMatrix.hpp"
#include "EigenVector.hpp"
#include "Mapping0D.hpp"

using namespace MainApplication;

namespace GeDiM
{
  namespace Examples
  {
    namespace Fem1D
    {
      // ***************************************************************************
      void Main::RunAllExamples()
      {
        Output::PrintLine('-');
        Output::PrintGenericMessage("FEM Examples", true);
        Output::PrintLine('-');

        Output::PrintGenericMessage("FEM 1D Examples", true);
        OneDimensionExamples::EP_SD_1D_PCC_L1();
        Output::PrintLine('-');
      }
      // ***************************************************************************
      // Test 1D Dirichlet Condition : 4.0 * x * (1.0 - x) + 1.5
      Output::ExitCodes Test1DDirichletCondition(const unsigned int& marker,
                                                 const MatrixXd& points,
                                                 VectorXd& results)
      {
        switch (marker)
        {
          case 1:
            results = 4.0 * points.row(0).array() * (1.0 - points.row(0).array()) + 1.5;
          return Output::Success;
          case 3:
            results = 4.0 * points.row(0).array() * (1.0 - points.row(0).array()) + 1.5;
          return Output::Success;
          default:
          return  Output::GenericError;
        }
      }
      // ***************************************************************************
      // Test 1D Neumann Condition : 4.0 * (1.0 - 2.0 * x)
      Output::ExitCodes Test1DNeumannCondition(const unsigned int& marker,
                                               const MatrixXd& points,
                                               VectorXd& results)
      {
        switch (marker)
        {
          case 2:
            results = 4 * (1.0 - 2.0 * points.row(0).array());
          return Output::Success;
          default:
          return  Output::GenericError;
        }
      }
      // ***************************************************************************
      // Test 1D Real Solution : 4.0 * x * (1.0 - x) + 1.5
      void Test1DSolution(const MatrixXd& points,
                          VectorXd& results)
      {
        results = 4.0 * points.row(0).array() * (1.0 - points.row(0).array()) + 1.5;
      }
      // ***************************************************************************
      void OneDimensionExamples::EP_SD_1D_PCC_L1()
      {
        /// <ul>
        unsigned int solutionDimension = 1;
        unsigned int leftBuoundaryCondition = 1, rightBoundaryCondition = 2;
        unsigned int femOrder = 1;

        size_t meshNumberCells = 7;
        bool symmetricProblem = true;

        /// <li> Create Domain
        GeometryFactory factory;
        ShapeCreator shapeCreator(factory);

        Segment& geometry = shapeCreator.CreateSegment(Vector3d(0.0, 0.0, 0.0),
                                                       Vector3d(1.0, 0.0, 0.0));

        Output::Assert(geometry.ComputeMeasure(), "%s: ComputeMeasure", __func__);

        /// <li> Create Mesh
        Mesh mesh;

        MeshCreator1DEquispaced meshCreator;

        meshCreator.SetMarkerDimension(solutionDimension);
        meshCreator.SetMinimumNumberOfCells(meshNumberCells);

        vector<unsigned int> boundaryConditionsVertices(2);
        for (unsigned int i = 0; i < solutionDimension; i++)
        {
          boundaryConditionsVertices[0] = leftBuoundaryCondition;
          boundaryConditionsVertices[1] = rightBoundaryCondition;

          meshCreator.SetBoundaryConditions(boundaryConditionsVertices, i);
        }

        Output::Assert(meshCreator.CreateMesh(geometry, mesh), "%s: CreateMesh", __func__);

        Output::Assert(mesh.NumberOfMaximalCells() == meshNumberCells, "%s: Check number mesh cells", __func__);
        Output::Assert(mesh.NumberOfCells0D() == meshNumberCells + 1, "%s: Check number mesh points", __func__);
        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(0) != nullptr, "%s: Check first Cell", __func__);

        for (unsigned int s = 0; s < solutionDimension; s++)
          Output::Assert(mesh.GetCell0D(0)->Marker(s) == leftBuoundaryCondition, "%s: Check Marker of solution %d in first Cell", __func__, s);

        Output::Assert(mesh.GetMaximalTreeNode(meshNumberCells - 1) != nullptr, "%s: Check last Cell", __func__);

        for (unsigned int s = 0; s < solutionDimension; s++)
          Output::Assert(mesh.GetCell0D(meshNumberCells)->Marker(s) == rightBoundaryCondition, "%s: Check Marker of solution %d in last Cell", __func__, s);

        /// <li> Create Reference Element
        Fem1DLagrangePCC referenceElement(femOrder);
        Output::Assert(referenceElement.Initialize(), "%s: Initialize Reference Element", __func__);

        /// <li> Create DofHandler
        MeshDofHandler dofHandler(referenceElement, mesh);

        dofHandler.SetSymmetricProblem(symmetricProblem);
        dofHandler.SetSolutionDimension(solutionDimension);

        Output::Assert(dofHandler.CreateDofs(), "%s: Create Dofs", __func__);
        Output::Assert(dofHandler.CreateDofPositions(), "%s: Create Dof Positions", __func__);
        Output::Assert(dofHandler.NumDofs() == solutionDimension * (meshNumberCells + meshNumberCells * (femOrder - 1)) && dofHandler.NumDirichletDofs() == solutionDimension, "%s: Check Num Dofs", __func__);

        /// <li> Create Quadrature
        NewQuadrature internalQuadrature, neumannQuadrature;
        Output::Assert(internalQuadrature.Initialize(1, 3, IQuadrature::Type::Gauss), "%s: Create Internal Quadrature", __func__);

        /// <li> Create values
        FemValues values;
        Output::Assert(values.Initialize(referenceElement, internalQuadrature), "%s: Initialize values", __func__);

        /// <li> Create Equation
        Mapping1D referenceElementMap;
        FemEllipticEquation equation(values,
                                     dofHandler,
                                     referenceElementMap);


        ConstantPhysicalParameter diffusionTerm(1.0);
        ConstantPhysicalParameter forcingTerm(8.0);

        Mapping0D neumannBorderMap;
        VariableBoundaryCondition dirichletConditions(Test1DDirichletCondition);
        VariableBoundaryCondition neumannConditions(Test1DNeumannCondition);

        equation.SetForcingTerm(forcingTerm);
        equation.SetDiffusionTerm(diffusionTerm);
        equation.SetDirichletBoundaryConditions(dirichletConditions);
        equation.SetNeumannBoundaryConditions(neumannConditions,
                                              neumannQuadrature,
                                              neumannBorderMap);

        /// <li> Create Assembler
        DomainAssembler assembler;

        Map<const MatrixXd> dofPositions(NULL, 0, 0), dirichletPositions(NULL, 0, 0);

        Output::Assert(dofHandler.DofPositions(dofPositions), "%s: Get dofPositions", __func__);
        Output::Assert(dofHandler.DirichletPositions(dirichletPositions), "%s: Get Dirichlet dofPositions", __func__);

        EigenSparseMatrix<SparseMatrix<double>> globalMatrix, dirichletMatrix;
        EigenVector<VectorXd> rightHandSide, solutionDirichlet;

        Output::Assert(assembler.Initialize(equation,
                                            mesh,
                                            dofHandler,
                                            rightHandSide,
                                            solutionDirichlet,
                                            globalMatrix,
                                            dirichletMatrix), "%s: Initialize assembler", __func__);
        Output::Assert(assembler.AssembleDiscreteSystem(), "%s: AssembleDiscreteSystem", __func__);

        /// <li> Create Solver
        EigenVector<VectorXd> rightHandSideEquation = rightHandSide - dirichletMatrix * solutionDirichlet;
        EigenVector<VectorXd> solution;
        VectorXd& rSolution = solution;

        EigenCholeskySolverConfig solverConfig;
        solverConfig.SymmetricProblem = symmetricProblem;
        EigenCholeskySolver solver(solverConfig);
        Output::Assert(solver.Initialize(globalMatrix,
                                         rightHandSideEquation,
                                         solution), "%s: Initialize solver", __func__);

        Output::Assert(solver.Solve(), "%s: Solve system", __func__);


        /// <li> Check Solution
        const SparseMatrix<double>& rGlobalMatrix = globalMatrix;
        const VectorXd& rSolutionDirichlet = solutionDirichlet;
        unsigned int dimension = dofPositions.rows();
        unsigned int numDofs = dofPositions.cols(), numDirichlet = dirichletPositions.cols();
        unsigned int totalDimension = numDofs + numDirichlet;
        VectorXd realSolution, numericSolution(totalDimension);
        MatrixXd totalPoints(dimension, totalDimension);

        numericSolution<< rSolution, rSolutionDirichlet;
        totalPoints<< dofPositions, dirichletPositions;

        Test1DSolution(totalPoints, realSolution);

        double errorEnergyNorm = (realSolution.segment(0, dofPositions.cols()) - rSolution).transpose() *
                                 rGlobalMatrix * (realSolution.segment(0, dofPositions.cols()) - rSolution);

        Output::Assert(errorEnergyNorm < 1e-12, "%s: Check energy norm", __func__);

        CsvExporter exportSolution;
        exportSolution.SetSeparator(',');
        exportSolution.AddColumn(totalPoints.data(), totalPoints.size(), "x");
        exportSolution.AddColumn(numericSolution.data(), numericSolution.size(), "numeric");
        exportSolution.AddColumn(realSolution.data(), realSolution.size(), "real");

        exportSolution.Export("FEMTest1D.csv");

        /// </ul>
      }
      // ***************************************************************************
    }
  }
}
