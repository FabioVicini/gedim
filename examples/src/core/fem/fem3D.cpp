#include "fem3D.hpp"
#include "Output.hpp"
#include "Segment.hpp"
#include "Mesh.hpp"
#include "MeshCreator3DTetgen.hpp"
#include "Fem3DTetrahedronLagrangePCC.hpp"
#include "MeshDofHandler.hpp"
#include "FemValues.hpp"
#include "NewQuadrature.hpp"
#include "FemEllipticEquation.hpp"
#include "ConstantPhysicalParameter.hpp"
#include "VariablePhysicalParameter.hpp"
#include "ConstantBoundaryCondition.hpp"
#include "VariableBoundaryCondition.hpp"
#include "DomainAssembler.hpp"
#include "EigenCholeskySolver.hpp"
#include "CsvExporter.hpp"
#include "GeometryFactory.hpp"
#include "ShapeCreator.hpp"
#include "EigenSparseMatrix.hpp"
#include "EigenVector.hpp"
#include "Mapping2D3D.hpp"
#include "MeshCreator3DHexahedron.hpp"
#include "Fem3DHexahedronLagrangePCC.hpp"

using namespace MainApplication;

namespace GeDiM
{
  namespace Examples
  {
    namespace Fem3D
    {
      // ***************************************************************************
      void Main::RunAllExamples()
      {
        Output::PrintLine('-');
        Output::PrintGenericMessage("FEM Examples", true);
        Output::PrintLine('-');

        Output::PrintGenericMessage("FEM 3D Examples", true);
        ThreeDimensionExamples::EP_SD_3D_Tetrahedron_PCC_L1();
        ThreeDimensionExamples::EP_SD_3D_Hexahedron_PCC_L1();
        Output::PrintLine('-');
      }
      // ***************************************************************************
      // Test 3D Forcing Term : 32.0*(y*(1.0-y) + x*(1.0-x))
      Output::ExitCodes Test3DForcingTerm(const MatrixXd& points,
                                          VectorXd& results)
      {
        results = 128.0 * (points.row(1).array() * (1.0 - points.row(1).array()) *
                           points.row(2).array() * (1.0 - points.row(2).array()) +
                           points.row(0).array() * (1.0 - points.row(0).array()) *
                           points.row(2).array() * (1.0 - points.row(2).array()) +
                           points.row(0).array() * (1.0 - points.row(0).array()) *
                           points.row(1).array() * (1.0 - points.row(1).array()));
        return Output::Success;
      }
      // ***************************************************************************
      // Test 3D Neumann Condition : 16.0 * [ (1.0 - 2.0 * x) * y * (1.0 - y); (1.0 - 2.0 * y) * x * (1.0 - x) ]
      Output::ExitCodes Test3DNeumannCondition(const unsigned int& marker,
                                               const MatrixXd& points,
                                               VectorXd& results)
      {
        switch (marker)
        {
          case 2:
            results = - 64.0 *
                      points.row(1).array() *
                      (1.0 - points.row(1).array()) *
                      points.row(2).array() *
                      (1.0 - points.row(2).array());
          return Output::Success;
          case 4:
            results = - 64.0 *
                      points.row(0).array() *
                      (1.0 - points.row(0).array()) *
                      points.row(1).array() *
                      (1.0 - points.row(1).array());
          return Output::Success;
          default:
          return  Output::GenericError;
        }
      }
      // ***************************************************************************
      // Test 3D Real Solution : 64.0 * x*y*z*(1.0 - x)*(1.0 - y)*(1.0 - z) + 1.7
      void Test3DSolution(const MatrixXd& points,
                          VectorXd& results)
      {
        results = 64.0 * (points.row(2).array() * (1.0 - points.row(2).array()) *
                          points.row(1).array() * (1.0 - points.row(1).array()) *
                          points.row(0).array() * (1.0 - points.row(0).array())) + 1.7;
      }
      // ***************************************************************************
      void ThreeDimensionExamples::EP_SD_3D_Tetrahedron_PCC_L1()
      {
        /// <ul>
        unsigned int solutionDimension = 1;
        unsigned int pointBoundaryCondition = 1;
        unsigned int leftBuoundaryCondition = 1, rightBoundaryCondition = 1;
        unsigned int upBuoundaryCondition = 4, downBoundaryCondition = 1;
        unsigned int frontBuoundaryCondition = 1, backBoundaryCondition = 2;
        unsigned int femOrder = 1;

        size_t meshNumberCells = 150;
        bool symmetricProblem = true;

        /// <li> Create Domain
        GeometryFactory factory;
        ShapeCreator shapeCreator(factory);

        Polyhedron& geometry = shapeCreator.CreateCube(Vector3d(0.0, 0.0, 0.0),
                                                       1.0);

        /// <li> Compute Domain Geometric Properties
        for (unsigned int f = 0; f < geometry.NumberOfFaces(); f++)
        {
          const Polygon& face = *geometry.Face(f);
          Polygon& polygon = factory.GetPolygon(face.Id());
          polygon.ComputeRotatedVertices();
        }
        Output::Assert(geometry.ComputeMeasure(), "%s: ComputeMeasure", __func__);
        Output::Assert(geometry.ComputeCentroid(), "%s: ComputeCentroid", __func__);
        Output::Assert(geometry.ComputeBarycenter(), "%s: ComputeBarycenter", __func__);
        Output::Assert(geometry.ComputeSquaredRadius(), "%s: ComputeSquaredRadius", __func__);

        /// <li> Create Mesh
        Mesh mesh;
        MeshCreator3DTetgen meshCreator;

        meshCreator.SetMarkerDimension(solutionDimension);
        meshCreator.SetMinimumNumberOfCells(meshNumberCells);

        vector<unsigned int> boundaryConditionsVertices(8), boundaryConditionsEdges(12), boundaryConditionsFaces(6);
        for (unsigned int i = 0; i < solutionDimension; i++)
        {
          boundaryConditionsVertices[0] = pointBoundaryCondition;
          boundaryConditionsVertices[1] = pointBoundaryCondition;
          boundaryConditionsVertices[2] = pointBoundaryCondition;
          boundaryConditionsVertices[3] = pointBoundaryCondition;
          boundaryConditionsVertices[4] = pointBoundaryCondition;
          boundaryConditionsVertices[5] = pointBoundaryCondition;
          boundaryConditionsVertices[6] = pointBoundaryCondition;
          boundaryConditionsVertices[7] = pointBoundaryCondition;

          boundaryConditionsEdges[0] = pointBoundaryCondition;
          boundaryConditionsEdges[1] = pointBoundaryCondition;
          boundaryConditionsEdges[2] = pointBoundaryCondition;
          boundaryConditionsEdges[3] = pointBoundaryCondition;
          boundaryConditionsEdges[0]= pointBoundaryCondition;
          boundaryConditionsEdges[1]= pointBoundaryCondition;
          boundaryConditionsEdges[2]= pointBoundaryCondition;
          boundaryConditionsEdges[3]= pointBoundaryCondition;
          boundaryConditionsEdges[4]= pointBoundaryCondition;
          boundaryConditionsEdges[5]= pointBoundaryCondition;
          boundaryConditionsEdges[6]= pointBoundaryCondition;
          boundaryConditionsEdges[7]= pointBoundaryCondition;
          boundaryConditionsEdges[8]= pointBoundaryCondition;
          boundaryConditionsEdges[9]= pointBoundaryCondition;
          boundaryConditionsEdges[10]= pointBoundaryCondition;
          boundaryConditionsEdges[11]= pointBoundaryCondition;

          boundaryConditionsFaces[0]= downBoundaryCondition;
          boundaryConditionsFaces[1]= upBuoundaryCondition;
          boundaryConditionsFaces[2]= backBoundaryCondition;
          boundaryConditionsFaces[3]= frontBuoundaryCondition;
          boundaryConditionsFaces[4]= leftBuoundaryCondition;
          boundaryConditionsFaces[5]= rightBoundaryCondition;

          meshCreator.SetBoundaryConditions(boundaryConditionsVertices,
                                            boundaryConditionsEdges,
                                            boundaryConditionsFaces, i);
        }

        Output::Assert(meshCreator.CreateMesh(geometry, mesh), "%s: CreateMesh", __func__);

        Output::Assert(mesh.NumberOfMaximalCells() > 0, "%s: Check number mesh cells", __func__);
        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(0) != nullptr, "%s: Check first Cell", __func__);

        for (unsigned int s = 0; s < solutionDimension; s++)
          Output::Assert(mesh.NumberOfCells0D() > 0 && mesh.GetCell0D(0)->Marker(s) == leftBuoundaryCondition, "%s: Check Marker of solution %d in first Cell", __func__, s);

        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(mesh.NumberOfMaximalCells() - 1) != nullptr, "%s: Check last Cell", __func__);

        /// <li> Create Reference Element
        Fem3DTetrahedronLagrangePCC referenceElement(femOrder);
        Output::Assert(referenceElement.Initialize(), "%s: Initialize Reference Element", __func__);

        /// <li> Create DofHandler
        MeshDofHandler dofHandler(referenceElement, mesh);

        dofHandler.SetSymmetricProblem(symmetricProblem);
        dofHandler.SetSolutionDimension(solutionDimension);

        Output::Assert(dofHandler.CreateDofs(), "%s: Create Dofs", __func__);
        Output::Assert(dofHandler.CreateDofPositions(), "%s: Create Dof Positions", __func__);
        Output::Assert(dofHandler.NumDofs() > 0 && dofHandler.NumDirichletDofs() > 0, "%s: Check Num Dofs", __func__);

        /// <li> Create Quadrature
        NewQuadrature internalQuadrature, neumannQuadrature;
        Output::Assert(internalQuadrature.Initialize(3, 3, IQuadrature::Type::Gauss), "%s: Create Internal Quadrature", __func__);
        Output::Assert(neumannQuadrature.Initialize(2, 3, IQuadrature::Type::Gauss), "%s: Create Neumann Quadrature", __func__);

        /// <li> Create values
        FemValues values;
        Output::Assert(values.Initialize(referenceElement,
                                         internalQuadrature), "%s: Initialize values", __func__);

        /// <li> Create Equation
        Mapping3D referenceElementMap;
        FemEllipticEquation equation(values,
                                     dofHandler,
                                     referenceElementMap);


        ConstantPhysicalParameter diffusionTerm(1.0);
        VariablePhysicalParameter forcingTerm(Test3DForcingTerm);

        ConstantBoundaryCondition dirichletConditions;
        dirichletConditions.SetValue(1, 1.7);

        Mapping2D3D neumannBorderMap;
        VariableBoundaryCondition neumannConditions(Test3DNeumannCondition);

        equation.SetForcingTerm(forcingTerm);
        equation.SetDiffusionTerm(diffusionTerm);
        equation.SetDirichletBoundaryConditions(dirichletConditions);
        equation.SetNeumannBoundaryConditions(neumannConditions,
                                              neumannQuadrature,
                                              neumannBorderMap);

        /// <li> Create Assembler
        DomainAssembler assembler;

        Map<const MatrixXd> dofPositions(NULL, 0, 0), dirichletPositions(NULL, 0, 0);

        Output::Assert(dofHandler.DofPositions(dofPositions), "%s: Get dofPositions", __func__);
        Output::Assert(dofHandler.DirichletPositions(dirichletPositions), "%s: Get dofPositions", __func__);

        EigenSparseMatrix<SparseMatrix<double>> globalMatrix, dirichletMatrix;
        EigenVector<VectorXd> rightHandSide, solutionDirichlet;

        Output::Assert(assembler.Initialize(equation,
                                            mesh,
                                            dofHandler,
                                            rightHandSide,
                                            solutionDirichlet,
                                            globalMatrix,
                                            dirichletMatrix), "%s: Initialize assembler", __func__);
        Output::Assert(assembler.AssembleDiscreteSystem(), "%s: AssembleDiscreteSystem", __func__);

        /// <li> Create Solver
        EigenVector<VectorXd> rightHandSideEquation = rightHandSide - dirichletMatrix * solutionDirichlet;
        EigenVector<VectorXd> solution;
        VectorXd& rSolution = solution;

        EigenCholeskySolverConfig solverConfig;
        solverConfig.SymmetricProblem = symmetricProblem;
        EigenCholeskySolver solver(solverConfig);
        Output::Assert(solver.Initialize(globalMatrix,
                                         rightHandSideEquation,
                                         solution), "%s: Initialize solver", __func__);

        Output::Assert(solver.Solve(), "%s: Solve system", __func__);


        /// <li> Check Solution
        const SparseMatrix<double>& rGlobalMatrix = globalMatrix;
        const VectorXd& rSolutionDirichlet = solutionDirichlet;
        unsigned int dimension = dofPositions.rows();
        unsigned int numDofs = dofPositions.cols(), numDirichlet = dirichletPositions.cols();
        unsigned int totalDimension = numDofs + numDirichlet;
        VectorXd realSolution, numericSolution(totalDimension);
        MatrixXd totalPoints(dimension, totalDimension);

        numericSolution<< rSolution, rSolutionDirichlet;
        totalPoints<< dofPositions, dirichletPositions;

        Test3DSolution(totalPoints, realSolution);

        double errorEnergyNorm = (realSolution.segment(0, dofPositions.cols()) - rSolution).transpose() *
                                 rGlobalMatrix * (realSolution.segment(0, dofPositions.cols()) - rSolution);

        Output::Assert(errorEnergyNorm < 1e-0, "%s: Check energy norm", __func__);

        VectorXd totalPointsX(totalDimension), totalPointsY(totalDimension), totalPointsZ(totalDimension);
        totalPointsX = totalPoints.row(0);
        totalPointsY = totalPoints.row(1);
        totalPointsZ = totalPoints.row(2);

        CsvExporter exportSolution;
        exportSolution.SetSeparator(',');
        exportSolution.AddColumn(totalPointsX.data(), totalPointsX.size(), "x");
        exportSolution.AddColumn(totalPointsY.data(), totalPointsY.size(), "y");
        exportSolution.AddColumn(totalPointsZ.data(), totalPointsZ.size(), "z");
        exportSolution.AddColumn(numericSolution.data(), numericSolution.size(), "numeric");
        exportSolution.AddColumn(realSolution.data(), realSolution.size(), "real");

        exportSolution.Export("FEMTetrahedronTest3D.csv");

        /// </ul>
      }
      // ***************************************************************************
      void ThreeDimensionExamples::EP_SD_3D_Hexahedron_PCC_L1()
      {
        /// <ul>
        unsigned int solutionDimension = 1;
        unsigned int pointBoundaryCondition = 1;
        unsigned int leftBuoundaryCondition = 1, rightBoundaryCondition = 1;
        unsigned int upBuoundaryCondition = 4, downBoundaryCondition = 1;
        unsigned int frontBuoundaryCondition = 1, backBoundaryCondition = 2;
        unsigned int femOrder = 1;

        size_t meshNumberCells = 20;
        bool symmetricProblem = true;

        /// <li> Create Domain
        GeometryFactory factory;
        ShapeCreator shapeCreator(factory);

        Polyhedron& geometry = shapeCreator.CreateCube(Vector3d(0.0, 0.0, 0.0),
                                                       1.0);

        /// <li> Compute Domain Geometric Properties
        for (unsigned int f = 0; f < geometry.NumberOfFaces(); f++)
        {
          const Polygon& face = *geometry.Face(f);
          Polygon& polygon = factory.GetPolygon(face.Id());
          polygon.ComputeRotatedVertices();
        }
        Output::Assert(geometry.ComputeMeasure(), "%s: ComputeMeasure", __func__);
        Output::Assert(geometry.ComputeCentroid(), "%s: ComputeCentroid", __func__);
        Output::Assert(geometry.ComputeBarycenter(), "%s: ComputeBarycenter", __func__);
        Output::Assert(geometry.ComputeSquaredRadius(), "%s: ComputeSquaredRadius", __func__);

        /// <li> Create Mesh
        Mesh mesh;
        MeshCreator3DHexahedron meshCreator;

        meshCreator.SetMarkerDimension(solutionDimension);
        meshCreator.SetMinimumNumberOfCells(meshNumberCells);

        vector<unsigned int> boundaryConditionsVertices(8), boundaryConditionsEdges(12), boundaryConditionsFaces(6);
        for (unsigned int i = 0; i < solutionDimension; i++)
        {
          boundaryConditionsVertices[0] = pointBoundaryCondition;
          boundaryConditionsVertices[1] = pointBoundaryCondition;
          boundaryConditionsVertices[2] = pointBoundaryCondition;
          boundaryConditionsVertices[3] = pointBoundaryCondition;
          boundaryConditionsVertices[4] = pointBoundaryCondition;
          boundaryConditionsVertices[5] = pointBoundaryCondition;
          boundaryConditionsVertices[6] = pointBoundaryCondition;
          boundaryConditionsVertices[7] = pointBoundaryCondition;

          boundaryConditionsEdges[0] = pointBoundaryCondition;
          boundaryConditionsEdges[1] = pointBoundaryCondition;
          boundaryConditionsEdges[2] = pointBoundaryCondition;
          boundaryConditionsEdges[3] = pointBoundaryCondition;
          boundaryConditionsEdges[0]= pointBoundaryCondition;
          boundaryConditionsEdges[1]= pointBoundaryCondition;
          boundaryConditionsEdges[2]= pointBoundaryCondition;
          boundaryConditionsEdges[3]= pointBoundaryCondition;
          boundaryConditionsEdges[4]= pointBoundaryCondition;
          boundaryConditionsEdges[5]= pointBoundaryCondition;
          boundaryConditionsEdges[6]= pointBoundaryCondition;
          boundaryConditionsEdges[7]= pointBoundaryCondition;
          boundaryConditionsEdges[8]= pointBoundaryCondition;
          boundaryConditionsEdges[9]= pointBoundaryCondition;
          boundaryConditionsEdges[10]= pointBoundaryCondition;
          boundaryConditionsEdges[11]= pointBoundaryCondition;

          boundaryConditionsFaces[0]= downBoundaryCondition;
          boundaryConditionsFaces[1]= upBuoundaryCondition;
          boundaryConditionsFaces[2]= backBoundaryCondition;
          boundaryConditionsFaces[3]= frontBuoundaryCondition;
          boundaryConditionsFaces[4]= leftBuoundaryCondition;
          boundaryConditionsFaces[5]= rightBoundaryCondition;

          meshCreator.SetBoundaryConditions(boundaryConditionsVertices,
                                            boundaryConditionsEdges,
                                            boundaryConditionsFaces, i);
        }

        Output::Assert(meshCreator.CreateMesh(geometry, mesh), "%s: CreateMesh", __func__);

        Output::Assert(mesh.NumberOfMaximalCells() > 0, "%s: Check number mesh cells", __func__);
        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(0) != nullptr, "%s: Check first Cell", __func__);

        for (unsigned int s = 0; s < solutionDimension; s++)
          Output::Assert(mesh.NumberOfCells0D() > 0 && mesh.GetCell0D(0)->Marker(s) == leftBuoundaryCondition, "%s: Check Marker of solution %d in first Cell", __func__, s);

        Output::Assert(mesh.NumberOfMaximalCells() > 0 && mesh.GetMaximalTreeNode(mesh.NumberOfMaximalCells() - 1) != nullptr, "%s: Check last Cell", __func__);

        /// <li> Create Reference Element
        Fem3DHexahedronLagrangePCC referenceElement(femOrder);
        Output::Assert(referenceElement.Initialize(), "%s: Initialize Reference Element", __func__);

        /// <li> Create DofHandler
        MeshDofHandler dofHandler(referenceElement, mesh);

        dofHandler.SetSymmetricProblem(symmetricProblem);
        dofHandler.SetSolutionDimension(solutionDimension);

        Output::Assert(dofHandler.CreateDofs(), "%s: Create Dofs", __func__);
        Output::Assert(dofHandler.CreateDofPositions(), "%s: Create Dof Positions", __func__);
        Output::Assert(dofHandler.NumDofs() > 0 && dofHandler.NumDirichletDofs() > 0, "%s: Check Num Dofs", __func__);

        /// <li> Create Quadrature
        NewQuadrature internalQuadrature, neumannQuadrature;
        Output::Assert(internalQuadrature.Initialize(3, 3, IQuadrature::Type::GaussHexahedron), "%s: Create Internal Quadrature", __func__);
        Output::Assert(neumannQuadrature.Initialize(2, 3, IQuadrature::Type::GaussSquare), "%s: Create Neumann Quadrature", __func__);

        /// <li> Create values
        FemValues values;
        Output::Assert(values.Initialize(referenceElement,
                                         internalQuadrature), "%s: Initialize values", __func__);

        /// <li> Create Equation
        Mapping3D referenceElementMap;
        FemEllipticEquation equation(values,
                                     dofHandler,
                                     referenceElementMap);


        ConstantPhysicalParameter diffusionTerm(1.0);
        VariablePhysicalParameter forcingTerm(Test3DForcingTerm);

        ConstantBoundaryCondition dirichletConditions;
        dirichletConditions.SetValue(1, 1.7);

        Mapping2D3D neumannBorderMap;
        VariableBoundaryCondition neumannConditions(Test3DNeumannCondition);

        equation.SetForcingTerm(forcingTerm);
        equation.SetDiffusionTerm(diffusionTerm);
        equation.SetDirichletBoundaryConditions(dirichletConditions);
        equation.SetNeumannBoundaryConditions(neumannConditions,
                                              neumannQuadrature,
                                              neumannBorderMap);

        /// <li> Create Assembler
        DomainAssembler assembler;

        Map<const MatrixXd> dofPositions(NULL, 0, 0), dirichletPositions(NULL, 0, 0);

        Output::Assert(dofHandler.DofPositions(dofPositions), "%s: Get dofPositions", __func__);
        Output::Assert(dofHandler.DirichletPositions(dirichletPositions), "%s: Get dofPositions", __func__);

        EigenSparseMatrix<SparseMatrix<double>> globalMatrix, dirichletMatrix;
        EigenVector<VectorXd> rightHandSide, solutionDirichlet;

        Output::Assert(assembler.Initialize(equation,
                                            mesh,
                                            dofHandler,
                                            rightHandSide,
                                            solutionDirichlet,
                                            globalMatrix,
                                            dirichletMatrix), "%s: Initialize assembler", __func__);
        Output::Assert(assembler.AssembleDiscreteSystem(), "%s: AssembleDiscreteSystem", __func__);

        /// <li> Create Solver
        EigenVector<VectorXd> rightHandSideEquation = rightHandSide - dirichletMatrix * solutionDirichlet;
        EigenVector<VectorXd> solution;
        VectorXd& rSolution = solution;

        EigenCholeskySolverConfig solverConfig;
        solverConfig.SymmetricProblem = symmetricProblem;
        EigenCholeskySolver solver(solverConfig);
        Output::Assert(solver.Initialize(globalMatrix,
                                         rightHandSideEquation,
                                         solution), "%s: Initialize solver", __func__);

        Output::Assert(solver.Solve(), "%s: Solve system", __func__);


        /// <li> Check Solution
        const SparseMatrix<double>& rGlobalMatrix = globalMatrix;
        const VectorXd& rSolutionDirichlet = solutionDirichlet;
        unsigned int dimension = dofPositions.rows();
        unsigned int numDofs = dofPositions.cols(), numDirichlet = dirichletPositions.cols();
        unsigned int totalDimension = numDofs + numDirichlet;
        VectorXd realSolution, numericSolution(totalDimension);
        MatrixXd totalPoints(dimension, totalDimension);

        numericSolution<< rSolution, rSolutionDirichlet;
        totalPoints<< dofPositions, dirichletPositions;

        Test3DSolution(totalPoints, realSolution);

        double errorEnergyNorm = (realSolution.segment(0, dofPositions.cols()) - rSolution).transpose() *
                                 rGlobalMatrix * (realSolution.segment(0, dofPositions.cols()) - rSolution);

        Output::Assert(errorEnergyNorm < 1e-0, "%s: Check energy norm", __func__);

        VectorXd totalPointsX(totalDimension), totalPointsY(totalDimension), totalPointsZ(totalDimension);
        totalPointsX = totalPoints.row(0);
        totalPointsY = totalPoints.row(1);
        totalPointsZ = totalPoints.row(2);

        CsvExporter exportSolution;
        exportSolution.SetSeparator(',');
        exportSolution.AddColumn(totalPointsX.data(), totalPointsX.size(), "x");
        exportSolution.AddColumn(totalPointsY.data(), totalPointsY.size(), "y");
        exportSolution.AddColumn(totalPointsZ.data(), totalPointsZ.size(), "z");
        exportSolution.AddColumn(numericSolution.data(), numericSolution.size(), "numeric");
        exportSolution.AddColumn(realSolution.data(), realSolution.size(), "real");

        exportSolution.Export("FEMHexahedronTest3D.csv");

        /// </ul>
      }
      // ***************************************************************************
    }
  }
}
