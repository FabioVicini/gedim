#ifndef __FEM3D_H
#define __FEM3D_H

namespace GeDiM
{
  namespace Examples
  {
    namespace Fem3D
    {
      class Main;
      class ThreeDimensionExamples;

      class Main
      {
        public:
          static void RunAllExamples();
      };

      class ThreeDimensionExamples
      {
        public:
          /// \brief 3D Elliptic Problem SingleDomain Tetrahedron Primal Conforming Constant Lagrange Element Degree 1
          static void EP_SD_3D_Tetrahedron_PCC_L1();
          /// \brief 3D Elliptic Problem SingleDomain Hexahedron Primal Conforming Constant Lagrange Element Degree 1
          static void EP_SD_3D_Hexahedron_PCC_L1();
      };
    }
  }
}

#endif // __FEM3D_H
