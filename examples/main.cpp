#include "fem1D.hpp"
#include "fem2D.hpp"
#include "fem3D.hpp"
#include "Output.hpp"
#include "UnitTestSummary.hpp"
#include "UnitTestSummary.hpp"

#include "MpiProcess.hpp"
#include "MpiParallelEnvironment.hpp"
#include "Input.hpp"

using namespace std;
using namespace MainApplication;
using namespace GeDiM;
using namespace GeDiM::Examples;

int main(int argc, char** argv)
{
  Input::Initialize(argc, argv);
  MpiParallelEnvironment::Initialize<MpiProcess>(argc,
                                                 argv);

  Output::PrintLine('*');

  /// <li> Initialize configuration
  Output::PrintGenericMessage("Starting all examples", true);

  Fem1D::Main::RunAllExamples();
  Output::PrintLine('*');

  Fem2D::Main::RunAllExamples();
  Output::PrintLine('*');

  Fem3D::Main::RunAllExamples();
  Output::PrintLine('*');

  UnitTestSummary::ExportTestResult("ExamplesResult.csv");
  UnitTestSummary::PrintSummary();
  Output::PrintLine('*');

  MpiParallelEnvironment::Finalize();
  Input::Reset();

  if (UnitTestSummary::NumberErrorTests() > 0)
    return EXIT_FAILURE;
  else
    return EXIT_SUCCESS;
}
