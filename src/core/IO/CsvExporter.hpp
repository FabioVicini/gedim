// Copyright (C) 2020 Vicini Fabio
//
// This file is part of the dissertation of the author.
//
// This is a free program: you can redistribute it and/or modify
// it under the terms of the author.
//
// This program is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.
//
// Modified by Vicini Fabio 2020
//
// First added:  2020-02-06

#ifndef __CSVEXPORTER_H
#define __CSVEXPORTER_H

#include "Output.hpp"
#include "ICsvExport.hpp"

using namespace MainApplication;

namespace GeDiM
{
  class CsvExporter;

  class CsvExporter
  {
    protected:
      char separator;
      list <string> header;
      list<const ICsvExportRow*> rows;
      list<vector<string>> cols;

    public:
      CsvExporter() { separator = ';'; }
      CsvExporter(const char& _separator) { SetSeparator(_separator); }

      void SetSeparator(const char& _separator) { separator = _separator; }

      /// Add column to the Csv file
      template<class T>
      void AddColumn(const T* columnData,
                     const unsigned int& columnDataSize,
                     const string headerName = "")
      {
        if (!headerName.empty())
          AddRowName(headerName);

        cols.push_back(vector<string>());
        vector<string>& columnValues = cols.back();
        columnValues.reserve(columnDataSize);

        for (unsigned int i = 0; i < columnDataSize; i++)
        {
          ostringstream column;
          column.precision(16);
          column<< scientific<< columnData[i];
          string data = column.str();
          columnValues.push_back(column.str());
        }
      }

      /// Add field name to row
      void AddRowName(const string& fieldName) {  header.push_back(fieldName); }
      /// Add row to the Csv file
      void AddRow(const ICsvExportRow& row) { rows.push_back(&row); }

      /// Export to csv file
      Output::ExitCodes Export(const string& filePath, const bool& append = false);

  };
}

#endif // __CSVEXPORTER_H
