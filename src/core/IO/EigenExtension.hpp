#ifndef __EIGEN_EXTENSION_H
#define __EIGEN_EXTENSION_H

#include <string>
#include <iostream>

#include "Eigen"
#include "Output.hpp"

using namespace std;
using namespace Eigen;

namespace MainApplication
{
  class EigenExtension;

  class EigenExtension
  {
    private:

    public:
      /// General print of a Eigen vector
      static ostream& PrintVector(ostream& out, const VectorXd& vec);

      /// General print of a Map Eigen vector
      static ostream& PrintVector(ostream& out, const Map<const VectorXd>& vec);

      template <typename Derived>
      static Output::ExitCodes SparseMatrixToMap(const unsigned int& rows,
                                                 const unsigned int& cols,
                                                 const unsigned int& nnz,
                                                 const int* innerVectors,
                                                 const int* innerIndeces,
                                                 const Derived* values,
                                                 Map<const SparseMatrix<Derived>>& map)
      {
        new (&map) Map<const SparseMatrix<Derived>>(rows, cols, nnz, innerVectors, innerIndeces, values);
        return Output::Success;
      }

      template <typename Derived>
      static Output::ExitCodes SparseMatrixToMap(const Ref<const SparseMatrix<Derived>> sparseMatrix,
                                                 Map<const SparseMatrix<Derived>>& map)
      {
        return SparseMatrixToMap(sparseMatrix.rows(),
                                 sparseMatrix.cols(),
                                 sparseMatrix.nonZeros(),
                                 sparseMatrix.outerIndexPtr(),
                                 sparseMatrix.innerIndexPtr(),
                                 sparseMatrix.valuePtr(),
                                 map);
      }

      template <typename Derived, int Rows, int Cols>
      static Output::ExitCodes ArrayToMap(const int& rows,
                                           const int& cols,
                                           const Derived* values,
                                           Map<const Array<Derived, Rows, Cols>>& map)
      {
        new (&map) Map<const Array<Derived, Rows, Cols>>(values, rows, cols);
        return Output::Success;
      }

      /// \brief Convert a matrix expressions to map
      /// \param matrix A const matrix
      /// \param map Resulting map
      template <typename Derived, int Rows, int Cols>
      static Output::ExitCodes ArrayToMap(const Array<Derived, Rows, Cols>& matrix,
                                           Map<const Array<Derived, Rows, Cols>>& map)
      {
        return ArrayToMap(matrix.rows(), matrix.cols(), matrix.data(), map);
      }

      template <typename Derived, int Rows, int Cols = 1, int Options>
      static Output::ExitCodes MatrixToMap(const int& rows,
                                           const int& cols,
                                           const Derived* values,
                                           Map<const Matrix<Derived, Rows, Cols, Options>>& map)
      {
        new (&map) Map<const Matrix<Derived, Rows, Cols, Options>>(values, rows, cols);
        return Output::Success;
      }

      /// \brief Convert a matrix expressions to map
      /// \param matrix A const matrix
      /// \param map Resulting map
      template <typename Derived, int Rows, int Cols, int Options>
      static Output::ExitCodes MatrixToMap(const Matrix<Derived, Rows, Cols, Options>& matrix,
                                           Map<const Matrix<Derived, Rows, Cols, Options>>& map)
      {
        return MatrixToMap(matrix.rows(), matrix.cols(), matrix.data(), map);
      }

      /// \brief Convert a vector of const matrix expressions to const map
      /// \param matrices A vector of const matrices
      /// \param maps Resulting vector of map
      template <typename Derived, int Rows, int Cols>
      static Output::ExitCodes MatrixToMap(const vector<Matrix<Derived, Rows, Cols>>& matrices,
                                           vector<Map<const Matrix<Derived, Rows, Cols>>>& maps)
      {
        maps.reserve(matrices.size());

        for (const Matrix<Derived, Rows, Cols>& matrix : matrices)
        {
          maps.push_back(Map<const Matrix<Derived, Rows, Cols>>(nullptr, 0, 0));
          auto result = MatrixToMap(matrix, maps.back());

          if (result != Output::Success)
            return  result;
        }

        return Output::Success;
      }

      static Output::ExitCodes ReadFromBinaryFile(const string& nameFile,
                                                  VectorXd& dataToRead,
                                                  const unsigned int& dataSizeToRead = 0,
                                                  const unsigned int& startingPosition = 0);
      static Output::ExitCodes WriteToBinaryFile(const string& nameFile,
                                                 const VectorXd& vec,
                                                 const unsigned int& dataSizeToWrite = 0,
                                                 const unsigned int& dataStartingPositionToWrite = 0,
                                                 const bool& append = false);
      static Output::ExitCodes ReadFromBinaryFile(const string& nameFile,
                                                  Map<VectorXd>& dataToRead,
                                                  const unsigned int& dataSizeToRead = 0,
                                                  const unsigned int& startingPosition = 0);
      static Output::ExitCodes WriteToBinaryFile(const string& nameFile,
                                                 const Map<const VectorXd>& vec,
                                                 const unsigned int& dataSizeToWrite = 0, const unsigned int& dataStartingPositionToWrite = 0, const bool& append = false);
      static Output::ExitCodes ReadFromBinaryFile(const string& nameFile,
                                                  unsigned int& rows,
                                                  unsigned int& cols,
                                                  vector<Triplet<double> >& triplets,
                                                  const unsigned int& startingPosition = 0);
      static Output::ExitCodes ReadFromBinaryFile(const string& nameFile,
                                                  SparseMatrix<double, RowMajor>& matrix,
                                                  const unsigned int& startingPosition = 0);
      static Output::ExitCodes ReadFromBinaryFile(const string& nameFile,
                                                  SparseMatrix<double,
                                                  ColMajor>& matrix,
                                                  const unsigned int& startingPosition = 0);
      static Output::ExitCodes WriteToBinaryFile(const string& nameFile,
                                                 const SparseMatrix<double, RowMajor>& matrix,
                                                 const UpLoType& matrixType = (UpLoType)0,
                                                 const bool& append = false);
      static Output::ExitCodes WriteToBinaryFile(const string& nameFile,
                                                 const SparseMatrix<double, ColMajor>& matrix,
                                                 const UpLoType& matrixType = (UpLoType)0,
                                                 const bool& append = false);
  };
}
#endif // __EIGEN_EXTENSION_H
