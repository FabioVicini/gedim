#include "NewQuadrature.hpp"
#include "NewGaussSegment.hpp"
#include "NewGaussTriangle.hpp"
#include "NewGaussSquare.hpp"
#include "NewGaussTetrahedron.hpp"
#include "NewGaussHexahedron.hpp"
#include "NewGaussLobattoSegment.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  Output::ExitCodes NewQuadrature::Initialize(const int& _dimension, const int& _order, const IQuadrature::Type& type)
  {
    Output::ExitCodes exitCode;
    dimension = _dimension;
    order = _order;
    quadratureType = type;
    switch(dimension)
    {
      case 0:
        points.resize(0,0);
        weights.resize(0);
        exitCode = Output::Success;
      break;
      case 1:
        switch(quadratureType)
        {
          case Type::Gauss:
            exitCode = NewGaussSegment::FillPointsAndWeights(order, points, weights);
          break;
          case Type::GaussLobatto:
            exitCode = NewGaussLobattoSegment::FillPointsAndWeights(order, points, weights);
          break;
            //			case Type::GaussJacobiAlpha1Beta0:
            //				exitCode = GaussJacobiSegment::FillPointsAndWeights(order, 1, 0, points, weights);
            //				break;
            //			case Type::GaussJacobiAlpha2Beta0:
            //				exitCode = GaussJacobiSegment::FillPointsAndWeights(order, 2, 0, points, weights);
            //				break;
          default:
            Output::PrintErrorMessage("Type %s of quadrature formula not implemented for dimension %d", false, quadratureType, dimension);
            exitCode = Output::UnimplementedMethod;
          break;
        }
      break;
      case 2:
        switch(quadratureType)
        {
          case Type::Gauss:
            exitCode = NewGaussTriangle::FillPointsAndWeights(order, points, weights);
          break;
          case Type::GaussSquare:
            exitCode = NewGaussSquare::FillPointsAndWeights(order, points, weights);
          break;
          default:
            Output::PrintErrorMessage("Type of quadrature formula not implemented for dimension %d", false, dimension);
            exitCode = Output::UnimplementedMethod;
          break;
        }
      break;
      case 3:
        switch(quadratureType)
        {
          case Type::Gauss:
            exitCode = NewGaussTetrahedron::FillPointsAndWeights(order, points, weights);
          break;
          case Type::GaussHexahedron:
            exitCode = NewGaussHexahedron::FillPointsAndWeights(order, points, weights);
          break;
          default:
            Output::PrintErrorMessage("Type of quadrature formula not implemented for dimension %d", false, dimension);
            exitCode = Output::UnimplementedMethod;
          break;
        }
      break;
      default:
        Output::PrintErrorMessage("Quadrature formula for dimension %d not implemented.", false, dimension);
        exitCode = Output::UnimplementedMethod;
      break;
    }
    return exitCode;
  }
}
