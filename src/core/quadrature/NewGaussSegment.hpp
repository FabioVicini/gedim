#ifndef __NEWGAUSSSEGMENT_H
#define __NEWGAUSSSEGMENT_H

#include "Output.hpp"
#include "Eigen"

using namespace MainApplication;
using namespace Eigen;
using namespace std;

namespace GeDiM
{
  /// Gauss quadrature rule for segments
  class NewGaussSegment
  {
  public:
    /// Writes quadrature points and weights in given input vectors.
    static Output::ExitCodes FillPointsAndWeights(const unsigned int& order, MatrixXd& points, VectorXd& weights);
  };
}
#endif
