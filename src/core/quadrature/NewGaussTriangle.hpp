#ifndef __NEWGAUSSTRIANGLE_H
#define __NEWGAUSSTRIANGLE_H

#include "Output.hpp"
#include "Eigen"

using namespace MainApplication;
using namespace Eigen;
using namespace std;

namespace GeDiM
{
  /// Gauss quadrature rule for Triangles
	class NewGaussTriangle
  {
  public:
    /// Writes quadrature points and weights in given input vectors.
    static Output::ExitCodes FillPointsAndWeights(const unsigned int& order, MatrixXd& points, VectorXd& weights);
	};
}
#endif
