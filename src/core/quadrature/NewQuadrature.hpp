#ifndef __NEWQUADRATURE_H
#define __NEWQUADRATURE_H

#include "Output.hpp"
#include "IQuadrature.hpp"
#include "Eigen"

using namespace MainApplication;
using namespace Eigen;
using namespace std;

namespace GeDiM
{
    class NewQuadrature : public IQuadrature
    {
        protected:
            IQuadrature::Type quadratureType; ///< Type of quadrature (a value in enum class Quadrature::Type)
            unsigned int dimension; ///< Dimension of the reference element
            unsigned int order; ///< Polynomial degree of the quadrature formula
						MatrixXd points; ///< Quadrature points of size dimension
            VectorXd weights; ///< Quadrature weights

        public:
            NewQuadrature() {}
            ~NewQuadrature() {}

            const NewQuadrature::Type& QuadratureType() const { return quadratureType; }
            const unsigned int& Dimension() const { return dimension; }
            const unsigned int& Order() const { return order; }
            unsigned int NumPoints() const { return points.cols(); }
            const VectorXd Point(const unsigned int& position) const { return points.col(position); }
            const MatrixXd& Points() const { return points; }
            const double& Weight(const unsigned int& position) const { return weights(position); }
            const VectorXd& Weights() const { return weights; }

            Output::ExitCodes Initialize(const int& _dimension, const int& _order, const IQuadrature::Type& type);
    };
}

#endif
