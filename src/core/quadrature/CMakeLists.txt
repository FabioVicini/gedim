set(gedim_core_quadrature_source
  # insert source files here, one per line
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussLobattoSegment.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussSegment.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussTetrahedron.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussTriangle.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussHexahedron.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussSquare.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewQuadrature.cpp
  PARENT_SCOPE
  )
set(gedim_core_quadrature_headers
  # insert header files here, one per line
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussLobattoSegment.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussSegment.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussTetrahedron.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussTriangle.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussHexahedron.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewGaussSquare.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/NewQuadrature.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/IQuadrature.hpp
  PARENT_SCOPE
  )

set(gedim_core_quadrature_include
	# insert subdirectories to be included here, one per line
  PARENT_SCOPE)
