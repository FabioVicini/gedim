#ifndef __IQUADRATURE_H
#define __IQUADRATURE_H

#include "Output.hpp"
#include "Eigen"

using namespace MainApplication;
using namespace Eigen;
using namespace std;

namespace GeDiM
{
  /// \brief Interface used to implement Quadrature Formula on the reference element.
  /// \author Andrea Borio, Alessandro D'Auria, Fabio Vicini.
  /// \copyright See top level LICENSE file for details.
  class IQuadrature
  {
    public:
      enum class Type
      {
        NotSet, ///< used to generate errors
        Gauss,
        GaussLobatto,
        GaussJacobiAlpha1Beta0,
        GaussJacobiAlpha2Beta0,
        GaussSquare,
        GaussHexahedron
      };
      virtual ~IQuadrature() {}

      /// \return the dimension of the quadrature formula
      virtual const unsigned int& Dimension() const = 0;
      /// \return the order of the quadrature formula
      virtual const unsigned int& Order() const = 0;
      /// \return the number of points of the quadrature formula
      virtual unsigned int NumPoints() const = 0;
      /// \return the i-th point of the quadrature formula
      virtual const VectorXd Point(const unsigned int& position) const = 0;
      /// \return the points of the quadrature formula as a matrix of dimension Dimension() x NumPoints()
      virtual const MatrixXd& Points() const = 0;
      /// \return the i-th weight of the quadrature formula
      virtual const double& Weight(const unsigned int& position) const = 0;
      /// \return the points of the quadrature formula as a vector of dimension NumPoints()
      virtual const VectorXd& Weights() const = 0;

      /// \brief Initialize the quadrature formula on the geometric reference element
      /// \param _dimension The dimension of the geometric reference element
      /// \param _order The order of the quadrature formula
      virtual Output::ExitCodes Initialize(const int& _dimension,
                                           const int& _order, const IQuadrature::Type& type) = 0;
  };
}

#endif // __IQUADRATURE_H
