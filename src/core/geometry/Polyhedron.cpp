#include "Polyhedron.hpp"
#include "DefineNumbers.hpp"
#include "MeasurerPoint.hpp"
#include "MappingPolygon.hpp"
#include "NewQuadrature.hpp"

namespace GeDiM
{
  Polyhedron::Polyhedron(const unsigned int& _id) : GeometricObject(_id, GeometricType::Unknown3D)
  {
  }

  Polyhedron& Polyhedron::operator=(Polyhedron&& polyhedron)
  {
    if(this != &polyhedron)
    {
      this->GeometricObject::operator =(move(polyhedron));

      vertices.clear();
      edges.clear();
      faces.clear();

      unsigned int numVertices = polyhedron.NumberOfVertices();
      unsigned int numEdges = polyhedron.NumberOfEdges();
      unsigned int numFaces = polyhedron.NumberOfFaces();
      vertices.reserve(numEdges);
      edges.reserve(numEdges);
      faces.reserve(numFaces);
      for(unsigned int numVert = 0; numVert < numVertices; numVert++)
        vertices.push_back(polyhedron.vertices[numVert]);
      for(unsigned int numSegm = 0; numSegm < numEdges; numSegm++)
        edges.push_back(polyhedron.edges[numSegm]);
      for(unsigned int numFc = 0; numFc < numFaces; numFc++)
        faces.push_back(polyhedron.faces[numFc]);

      polyhedron.vertices.clear();
      polyhedron.edges.clear();
      polyhedron.faces.clear();
      polyhedron.centroid = nullptr;
      polyhedron.barycenter = nullptr;
    }
    return *this;
  }

  Polyhedron& Polyhedron::operator=(const Polyhedron& polyhedron)
  {
    this->GeometricObject::operator =(polyhedron);

    unsigned int numVertices = polyhedron.NumberOfVertices();
    unsigned int numEdges = polyhedron.NumberOfEdges();
    unsigned int numFaces = polyhedron.NumberOfFaces();
    vertices.reserve(numEdges);
    edges.reserve(numEdges);
    faces.reserve(numFaces);
    for(unsigned int numVert = 0; numVert < numVertices; numVert++)
      vertices.push_back(polyhedron.vertices[numVert]);
    for(unsigned int numSegm = 0; numSegm < numEdges; numSegm++)
      edges.push_back(polyhedron.edges[numSegm]);
    for(unsigned int numFc = 0; numFc < numFaces; numFc++)
      faces.push_back(polyhedron.faces[numFc]);

    return *this;
  }

  Polyhedron::~Polyhedron()
  {
    vertices.clear();
    edges.clear();
    faces.clear();
  }

  //************************************************************
  Segment Polyhedron::CreateEdge(const unsigned int& firstPoint, const unsigned int& secondPoint, const unsigned int& id)
  {
    unsigned int localId = (id == 0)? edges.size() : id;
    Segment segment(localId);
    segment.AddVertex(*vertices[firstPoint]);
    segment.AddVertex(*vertices[secondPoint]);

    return move(segment);
  }

  //************************************************************
  Polygon Polyhedron::CreateFace(const vector<unsigned int>& vectorPoints, const vector<unsigned int>& vectorEdges, const unsigned int& id)
  {
    unsigned int localId = (id == 0)? faces.size() : id;
    Polygon polygon(localId);
    unsigned int numVertices = vectorPoints.size();
    polygon.Initialize(numVertices, numVertices);
    for(unsigned int numVert = 0; numVert < numVertices; numVert++)
    {
      polygon.AddVertex(*vertices[vectorPoints[numVert]]);
      polygon.AddEdge(*edges[vectorEdges[numVert]]);
    }
    polygon.Compute3DPolygonProperties();
    return polygon;
  }
  //************************************************************
  Output::ExitCodes Polyhedron::ComputeCentroid(Point& _centroid) const
  {
    if(typeObj != Polyhedron::Unknown3D)
    {
      return ComputeBarycenter(_centroid);
    }
    _centroid.setZero();

    NewQuadrature quadrature;
    quadrature.Initialize(2, 2, NewQuadrature::Type::Gauss);
    const MatrixXd& quadPoints = quadrature.Points();
    unsigned int numQuadPoints = quadrature.NumPoints();

    MatrixXd quadraturePoints;
    MappingBoundaryPolyhedron mapBoundary;
    mapBoundary.SetNumPoints(numQuadPoints);
    mapBoundary.Compute(*this, false);
    mapBoundary.F(quadPoints, quadraturePoints);
    Matrix3Xd& normalDetJ = mapBoundary.NormalDetJ();

    //    for(unsigned int subFc = 0; subFc < numSubFaces; subFc++)
    //      normalDetJ.block(0,subFc*numQuadPoints,3,numQuadPoints) *= quadrature.Weights().asDiagonal();

    _centroid(0) = quadraturePoints.row(0) * normalDetJ.row(0).asDiagonal() * quadraturePoints.row(0).transpose();
    _centroid(1) = quadraturePoints.row(1) * normalDetJ.row(1).asDiagonal() * quadraturePoints.row(1).transpose();
    _centroid(2) = quadraturePoints.row(2) * normalDetJ.row(2).asDiagonal() * quadraturePoints.row(2).transpose();

    double localMeasure = 0.0;
    if(measure < 1.0E-14)
    {
      localMeasure = (quadraturePoints.row(0).dot(normalDetJ.row(0)));
      _centroid *= 0.5 / localMeasure;
    }
    else
    {
      _centroid *= 0.5 * RAT1_6 / measure;
    }

    return Output::Success;
  }

  //************************************************************
  Output::ExitCodes Polyhedron::ComputeMeasure(double& _measure) const
  {
    _measure = 0.0;
    MappingBoundaryPolyhedron mapBoundary;
    MatrixXd constantQuadrature;
    mapBoundary.Compute(*this, true);
    mapBoundary.F(Vector2d(RAT1_3, RAT1_3), constantQuadrature);
    const MatrixXd& normalDetJ = mapBoundary.NormalDetJ();

    _measure = 0.5*(constantQuadrature.row(0)).dot(normalDetJ.row(0));

    return Output::Success;
  }
  //************************************************************
  Output::ExitCodes Polyhedron::ComputeSquaredRadius(double& _squaredRadius) const
  {
    _squaredRadius = 0.0;

    const unsigned int numberVertices = NumberOfVertices();
    Point centroidTemp;
    if(centroid != NULL)
      ComputeCentroid(centroidTemp);
    else
      centroidTemp = *centroid;

    double squaredDistance = 0.0;
    for (unsigned int v = 0; v < numberVertices; v++)
    {
      squaredDistance = (centroidTemp - *vertices[v]).squaredNorm();
      if (_squaredRadius < squaredDistance)
        _squaredRadius = squaredDistance;
    }

    return Output::Success;
  }

  //************************************************************
  Output::ExitCodes Polyhedron::ComputeAspectRatio(double& _aspectRatio) const
  {
    Point localCentroid;
    const Point* pointerCentroid = &localCentroid;
    if(centroid == NULL)
      ComputeCentroid(localCentroid);
    else
      pointerCentroid = centroid;

    double outRadius = 0.0;
    double inRadius = numeric_limits<double>::max();
    for(unsigned int numPnt = 0; numPnt < NumberOfVertices(); numPnt++)
    {
      const Vector3d& origin = *vertices[numPnt];
      double outRadiusTemp = MeasurerPoint::SquaredDistancePoints(*pointerCentroid, origin);
      if(outRadiusTemp > outRadius)
        outRadius = outRadiusTemp;
    }

    Vector3d normalFace;
    double planeTranslation = 0.0;
    for(unsigned int numFac = 0; numFac < NumberOfFaces(); numFac++)
    {
      const Polygon* face = Face(numFac);
      if(face->Normal() != NULL)
      {
        normalFace = *face->Normal();
        planeTranslation = face->PlaneTranslation();
      }
      else
        face->ComputePlane(normalFace, planeTranslation);

      double inRadiusTemp = abs(MeasurerPoint::DistancePointPlane(*pointerCentroid, normalFace, planeTranslation));
      if( inRadiusTemp < inRadius)
        inRadius = inRadiusTemp;
    }
    _aspectRatio = sqrt(outRadius) / inRadius ;

    return Output::Success;
  }

  Output::ExitCodes Polyhedron::ComputePrincipalAxis(Vector3d& axis, const unsigned int& position)
  {
    Matrix3d inertiaTensor;
    ComputeInertiaTensor(inertiaTensor);

    SelfAdjointEigenSolver<Matrix3d> eigSolver(inertiaTensor);
    axis = eigSolver.eigenvectors().col(position);

    return Output::Success;
  }

  Output::ExitCodes Polyhedron::ComputeInertiaTensor(Matrix3d& inertiaTensor, const bool& boundary)
  {
    Point centroid;
    centroid.setZero();
    const Point* centroidConst = &centroid;
    if(Centroid() == NULL)
      ComputeCentroid(centroid);
    else
      centroidConst = Centroid();

    vector<double> momentums(6, 0.0);
    NewQuadrature quadrature;
    MatrixXd quadraturePoints;

    if(boundary)
    {
      quadrature.Initialize(2, 3, NewQuadrature::Type::Gauss);
//      const VectorXd& weights = quadrature.Weights();
      const MatrixXd& quadPoints = quadrature.Points();
      unsigned int numQuadPoints = quadrature.NumPoints();

      MappingBoundaryPolyhedron mapBoundary;
      mapBoundary.SetNumPoints(numQuadPoints);
      mapBoundary.Compute(*this, false);
      mapBoundary.F(quadPoints, quadraturePoints);
      Matrix3Xd& normalDetJ = mapBoundary.NormalDetJ();
      unsigned int numSubFaces = mapBoundary.NumberOfSubFaces();

//      VectorXd weightsFaces(weights.size() * numSubFaces);
      quadraturePoints.colwise() -= *centroidConst;
//      MatrixXd quadraturePointsTwin = quadraturePoints;
      MatrixXd squareQuadraturePoints = quadraturePoints.array().square();

//      weightsFaces = weights.replicate(numSubFaces, 1);
//      for(unsigned int subFc = 0; subFc < numSubFaces; subFc++)
//        quadraturePoints.block(0,subFc*numQuadPoints,3,numQuadPoints).array().colwise() *= normalDetJ.col(subFc).array();
//      quadraturePoints *= weightsFaces.asDiagonal();

      for(unsigned int subFc = 0; subFc < numSubFaces; subFc++)
        normalDetJ.block(0, subFc*numQuadPoints, 3, numQuadPoints) *= quadrature.Weights().asDiagonal();


      //moment xx (x * (y^2+z^2))
      momentums[0] = quadraturePoints.row(0) * normalDetJ.row(0).asDiagonal() * (squareQuadraturePoints.bottomRows(2).colwise().sum()).transpose();
      //moment yy
      momentums[1] = quadraturePoints.row(1) * normalDetJ.row(1).asDiagonal() * (squareQuadraturePoints.row(0) + squareQuadraturePoints.row(2)).transpose();
      //moment zz
      momentums[2] = quadraturePoints.row(2) * normalDetJ.row(2).asDiagonal() * (squareQuadraturePoints.topRows(2).colwise().sum()).transpose();
      //moment yz (xyz)
      momentums[3] = quadraturePoints.row(0) * normalDetJ.row(0).asDiagonal() * (quadraturePoints.row(1).cwiseProduct(quadraturePoints.row(2))).transpose();
      //moment xz
      momentums[4] = quadraturePoints.row(1) * normalDetJ.row(1).asDiagonal() * (quadraturePoints.row(0).cwiseProduct(quadraturePoints.row(2))).transpose();
      //moment xy
      momentums[5] = quadraturePoints.row(2) * normalDetJ.row(2).asDiagonal() * (quadraturePoints.row(0).cwiseProduct(quadraturePoints.row(1))).transpose();
    }
    else
    {
      quadrature.Initialize(3, 2, NewQuadrature::Type::Gauss);
      const MatrixXd& quadPoints = quadrature.Points();
      const VectorXd& weights = quadrature.Weights();

      MappingPolyhedron mapPolyhedron;
      mapPolyhedron.Compute(*this, false);
      mapPolyhedron.F(quadPoints, quadraturePoints);
      quadraturePoints.colwise() -= *centroidConst;
      VectorXd weightsRep = weights.replicate(mapPolyhedron.NumberOfSubTetrahedra(),1);
      for(unsigned int subTr = 0; subTr < mapPolyhedron.NumberOfSubTetrahedra(); subTr++)
        weightsRep.segment<4>(subTr*4) *= mapPolyhedron.DetJ()(subTr);

      Matrix3d temp = quadraturePoints * weightsRep.asDiagonal() * quadraturePoints.transpose();

      //moment xx
      momentums[0] = temp(1,1) + temp(2,2);
      //moment yy
      momentums[1] = temp(0,0) + temp(2,2);
      //moment zz
      momentums[2] = temp(0,0) + temp(1,1);
      //moment yz
      momentums[3] = temp(1,2);
      //moment xz
      momentums[4] = temp(0,2);
      //moment xy
      momentums[5] = temp(0,1);

    }
    if(abs(momentums[3]) < 1.0E-7)
      momentums[3] = 0.0;
    if(abs(momentums[4]) < 1.0E-7)
      momentums[4] = 0.0;
    if(abs(momentums[5]) < 1.0E-7)
      momentums[5] = 0.0;

    inertiaTensor << momentums[0], -momentums[5], -momentums[4],
        -momentums[5],  momentums[1], -momentums[3],
        -momentums[4], -momentums[3], momentums[2];
    return Output::Success;
  }
}

