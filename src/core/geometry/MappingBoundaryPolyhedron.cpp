#include "MappingBoundaryPolyhedron.hpp"
#include "Polygon.hpp"
#include "Point.hpp"

namespace GeDiM
{
  void MappingBoundaryPolyhedron::ComputeTriangle(const IGeometricObject& polygon, const bool& computeInverse, unsigned int& subFace, const bool& sign)
  {
    j.block<3,2>(3*subFace, 0).col(0) = (*polygon.Vertex(1)) - (*polygon.Vertex(0));
    j.block<3,2>(3*subFace, 0).col(1) = (*polygon.Vertex(2)) - (*polygon.Vertex(0));

    Vector3d indipendentColumn =  j.block<3,2>(3*subFace, 0).col(0).cross(( j.block<3,2>(3*subFace, 0).col(1)));
    double norm = indipendentColumn.norm();
    detJ.segment(subFace*numPointsToMap, subFace*numPointsToMap + numPointsToMap).setConstant(norm) ;
    if(sign)
      j.block<3,2>(3*subFace, 0).col(0).swap(j.block<3,2>(3*subFace, 0).col(1));

    b.segment<3>(subFace*3) = *polygon.Vertex(0);

    if(computeInverse)
    {
      Matrix3d temp;
      temp << j.block<3,2>(3*subFace, 0), indipendentColumn;
      jInverse.block<2,3>(0,subFace*3) = temp.inverse().block(0, 0, 2, 3);
    }

  }

  void MappingBoundaryPolyhedron::ComputeParallelogram(const IGeometricObject& polygon, const bool& computeInverse, const unsigned int& subFace, const bool& sign)
  {
    j.block<3,2>(3*subFace, 0).col(0) = (*polygon.Vertex(1)) - (*polygon.Vertex(0));
    j.block<3,2>(3*subFace, 0).col(1) = (*polygon.Vertex(3)) - (*polygon.Vertex(0));

    Vector3d indipendentColumn =  j.block<3,2>(3*subFace, 0).col(0).cross(( j.block<3,2>(3*subFace, 0).col(1)));
    double norm = indipendentColumn.norm();
    detJ.segment(subFace*numPointsToMap, subFace*numPointsToMap + numPointsToMap).setConstant(norm);

    if(sign)
      j.block<3,2>(3*subFace, 0).col(0).swap(j.block<3,2>(3*subFace, 0).col(1));

    b.segment<3>(subFace*3) = *polygon.Vertex(0);

    if(computeInverse)
    {
      Matrix3d temp;
      temp << j.block<3,2>(3*subFace, 0), indipendentColumn;
      jInverse.block<2,3>(0,subFace*3) = temp.inverse().block(0, 0, 2, 3);
    }

  }

  void MappingBoundaryPolyhedron::ComputePolygon(const IGeometricObject& polygon, const Point& centroid, const bool& computeInverse, const unsigned int& subFace, const bool& sign)
  {
    Matrix32d tempMatrix;
    Vector3d tangentFirst, tangentSecond;
    for(unsigned int numEdg = 0; numEdg < polygon.NumberOfEdges(); numEdg++)
    {
      unsigned int numEdgNext = (numEdg+1)%polygon.NumberOfEdges();
      tangentFirst = (*polygon.Vertex(numEdgNext)) - (*polygon.Vertex(numEdg));
      tangentSecond = centroid - (*polygon.Vertex(numEdg));
      tempMatrix.col(0) = tangentFirst;
      tempMatrix.col(1) = tangentSecond;

      Vector3d indipendentColumn = (tangentFirst).cross(tangentSecond);
      double norm = indipendentColumn.norm();
      unsigned int start = (subFace + numEdg) * numPointsToMap;
      detJ.segment(start, numPointsToMap).setConstant(norm);

      if(sign)
        tempMatrix.col(0).swap(tempMatrix.col(1));

      j.block(3*(subFace+numEdg), 0, 3, 2) = tempMatrix;
      b.segment<3>(3*(subFace+numEdg)) = *polygon.Vertex(numEdg);
      if(computeInverse)
      {
        Matrix3d temp;
        temp << tempMatrix, indipendentColumn;
        jInverse.block(0,3*(subFace+numEdg), 2, 3) = temp.inverse().block(0, 0, 2, 3);
      }
      tempMatrix.setZero();
    }
  }

  Output::ExitCodes MappingBoundaryPolyhedron::Compute(const IGeometricObject& polyhedron, const bool& computeInverse)
  {
    unsigned int numberOfFaces = polyhedron.NumberOfFaces();
    numberOfSubFaces = 0;

    vector<Point> centroidFaces(numberOfFaces);
    for(unsigned int numFac = 0; numFac < numberOfFaces; numFac++)
    {
      const Polygon& face = *polyhedron.Face(numFac);
      if(face.Type() != Polygon::Triangle)
      {
        if(face.Centroid() == NULL)
          face.ComputeCentroid(centroidFaces[numFac]);
        else
          centroidFaces[numFac] = *face.Centroid();

        numberOfSubFaces += face.NumberOfEdges();
      }
      else
        numberOfSubFaces += 1;
    }

    j.resize(3*numberOfSubFaces, 2);
    normalDetJ.resize(3, numPointsToMap * numberOfSubFaces);
    normalDetJ.setZero();
    j.setZero();
    if(computeInverse)
      jInverse.resize(2, 3*numberOfSubFaces);

    detJ.resize(numPointsToMap * numberOfSubFaces);
    detJ.setZero();
    b.resize(3*numberOfSubFaces);

    Vector3d normalFace;
    double planeTranslation = 0.0;
    double prodScalar = 0.0;

    Point localBarycenter;
    polyhedron.ComputeBarycenter(localBarycenter);

    unsigned int counterPointsToMap = 0;
    unsigned int counterSubFace = 0;
    for(unsigned int numFac = 0; numFac < numberOfFaces; numFac++)
    {
      const Polygon& face = *polyhedron.Face(numFac);

      if(face.Normal() != NULL)
      {
        normalFace = *face.Normal();
        planeTranslation = face.PlaneTranslation();
      }
      else
      {
        Output::ExitCodes result = face.ComputePlane(normalFace, planeTranslation);

        if (result != Output::Success)
        {
          Output::PrintErrorMessage("%s: ComputePlane failed", false, __func__);
          return result;
        }
      }

      prodScalar = ((normalFace).dot(localBarycenter) < planeTranslation + 1.0E-7)? 1.0 : -1.0;
      normalFace *= prodScalar;

      switch (face.Type())
      {
        case IGeometricObject::Triangle:
          ComputeTriangle(face, computeInverse, counterSubFace, signbit(prodScalar));
          normalDetJ.block(0, counterPointsToMap, 3, numPointsToMap) = (normalFace * detJ(counterPointsToMap)).replicate(1, numPointsToMap);
          counterPointsToMap += numPointsToMap;
          counterSubFace++;
        break;
        case IGeometricObject::Parallelogram:
          ComputeParallelogram(face, computeInverse, counterSubFace, signbit(prodScalar));
          normalDetJ.block(0, counterPointsToMap, 3, numPointsToMap) = (normalFace * detJ(counterPointsToMap)).replicate(1, numPointsToMap);
          counterPointsToMap += numPointsToMap;
          counterSubFace++;
        break;
        default:
          unsigned int numEdges = face.NumberOfEdges();
          unsigned int numCols = numPointsToMap*numEdges;
          ComputePolygon(face, centroidFaces[numFac], computeInverse, counterSubFace, signbit(prodScalar));
          normalDetJ.block(0, counterPointsToMap, 3, numCols) = normalFace.replicate(1, numCols);
          normalDetJ.block(0, counterPointsToMap, 3, numCols) *= detJ.segment(counterPointsToMap, numCols).asDiagonal();
          counterPointsToMap += numCols;
          counterSubFace += numEdges;
        break;
      }
    }
    return Output::Success;
  }

  Output::ExitCodes MappingBoundaryPolyhedron::F(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    result.resize(3, numCols * numberOfSubFaces);

    MatrixXd tempResult = (j*x).colwise() + b;
    for(unsigned int numTetra = 0; numTetra < numberOfSubFaces; numTetra++)
    {
      result.block(0, numCols*numTetra, 3, numCols) = tempResult.block(numTetra*3, 0, 3, numCols);
    }
    return Output::Success;
  }

  Output::ExitCodes MappingBoundaryPolyhedron::FInverse(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    unsigned int newNumCols = numCols/numberOfSubFaces;
    MatrixXd reshapeX(3*numberOfSubFaces, newNumCols);
    for(unsigned int numTetra = 0; numTetra < numberOfSubFaces; numTetra++)
    {
      reshapeX.block(numTetra*3, 0, 3, newNumCols) = x.block(0, newNumCols*numTetra, 3, newNumCols);
    }
    result.setZero();
    reshapeX.colwise() -= b;
    result = jInverse.block(0, 0, 2, 3)*(reshapeX.block(0, 0, 3, newNumCols));
    return Output::Success;
  }
}
