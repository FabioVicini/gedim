#include "Mapping1D2D.hpp"
#include "Point.hpp"
namespace GeDiM
{
  Output::ExitCodes Mapping1D2D::Compute(const IGeometricObject& line, const bool& computeInverse)
  {
    if(line.Type() != IGeometricObject::SegmentObj)
      return Output::GenericError;

    j(0) = line.Vertex(1)->x() - line.Vertex(0)->x();
    j(1) = line.Vertex(1)->y() - line.Vertex(0)->y();

    b = line.Vertex(0)->head<2>();
    detJ = j.norm();

    if(computeInverse)
    {
      jInverse.setZero();
      jInverse(0) = 1.0 / j(0);
    }
    return Output::Success;
  }
}
