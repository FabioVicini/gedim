#include "MappingPolygon.hpp"
#include "Point.hpp"

namespace GeDiM
{
  Output::ExitCodes MappingPolygon::Compute(const IGeometricObject& polygon, const bool& computeInverse)
  {
    Point centroid;
    centroid.setZero();
    const Point* centroidConst = &centroid;
    if(polygon.Centroid() == NULL)
      polygon.ComputeCentroid(centroid);
    else
      centroidConst = polygon.Centroid();


    unsigned int dimension = Dimension();
    numberOfSubTriangles = polygon.NumberOfEdges();
    j.resize(dimension*numberOfSubTriangles, 2);
    if(computeInverse)
      jInverse.resize(2, dimension*numberOfSubTriangles);

    detJ.resize(numberOfSubTriangles);
    b.resize(dimension*numberOfSubTriangles);

    if(twoDPolygon)
    {
      Matrix2d tempMatrix;
      for(unsigned int numTri = 0; numTri < numberOfSubTriangles; numTri++)
      {
        unsigned int numEdgNext = (numTri+1)%numberOfSubTriangles;

        tempMatrix(0, 0) = (*polygon.Vertex(numEdgNext)).x() - (*polygon.Vertex(numTri)).x();
        tempMatrix(0, 1) = (*centroidConst).x() - (*polygon.Vertex(numTri)).x();
        tempMatrix(1, 0) = (*polygon.Vertex(numEdgNext)).y() - (*polygon.Vertex(numTri)).y();
        tempMatrix(1, 1) = (*centroidConst).y() - (*polygon.Vertex(numTri)).y();

        detJ(numTri) = tempMatrix.determinant();
        if(detJ(numTri)  < 0)
        {
          tempMatrix.col(0).swap(tempMatrix.col(1));
          detJ(numTri) = -detJ(numTri);
        }

        j.block<2,2>(numTri*2, 0) = tempMatrix;
        b.segment<2>(numTri*2) << polygon.Vertex(numTri)->x(), polygon.Vertex(numTri)->y();

        if(computeInverse)
          jInverse.block<2,2>(0,numTri*2) = tempMatrix.inverse();

        tempMatrix.setZero();
      }
    }
    else
    {
      MatrixXd tempMatrix(3, 2);
      Vector3d tangentFirst, tangentSecond;
      for(unsigned int numTri = 0; numTri < numberOfSubTriangles; numTri++)
      {
        unsigned int numEdgNext = (numTri+1)%numberOfSubTriangles;
        tangentFirst = (*polygon.Vertex(numEdgNext)) - (*polygon.Vertex(numTri));
        tangentSecond = (*centroidConst) - (*polygon.Vertex(numTri));
        tempMatrix.col(0) = tangentFirst;
        tempMatrix.col(1) = tangentSecond;

        Vector3d indipendentColumn = (tangentFirst).cross(tangentSecond);
        detJ(numTri) = (indipendentColumn).norm();
        if(detJ(numTri)  < 0)
        {
          tempMatrix.col(0).swap(tempMatrix.col(1));
          detJ(numTri) = -detJ(numTri);
        }

        j.block<3,2>(numTri*3, 0) = tempMatrix;
        b.segment<3>(numTri*3) = *polygon.Vertex(numTri);
        if(computeInverse)
        {
          Matrix3d temp;
          temp << tempMatrix, indipendentColumn;
          jInverse.block<2,3>(0,numTri*2) = temp.inverse().block(0, 0, 2, 3);
        }
        tempMatrix.setZero();
      }
    }

    return Output::Success;
  }

  Output::ExitCodes MappingPolygon::F(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    unsigned int dimension = Dimension();
    result.resize(dimension, numCols * numberOfSubTriangles);

    MatrixXd tempResult = (j*x).colwise() + b;
    for(unsigned int numTri = 0; numTri < numberOfSubTriangles; numTri++)
    {
      result.block(0, numCols*numTri, dimension,numCols) = tempResult.block(numTri*dimension, 0, dimension, numCols);
    }
    return Output::Success;
  }

  Output::ExitCodes MappingPolygon::FInverse(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    unsigned int newNumCols = numCols/numberOfSubTriangles;
    MatrixXd reshapeX(Dimension()*numberOfSubTriangles, newNumCols);
    for(unsigned int numTri = 0; numTri < numberOfSubTriangles; numTri++)
    {
      reshapeX.block(numTri*2, 0, 2, newNumCols) = x.block(0, newNumCols*numTri, 2, newNumCols);
    }
    result.setZero();
    reshapeX.colwise() -= b;
    result = jInverse.block(0, 0, 2, 2)*(reshapeX.block(0, 0, 2, newNumCols));
    return Output::Success;
  }

  //============================================================================

}
