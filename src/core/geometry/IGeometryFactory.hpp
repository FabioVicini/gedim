#ifndef __IGEOMETRYFACTORY_H
#define __IGEOMETRYFACTORY_H

#include "Polyhedron.hpp"

using namespace std;

namespace GeDiM
{
  class IGeometryFactory;

  /// \brief Interface for Geometry factory creation.
  /// \copyright See top level LICENSE file for details.
  class IGeometryFactory
  {
    public:
      virtual ~IGeometryFactory() {}

      /// \brief Reset the class removing all the elements
      virtual void Reset() = 0;

      /// \return The Number of Points
      virtual unsigned int NumberPoints() const = 0;
      /// \return The Number of Segments
      virtual unsigned int NumberSegments() const = 0;
      /// \return The Number of Polygons
      virtual unsigned int NumberPolygons() const = 0;
      /// \return The Number of Polyhedrons
      virtual unsigned int NumberPolyhedrons() const = 0;

      /// \return Get the Point correspondent to id
      /// \note No check of the existence is performed
      virtual Point& GetPoint(const unsigned int& id) = 0;
      /// \return Get the Point correspondent to id
      /// \note No check of the existence is performed
      virtual const Point& GetPoint(const unsigned int& id) const = 0;
      /// \return Get the Segment correspondent to id
      /// \note No check of the existence is performed
      virtual Segment& GetSegment(const unsigned int& id) = 0;
      /// \return Get the Segment correspondent to id
      /// \note No check of the existence is performed
      virtual const Segment& GetSegment(const unsigned int& id) const = 0;
      /// \return Get the Polygon correspondent to id
      /// \note No check of the existence is performed
      virtual Polygon& GetPolygon(const unsigned int& id) = 0;
      /// \return Get the Polygon correspondent to id
      /// \note No check of the existence is performed
      virtual const Polygon& GetPolygon(const unsigned int& id) const = 0;
      /// \return Get the Polyhedron correspondent to id
      /// \note No check of the existence is performed
      virtual Polyhedron& GetPolyhedron(const unsigned int& id) = 0;
      /// \return Get the Polyhedron correspondent to id
      /// \note No check of the existence is performed
      virtual const Polyhedron& GetPolyhedron(const unsigned int& id) const = 0;

      /// \return Check the existence of Point
      virtual  bool ExistPoint(const unsigned int& id) const = 0;
      /// \return Check the existence of Segment
      virtual  bool ExistSegment(const unsigned int& id) const = 0;
      /// \return Check the existence of Polygon
      virtual  bool ExistPolygon(const unsigned int& id) const = 0;
      /// \return Check the existence of Polyhedron
      virtual  bool ExistPolyhedron(const unsigned int& id) const = 0;

      /// \brief Create new Point
      /// \return the Point created
      virtual Point& CreatePoint() = 0;
      /// \brief Create new Segment
      /// \return the Segment created
      virtual Segment& CreateSegment() = 0;
      /// \brief Create new Polygon
      /// \return the Polygon created
      virtual Polygon& CreatePolygon() = 0;
      /// \brief Create new Polyhedron
      /// \return the Polyhedron created
      virtual Polyhedron& CreatePolyhedron() = 0;
  };
}

#endif // __IGEOMETRYFACTORY_H
