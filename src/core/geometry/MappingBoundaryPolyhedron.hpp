#ifndef __MAPPINGBOUNDARYPOLYHEDRON_H
#define __MAPPINGBOUNDARYPOLYHEDRON_H

#include "Output.hpp"
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{

  class MappingBoundaryPolyhedron : public IMapping
  {
    protected:

      MatrixX2d j; ///< Gradient of the transformation F, J
      Matrix2Xd jInverse; ///< Gradient of the transformation F, J
      VectorXd b; ///< Constant member of the function F
      VectorXd detJ; ///< Determinant of the J
      Matrix3Xd normalDetJ; /// Normal to the subFaces
      unsigned int numberOfSubFaces; ///< Number of sub Faces of the Polyhedron
      unsigned int numPointsToMap; ///< Number of point to map in each sub Face

      void ComputeTriangle(const IGeometricObject& polygon, const bool& computeInverse, unsigned int& subFace, const bool& sign);
      void ComputeParallelogram(const IGeometricObject& polygon, const bool& computeInverse, const unsigned int& subFace, const bool& sign);
      void ComputePolygon(const IGeometricObject& polygon, const Point& centroid, const bool& computeInverse, const unsigned int& subFace, const bool& sign);

    public:
      MappingBoundaryPolyhedron() { numPointsToMap = 1; }
      ~MappingBoundaryPolyhedron() {}

      void SetNumPoints(const unsigned int& _numPointsToMap)  {numPointsToMap = _numPointsToMap; }
      unsigned int Dimension() const { return 3; }

      Output::ExitCodes Compute(const IGeometricObject& polygon, const bool& computeInverse = true);

      /// Map from the reference element to the generic cell
      Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const;
      /// Map from the generic cell to reference element
      Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const;

      MatrixXd J() const { return j; }
      MatrixXd JInverse() const { return jInverse; }
      VectorXd DetJ() const { return detJ; }

      unsigned int NumberOfSubFaces() const { return numberOfSubFaces; }

      Matrix3Xd& NormalDetJ() { return normalDetJ; }
  };
}

#endif // __MAPPING_H
