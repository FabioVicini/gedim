#include "Mapping1D.hpp"
#include "Point.hpp"
#include "Segment.hpp"

namespace GeDiM
{
  Output::ExitCodes Mapping1D::Compute(const IGeometricObject& line, const bool& computeInverse)
  {
    if(line.Type() != Segment::SegmentObj)
      return Output::GenericError;

    j = line.Vertex(1)->x() - line.Vertex(0)->x();

    if(j < 0)
    {
      j = -j;
      b = line.Vertex(1)->x();
    }
    else
      b = line.Vertex(0)->x();

    detJ = j;

    if(computeInverse)
      jInverse = 1.0 / detJ;
    return Output::Success;
  }
}
