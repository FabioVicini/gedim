#ifndef __MAPPING2D3D_H
#define __MAPPING2D3D_H

#include "Eigen"
#include "Output.hpp"
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class Mapping2D3D : public IMapping
  {
    protected:
      Matrix32d j; ///< Gradient of the transformation F, J
      Matrix23d jInverse; ///< Gradient of the transformation F, J
      Vector3d b; ///< Constant member of the function F
      double detJ; ///< Determinant of the J

      void ComputeJTriangle(const IGeometricObject& polygon);
      void ComputeJParallelogram(const IGeometricObject& polygon);

    public:
      Mapping2D3D() {}
      ~Mapping2D3D() {}

      unsigned int Dimension() const { return 3; }

      Output::ExitCodes Compute(const IGeometricObject& polygon, const bool& computeInverse = false);

      /// Map from the reference element to the generic cell
      Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const {  result.setZero(); result = (j*x).colwise() +b; return Output::Success; }
      /// Map from the generic cell to reference element
      Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const {  result.setZero(); result = jInverse*(x.colwise()-b); return Output::Success; }

      MatrixXd J() const { return j; }
      MatrixXd JInverse() const { return jInverse; }
      VectorXd DetJ() const { return MatrixXd::Constant(1, 1, detJ); }
  };
}
#endif
