#include "GeometricObject.hpp"
#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"

namespace GeDiM
{
  // ***************************************************************************
  Output::ExitCodes GeometricObject::ComputePositionPoint()
  {
    idPositionPoint.clear();

    for(unsigned int numPnt = 0; numPnt < NumberOfVertices(); numPnt++)
    {
      unsigned int id = vertices[numPnt]->Id();
      idPositionPoint.insert(make_pair(id, numPnt));
    }

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes GeometricObject::FreeCentroid()
  {
    if(centroid != NULL)
      delete centroid;

    centroid = NULL;
    return Output::Success;
  }

  Output::ExitCodes GeometricObject::FreeBarycenter()
  {
    if(barycenter != NULL)
      delete barycenter;

    barycenter = NULL;
    return Output::Success;
  }

  ostream& operator<<(ostream& out, const GeometricObject& obj)
  {
    out << "id Points : ";
    for(unsigned int numVert = 0; numVert < obj.NumberOfVertices(); numVert++)
      out << obj.Vertex(numVert)->Id() << " ";
    out << endl;

    out << "id Edges : ";
    for(unsigned int numEdg = 0; numEdg < obj.NumberOfEdges(); numEdg++)
      out << (obj.Edge(numEdg))->Id() << " ";
    out << endl;


    for(unsigned int numFac = 0; numFac < obj.NumberOfFaces(); numFac++)
    {
      if(numFac == 0)
        out << "id Faces : ";
      out << (obj.Face(numFac))->Id() << " ";
    }
    out << endl;

    return out;
  }
  // ***************************************************************************
  Output::ExitCodes GeometricObject::ComputeDiameter(double& _diameter) const
  {
    _diameter = 0.0;
    double distance = 0.0;
    for(unsigned int i = 0; i < NumberOfVertices(); ++i)
    {
      for(unsigned int j = i+1; j < NumberOfVertices(); ++j)
      {
        distance = (*Vertex(i) - *Vertex(j)).squaredNorm();
        if(distance > _diameter)
          _diameter = distance;
      }
    }
    _diameter = sqrt(_diameter);
    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes GeometricObject::ComputeBarycenter(Point& _barycenter) const
  {
    _barycenter.setZero();
    double inverseNumVertices = 1.0/NumberOfVertices();
    for (unsigned int i = 0; i < NumberOfVertices(); i++)
      _barycenter += *vertices[i];

    _barycenter *= inverseNumVertices;

    return Output::Success;
  }
}
