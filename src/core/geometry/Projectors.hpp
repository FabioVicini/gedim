#ifndef PROJECTORS_HPP
#define PROJECTORS_HPP
#include "Output.hpp"
#include "MeasurerPoint.hpp"
#include "Eigen"

using namespace std;
using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
  class Projectors;

  class Projectors
  {
    public:
      ///
      /// \brief OrthogonalProjectionPointLine
      /// \param point
      /// \param pointLine
      /// \param normalizedTangentLine
      /// \param coordinateCurvilinear
      /// \param projectedPoint
      ///
      static void OrthogonalProjectionPointLine(const Vector3d& point,
                                                const Vector3d& pointLine,
                                                const Vector3d& tangentLine,
                                                double& coordinateCurvilinear,
                                                Vector3d& projectedPoint)

      { coordinateCurvilinear = (tangentLine.dot(point-pointLine))/tangentLine.squaredNorm();
        projectedPoint = pointLine +  coordinateCurvilinear * tangentLine; }
      ///
      /// \brief DirectionalProjectionPointLine
      /// \param point
      /// \param directionProjection
      /// \param pointLine
      /// \param normalizedTangentLine
      /// \param coordinateCurvilinear
      /// \param projectedPoint
      ///
      static void DirectionalProjectionPointLine(const Vector3d& point,
                                                 const Vector3d& directionProjection,
                                                 const Vector3d& pointLine,
                                                 const Vector3d& normalizedTangentLine,
                                                 double& coordinateCurvilinear,
                                                 Vector3d& projectedPoint);
      ///
      /// \brief OrthogonalProjectionPointPlane
      /// \param point
      /// \param normalizedPlaneNormal
      /// \param planeTranslation
      /// \param projectedPoint
      ///
      static void OrthogonalProjectionPointPlane(const Vector3d& point,
                                                 const Vector3d& normalizedPlaneNormal,
                                                 const double& planeTranslation,
                                                 Vector3d& projectedPoint)
      { projectedPoint = point - MeasurerPoint::DistancePointPlane(point, normalizedPlaneNormal, planeTranslation) * normalizedPlaneNormal; }
      ///
      /// \brief ProjectPointsOnLeastSquarePlane
      /// \param vertices
      ///
      static void ProjectPointsOnLeastSquarePlane(vector<Vector3d>& vertices);
      ///
      /// \brief DirectionalProjectionPointPlane
      /// \param point
      /// \param directionProjection
      /// \param normalizedPlaneNormal
      /// \param planeTranslation
      /// \param coordinateCurvilinear
      /// \param projectedPoint
      ///
      static void DirectionalProjectionPointPlane(const Vector3d& point,
                                                  const Vector3d& directionProjection,
                                                  const Vector3d& normalizedPlaneNormal,
                                                  const double& planeTranslation,
                                                  double& coordinateCurvilinear,
                                                  Vector3d& projectedPoint);
  };

}

#endif // PROJECTORS_HPP
