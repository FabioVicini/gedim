#ifndef IGEOMETRICOBJECT_H
#define IGEOMETRICOBJECT_H

#include "Output.hpp"
#include "IMapping.hpp"

using namespace MainApplication;

namespace GeDiM
{
  class Point;
  class Segment;
  class Polygon;


  /// \brief Interface class for all geometric objects.
  /// \author Andrea Borio, Alessandro D'Auria.
  /// \copyright See top level LICENSE file for details.
  class IGeometricObject
  {
    public:
      /// \brief Types of geometric object.
      typedef enum Type : unsigned short
      {
        Unknown = 0, ///< Unknown geometric object. Used as default value.
        PointObj = 1, ///< Point.
        SegmentObj = 11, ///< Segment.
        Unknown2D = 20, ///< Unknown 2D geometric object. Used as default value for polygons.
        Triangle = 23, ///< Triangle.
        Parallelogram = 24, ///< Quadrilateral with parallel edges.
        Rectangle = 25, ///< Rectangle.
        Square = 26, ///< Square.
        Quadrilateral = 242, ///< Quadrilateral with no parallel edges.
        Unknown3D = 30, ///< Unknown 3D geometric object. Used as default value for polyhedra.
        Tetrahedron = 34, ///< Tetrahedron.
        Hexahedron = 36, ///< Hexaedron.
        Cube = 37, ///< Cube.
      } GeometricType;

      /// \brief Default constructor
      IGeometricObject() {}
      /// \brief Virtual destructor.
      virtual ~IGeometricObject() {}

      /// \brief Set the type of the geometric object.
      /// \param type The type of the object.
      virtual void SetType(const GeometricType& type) = 0;
      /// \brief Set the global Id of the geometric object.
      /// \param _globalId The value of the global Id.
      virtual void SetGlobalId(const unsigned int& _globalId) = 0;
      /// \brief Set the Id of the geometric object.
      /// \param _id The value of the id.
      virtual void SetId(const unsigned int& _id) = 0;

      /// \brief Get the global Id of the geometric object.
      /// \returns A const reference to the global Id of the geometric object.
      virtual const unsigned int& GlobalId() const = 0;
      /// \brief Get the id of the geometric object.
      /// \returns A const reference to the Id of the geometric object
      virtual const unsigned int& Id() const = 0;
      /// \brief Get the type of the geometric object.
      /// \returns A const reference to the type of the object.
      /// \sa GeometricType.
      virtual GeometricType Type() const = 0;
      /// \brief Get the dimension of the geometric object.
      /// \returns A const reference to the dimension of the geometric object
      virtual unsigned int Dimension() const = 0;
      /// \returns The total number of vertices of the geometric object.
      virtual unsigned int NumberOfVertices() const = 0;
      /// \param position The position of the vertex in the geometric object.
      /// \returns A const pointer to the vertex.
      virtual const Point* Vertex(const unsigned int& position) const = 0;
      /// \returns The total number of edges of the geometric object.
      virtual unsigned int NumberOfEdges() const = 0;
      /// \param position The position of the edge in the geometric object.
      /// \returns A const pointer to the edge.
      virtual const Segment* Edge(const unsigned int& position) const = 0;
      /// \returns The number of faces of the geometric object.
      virtual unsigned int NumberOfFaces() const = 0;
      /// \param position The position of the face in the geometric object.
      /// \returns A const pointer to the face.
      virtual const Polygon* Face(const unsigned int& position) const = 0;
      /// \returns The measure of the geometric object
      virtual double Measure() const = 0;
      /// \brief Get diameter of the object.
      /// \returns The maximum distance between any two vertices of the object.
      virtual double Diameter() const = 0;
      /// \returns The squared radius of the geometric object
      virtual double SquaredRadius() const = 0;
      /// \returns The aspect ratio of the geometric object
      virtual double AspectRatio() const = 0;
      /// \returns The centroid of the geometric object
      virtual const Point* Centroid() const = 0;
      /// \returns The barycenter of the geometric object
      virtual const Point* Barycenter() const = 0;

      /// \brief Computes the map from geometry to reference element
      /// \param map The computed map
      /// \param computeInverse Compute the map inverse
      /// \default true
      /// \return The result of the computation
      virtual Output::ExitCodes ComputeMap(IMapping*& map,
                                           const bool& computeInverse = true) const = 0;
      /// \brief Computes the barycenter and stores it in the geometric object
      /// \returns MainApplication::Output::Success if the method was successful
      virtual Output::ExitCodes ComputeBarycenter() = 0;
      /// \brief Compute the barycenter of the geometric object
      /// \param[out] _barycenter : the computed barycenter
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeBarycenter(Point& _barycenter) const = 0;
      /// \brief Compute the centroid and stores it in the geometric object.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeCentroid() = 0;
      /// \brief Compute the centroid of the geometric object.
      /// \param[out] _centroid : the computed centroid.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeCentroid(Point& _centroid) const = 0;
      /// \brief Compute the measure and stores it in the geometric object.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeMeasure() = 0;
      /// \brief Compute the measure of the geometric object
      /// \param[out] measure : the computed measure
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeMeasure(double& measure) const = 0;
      /// \brief Compute the diameter and stores it in the geometric object.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeDiameter() = 0;
      /// \brief Compute the diameter of the geometric object.
      /// \param[out] diameter The computed diameter.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeDiameter(double& diameter) const = 0;
      /// \brief Computes the squared radius and stores it in the geometric object.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeSquaredRadius() = 0;
      /// \brief Computes the radius of the geometric object.
      /// \param[out] _radius : the computed radius.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeSquaredRadius(double& _radius) const = 0;
      /// \brief Computes the aspect ratio and stores it in the geometric object.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeAspectRatio() = 0;
      /// \brief Computes the aspectRatio of the geometric object
      /// \param[out] _aspectRatio : the computed aspectRatio
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes ComputeAspectRatio(double& _aspectRatio) const = 0;
      /// \brief PositionPoint
      /// \param point
      /// \return
      virtual unsigned int PositionPoint(const Point& point) const = 0;
      ///
      /// \brief ComputePositionPoint
      /// \return
      ///
      virtual Output::ExitCodes ComputePositionPoint() = 0;
  };
}

#endif
