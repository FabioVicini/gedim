#ifndef POINT_HPP
#define POINT_HPP

#include "Eigen"
#include "IGeometricObject.hpp"

using namespace Eigen;

namespace GeDiM
{
  /// \brief Class representing a point.
  /// \details Notice that this class inherits from Eigen::Vector3d, so
  /// all methods implemented in Eigen library can be used on objects of this type.
  /// \copyright See top level LICENSE file for details.
  /// \sa https://eigen.tuxfamily.org/dox/group__DenseMatrixManipulation__chapter.html.
  class Point : public Vector3d, public IGeometricObject
  {
    protected:
      unsigned int id; ///< The point id in the mesh.
      unsigned int globalId; ///< The point id in a network of meshes.
    public:
      /// Constructor setting \ref id and \ref globalId to a value.
      /// \param _id The value to which \ref id and \ref globalId will be set.
      explicit Point(const unsigned int& _id = 0): Vector3d() { id = _id; globalId = _id; }
      /// \brief Constructor of 1D point setting \ref id and \ref globalId.
      /// \details The point is represented as 3D vector with two zero coordinates.
      /// \param x The first coordinate.
      /// \param _id The value to which \ref id and \ref globalId will be set.
      explicit Point(const double& x, const unsigned int& _id = 0): Vector3d(x, 0.0, 0.0) { id = _id; globalId = _id; }
      /// \brief Constructor of 2D point setting \ref id and \ref globalId.
      /// \details The point is represented as 3D vector with one zero coordinate.
      /// \param x The first coordinate.
      /// \param y The second coordinate.
      /// \param _id The value to which \ref id and \ref globalId will be set.
      explicit Point(const double& x, const double& y, const unsigned int& _id = 0) : Vector3d(x, y, 0.0) { id = _id; globalId = _id; }
      /// \brief Constructor of 2D point setting \ref id and \ref globalId.
      /// \param x The first coordinate.
      /// \param y The second coordinate.
      /// \param z The third coordinate.
      /// \param _id The value to which \ref id and \ref globalId will be set.
      explicit Point(const double& x, const double& y, const double& z, const unsigned int& _id = 0): Vector3d(x, y, z) { id = _id; globalId = _id; }
      Point(const Point& point) : Vector3d(point) { id = point.id; globalId = point.globalId; } ///< Copy constructor.
      /// \brief Constructor from a Vector3d object.
      /// \details \ref id and \ref globalId are left undefined.
      Point(const Vector3d& other) : Vector3d(other) {}
      /// Move constructor
      Point(Point&& other) : Vector3d(move(other))
      {
        if(this != &other)
        {
          id = other.id;
          globalId = other.globalId;
          other.id = 0;
          other.globalId = 0;
        }
      }
      /// \brief Constructor from a Eigen::MatrixBase object.
      /// \details \ref id and \ref globalId are left undefined.
      template<typename OtherDerived>
      Point(const MatrixBase<OtherDerived>& other)
        : Vector3d(other)
      { }
      virtual ~Point() {} ///< Default destructor.

      /// Copy-assignment operator.
      Point& operator=(const Point& other)
      {
        this->Vector3d::operator=(other);
        id = other.id;
        globalId = other.globalId;
        return *this;
      }
      /// Assignment operator.
      Point& operator=(Point&& other)
      {
        if(this != &other)
        {
          other.swap(*this);
        }
        return *this;
      }
      /// Copy-assignment operator from a Eigen::MatrixBase object.
      template<typename OtherDerived>
      Point & operator= (const MatrixBase <OtherDerived>& other)
      {
        this->Vector3d::operator=(other);
        return *this;
      }

      /// \brief Set point coordinates.
      /// \param x The first coordinate.
      /// \param y The second coordinate.
      /// \param z The third coordinate.
      inline void SetCoordinates(const double& x, const double& y = 0.0, const double& z = 0.0) {*this << x, y, z;}
      /// Set point coordinates from a Eigen::Vector3d object.
      /// \param coordinates Vector containing the new coordinates.
      inline void SetCoordinates(const Vector3d& coordinates) {*this << coordinates[0], coordinates[1], coordinates[2];}
      /// \brief Set point \ref id.
      /// \param _id The value to which \ref id is set.
      inline void SetId(const unsigned int& _id) { id = _id; }
      /// \brief Set point \ref globalId.
      /// \param _id The value to which \ref globalId is set.
      inline void SetGlobalId(const unsigned int& _id) { globalId = _id; }
      /// \brief Set the type of the geometric object.
      /// \param type The type of the object.
      void SetType(const GeometricType&) { }
      /// \brief Get point \ref id.
      /// \returns A const reference to \ref id.

      /// \brief Computes the barycenter and stores it in the geometric object
      /// \returns MainApplication::Output::Success if the method was successful
      inline Output::ExitCodes ComputeBarycenter() { return Output::Success; }
      inline Output::ExitCodes ComputeCentroid() { return Output::Success; }
      /// \brief Compute the measure and stores it in the geometric object
      /// \returns MainApplication::Output::Success if the method was successful
      inline Output::ExitCodes ComputeMeasure() { return Output::Success; }
      inline Output::ExitCodes ComputeDiameter() { return Output::Success; }
      /// Computes the radius and stores it in the geometric object
      inline Output::ExitCodes ComputeSquaredRadius() { return Output::Success; }
      inline Output::ExitCodes ComputeAspectRatio() { return Output::Success; }

      const unsigned int& Id() const { return id; }
      /// \brief Get point \ref globalId.
      /// \returns A const reference to \ref globalId.
      const unsigned int& GlobalId() const { return globalId; }
      GeometricType Type() const { return GeometricType::PointObj; }
      unsigned int Dimension() const { return 0; }
      /// @return The total number of vertices of the geometric object.
      unsigned int NumberOfVertices() const { return 1; }
      /// @return The vertex at position p of the geometric object.
      const Point* Vertex(const unsigned int&) const { return this; }
      /// @return The total number of edges of the geometric object.
      unsigned int NumberOfEdges() const { return 0; }
      /// @return The edge at position p of the geometric object.
      const Segment* Edge(const unsigned int&) const { return nullptr; }
      /// @return The number of faces of the geometric object.
      unsigned int NumberOfFaces() const { return 0; }
      /// @return The face at position p of the geometric object.
      const Polygon* Face(const unsigned int&) const { return nullptr; }
      /// @return The measure of the geometric object
      inline double Measure() const { return 0; }
      inline double Diameter() const { return 0; }
      /// @return The squared radius of the geometric object
      inline double SquaredRadius() const { return 0; }
      /// @return The aspect ratio of the geometric object
      inline double AspectRatio() const { return 1; }
      /// @return The centroid of the geometric object
      inline const Point* Centroid() const { return this; }
      /// @return The barycenter of the geometric object
      inline const Point* Barycenter() const { return this; }
      /// \brief Computes the barycenter of the geometric object.
      /// \param[out] _barycenter : the computed barycenter
      /// \returns MainApplication::Output::Success if the method was successful
      inline Output::ExitCodes ComputeBarycenter(Point& _barycenter) const { _barycenter = *this; return Output::Success; }
      /// Computes the centroid of the geometric object.
      /// \param[out] _centroid The computed centroid.
      /// \returns MainApplication::Output::Success if the method was successful
      inline Output::ExitCodes ComputeCentroid(Point& _centroid) const { _centroid = *this; return Output::Success; }
      /// \brief Compute the measure of the geometric object
      /// \param[out] _measure : the computed measure
      /// \returns MainApplication::Output::Success if the method was successful
      inline Output::ExitCodes ComputeMeasure(double& measure) const { measure = 0.0; return Output::Success; }
      virtual Output::ExitCodes ComputeDiameter(double& diameter) const { diameter = 0.0; return Output::Success; }
      /// \brief Computes the radius of the geometric object.
      /// \param[out] _radius : the computed radius.
      inline Output::ExitCodes ComputeSquaredRadius(double& _radius) const { _radius = 0.0; return Output::Success; }
      ///
      /// \brief ComputeAspectRatio
      /// \param _aspectRatio
      /// \return
      ///
      inline Output::ExitCodes ComputeAspectRatio(double& _aspectRatio) const { _aspectRatio = 1.0; return Output::Success; }
      ///
      /// \brief ComputeMap
      /// \param map
      /// \param computeInverse
      /// \return
      ///
      Output::ExitCodes ComputeMap(IMapping*& map,
                                   const bool& = true) const { map = nullptr; return Output::Success; }


      unsigned int PositionPoint(const Point&) const { return 0; }
      Output::ExitCodes ComputePositionPoint() { return Output::Success; }
  };
}
#endif //VERTEX_HPP
