#ifndef __ISHAPECREATOR_H
#define __ISHAPECREATOR_H

#include "Output.hpp"
#include "Eigen"
#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Polyhedron.hpp"

using namespace std;
using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
  /// \brief Interface for shape creation.
  /// \copyright See top level LICENSE file for details.
  class IShapeCreator
  {
    public:
      virtual ~IShapeCreator() {}

      /// \brief Create a Segment using coordinates
      /// \param origin the origin of the segment
      /// \param end the end of the segment
      /// \return the result of the creation
      ///
      virtual Segment& CreateSegment(const Vector3d& origin,
                                     const Vector3d& end) = 0;
      /// \brief Create a polygon using vertices coordinates clocwise
      /// \param vertices the coordinates of vertices
      /// \return the result of the creation
      virtual Polygon& CreatePolygon(const vector<Vector3d>& vertices) = 0;
      /// \brief Create a Triangle from 3 points
      /// \param p1 coordinates of point 1
      /// \param p2 coordinates of point 2
      /// \param p3 coordinates of point 3
      /// \return the result of the creation
      virtual Polygon& CreateTriangle(const Vector3d& p1,
                                      const Vector3d& p2,
                                      const Vector3d& p3) = 0;
      /// \brief Create a Parallelogram as defined in https://en.wikipedia.org/wiki/Parallelogram
      /// \param origin the origin in the 3D space
      /// \param lengthVector the length vector direction
      /// \param widthVector the width vector direction
      /// \return the result of the creation
      virtual Polygon& CreateParallelogram(const Vector3d& origin,
                                           const Vector3d& lengthVector,
                                           const Vector3d& widthVector) = 0;
      /// \brief Create a Rectangle in the 2D space
      /// \param origin the origin in the 2D space
      /// \param base the length of the base
      /// \param height the length of the height
      /// \return the result of the creation
      virtual Polygon& CreateRectangle(const Vector2d& origin,
                                       const double& base,
                                       const double& height) = 0;
      /// \brief Create a Square in the 2D space
      /// \param origin the origin in the 2D space
      /// \param edge the length of the edges
      /// \return the result of the creation
      virtual Polygon& CreateSquare(const Vector2d& origin,
                                    const double& edge) = 0;
      ///
      /// \brief Create Regular Polygon
      /// \param numEdges of the regular polygon
      /// \param lengthRadius of the radius of the circumference inscribed
      /// \param convertRadius use the variable lenghtRadius as the lenght of the edge of the polygon
      /// \return the regular polygon with numEdges edges
      ///
      virtual Polygon& CreateRegularPolygon(const unsigned int& numEdges,
                                            const double& lengthRadius = 1,
                                            const bool& convertRadius = false) = 0;
      /// \brief Create a Parallelepiped as defined in https://en.wikipedia.org/wiki/Parallelepiped
      /// \param origin the origin in the 3D space
      /// \param lengthVector the length vector direction
      /// \param heightVector the height vector direction
      /// \param widthVector the width vector direction
      /// \return the result of the creation
      virtual Polyhedron& CreateParallelepiped(const Vector3d& origin,
                                               const Vector3d& lengthVector,
                                               const Vector3d& heightVector,
                                               const Vector3d& widthVector) = 0;
      /// \brief Create a Parallelogrammic Prism in the 3D space
      /// \param origin the origin in the 3D space
      /// \param base the length of the base
      /// \param height the length of the height
      /// \param width the length of the width
      /// \return the result of the creation
      virtual Polyhedron& CreateParallelogrammicPrism(const Vector3d& origin,
                                                      const double& base,
                                                      const double& height,
                                                      const double& width) = 0;
      /// \brief Create a Cube in the 3D space
      /// \param origin the origin in the 3D space
      /// \param edge the length of the edges
      /// \return the result of the creation
      virtual Polyhedron& CreateCube(const Vector3d& origin,
                                     const double& edge) = 0;
  };
}

#endif // __ISHAPECREATOR_H
