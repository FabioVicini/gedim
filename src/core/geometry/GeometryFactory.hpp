#ifndef __GEOMETRYFACTORY_H
#define __GEOMETRYFACTORY_H

#include "IGeometryFactory.hpp"
#include <unordered_map>

using namespace std;

namespace GeDiM
{
  class IGeometryFactory;

  /// \brief Geometry factory creation.
  /// \copyright See top level LICENSE file for details.
  class GeometryFactory : public IGeometryFactory
  {
    protected:
      unordered_map<int, Point> points;
      unordered_map<int, Segment> segments;
      unordered_map<int, Polygon> polygons;
      unordered_map<int, Polyhedron> polyhedrons;

    public:
      GeometryFactory() { }
      ~GeometryFactory() { }

      void Reset();

      unsigned int NumberPoints() const { return points.size(); }
      unsigned int NumberSegments() const  { return segments.size(); }
      unsigned int NumberPolygons() const { return polygons.size(); }
      unsigned int NumberPolyhedrons() const { return polyhedrons.size(); }

      Point& GetPoint(const unsigned int& id) { return points.at(id); }
      const Point& GetPoint(const unsigned int& id) const { return points.at(id); }
      Segment& GetSegment(const unsigned int& id) { return segments.at(id); }
      const Segment& GetSegment(const unsigned int& id) const { return segments.at(id); }
      Polygon& GetPolygon(const unsigned int& id) { return polygons.at(id); }
      const Polygon& GetPolygon(const unsigned int& id) const{ return polygons.at(id); }
      Polyhedron& GetPolyhedron(const unsigned int& id) { return polyhedrons.at(id); }
      const Polyhedron& GetPolyhedron(const unsigned int& id) const { return polyhedrons.at(id); }

      bool ExistPoint(const unsigned int& id) const { return points.find(id) != points.end(); }
      bool ExistSegment(const unsigned int& id) const { return segments.find(id) != segments.end(); }
      bool ExistPolygon(const unsigned int& id) const { return polygons.find(id) != polygons.end(); }
      bool ExistPolyhedron(const unsigned int& id) const { return polyhedrons.find(id) != polyhedrons.end(); }

      Point& CreatePoint();
      Segment& CreateSegment();
      Polygon& CreatePolygon();
      Polyhedron& CreatePolyhedron();
  };
}

#endif // __GEOMETRYFACTORY_H
