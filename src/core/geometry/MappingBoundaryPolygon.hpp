#ifndef __MAPPINGBOUNDARYPOLYGON_H
#define __MAPPINGBOUNDARYPOLYGON_H

#include "Output.hpp"
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MappingBoundaryPolygon : public IMapping
  {
    protected:
      VectorXd j; ///< Gradient of the transformation F, J
      VectorXd jInverse; ///< Gradient of the transformation F, J
      VectorXd b; ///< Constant member of the function F
      VectorXd detJ; ///< Determinants of the J of single subTriangle
      Matrix2Xd normalDetJ; ///< Normal to the edges multiplied per detJ
      unsigned int numberOfEdges; ///< Number of edges of the Polygon
      unsigned int numPointsToMap; ///< Number of points to Map on the boundary;

    public:
      MappingBoundaryPolygon() { numPointsToMap = 1; }
      ~MappingBoundaryPolygon() {}

      void SetNumPoints(const unsigned int& _numPointsToMap)  {numPointsToMap = _numPointsToMap; }

      unsigned int Dimension() const { return 2; }

      Output::ExitCodes Compute(const IGeometricObject& polygon, const bool& computeInverse = true);

      /// Map from the reference element to the generic cell
      Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const;
      /// Map from the generic cell to reference element
      Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const;

      MatrixXd J() const { return j; }
      MatrixXd JInverse() const { return jInverse; }
      VectorXd DetJ() const { return detJ; }

      Matrix2Xd& NormalDetJ() { return normalDetJ; }
      unsigned int NumberOfEdges() const { return numberOfEdges; }
  };
}

#endif // __MAPPING_H
