#include "ShapeCreator.hpp"

namespace GeDiM
{
  // ***************************************************************************
  Point& ShapeCreator::CreatePoint(const Vector3d& coordinates)
  {
    Point& point = _factory.CreatePoint();
    point.SetCoordinates(coordinates.x(),
                         coordinates.y(),
                         coordinates.z());
    return point;
  }
  // ***************************************************************************
  Segment& ShapeCreator::CreateEdge(const vector<Point*>& points,
                                    const vector<unsigned int>& positions)
  {
    Segment& edge = _factory.CreateSegment();

    if (positions.empty())
    {
      edge.AddVertex(*points[0]);
      edge.AddVertex(*points[1]);
    }
    else
    {
      edge.AddVertex(*points[positions[0]]);
      edge.AddVertex(*points[positions[1]]);
    }

    return edge;
  }
  // ***************************************************************************
  Polygon& ShapeCreator::CreatePolygon(const vector<Point*>& points,
                                       const vector<Segment*>& edges,
                                       const vector<unsigned int>& vertexPositions,
                                       const vector<unsigned int>& edgePositions)
  {
    Polygon& polygon = _factory.CreatePolygon();

    if (vertexPositions.empty())
    {
      unsigned int numVertices = points.size();
      polygon.InitializeVertices(numVertices);
      for (unsigned int v = 0; v < numVertices; v++)
        polygon.AddVertex(*points[v]);
    }
    else
    {
      unsigned int numVertices = vertexPositions.size();
      polygon.InitializeVertices(numVertices);
      for (unsigned int v = 0; v < numVertices; v++)
        polygon.AddVertex(*points[vertexPositions[v]]);
    }

    if (edgePositions.empty())
    {
      unsigned int numEdges = edges.size();
      polygon.InitializeEdges(numEdges);
      for (unsigned int e = 0; e < numEdges; e++)
        polygon.AddEdge(*edges[e]);
    }
    else
    {
      unsigned int numEdges = edgePositions.size();
      polygon.InitializeEdges(numEdges);
      for (unsigned int e = 0; e < numEdges; e++)
        polygon.AddEdge(*edges[edgePositions[e]]);
    }

    return polygon;
  }
  // ***************************************************************************
  Polyhedron& ShapeCreator::CreatePolyhedron(const vector<Point*>& points,
                                             const vector<Segment*>& edges,
                                             const vector<Polygon*>& faces)
  {
    Polyhedron& polyhedron = _factory.CreatePolyhedron();

    unsigned int numVertices = points.size();
    polyhedron.InitializeVertices(numVertices);
    for (unsigned int v = 0; v < numVertices; v++)
      polyhedron.AddVertex(*points[v]);


    unsigned int numEdges = edges.size();
    polyhedron.InitializeEdges(numEdges);
    for (unsigned int e = 0; e < numEdges; e++)
      polyhedron.AddEdge(*edges[e]);

    unsigned int numFaces = faces.size();
    polyhedron.InitializeFaces(numFaces);
    for (unsigned int f = 0; f < numFaces; f++)
    {
      polyhedron.AddFace(*faces[f]);
    }

    return polyhedron;
  }
  // ***************************************************************************
  ShapeCreator::ShapeCreator(IGeometryFactory& factory) : _factory(factory)
  {
  }
  // ***************************************************************************
  Segment& ShapeCreator::CreateSegment(const Vector3d& origin,
                                       const Vector3d& end)
  {
    vector<Point*> points(2, nullptr);
    points[0] = &CreatePoint(origin);
    points[1] = &CreatePoint(end);

    return CreateEdge(points);
  }
  // ***************************************************************************
  Polygon& ShapeCreator::CreatePolygon(const vector<Vector3d>& vertices)
  {
    unsigned int numVertices = vertices.size();
    vector<Point*> points(numVertices, nullptr);
    vector<Segment*> edges(numVertices, nullptr);

    for (unsigned int v = 0; v < numVertices; v++)
      points[v] = &CreatePoint(vertices[v]);

    for (unsigned int e = 0; e < numVertices; e++)
      edges[e] = &CreateEdge(points, vector<unsigned int> { e, (e + 1) % numVertices });

    return CreatePolygon(points, edges);
  }
  // ***************************************************************************
  Polygon& ShapeCreator::CreateTriangle(const Vector3d& p1,
                                        const Vector3d& p2,
                                        const Vector3d& p3)
  {
    return CreatePolygon(vector<Vector3d> { p1, p2, p3 });
  }
  // ***************************************************************************
  Polygon& ShapeCreator::CreateParallelogram(const Vector3d& origin,
                                             const Vector3d& lengthVector,
                                             const Vector3d& widthVector)
  {
    Polygon& parallelogram = CreatePolygon(vector<Vector3d> { origin,
          origin + lengthVector,
          origin + lengthVector + widthVector,
          origin + widthVector});
    parallelogram.SetType(Polygon::Parallelogram);
    return parallelogram;
  }
  // ***************************************************************************
  Polygon& ShapeCreator::CreateRectangle(const Vector2d& origin,
                                         const double& base,
                                         const double& height)
  {
    Polygon& rectangle = CreateParallelogram(Vector3d(origin.x(), origin.y(), 0.0),
                               Vector3d(base, 0.0, 0.0),
                               Vector3d(0.0, height, 0.0));
    rectangle.Set2DPolygon();
    rectangle.SetType(Polygon::Rectangle);

    return rectangle;
  }
  // ***************************************************************************
  Polygon& ShapeCreator::CreateSquare(const Vector2d& origin,
                                      const double& edge)
  {
    Polygon& square = CreateRectangle(origin, edge, edge);
    square.SetType(Polygon::Square);

    return square;
  }

  // ***************************************************************************
  Polygon&ShapeCreator::CreateRegularPolygon(const unsigned int& numEdges,
                                             const double& lengthRadius,
                                             const bool& convertRadius)
  {
    vector<Vector3d> vertices(numEdges);
    double theta = 2 * GEDIM_PI / numEdges;
    double thetaStart = GEDIM_PI * 0.25;

    double lenght = lengthRadius;
    if(convertRadius)
    {
      lenght /=  sin(theta * 0.5);
      lenght *= 0.5;
    }

    double xOffset = sin(GEDIM_PI * 0.25) * lenght;
    double yOffset = cos(GEDIM_PI * 0.25) * lenght;
    for(unsigned int i = 0; i < numEdges; i++)
    {
      vertices[i].setZero();
      vertices[i].x() = - ( sin( thetaStart ) * lenght ) + xOffset;
      vertices[i].y() = ( cos( thetaStart ) * lenght ) + yOffset;
      thetaStart += theta;
    }

    return CreatePolygon(vertices);
  }
  // ***************************************************************************
  Polyhedron& ShapeCreator::CreateParallelepiped(const Vector3d& origin,
                                                 const Vector3d& lengthVector,
                                                 const Vector3d& heightVector,
                                                 const Vector3d& widthVector)
  {
    vector<Point*> points(8, nullptr);
    vector<Segment*> edges(12, nullptr);
    vector<Polygon*> faces(6, nullptr);

    points[0] = &CreatePoint(origin);
    points[1] = &CreatePoint(origin + lengthVector);
    points[2] = &CreatePoint(origin + lengthVector + widthVector);
    points[3] = &CreatePoint(origin + widthVector);
    points[4] = &CreatePoint(origin + heightVector);
    points[5] = &CreatePoint(origin + heightVector + lengthVector);
    points[6] = &CreatePoint(origin + heightVector + lengthVector + widthVector);
    points[7] = &CreatePoint(origin + heightVector + widthVector);

    edges[0] = &CreateEdge(points, vector<unsigned int> { 0, 1 });
    edges[1] = &CreateEdge(points, vector<unsigned int> { 1, 2 });
    edges[2] = &CreateEdge(points, vector<unsigned int> { 2, 3 });
    edges[3] = &CreateEdge(points, vector<unsigned int> { 3, 0 });
    edges[4] = &CreateEdge(points, vector<unsigned int> { 4, 5 });
    edges[5] = &CreateEdge(points, vector<unsigned int> { 5, 6 });
    edges[6] = &CreateEdge(points, vector<unsigned int> { 6, 7 });
    edges[7] = &CreateEdge(points, vector<unsigned int> { 7, 4 });
    edges[8] = &CreateEdge(points, vector<unsigned int> { 0, 4 });
    edges[9] = &CreateEdge(points, vector<unsigned int> { 1, 5 });
    edges[10] = &CreateEdge(points, vector<unsigned int> { 2, 6 });
    edges[11] = &CreateEdge(points, vector<unsigned int> { 3, 7 });


    faces[0] = &CreatePolygon(points,
                              edges,
                              vector<unsigned int> { 0, 1, 2, 3 },
                              vector<unsigned int> { 0, 1, 2, 3 });
    faces[1] = &CreatePolygon(points,
                              edges,
                              vector<unsigned int> { 4, 5, 6, 7 },
                              vector<unsigned int> { 4, 5, 6, 7 });
    faces[2] = &CreatePolygon(points,
                              edges,
                              vector<unsigned int> { 0, 3, 7, 4 },
                              vector<unsigned int> { 3, 11, 7, 8 });
    faces[3] = &CreatePolygon(points,
                              edges,
                              vector<unsigned int> { 1, 2, 6, 5 },
                              vector<unsigned int> { 1, 10, 5, 9 });
    faces[4] = &CreatePolygon(points,
                              edges,
                              vector<unsigned int> { 0, 1, 5, 4 },
                              vector<unsigned int> { 0, 9, 4, 8 });
    faces[5] = &CreatePolygon(points,
                              edges,
                              vector<unsigned int> { 3, 2, 6, 7 },
                              vector<unsigned int> { 2, 10, 6, 11 });

    return CreatePolyhedron(points, edges, faces);
  }
  // ***************************************************************************
  Polyhedron& ShapeCreator::CreateParallelogrammicPrism(const Vector3d& origin,
                                                        const double& length,
                                                        const double& height,
                                                        const double& width)
  {
    return CreateParallelepiped(origin,
                                Vector3d(length, 0.0, 0.0),
                                Vector3d(0.0, 0.0, height),
                                Vector3d(0.0, width, 0.0));
  }
  // ***************************************************************************
  Polyhedron& ShapeCreator::CreateCube(const Vector3d& origin,
                                       const double& edge)
  {
    Polyhedron& cube = CreateParallelogrammicPrism(origin,
                                       edge,
                                       edge,
                                       edge);
    cube.SetType(Polyhedron::Cube);

    return cube;
  }
  // ***************************************************************************
}
