#include "Polygon.hpp"
#include "DefineNumbers.hpp"
#include "MeasurerPoint.hpp"
#include "NewQuadrature.hpp"
#include "algorithm"

namespace GeDiM
{
  // ***************************************************************************
  Polygon::Polygon(const unsigned int& _id) : GeometricObject(_id, GeometricType::Unknown2D), IRotation()
  {
    twoDimensional = false;
    planeNormal = NULL;
    planeTranslation = 0.0;
    rotatedCentroid = NULL;
    rotatedBarycenter = NULL;
  }

  Polygon& Polygon::operator=(Polygon&& polygon)
  {
    if(this != &polygon)
    {
      this->IRotation::operator =(move(polygon));
      this->GeometricObject::operator =(move(polygon));

      vertices.clear();
      edges.clear();
      delete rotatedBarycenter;
      delete rotatedCentroid;
      delete planeNormal;
      rotatedVertices.clear();

      twoDimensional = polygon.twoDimensional;
      unsigned int numVertices = polygon.NumberOfVertices();
      vertices.reserve(numVertices);
      edges.reserve(numVertices);
      for(unsigned int numVert = 0; numVert < numVertices; numVert++)
      {
        vertices.push_back(polygon.vertices[numVert]);
        edges.push_back(polygon.edges[numVert]);
      }
      planeNormal = polygon.planeNormal;
      planeTranslation = polygon.planeTranslation;

      rotatedCentroid = polygon.rotatedCentroid;
      rotatedBarycenter = polygon.rotatedBarycenter;
      unsigned int numRotVertices = polygon.NumberOfRotatedVertices();
      rotatedVertices.reserve(numRotVertices);
      for(unsigned int numVert = 0; numVert < numRotVertices; numVert++)
        rotatedVertices.push_back(polygon.rotatedVertices[numVert]);

      polygon.vertices.clear();
      polygon.edges.clear();
      polygon.rotatedBarycenter = nullptr;
      polygon.rotatedCentroid = nullptr;
      polygon.planeNormal = nullptr;
      polygon.rotatedVertices.clear();
    }
    return *this;
  }

  Polygon& Polygon::operator=(const Polygon& polygon)
  {
    this->IRotation::operator =(polygon);
    this->GeometricObject::operator =(polygon);

    twoDimensional = polygon.twoDimensional;

    if(polygon.planeNormal != NULL)
      planeNormal = new Vector3d(*polygon.planeNormal);
    planeTranslation = polygon.planeTranslation;
    if(polygon.rotatedCentroid != NULL)
      rotatedCentroid = new Point(*polygon.rotatedCentroid);
    if(polygon.rotatedBarycenter != NULL)
      rotatedBarycenter = new Point(*polygon.rotatedBarycenter);

    unsigned int numVertices = polygon.NumberOfVertices();
    vertices.reserve(numVertices);
    edges.reserve(numVertices);
    for(unsigned int numVert = 0; numVert < numVertices; numVert++)
    {
      vertices.push_back(polygon.vertices[numVert]);
      edges.push_back(polygon.edges[numVert]);
    }

    return *this;
  }

  Polygon::~Polygon()
  {
    if(planeNormal != NULL)
      delete planeNormal;

    if(rotatedCentroid != NULL)
      delete rotatedCentroid;

    if(rotatedBarycenter != NULL)
      delete rotatedBarycenter;

    vertices.clear();

    for(unsigned int vrt = 0; vrt < rotatedVertices.size(); vrt++)
      delete rotatedVertices[vrt];
    rotatedVertices.clear();

    edges.clear();
  }

  // ***************************************************************************
  Segment Polygon::CreateEdge(const unsigned int& firstPoint, const unsigned int& secondPoint, const unsigned int& id)
  {
    unsigned int localId = (id == 0)? edges.size() : id;
    Segment segment(localId);
    segment.AddVertex(*vertices[firstPoint]);
    segment.AddVertex(*vertices[secondPoint]);

    return move(segment);
  }

  // ***************************************************************************
  void Polygon::AllocateRotatedVertices()
  {
    rotatedVertices.resize(NumberOfVertices());
    for(unsigned int vrt = 0; vrt < NumberOfVertices(); vrt++)
      rotatedVertices[vrt] = new Point();
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeCentroid(Point& _centroid) const
  {
    if(typeObj != Polygon::Unknown2D)
    {
      return ComputeBarycenter(_centroid);
    }
    _centroid.setZero();

    NewQuadrature quadrature;
    quadrature.Initialize(1, 2, NewQuadrature::Type::Gauss);
    const MatrixXd& quadPoints = quadrature.Points();
    unsigned int numQuadPoints = quadrature.NumPoints();

    MatrixXd quadraturePoints;
    MappingBoundaryPolygon mapBoundary;
    mapBoundary.SetNumPoints(numQuadPoints);
    mapBoundary.Compute(*this, false);
    mapBoundary.F(quadPoints, quadraturePoints);
    Matrix2Xd& normalDetJ = mapBoundary.NormalDetJ();

    //    for(unsigned int subFc = 0; subFc < numEdges; subFc++)
    //      normalDetJ.block(0,subFc*numQuadPoints,2,numQuadPoints) *= quadrature.Weights().asDiagonal();

    _centroid(0) = quadraturePoints.row(0) * normalDetJ.row(0).asDiagonal() * quadraturePoints.row(0).transpose();
    _centroid(1) = quadraturePoints.row(1) * normalDetJ.row(1).asDiagonal() * quadraturePoints.row(1).transpose();

    double localMeasure = 0.0;
    if(measure < 1.0E-14)
    {
      localMeasure = (quadraturePoints.row(0).dot(normalDetJ.row(0)));
      _centroid *= 0.5 / localMeasure;
    }
    else
    {
      _centroid *= 0.25 / measure;
    }

    if(!twoDimensional)
    {
      if(rotation == NULL)
      {
        Matrix3d localRotationMatrix;
        Vector3d localTranslation(0.0,0.0,0.0);
        ComputeRotationMatrix(localRotationMatrix, localTranslation);
        RotatePointWithInput(_centroid, _centroid, localRotationMatrix, localTranslation, true);
      }
      else
      {
        _centroid = RotatePoint(_centroid, true , true);
      }
    }
    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeMeasure(double& _measure) const
  {
    if(!twoDimensional && rotatedVertices.size() == 0)
    {
      Output::PrintErrorMessage("%s: Impossible to compute area, no rotated vertices found", false, __func__);
      return Output::GenericError;
    }

    if(twoDimensional && vertices.size() == 0)
    {
      Output::PrintErrorMessage("%s: Impossible to compute area, no vertices found", false, __func__);
      return Output::GenericError;
    }
    MappingBoundaryPolygon mapBoundary;
    mapBoundary.Compute(*this, false);

    NewQuadrature quadrature;
    quadrature.Initialize(1,1,NewQuadrature::Type::Gauss);

    MatrixXd constantQuadrature;
    mapBoundary.F(quadrature.Points(), constantQuadrature);
    const Matrix2Xd& normalDetJ = mapBoundary.NormalDetJ();
    _measure =  (constantQuadrature.row(0)).dot(normalDetJ.row(0));

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeSquaredRadius(double& _squaredRadius) const
  {
    _squaredRadius = 0.0;
    const unsigned int numberVertices = this->NumberOfVertices();

    Point* centroidTemp = NULL;
    if(centroid != NULL)
      centroidTemp = centroid;
    else
      ComputeCentroid(*centroidTemp);

    double squaredDistance = 0.0;
    for (unsigned int numPnt = 0; numPnt < numberVertices; numPnt++)
    {
      squaredDistance = MeasurerPoint::SquaredDistancePoints(*centroidTemp, *Vertex(numPnt));
      if (_squaredRadius < squaredDistance)
        _squaredRadius = squaredDistance;
    }
    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeAspectRatio(double& _aspectRatio) const
  {
    if(!twoDimensional && rotatedVertices.size() == 0)
    {
      Output::PrintErrorMessage("Impossible to compute aspect ratio with 3d polygon", true);
      return Output::GenericError;
    }

    Point centroidLocal;
    centroidLocal.setZero();
    const Point* centroidConst = &centroidLocal;
    if(twoDimensional)
    {
      if(centroid != NULL)
        centroidConst = centroid;
      else
        ComputeCentroid(centroidLocal);
    }
    else
    {
      if(rotatedCentroid != NULL)
        centroidConst = rotatedCentroid;
      else if(!twoDimensional)
        ComputeRotatedCentroid(centroidLocal);
    }

    double outRadius = 0.0;
    double inRadius = numeric_limits<double>::max();
    unsigned int numVertices = NumberOfVertices();
    for(unsigned int numPnt = 0; numPnt < numVertices; numPnt++)
    {
      Vector3d tangentVectorEdge = *RotatedVertex((numPnt+1)%numVertices) - *RotatedVertex(numPnt);
      tangentVectorEdge.normalize();
      double inRadiusTemp = abs(MeasurerPoint::DistancePointLine(*centroidConst, *RotatedVertex(numPnt), tangentVectorEdge));
      double outRadiusTemp = MeasurerPoint::SquaredDistancePoints(*centroidConst, *Vertex(numPnt));

      if(inRadiusTemp < inRadius)
        inRadius = inRadiusTemp;
      if(outRadiusTemp > outRadius)
        outRadius = outRadiusTemp;
    }
    _aspectRatio = sqrt(outRadius) / inRadius;
    return Output::Success;
  }

  Output::ExitCodes Polygon::ComputeRatioMaxMinEdge(double& _ratio) const
  {
    double edgeMinCell = numeric_limits<double>::max();
    double edgeMaxCell = 0.0;
    double length = 0.0;
    for(unsigned int numEdge = 0; numEdge < NumberOfEdges(); numEdge++)
    {
      const Segment& edge = *Edge(numEdge);
      edge.ComputeMeasure(length);
      if(length < edgeMinCell)
        edgeMinCell = length;
      if(length > edgeMaxCell)
        edgeMaxCell = length;
    }
    _ratio = edgeMaxCell / edgeMinCell;
    return Output::Success;

  }
  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeSquaredInRadius(double& _inRadius) const
  {
    if(!twoDimensional && rotatedVertices.size() == 0)
    {
      Output::PrintErrorMessage("Impossible to compute aspect ratio with 3d polygon", true);
      return Output::GenericError;
    }

    Point centroidLocal;
    centroidLocal.setZero();
    const Point* centroidConst = &centroidLocal;
    if(twoDimensional)
    {
      if(centroid != NULL)
        centroidConst = centroid;
      else
        ComputeCentroid(centroidLocal);
    }
    else
    {
      if(rotatedCentroid != NULL)
        centroidConst = rotatedCentroid;
      else if(!twoDimensional)
        ComputeRotatedCentroid(centroidLocal);
    }

    _inRadius = numeric_limits<double>::max();
    unsigned int numVertices = NumberOfVertices();
    for(unsigned int numPnt = 0; numPnt < numVertices; numPnt++)
    {
      Vector3d tangentVectorEdge = *RotatedVertex((numPnt+1)%numVertices) - *RotatedVertex(numPnt);
      double inRadiusTemp = abs(MeasurerPoint::DistancePointLine(*centroidConst, *RotatedVertex(numPnt), tangentVectorEdge));

      if(inRadiusTemp < _inRadius)
        _inRadius = inRadiusTemp;
    }
//    _inRadius *= _inRadius;
    return Output::Success;
  }

  // **********************c*****************************************************
  Output::ExitCodes Polygon::ComputeNormalPlane(Vector3d& normalPlane, const bool& normalize) const
  {
    /// <li> Compute normal vector N
    normalPlane.setZero();
    Vector3d edge = *vertices[1] - *vertices[0];
    Vector3d edgePrevious = *vertices[NumberOfVertices() - 1] - *vertices[0];
    normalPlane.noalias() += edge.cross(edgePrevious);
    for(unsigned int i = 1; i < NumberOfVertices(); i++)
    {
      edge = *vertices[(i + 1)%NumberOfVertices()] - *vertices[i];
      edgePrevious = *vertices[i - 1] - *vertices[i];
      normalPlane.noalias() += edge.cross(edgePrevious);
    }
    if(normalize)
      normalPlane.normalize();

    /// <li> Compute d


    return Output::Success;
    /// </ul>
  }

  Output::ExitCodes Polygon::ComputeRotationMatrix(Matrix3d& rotationMatrix, Vector3d& translation, const double& rotationTolerance) const
  {
    Vector3d* normalPlane;
    if(planeNormal == NULL)
    {
      normalPlane = new Vector3d(0.0,0.0,0.0);
      ComputeNormalPlane(*normalPlane);
    }
    else
      normalPlane = planeNormal;

    unsigned int numVertices = NumberOfVertices();
    MatrixXd Z(3, numVertices);
    MatrixXd W(3, numVertices);
    Matrix3d H;
    Vector3d V1mV0 = *vertices[1] - *vertices[0];
    double normVectorOne = V1mV0.norm();
    Z.col(0) = V1mV0;
    W.col(0) << normVectorOne * 1.0, 0.0, 0.0;
    for (unsigned int i = 2; i < numVertices; i++)
    {
      Vector3d VimV0 = *vertices[i] - *vertices[0];
      Z.col(i - 1) = VimV0;

      double normVectorI = VimV0.norm();
      double cosTheta = VimV0.dot(V1mV0) / (normVectorOne * normVectorI);

      if(cosTheta > 1.0 - rotationTolerance)
        W.col(i - 1) << normVectorI, 0.0, 0.0;
      else if(cosTheta < -1.0 + rotationTolerance)
        W.col(i - 1) << -normVectorI, 0.0, 0.0;
      else if((cosTheta > -rotationTolerance) && (cosTheta < rotationTolerance))
        W.col(i - 1) << 0.0, normVectorI, 0.0;
      else
      {
        //				double angleBetweenVectors = acos(cosTheta);
        //				W.col(i - 1) << normVectorI * cosTheta, normVectorI * sin(angleBetweenVectors), 0;
        W.col(i - 1) << normVectorI * cosTheta, normVectorI * sqrt(1.0 - cosTheta*cosTheta), 0;
      }
    }
    Z.col(numVertices - 1) = *normalPlane;
    W.col(numVertices - 1)<< 0, 0, 1;
    H = W * Z.transpose();
    JacobiSVD<MatrixXd> svd(H, ComputeThinU | ComputeThinV);

    rotationMatrix =  svd.matrixV() * (svd.matrixU()).transpose() ;
    translation = *vertices[0];

    if(planeNormal == NULL)
      delete normalPlane;

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeRotatedVertices(vector<Point*>& rotatedVertices) const
  {
    if(rotatedVertices.size() == 0)
      rotatedVertices.resize(NumberOfVertices());

    if(rotation == NULL)
    {
      Matrix3d localRotationMatrix;
      Vector3d localTranslation(0.0,0.0,0.0);
      ComputeRotationMatrix(localRotationMatrix, localTranslation);
      for(unsigned int v = 0; v < NumberOfVertices(); v++)
      {
        rotatedVertices[v] = new Point(*vertices[v]);
        RotatePointWithInput(*rotatedVertices[v], *vertices[v], localRotationMatrix, localTranslation);
      }
    }
    else
    {
      for(unsigned int v = 0; v < NumberOfVertices(); v++)
      {
        rotatedVertices[v] = new Point(*vertices[v]);
        RotatePoint(*rotatedVertices[v], *vertices[v]);
      }
    }

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeRotatedVerticesWithInput(const Matrix3d& rotationMatrix, const Vector3d& translationVector, const bool& inverse)
  {
    if(rotatedVertices[0] == NULL)
      AllocateRotatedVertices();

    for(unsigned int v = 0; v < NumberOfVertices(); v++)
      RotatePointWithInput(*rotatedVertices[v], *vertices[v], rotationMatrix, translationVector, inverse);

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeRotatedCentroid(Point& rotatedCentroid) const
  {
    Point* localCentroid;
    if(centroid == NULL)
    {
      localCentroid = new Point(0.0,0.0,0.0);
      ComputeCentroid(*localCentroid);
    }
    else
      localCentroid = centroid;


    if(rotation == NULL)
    {
      Matrix3d localRotationMatrix;
      Vector3d localTranslation(0.0,0.0,0.0);
      ComputeRotationMatrix(localRotationMatrix, localTranslation);
      RotatePointWithInput(rotatedCentroid, *localCentroid, localRotationMatrix, localTranslation);
    }
    else
    {
      RotatePoint(rotatedCentroid, *localCentroid);
    }

    if(centroid == NULL)
      delete localCentroid;

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeRotatedCentroidWithInput(const Matrix3d& rotationMatrix, const Vector3d& translationVector, const bool& inverse)
  {
    if(rotatedCentroid == NULL)
      rotatedCentroid = new Point();

    RotatePointWithInput(*rotatedCentroid, *centroid, rotationMatrix, translationVector, inverse);

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeRotatedBarycenter(Point& rotatedBarycenter) const
  {
    Point* localBarycenter;
    if(barycenter == NULL)
    {
      localBarycenter = new Point(0.0,0.0,0.0);
      ComputeBarycenter(*localBarycenter);
    }
    else
      localBarycenter = barycenter;


    if(rotation == NULL)
    {
      Matrix3d localRotationMatrix;
      Vector3d localTranslation(0.0,0.0,0.0);
      ComputeRotationMatrix(localRotationMatrix, localTranslation);
      RotatePointWithInput(rotatedBarycenter, *localBarycenter, localRotationMatrix, localTranslation);
    }
    else
    {
      RotatePoint(rotatedBarycenter, *localBarycenter);
    }

    if(barycenter == NULL)
      delete localBarycenter;

    return Output::Success;
  }

  // ***************************************************************************
  void Polygon::ComputeRotatedBarycenterWithInput(const Matrix3d& rotationMatrix, const Vector3d& translationVector, const bool& inverse)
  {
    if(rotatedBarycenter == NULL)
      rotatedBarycenter = new Point();

    RotatePointWithInput(*rotatedBarycenter, *barycenter, rotationMatrix, translationVector, inverse);
  }

  // ***************************************************************************
  Polygon::Position Polygon::PointInPlane(const Point& point, const double& tolerance) const
  {
    double translation = 0.0;
    Position position;
    Vector3d normal;
    normal.setZero();
    const Vector3d* normalConst = &normal;
    if(planeNormal == NULL)
      ComputePlane(normal,translation);
    else
    {
      normalConst = Normal();
      translation = PlaneTranslation();
    }
    double result = (point.dot(*normalConst) - translation);
    if( result < -tolerance)
      position = Position::Down;
    else if( result > tolerance)
      position = Position::Up;
    else
      position = Position::OnPlane;

    return position;
  }

  // ***************************************************************************
  bool Polygon::PointInside(const Point& point, const double& tolerance) const
  {

    Vector3d tangentVectorDifference;
    double crossProd = 0.0;
    if(!twoDimensional)
    {
      if(!PointInPlane(point))
        return false;

      Vector3d normal;
      const Vector3d* normalPlane = &normal;
      if(planeNormal != NULL)
        normalPlane = planeNormal;
      else
        ComputeNormalPlane(normal);

      Matrix3d matrix;
      matrix.col(0) = *normalPlane;
      matrix.col(2) = point - *vertices[0];
      for(unsigned int pntVert = 0; pntVert < NumberOfVertices() ; pntVert++)
      {
        matrix.col(1) = *vertices[(pntVert+1)%NumberOfVertices()] - *vertices[pntVert];

        crossProd = matrix.determinant();

        //        double crossProduct =  normalPlane.coeff(0) *
        //                               (tangentEdge.coeff(1) * tangentDiffPoint.coeff(2) - tangentDiffPoint.coeff(1) * tangentEdge.coeff(2))
        //                               - normalPlane.coeff(1) *
        //                               (tangentEdge.coeff(0) * tangentDiffPoint.coeff(2) - tangentDiffPoint.coeff(0) * tangentEdge.coeff(2))
        //                               + normalPlane.coeff(2) *
        //                               (tangentEdge.coeff(0) * tangentDiffPoint.coeff(1) - tangentDiffPoint.coeff(0) * tangentEdge.coeff(1));

        if(crossProd < -tolerance)
          return false;

        matrix.col(2) -= matrix.col(1);
      }
    }
    else
    {
      tangentVectorDifference = point - *vertices[0];

      for(unsigned int pntVert = 0; pntVert < NumberOfVertices() ; pntVert++)
      {
        Vector3d tangentVectorEdge = *vertices[(pntVert+1)%NumberOfVertices()] - *vertices[pntVert];

        crossProd = tangentVectorEdge.x() * tangentVectorDifference.y() - tangentVectorDifference.x() * tangentVectorEdge.y();

        if(crossProd < -tolerance)
          return false;

        tangentVectorDifference -= tangentVectorEdge;
      }
    }
    return true;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::Compute2DPolygonProperties()
  {
    ComputeMeasure();
    ComputeCentroid();
    return ComputeDiameter();
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::Compute3DPolygonProperties()
  {
    ComputePlane();
    ComputeRotationMatrix();
    ComputeRotatedVertices();

    ComputeMeasure();
    ComputeCentroid();
    ComputeRotatedCentroid();
    ComputeBarycenter();

    return ComputeDiameter();
  }

  Output::ExitCodes Polygon::Free2DPolygonProperties()
  {
    FreeBarycenter();
    FreeCentroid();
    return Output::Success;
  }

  Output::ExitCodes Polygon::Free3DPolygonProperties()
  {
    FreeRotatedBarycenter();
    FreeRotationMatrix();
    FreeRotatedVertices();
    FreeRotatedCentroid();
    FreeSubTriangles();
    FreePlane();
    FreeBarycenter();
    FreeCentroid();
    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::FreeRotationMatrix()
  {
    if(rotation != NULL)
      delete rotation;

    if(originTranslation != NULL)
      delete originTranslation;

    rotation = NULL;
    originTranslation = NULL;
    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::FreePlane()
  {
    if(planeNormal != NULL)
      delete planeNormal;

    planeNormal = NULL;
    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::FreeRotatedVertices()
  {
    for(unsigned int vrt = 0; vrt < rotatedVertices.size(); vrt++)
      delete rotatedVertices[vrt];
    rotatedVertices.clear();

    return Output::Success;
  }
  // ***************************************************************************s
  Output::ExitCodes Polygon::FreeRotatedCentroid()
  {
    if(rotatedCentroid != NULL)
      delete rotatedCentroid;

    rotatedCentroid = NULL;
    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::FreeRotatedBarycenter()
  {
    if(rotatedBarycenter != NULL)
      delete rotatedBarycenter;

    rotatedBarycenter = NULL;
    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::FreeSubTriangles()
  {
    for(unsigned int subT = 0; subT < subTriangles.size(); subT++)
      delete subTriangles[subT];
    subTriangles.clear();

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::AllignEdgesVertices()
  {
    const unsigned int& firstIdPointFace = Vertex(0)->Id();
    const unsigned int& secondIdPointFace = Vertex(1)->Id();
    unsigned int shift = 0;
    for(unsigned int edg = 0; edg < NumberOfEdges(); edg++)
    {
      const Segment& edgeTemp = Segment(edg);
      const unsigned int& idFirstPoint = edgeTemp.Vertex(0)->Id();
      const unsigned int& idSecondPoint = edgeTemp.Vertex(1)->Id();
      if((firstIdPointFace == idFirstPoint && secondIdPointFace == idSecondPoint) || (firstIdPointFace == idSecondPoint && secondIdPointFace == idFirstPoint))
      {
        shift = edg;
        break;
      }
    }
    if(shift == 0)
      return Output::Success;

    rotate(edges.begin(), edges.begin()+shift, edges.end());

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::ChangeOrientation()
  {
    reverse(vertices.begin(), vertices.end());
    reverse(edges.begin(), edges.end()-1);

    reverse(subTriangles.begin(), subTriangles.end());

    if(idPositionPoint.size() != 0)
      ComputePositionPoint();

    if(planeNormal != NULL)
    {
      *planeNormal *= -1;
      planeTranslation *= -1;
    }

    if(rotation != NULL)
      ComputeRotationMatrix(*rotation, *originTranslation);

    if(NumberOfRotatedVertices() > 0)
      ComputeRotatedVertices();

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ShiftPositionVertices(const unsigned int& positionVertex)
  {
    rotate(vertices.begin(), vertices.begin()+positionVertex, vertices.end());
    rotate(edges.begin(), edges.begin()+positionVertex, edges.end());

    if(idPositionPoint.size() != 0)
      ComputePositionPoint();

    if(!twoDimensional)
    {
      if(rotation != NULL)
      {
        originTranslation->x() = vertices[0]->x();
        originTranslation->y() = vertices[0]->y();
        originTranslation->z() = vertices[0]->z();
      }

      if(NumberOfRotatedVertices() > 0)
      {
        ComputeRotatedVertices();
      }
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Polygon::ComputePrincipalAxis(Vector3d& axis, const unsigned int& position)
  {
    Matrix2d inertiaTensor;
    ComputeInertiaTensor(inertiaTensor);

    SelfAdjointEigenSolver<Matrix2d> eigSolver(inertiaTensor);
    axis.setZero();
    axis.head<2>() = eigSolver.eigenvectors().col(position);

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Polygon::ComputeInertiaTensor(Matrix2d& inertiaTensor, const bool& boundary)
  {
    Point centroid;
    centroid.setZero();
    const Point* centroidConst = &centroid;
    if(Centroid() == NULL)
      ComputeCentroid(centroid);
    else
      centroidConst = Centroid();

    vector<double> momentums(3, 0.0);
    NewQuadrature quadrature;
    MatrixXd quadraturePoints;
    if(boundary)
    {
      quadrature.Initialize(1, 3, NewQuadrature::Type::Gauss);
      const MatrixXd& quadPoints = quadrature.Points();
      unsigned int numQuadPoints = quadrature.NumPoints();
      MappingBoundaryPolygon mapBoundary;
      mapBoundary.SetNumPoints(numQuadPoints);
      mapBoundary.Compute(*this, false);
      mapBoundary.F(quadPoints, quadraturePoints);

      Matrix2Xd& normalDetJ = mapBoundary.NormalDetJ();

      quadraturePoints.colwise() -= (*centroidConst).head<2>();
      VectorXd xPerY =  quadraturePoints.row(0).array() * quadraturePoints.row(1).array();
      MatrixXd squareQuadraturePoints = quadraturePoints.array().square();

      //moment xx
      momentums[0] = quadraturePoints.row(0) * normalDetJ.row(0).asDiagonal() * squareQuadraturePoints.row(1).transpose();
      //moment yy
      momentums[1] = quadraturePoints.row(1) * normalDetJ.row(1).asDiagonal() * squareQuadraturePoints.row(0).transpose();
      //moment xy
      momentums[2] = quadraturePoints.row(0) * normalDetJ.row(0).asDiagonal() * xPerY;

      momentums[0] *= 0.5;
      momentums[1] *= 0.5;
      momentums[2] *= 0.25;
    }
    else
    {
      quadrature.Initialize(2, 2, NewQuadrature::Type::Gauss);
      const MatrixXd& quadPoints = quadrature.Points();
      const VectorXd& weights = quadrature.Weights();

      MappingPolygon mapPolygon;
      mapPolygon.Compute(*this, false);
      mapPolygon.F(quadPoints, quadraturePoints);

      quadraturePoints.colwise() -= (*centroidConst).head<2>();

      VectorXd weightsRep = weights.replicate(mapPolygon.NumberOfSubTriangles(), 1);
      for(unsigned int subTr = 0; subTr < mapPolygon.NumberOfSubTriangles(); subTr++)
        weightsRep.segment<3>(subTr*3) *= mapPolygon.DetJ()(subTr);

      Matrix2d temp = quadraturePoints * weightsRep.asDiagonal() * quadraturePoints.transpose();

      //moment xx
      momentums[0] = temp(1,1);
      //moment yy
      momentums[1] = temp(0,0);
      //moment xy
      momentums[2] = temp(0,1);
    }
    if(abs(momentums[2]) < 1.0E-7)
      momentums[2] = 0.0;
    inertiaTensor << momentums[0], -momentums[2], -momentums[2], momentums[1];
    return Output::Success;
  }

}
