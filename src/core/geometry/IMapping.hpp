#ifndef __IMAPPING_H
#define __IMAPPING_H

#include "Eigen"
#include "IGeometricObject.hpp"
#include "Output.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

typedef Matrix<double, 3, 2> Matrix32d;
typedef Matrix<double, 2, 3> Matrix23d;

namespace GeDiM
{
  class IGeometricObject;

	/// \brief Interface class for the Map F from reference element to geometry.
	/// \author Andrea Borio, Alessandro D'Auria, Fabio Vicini.
	/// \copyright See top level LICENSE file for details.
	class IMapping
	{
		public:
			virtual ~IMapping() {}

      /// \return The dimension of the map
      virtual unsigned int Dimension() const = 0;

			/// \brief Gradient of the transformation F
			virtual MatrixXd J() const = 0;
			/// \brief Gradient of the inverse transformation F^-1
			virtual MatrixXd JInverse() const = 0;
      /// \brief Determinant of the gradient of the transfromation F
      virtual VectorXd DetJ() const = 0;

			/// \brief Compute of the Map with the original geometry
			/// \param geometry The geometry of the map
			/// \return The result of the initialization
      virtual Output::ExitCodes Compute(const IGeometricObject& geometry,
                                        const bool& computeInverse = true) = 0;
			/// \brief Map from the geometry to reference element
      /// \param x The points of the geometry to be mapped. Size is Dimension x NumberPoints
      /// \param result The points of the reference element mapped. Size is Dimension x NumberPoints
			/// \return The result of the transformation
      virtual Output::ExitCodes FInverse(const MatrixXd& x,
                                         MatrixXd& result) const = 0;
			/// \brief Map from the reference element to geometry
      /// \param x The points of the reference element to be mapped. Size is Dimension x NumberPoints
      /// \param result The points of the geometry mapped. Size is Dimension x NumberPoints
			/// \return The result of the transformation
      virtual Output::ExitCodes F(const MatrixXd& x,
                                  MatrixXd& result) const = 0;
	};
}

#endif // __IMAPPING_H
