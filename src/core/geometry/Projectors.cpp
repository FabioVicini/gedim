#include "Projectors.hpp"
#include "LeastSquarePlane.hpp"
namespace GeDiM
{

  void Projectors::DirectionalProjectionPointLine(const Vector3d& point,
                                                  const Vector3d& directionProjection,
                                                  const Vector3d& pointLine,
                                                  const Vector3d& normalizedTangentLine,
                                                  double& coordinateCurvilinear,
                                                  Vector3d& projectedPoint)
  {
    Vector3d normal(directionProjection(1), -directionProjection(0), 0.0);
    coordinateCurvilinear = normal.dot(point - pointLine);
    coordinateCurvilinear /= normal.dot(normalizedTangentLine);
    projectedPoint = pointLine + coordinateCurvilinear * normalizedTangentLine;

    return;
  }

  void Projectors::ProjectPointsOnLeastSquarePlane(vector<Vector3d>& vertices)
  {
    if(vertices.size() < 3)
    {
      Output::PrintWarningMessage("Number vertices to project less than 3 ", true);
      return;
    }

    Vector3d normal;
    double planeTranslation;
    LeastSquarePlane::ComputeLeastSquarePlane(vertices, normal, planeTranslation);
    for(unsigned int numPoint = 0; numPoint < vertices.size(); numPoint++)
    {
      Vector3d& vertex = vertices[numPoint];
      vertex.noalias() = vertex - MeasurerPoint::DistancePointPlane(vertex, normal, planeTranslation) * normal;
    }

    return;
  }

  void Projectors::DirectionalProjectionPointPlane(const Vector3d& point,
                                                   const Vector3d& directionProjection,
                                                   const Vector3d& normalizedPlaneNormal,
                                                   const double& planeTranslation,
                                                   double& coordinateCurvilinear,
                                                   Vector3d& projectedPoint)
  {
    coordinateCurvilinear = planeTranslation - normalizedPlaneNormal.dot(point);
    coordinateCurvilinear /= normalizedPlaneNormal.dot(directionProjection);
    projectedPoint = point + coordinateCurvilinear * directionProjection;

    return;
  }

}
