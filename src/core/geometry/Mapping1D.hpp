#ifndef __MAPPING1D_H
#define __MAPPING1D_H

#include "MacroDefinitions.hpp"
#include "Eigen"
#include "Output.hpp"
#include <cstdlib>
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class Mapping1D : public IMapping
  {
    protected:

      double j; ///< Gradient of the transformation F, J
      double jInverse; ///< Gradient of the transformation F, J
      double b; ///< Constant member of the function F
      double detJ; ///< Determinant of the J

    public:
      Mapping1D() {}
      ~Mapping1D() {}

      unsigned int Dimension() const { return 1; }

      Output::ExitCodes Compute(const IGeometricObject& line, const bool& computeInverse = true);
      /// Map from the reference element to the generic cell
      Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const { result = (j*x).array() + b; return Output::Success; }
      /// Map from the generic cell to reference element
      Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const { result = jInverse*(x.array() - b); return Output::Success; }

      MatrixXd J() const { return MatrixXd::Constant(1, 1, j); }
      MatrixXd JInverse() const { return  MatrixXd::Constant(1, 1, jInverse); }
      VectorXd DetJ() const { return MatrixXd::Constant(1, 1, detJ); }
  };
}

#endif // __MAPPING1D_H
