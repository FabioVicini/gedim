#ifndef __SHAPECREATOR_H
#define __SHAPECREATOR_H

#include "IGeometryFactory.hpp"
#include "IShapeCreator.hpp"

namespace GeDiM
{
  /// \brief Interface for shape creation.
  /// \copyright See top level LICENSE file for details.
  class ShapeCreator : public IShapeCreator
  {
    protected:
      IGeometryFactory& _factory;

      Point& CreatePoint(const Vector3d& coordinates);
      Segment& CreateEdge(const vector<Point*>& points,
                          const vector<unsigned int>& positions = vector<unsigned int> { });
      Polygon& CreatePolygon(const vector<Point*>& points,
                             const vector<Segment*>& edges,
                             const vector<unsigned int>& vertexPositions = vector<unsigned int> { },
                             const vector<unsigned int>& edgePositions = vector<unsigned int> { });
      Polyhedron& CreatePolyhedron(const vector<Point*>& points,
                                   const vector<Segment*>& edges,
                                   const vector<Polygon*>& faces);
    public:
      ShapeCreator(IGeometryFactory& factory);
      virtual ~ShapeCreator() {}

      Segment& CreateSegment(const Vector3d& origin,
                             const Vector3d& end);
      Polygon& CreatePolygon(const vector<Vector3d>& vertices);
      Polygon& CreateTriangle(const Vector3d& p1,
                              const Vector3d& p2,
                              const Vector3d& p3);
      Polygon& CreateParallelogram(const Vector3d& origin,
                                   const Vector3d& lengthVector,
                                   const Vector3d& widthVector);
      Polygon& CreateRectangle(const Vector2d& origin,
                               const double& base,
                               const double& height);
      Polygon& CreateSquare(const Vector2d& origin,
                            const double& edge);
      Polygon& CreateRegularPolygon(const unsigned int& numEdges,
                                    const double& lengthRadius = 1,
                                    const bool& convertRadius = false);
      Polyhedron& CreateParallelepiped(const Vector3d& origin,
                                       const Vector3d& lengthVector,
                                       const Vector3d& heightVector,
                                       const Vector3d& widthVector);
      Polyhedron& CreateParallelogrammicPrism(const Vector3d& origin,
                                              const double& length,
                                              const double& height,
                                              const double& width);
      Polyhedron& CreateCube(const Vector3d& origin,
                             const double& edge);
  };
}

#endif // __SHAPECREATOR_H
