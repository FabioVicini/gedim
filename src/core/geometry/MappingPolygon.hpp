#ifndef __MAPPINGPOLYGON_H
#define __MAPPINGPOLYGON_H

#include "Output.hpp"
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MappingPolygon : public IMapping
  {
    protected:

      MatrixXd j; ///< Gradient of the transformation F, J
      MatrixXd jInverse; ///< Gradient of the transformation F, J
      VectorXd b; ///< Constant member of the function F
      VectorXd detJ; ///< Determinants of the J of single subTriangle
      unsigned int numberOfSubTriangles; ///< Number of subTriangles in the Polygon
      bool twoDPolygon; ///< Polygon 2D or 3D

    public:
      MappingPolygon(const bool& twoD = true) { twoDPolygon = twoD; }
      ~MappingPolygon() {}

      unsigned int Dimension() const { return ( (twoDPolygon)? 2 : 3); }

      Output::ExitCodes Compute(const IGeometricObject& polygon, const bool& computeInverse = true);

      /// Map from the reference element to the generic cell
      Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const;
      /// Map from the generic cell to reference element
      Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const;

      MatrixXd J() const { return j; }
      MatrixXd JInverse() const { return jInverse; }
      VectorXd DetJ() const { return detJ; }
      unsigned int NumberOfSubTriangles() const { return numberOfSubTriangles; }
  };
}

#endif // __MAPPING_H
