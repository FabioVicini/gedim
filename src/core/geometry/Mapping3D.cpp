#include "Mapping3D.hpp"
#include "Polyhedron.hpp"

namespace GeDiM
{
  void Mapping3D::ComputeJTetrahedron(const IGeometricObject& polyhedron)
  {
    j.col(0) = *polyhedron.Vertex(1) - *polyhedron.Vertex(0);
    j.col(1) = *polyhedron.Vertex(2) - *polyhedron.Vertex(0);
    j.col(2) = *polyhedron.Vertex(3) - *polyhedron.Vertex(0);
  }

  void Mapping3D::ComputeJHexahedron(const IGeometricObject& polyhedron)
  {
    vector<const Point*> positionVertex;
    positionVertex.reserve(3);
    const Point& firstVertex = *polyhedron.Vertex(0);
    for(unsigned int numEdg = 0; numEdg < polyhedron.NumberOfEdges(); numEdg++)
    {
      const Segment& segment = *polyhedron.Edge(numEdg);
      bool first = segment.Vertex(0) == &firstVertex;
      bool second = segment.Vertex(1) == &firstVertex;
      if( (first || second) )
      {
        if(first)
          positionVertex.push_back(segment.Vertex(1));
        else
          positionVertex.push_back(segment.Vertex(0));
      }
    }

    j.col(0) = *positionVertex[0] - *polyhedron.Vertex(0);
    j.col(1) = *positionVertex[1] - *polyhedron.Vertex(0);
    j.col(2) = *positionVertex[2] - *polyhedron.Vertex(0);

  }

  Output::ExitCodes Mapping3D::Compute(const IGeometricObject& polyhedron, const bool& computeInverse)
  {
    switch(polyhedron.Type())
    {
      case IGeometricObject::Tetrahedron:
        ComputeJTetrahedron(polyhedron);
      break;
      case IGeometricObject::Hexahedron:
      case IGeometricObject::Cube:
        ComputeJHexahedron(polyhedron);
      break;
      default:
        Output::PrintErrorMessage("Mapping for general polygon does not exist",true);
      return Output::GenericError;
      break;
    }
    detJ = j.determinant();
    if(detJ < 0)
    {
      j.col(0).swap(j.col(1));
      detJ = -detJ;
    }

    b = *polyhedron.Vertex(0);
    if(computeInverse)
      jInverse = j.inverse();

    return Output::Success;
  }
}
