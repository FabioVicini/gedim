#include "GeometryFactory.hpp"

namespace GeDiM
{
  // ***************************************************************************
  void GeometryFactory::Reset()
  {
    points.clear();
    segments.clear();
    polygons.clear();
    polyhedrons.clear();
  }
  // ***************************************************************************
  Point& GeometryFactory::CreatePoint()
  {
    unsigned int newId = points.size();
    points.insert(make_pair(newId, Point(newId)));
    Point& point = GetPoint(newId);
    return point;
  }
  // ***************************************************************************
  Segment& GeometryFactory::CreateSegment()
  {
    unsigned int newId = segments.size();
    segments.insert(make_pair(newId, Segment(newId)));
    Segment& segment = GetSegment(newId);
    return segment;
  }
  // ***************************************************************************
  Polygon& GeometryFactory::CreatePolygon()
  {
    unsigned int newId = polygons.size();
    polygons.insert(make_pair(newId, Polygon(newId)));
    Polygon& polygon = GetPolygon(newId);
    return polygon;
  }
  // ***************************************************************************
  Polyhedron& GeometryFactory::CreatePolyhedron()
  {
    unsigned int newId = polyhedrons.size();
    polyhedrons.insert(make_pair(newId, Polyhedron(newId)));
    Polyhedron& polyhedron = GetPolyhedron(newId);
    return polyhedron;
  }
  // ***************************************************************************
}
