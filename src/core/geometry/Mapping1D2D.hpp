#ifndef __MAPPING1D2D_H
#define __MAPPING1D2D_H

#include "Eigen"
#include "Output.hpp"
#include <cstdlib>
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
class Mapping1D2D : public IMapping
{
  protected:

    Vector2d j; ///< Gradient of the transformation F, J
    Vector2d jInverse; ///< Gradient of the transformation F, J
    Vector2d b; ///< Constant member of the function F
    double detJ; ///< Determinant of the J

  public:
    Mapping1D2D() {}
    ~Mapping1D2D() {}

    unsigned int Dimension() const { return 2; }

    Output::ExitCodes Compute(const IGeometricObject& line, const bool& computeInverse = true);
    /// Map from the reference element to the generic cell
    Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const { result = (j*x).colwise() + b; return Output::Success; }
    /// Map from the generic cell to reference element
    Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const { result = jInverse.transpose()*(x.colwise() - b); return Output::Success; }

    MatrixXd J() const { return j; }
    MatrixXd JInverse() const { return  jInverse; }
    VectorXd DetJ() const { return MatrixXd::Constant(1, 1, detJ); }
};
}
#endif // __MAPPING1D2D_H
