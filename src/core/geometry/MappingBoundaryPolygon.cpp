#include "MappingBoundaryPolygon.hpp"
#include "Point.hpp"
#include "Polygon.hpp"

namespace GeDiM
{
  Output::ExitCodes MappingBoundaryPolygon::Compute(const IGeometricObject& polygon, const bool& computeInverse)
  {
    const Polygon& polygonRef = static_cast<const Polygon&>(polygon);

    numberOfEdges = polygon.NumberOfEdges();
    j.resize(2*numberOfEdges);
    if(computeInverse)
    {
      jInverse.resize(2*numberOfEdges);
      jInverse.setZero();
    }

    detJ.resize(numberOfEdges);
    normalDetJ.resize(Eigen::NoChange, numPointsToMap * numberOfEdges);
    normalDetJ.setZero();

    b.resize(2*numberOfEdges);

    Vector2d tempMatrix;
    Vector2d tempMatrix2;
    for(unsigned int edg = 0; edg < numberOfEdges; edg++)
    {
      unsigned int numEdgNext = (edg+1)%numberOfEdges;

      if(polygonRef.Is2DPolygon())
      {
        tempMatrix(0) = polygon.Vertex(numEdgNext)->x() - polygon.Vertex(edg)->x();
        tempMatrix(1) = polygon.Vertex(numEdgNext)->y() - polygon.Vertex(edg)->y();
        b.block<2,1>(2*edg, 0) << polygon.Vertex(edg)->x(), polygon.Vertex(edg)->y();
      }
      else
      {
        tempMatrix(0) = polygonRef.RotatedVertex(numEdgNext)->x() - polygonRef.RotatedVertex(edg)->x();
        tempMatrix(1) = polygonRef.RotatedVertex(numEdgNext)->y() - polygonRef.RotatedVertex(edg)->y();
        b.block<2,1>(2*edg, 0) << polygonRef.RotatedVertex(edg)->x(), polygonRef.RotatedVertex(edg)->y();
      }

      detJ(edg) = tempMatrix.norm();
      if(detJ(edg)  < 0)
      {
        tempMatrix.col(0).swap(tempMatrix.col(1));
        detJ(edg) = -detJ(edg);
      }
      tempMatrix2(0) = tempMatrix(1);
      tempMatrix2(1) = -tempMatrix(0);
      normalDetJ.block(0, numPointsToMap*edg, Dimension(), numPointsToMap) = tempMatrix2.replicate(1, numPointsToMap);

      j.block<2,1>(2*edg, 0) = tempMatrix;

      if(computeInverse)
      {
        jInverse(2*edg) = 1.0 / j(2*edg);
      }

      tempMatrix.setZero();
    }

    return Output::Success;
  }

  Output::ExitCodes MappingBoundaryPolygon::F(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    unsigned int dimension = Dimension();
    result.resize(dimension, numCols * numberOfEdges);

    MatrixXd tempResult = (j * x).colwise() + b;
    for(unsigned int edg = 0; edg < numberOfEdges; edg++)
    {
      result.block(0, numCols*edg, dimension, numCols) = tempResult.block(edg*dimension, 0, dimension, numCols);
    }
    return Output::Success;
  }

  Output::ExitCodes MappingBoundaryPolygon::FInverse(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    unsigned int newNumCols = numCols/numberOfEdges;
    MatrixXd reshapeX(2*numberOfEdges, newNumCols);
    for(unsigned int numTri = 0; numTri < numberOfEdges; numTri++)
    {
      reshapeX.block(2*numTri, 0, 2, newNumCols) = x.block(0, newNumCols*numTri, 2, newNumCols);
    }
    result.setZero();
    reshapeX.colwise() -= b;
    result = jInverse.segment<2>(0).transpose() * ((reshapeX.block(0,0,2,newNumCols)));
    return Output::Success;
  }

  //============================================================================

}
