#include "Mapping2D3D.hpp"
#include "Polygon.hpp"

namespace GeDiM
{
  void Mapping2D3D::ComputeJTriangle(const IGeometricObject& polygon)
  {
    j.col(0) = (*polygon.Vertex(1)) - (*polygon.Vertex(0));
    j.col(1) = (*polygon.Vertex(2)) - (*polygon.Vertex(0));
  }

  void Mapping2D3D::ComputeJParallelogram(const IGeometricObject& polygon)
  {
    j.col(0) = (*polygon.Vertex(1)) - (*polygon.Vertex(0));
    j.col(1) = (*polygon.Vertex(3)) - (*polygon.Vertex(0));
  }

  Output::ExitCodes Mapping2D3D::Compute(const IGeometricObject& polygon, const bool& computeInverse)
  {
    switch(polygon.Type())
    {
      case Polygon::Triangle:
        ComputeJTriangle(polygon);
      break;
      case Polygon::Parallelogram:
      case Polygon::Rectangle:
      case Polygon::Square:
        ComputeJParallelogram(polygon);
      break;
      default:
        Output::PrintErrorMessage("Mapping for general polygon does not exist",true);
      return Output::GenericError;
      break;
    }
    Vector3d indipendentColumn = j.col(0).cross((j.col(1)));
    detJ = indipendentColumn.norm();
    if(detJ < 0)
    {
      j.col(0).swap(j.col(1));
      detJ = -detJ;
    }
    b << *polygon.Vertex(0);

    if(computeInverse)
    {
      Matrix3d temp;
      temp << j, indipendentColumn;
      jInverse = temp.inverse().block(0, 0, 2, 3);
    }

    return Output::Success;
  }
}
