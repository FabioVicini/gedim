
#include "LeastSquarePlane.hpp"

namespace GeDiM
{

  void LeastSquarePlane::ComputeLeastSquarePlane(const vector<Vector3d>& vertices, Vector3d& normal, double& planeTranslation)
  {
    Vector3d barycenter;
    barycenter.setZero();
    unsigned int numPoints = vertices.size();
    double inverseNumPoints = 1.0/numPoints;
    for(unsigned int numPnt = 0; numPnt < numPoints; numPnt++)
      barycenter += vertices[numPnt]*inverseNumPoints;

    Matrix3d matrix;
    matrix.setZero();
    for(unsigned int numPnt = 0; numPnt < numPoints; numPnt++)
    {
      Vector3d diff = vertices[numPnt] - barycenter;
      Matrix3d localMat = diff*diff.transpose();
      matrix += localMat;
    }

    SelfAdjointEigenSolver<Matrix3d> eigSolver(matrix);

    normal = eigSolver.eigenvectors().col(0);

    planeTranslation = normal.dot(barycenter);

    return;
  }

}
