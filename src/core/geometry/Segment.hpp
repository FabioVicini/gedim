#ifndef SEGMENT_HPP
#define SEGMENT_HPP

#include "GeometricObject.hpp"
#include "Mapping1D.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class Segment;

  /// \brief Class representing a segment.
  /// \copyright See top level LICENSE file for details.
  class Segment : public GeometricObject
  {
  public:
    enum Position
    {
      AtTheLeft = 0,
      AtTheRight = 1,
      Beyond = 2,
      Behind = 3,
      Between = 4,
      AtTheOrigin = 5,
      AtTheEnd = 6
    };

  protected:

    Vector3d* tangent; ///< Pointer to the normalized vector tangent
    Vector3d* normal; ///< Pointer to the normalized vector normal

  public:
    ///
    /// \brief Segment
    /// \param _id
    ///
    explicit Segment(const unsigned int& _id = 0);
    ///
    /// \brief Segment
    /// \param segment
    ///
    Segment(const Segment& segment) : Segment() { *this = segment; }
    Segment(Segment&& segment) : Segment() { *this = move(segment); }
    Segment& operator=(Segment&& segment);
    Segment& operator=(const Segment& segment);

    virtual ~Segment();
    ///
    /// \brief Initialize
    /// \param origin
    /// \param end
    ///
    inline void Initialize(const Point& origin, const Point& end) {vertices.reserve(2); vertices.push_back(&origin); vertices.push_back(&end); }
    ///
    /// \brief Flip
    ///
    inline void Flip() { const Point* tempPoint = vertices[0]; vertices[0] = vertices[1]; vertices[1] = tempPoint; }
    ///
    /// \brief Dimension
    /// \return
    ///
    inline unsigned int Dimension() const { return 1; }
    ///
    /// \brief Origin
    /// \return
    ///
    inline const Point& Origin() const { return *vertices[0]; }
    ///
    /// \brief End
    /// \return
    ///
    inline const Point& End() const { return *vertices[1]; }
    ///
    /// \brief Tangent
    /// \return
    ///
    inline const Vector3d& Tangent() const { return *tangent; }
    ///
    /// \brief Normal
    /// \return
    ///
    inline const Vector3d& Normal() const { return *normal; }
    ///Compute Tangent
    /// \brief Compute the normalized tangent of the segment and save inside the object
    inline void ComputeTangent(const bool& normalize = true)
    { if(tangent == NULL) tangent = new Vector3d(); ComputeTangent(*tangent, normalize); }
    ///Compute Tangent
    /// \brief Compute the normalized tangent of the segment and save the result in the output
    /// \param[out] _tangent is a Vector3d where it will be saved the tangent of the segment
    Output::ExitCodes ComputeTangent(Vector3d& _tangent, const bool& normalize = true) const;
    ///Compute Normal
    /// \brief Compute the normalized normal of the segment and save inside the object
    inline void ComputeNormal(const bool& normalize = true)
    { if(normal == NULL) normal = new Vector3d(); ComputeNormal(*normal, normalize); }
    ///Compute Normal
    /// \brief Compute the normalized normal of the segment and save the result in the output
    /// \param[out] _normal is a Vector3d where it will be saved the normal of the segment
    Output::ExitCodes ComputeNormal(Vector3d& _normal, const bool& normalize = true) const;
    ///Compute the signum Normal with respect to the input
    /// \brief Compute the normalized normal of the segment and save the result in the output
    /// \param[out] true is coherent with the normal defined by the segment
    bool ComputeSignNormal() const;
    /// \brief Transform in a Point/s on line given a coordinate curvilinear or a matrix coordinate curvilinears
    /// \param[in] single coordinate curvilinear or matrix coordinate Curvilinears
    /// \param[out] point on the line at the parametric coordinate given in input
    void ComputePointOnSegment(const double& curvilinearCoordinate, Point& point) const { point = *tangent * curvilinearCoordinate * measure; point.noalias() += *vertices[0]; }
    ///
    /// \brief ComputePointOnSegment
    /// \param curvilinearCoordinate
    /// \param points
    ///
    void ComputePointOnSegment(const MatrixXd& curvilinearCoordinate, MatrixXd& points) const { points = (*tangent * measure) * (curvilinearCoordinate) ; points.colwise() += *vertices[0]; }
    /// \brief Transform a Point or a matrix of Points on line in coordinate curvilinear/s
    /// \param[in] single or matrix point on the line
    /// \param[out] coordinate curvilinear/
    void ComputeCoordinateCurvilinear(const Point& point, double& curvilinearCoordinate) const {Vector3d tangentVectorDiff = point - *vertices[0]; curvilinearCoordinate = (*tangent).dot(tangentVectorDiff) / measure; }
    ///
    /// \brief ComputeCoordinateCurvilinear
    /// \param points
    /// \param curvilinearCoordinates
    ///
    void ComputeCoordinateCurvilinear(const MatrixXd& points, MatrixXd& curvilinearCoordinates) const {MatrixXd tangentVectorDiff = points.colwise() - *vertices[0]; curvilinearCoordinates = (*tangent / measure) * (tangentVectorDiff); }
    /// \brief Compute the centroid of the edge and save it inside the object
    inline Output::ExitCodes ComputeCentroid() { if(centroid == NULL) centroid = new Point(); return ComputeBarycenter(*centroid); }
    ///
    /// \brief ComputeCentroid
    /// \param _centroid
    /// \return
    ///
    Output::ExitCodes ComputeCentroid(Point& _centroid) const {return ComputeBarycenter(_centroid); }
    /// \brief Compute the length of the edge and save it inside the object
    inline Output::ExitCodes ComputeMeasure() { return ComputeMeasure(measure); }
    ///
    /// \brief ComputeMeasure
    /// \param _measure
    /// \return
    ///
    Output::ExitCodes ComputeMeasure(double& _measure) const;
    /// \brief Compute the radius of the edge and save it inside the object
    inline Output::ExitCodes ComputeSquaredRadius() { return ComputeSquaredRadius(squaredRadius); }
    ///
    /// \brief ComputeSquaredRadius
    /// \param _squaredRadius
    /// \return
    ///
    Output::ExitCodes ComputeSquaredRadius(double& _squaredRadius) const;
    /// \brief Compute the radius of the edge and save it inside the object
    inline Output::ExitCodes ComputeAspectRatio() { return ComputeAspectRatio(aspectRatio); }
    ///
    /// \brief ComputeAspectRatio
    /// \param _aspectRatio
    /// \return
    ///
    Output::ExitCodes ComputeAspectRatio(double& _aspectRatio) const { _aspectRatio = 1.0; return Output::Success; }
    /// \brief Compute the position of a point given in input with respect to the segment
    /// \param[in] point
    /// \param[in] tollerance set to a default value = 1.0E-7
    /// \warning It works only with 2D segments
    Segment::Position PointPosition(const Point& point, const double& tollerance = 1e-7) const;
    /// \brief Convenience implementation of \ref IGeometricObject::NumberOfEdges().
    /// \returns Always 1.
    inline unsigned int NumberOfEdges() const  { return 1; }
    /// \brief Convenience implementation of \ref IGeometricObject::Edge().
    /// \returns this.
    inline const Segment* Edge(const unsigned int&) const { return this; }
    /// \brief Convenience implementation of \ref IGeometricObject::NumberOfFaces().
    /// \returns Always 0.
    inline unsigned int NumberOfFaces() const  { return 0; }
    /// \brief Convenience implementation of \ref IGeometricObject::Edge().
    /// \returns nullptr.
    inline const Polygon* Face(const unsigned int&) const { return nullptr; }
    /// \brief ComputeMap
    /// \param map
    /// \param computeInverse
    /// \return
    Output::ExitCodes ComputeMap(IMapping*& map,
                                 const bool& computeInverse = true) const
    {
      map = new Mapping1D();
      return map->Compute(*this, computeInverse);
    }
  };
}
#endif //SEGMENT_HPP
