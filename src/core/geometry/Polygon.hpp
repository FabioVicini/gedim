#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "Segment.hpp"
#include "DefineNumbers.hpp"
#include "IRotation.hpp"
#include "Mapping2D.hpp"
#include "Mapping2D3D.hpp"
#include "MappingPolygon.hpp"
#include "MappingBoundaryPolygon.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Class representing a polygon.
  /// \copyright See top level LICENSE file for details.
  class Polygon : public GeometricObject, public IRotation
  {
    public:

      enum Position
      {
        Down = -1,
        OnPlane = 0,
        Up = 1,
      };

    protected:
      bool twoDimensional;

      vector< const Segment* > edges; ///< list of the edges in the polygon

      Vector3d* planeNormal; ///< Normal of plane containing the polygon in the space: comes from the equation ax+by+cz = d
      double planeTranslation; ///< plane translation in the space: comes from the equation ax+by+cz = d

      Point* rotatedCentroid; ///< Rotated centroid on the plane xy
      Point* rotatedBarycenter; ///< Rotated barycenter on the plane xy
      vector< Point* > rotatedVertices; ///< Rotated vertices on the plane xy

      vector< Polygon* > subTriangles;

    public:
      /// \brief Default constructor.
      /// \param _id The id of the polygon.
      explicit Polygon(const unsigned int& _id = 0);
      /// \brief Copy constructor.
      /// \param polygon The polygon that is copied to *this.
      Polygon(const Polygon& polygon) : Polygon() { *this = polygon; }
      /// \brief Move constructor.
      /// \param polygon The polygon that is moved to *this.
      Polygon(Polygon&& polygon) noexcept : Polygon() { *this = move(polygon); }
      Polygon& operator=(Polygon&& polygon);
      Polygon& operator=(const Polygon& polygon);
      virtual ~Polygon();

      /// \brief Clone function.
      /// \details Create a new pointer of the same object type
      /// \return GeometricObject*
      inline GeometricObject* Clone() const { return new Polygon(); }
      /// \brief Setter for 2D polygon
      /// \details Set the attribute twoD
      /// \param[in] twoD Is set to default equal to true
      inline void Set2DPolygon(const bool& twoD = true) { twoDimensional = twoD; }
      /// \brief Check if the polygon is 2D
      /// \return True or False
      const bool& Is2DPolygon() const { return twoDimensional; }
      /// \brief Initialize the container for the vertices and edges.
      /// \param[in] numberOfVertices
      /// \param[in] numberOfEdges
      /// \sa
      inline void Initialize(const unsigned int& numberOfVertices, const unsigned int& numberOfEdges) { InitializeVertices(numberOfVertices); InitializeEdges(numberOfEdges);}
      ///
      /// \brief Allocate
      /// \param numberOfVertices
      /// \param numberOfEdges
      ///
      inline void Allocate(const unsigned int& numberOfVertices, const unsigned int& numberOfEdges) { AllocateVertices(numberOfVertices); AllocateEdges(numberOfEdges);}
      ///
      /// \brief InitializeEdges
      /// \param numberOfEdges
      ///
      inline void InitializeEdges(const unsigned int& numberOfEdges) { edges.reserve(numberOfEdges);}
      ///
      /// \brief AllocateEdges
      /// \param numberOfEdges
      ///
      inline void AllocateEdges(const unsigned int& numberOfEdges) { edges.resize(numberOfEdges);}
      ///
      /// \brief AddEdge
      /// \param edge
      ///
      inline void AddEdge(const Segment& edge) { edges.push_back(&edge); }
      ///
      /// \brief AddEdges
      /// \param _edges
      ///
      inline void AddEdges(const vector< const Segment* >& _edges) {edges = _edges; }
      ///
      /// \brief InsertEdge
      /// \param edge
      /// \param position
      ///
      inline void InsertEdge(const Segment& edge, const unsigned int& position) { edges[position] = &edge; }
      inline unsigned int NumberOfEdges() const { return edges.size(); }
      inline const Segment* Edge(const unsigned int& position) const { return edges[position]; }
      ///
      /// \brief Edges
      /// \return
      ///
      inline const vector< const Segment* >& Edges() const { return edges; }
      ///
      /// \brief CreateEdge
      /// \param firstPoint
      /// \param secondPoint
      /// \param id
      /// \return
      ///
      Segment CreateEdge(const unsigned int& firstPoint, const unsigned int& secondPoint, const unsigned int& id = 0);

      /// \brief Convenience implementation of \ref IGeometricObject::NumberOfFaces().
      /// \returns Always 1.
      inline unsigned int NumberOfFaces() const  { return 1; }
      /// \brief Convenience implementation of \ref IGeometricObject::Edge().
      /// \returns nullptr.
      inline const Polygon* Face(const unsigned int&) const  { return this; }
      ///
      /// \brief Dimension
      /// \return
      ///
      inline unsigned int Dimension() const { return 2; }
      ///Compute Centroid
      /// \brief Compute the centroid of the polygon and save inside the object
      Output::ExitCodes ComputeCentroid() { if(centroid == NULL) centroid = new Point();  return ComputeCentroid(*centroid); }
      Output::ExitCodes ComputeCentroid(Point& _centroid) const;

      ///Compute Measure
      /// \brief Compute the length of the edge and save inside the object
      inline Output::ExitCodes ComputeMeasure() { return ComputeMeasure(measure); }
      Output::ExitCodes ComputeMeasure(double& _measure) const;

      ///Compute radius
      /// \brief Compute the radius of the edge and save inside the object
      inline Output::ExitCodes ComputeSquaredRadius() { return ComputeSquaredRadius(squaredRadius); }
      Output::ExitCodes ComputeSquaredRadius(double& _squaredRadius) const;
      inline Output::ExitCodes ComputeAspectRatio() { return ComputeAspectRatio(aspectRatio); }

      Output::ExitCodes ComputeAspectRatio(double& _aspectRatio) const;
      ///
      /// \brief ComputeRatioMaxMinEdge
      /// \param _ratio
      /// \return
      ///
      Output::ExitCodes ComputeRatioMaxMinEdge(double& _ratio) const;
      ///
      /// \brief ComputeInRadius
      /// \param _inRadius
      /// \return
      ///
      Output::ExitCodes ComputeSquaredInRadius(double& _inRadius) const;
      void AllocateNormalPlane() { if(planeNormal == NULL) planeNormal = new Vector3d(0.0,0.0,0.0); }
      void DeAllocateNormalPlane() { if(planeNormal != NULL) delete planeNormal; }
      ///
      /// \brief ComputeNormalPlane
      /// \param normalize
      /// \return
      ///
      Output::ExitCodes ComputeNormalPlane(const bool& normalize = true)
      { AllocateNormalPlane(); return ComputeNormalPlane(*planeNormal, normalize); }
      ///
      /// \brief ComputeNormalPlane
      /// \param normalPlane
      /// \param normalize
      /// \return
      ///
      Output::ExitCodes ComputeNormalPlane(Vector3d& normalPlane, const bool& normalize = true) const;
      /*!
       * \brief Normal
       * \return
       */
      const Vector3d* Normal() const {return planeNormal; }
      /*!
       * \brief ComputePlaneTranslation
       * \return
       */
      Output::ExitCodes ComputePlaneTranslation() { return ComputePlaneTranslation(*planeNormal, planeTranslation); }
      /*!
       * \brief ComputePlaneTranslation
       * \param normalPlane
       * \param translation
       * \return
       */
      Output::ExitCodes ComputePlaneTranslation(const Vector3d& normalPlane, double& translation) const { translation = normalPlane.dot(*vertices[0]); return Output::Success; }
      /*!
       * \brief PlaneTranslation
       * \return
       */
      const double& PlaneTranslation() const { return planeTranslation; }
      ///
      /// \brief ComputePlane
      /// \param normalize
      /// \return
      ///
      Output::ExitCodes ComputePlane(const bool& normalize = true) { if (ComputeNormalPlane(normalize) == Output::Success) return ComputePlaneTranslation(); else return Output::GenericError; }
      ///
      /// \brief ComputePlane
      /// \param normalPlane
      /// \param translation
      /// \param normalize
      /// \return
      ///
      Output::ExitCodes ComputePlane(Vector3d& normalPlane, double& translation, const bool& normalize = true) const
      { ComputeNormalPlane(normalPlane, normalize); return ComputePlaneTranslation(normalPlane, translation); }
      ///
      /// \brief AllocateRotationMatrix
      ///
      void AllocateRotationMatrix() { if(rotation == NULL) rotation = new Matrix3d(); if(originTranslation == NULL) originTranslation = new Vector3d(); }
      ///
      /// \brief DeAllocateRotationMatrix
      ///
      void DeAllocateRotationMatrix() { if(rotation != NULL) { delete rotation; delete originTranslation;} }
      /*!
       * \brief ComputeRotationMatrix
       * \param rotationTolerance
       * \return
       */
      inline Output::ExitCodes ComputeRotationMatrix(const double& = TOLERANCEORTHOGANILITY) { AllocateRotationMatrix(); return ComputeRotationMatrix(*rotation, *originTranslation); }
      /*!
       * \brief ComputeRotationMatrix
       * \param rotationMatrix
       * \param translation
       * \param rotationTolerance
       * \return
       */
      Output::ExitCodes ComputeRotationMatrix(Matrix3d& rotationMatrix, Vector3d& translation, const double& rotationTolerance = TOLERANCEORTHOGANILITY) const;
      /*!
       * \brief AllocateRotatedVertices
       */
      inline void AllocateRotatedVertices();
      ///
      /// \brief RotatedVertex
      /// \param position
      /// \return
      ///
      inline const Point* RotatedVertex(const unsigned int& position) const {return ((twoDimensional)? vertices[position] : rotatedVertices[position]);}
      ///
      /// \brief RotatedVertices
      /// \return
      ///
      inline const vector< Point* >& RotatedVertices() const { return rotatedVertices; }
      /*!
       * \brief NumberOfRotatedVertices
       * \return
       */
      inline unsigned int NumberOfRotatedVertices() const { return rotatedVertices.size(); }
      /*!
       * \brief ComputeRotatedVertices
       * \return
       */
      Output::ExitCodes ComputeRotatedVertices() { FreeRotatedVertices(); return ComputeRotatedVertices(rotatedVertices);}
      /*!
       * \brief ComputeRotatedVertices
       * \param rotatedVertices
       * \return
       */
      Output::ExitCodes ComputeRotatedVertices(vector<Point*>& rotatedVertices) const;
      /*!
       * \brief ComputeRotatedVerticesWithInput
       * \param rotationMatrix
       * \param translationVector
       * \param inverse
       * \return
       */
      Output::ExitCodes ComputeRotatedVerticesWithInput(const Matrix3d& rotationMatrix, const Vector3d& translationVector, const bool& inverse = false);
      /*!
       * \brief ComputeRotatedCentroid
       * \return
       */
      Output::ExitCodes ComputeRotatedCentroid() { if(rotatedCentroid == NULL) rotatedCentroid = new Point(0.0,0.0,0.0); return ComputeRotatedCentroid(*rotatedCentroid); }
      /*!
       * \brief ComputeRotatedCentroid
       * \param rotatedCentroid
       * \return
       */
      Output::ExitCodes ComputeRotatedCentroid(Point& rotatedCentroid) const;
      /*!
       * \brief ComputeRotatedCentroidWithInput
       * \param rotationMatrix
       * \param translationVector
       * \param inverse
       * \return
       */
      Output::ExitCodes ComputeRotatedCentroidWithInput(const Matrix3d& rotationMatrix, const Vector3d& translationVector, const bool& inverse = false);
      /*!
       * \brief RotatedCentroid
       * \return
       */
      inline const Point& RotatedCentroid() const { return *rotatedCentroid; }
      /*!
       * \brief ComputeRotatedBarycenter
       * \return
       */
      Output::ExitCodes ComputeRotatedBarycenter() { if(rotatedBarycenter == NULL) rotatedBarycenter= new Point(0.0,0.0,0.0); return ComputeRotatedCentroid(*rotatedBarycenter); }
      /*!
       * \brief ComputeRotatedBarycenter
       * \param rotatedBarycenter
       * \return
       */
      Output::ExitCodes ComputeRotatedBarycenter(Point& rotatedBarycenter) const;
      /*!
       * \brief ComputeRotatedBarycenterWithInput
       * \param rotationMatrix
       * \param translationVector
       * \param inverse
       */
      void ComputeRotatedBarycenterWithInput(const Matrix3d& rotationMatrix, const Vector3d& translationVector, const bool& inverse = false);
      /*!
       * \brief RotatedBarycenter
       * \return
       */
      inline const Point& RotatedBarycenter() const { return *rotatedBarycenter; }
      ///
      /// \brief PointInPlane
      /// \param point
      /// \param tolerance
      /// \return
      ///
      Position PointInPlane(const Point& point, const double& tolerance = TOLERANCEINTERSECTION) const;
      /*!
       * \brief PointInside
       * \param point
       * \param tolerance
       * \return
       */
      bool PointInside(const Point& point, const double& tolerance = TOLERANCEPARALLELISM) const;
      /*!
       * \brief Compute2DPolygonProperties
       * \return
       */
      Output::ExitCodes Compute2DPolygonProperties();
      /*!
       * \brief Compute3DPolygonProperties
       * \return
       */
      Output::ExitCodes Compute3DPolygonProperties();
      ///
      /// \brief Free2DPolygonProperties
      /// \return
      ///
      Output::ExitCodes Free2DPolygonProperties();
      ///
      /// \brief Free3DPolygonProperties
      /// \return
      ///
      Output::ExitCodes Free3DPolygonProperties();
      /*!
       * \brief FreeRotationMatrix
       * \return
       */
      Output::ExitCodes FreeRotationMatrix();
      /*!
       * \brief FreePlane
       * \return
       */
      Output::ExitCodes FreePlane();
      /*!
       * \brief FreeRotatedVertices
       * \return
       */
      Output::ExitCodes FreeRotatedVertices();
      /*!
       * \brief FreeRotatedCentroid
       * \return
       */
      Output::ExitCodes FreeRotatedCentroid();
      /*!
       * \brief FreeRotatedBarycenter
       * \return
       */
      Output::ExitCodes FreeRotatedBarycenter();
      /*!
       * \brief FreeSubTriangles
       * \return
       */
      Output::ExitCodes FreeSubTriangles();
      ///
      /// \brief AllignEdgesVertices
      /// \return
      ///
      virtual Output::ExitCodes AllignEdgesVertices();
      ///
      /// \brief ChangeOrientation
      /// \return
      ///
      virtual Output::ExitCodes ChangeOrientation();
      ///
      /// \brief ChangePositionVertices
      /// \param positionVertex
      /// \return
      ///
      virtual Output::ExitCodes ShiftPositionVertices(const unsigned int& positionVertex);
      ///
      /// \brief ComputePrincipalAxis
      /// \param axis
      /// \param position
      /// \return
      ///
      Output::ExitCodes ComputePrincipalAxis(Vector3d& axis, const unsigned int& position = 0);
      ///
      /// \brief ComputeInertiaTensor
      /// \param inertiaTensor
      /// \return
      ///
      Output::ExitCodes ComputeInertiaTensor(Matrix2d& inertiaTensor, const bool& boundary = false);
      Output::ExitCodes ComputeMap(IMapping*& map,
                                   const bool& computeInverse = true) const
      {

        switch (typeObj)
        {
          case Triangle:
          case Parallelogram:
          case Rectangle:
          case Square:
            if (twoDimensional)
              map = new Mapping2D();
            else
              map = new Mapping2D3D();
          break;
          default:
            map = new MappingPolygon(twoDimensional);
        }

        return map->Compute(*this, computeInverse);
      }
      ///
      /// \brief ComputeMapBoundary
      /// \param map
      /// \param computeInverse
      /// \return
      ///
      Output::ExitCodes ComputeMapBoundary(IMapping*& map,
                                           const bool& computeInverse = true) const
      {
        map = new MappingBoundaryPolygon();

        return map->Compute(*this, computeInverse);
      }
  };
}
#endif //POLYGON_HPP
