#include "Mapping2D.hpp"
#include "Point.hpp"
#include "Polygon.hpp"

namespace GeDiM
{
  void Mapping2D::ComputeJTriangle(const IGeometricObject& polygon)
  {
    j(0, 0) = (*polygon.Vertex(1)).x() - (*polygon.Vertex(0)).x();
    j(0, 1) = (*polygon.Vertex(2)).x() - (*polygon.Vertex(0)).x();
    j(1, 0) = (*polygon.Vertex(1)).y() - (*polygon.Vertex(0)).y();
    j(1, 1) = (*polygon.Vertex(2)).y() - (*polygon.Vertex(0)).y();
  }

  void Mapping2D::ComputeJParallelogram(const IGeometricObject& polygon)
  {
    j(0, 0) = (*polygon.Vertex(1)).x() - (*polygon.Vertex(0)).x();
    j(0, 1) = (*polygon.Vertex(3)).x() - (*polygon.Vertex(0)).x();
    j(1, 0) = (*polygon.Vertex(1)).y() - (*polygon.Vertex(0)).y();
    j(1, 1) = (*polygon.Vertex(3)).y() - (*polygon.Vertex(0)).y();
  }

  Output::ExitCodes Mapping2D::Compute(const IGeometricObject& polygon, const bool& computeInverse)
  {
    switch(polygon.Type())
    {
      case IGeometricObject::Triangle:
        ComputeJTriangle(polygon);
      break;
      case IGeometricObject::Parallelogram:
      case IGeometricObject::Rectangle:
      case IGeometricObject::Square:
        ComputeJParallelogram(polygon);
      break;
      default:
        Output::PrintErrorMessage("Mapping for general polygon does not exist",true);
      return Output::GenericError;
      break;
    }

    detJ = j.determinant();
    if(detJ < 0)
    {
      j.col(0).swap(j.col(1));
      detJ = -detJ;
    }

    b << polygon.Vertex(0)->x(), polygon.Vertex(0)->y();

    if(computeInverse)
      jInverse = j.inverse();

    return Output::Success;
  }
}
