#ifndef __MAPPING3D_H
#define __MAPPING3D_H

#include "MacroDefinitions.hpp"
#include "Eigen"
#include "Output.hpp"
#include <cstdlib>
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class Mapping3D : public IMapping
  {
    protected:
      Matrix3d j; ///< Gradient of the transformation F, J
      Matrix3d jInverse; ///< Gradient of the transformation F, J
      Vector3d b; ///< Constant member of the function F
      double detJ; ///< Determinant of the J

      void ComputeJTetrahedron(const IGeometricObject& polyhedron);
      void ComputeJHexahedron(const IGeometricObject& polyhedron);

    public:
      Mapping3D() {}
      ~Mapping3D() {}

      unsigned int Dimension() const { return 3; }

      Output::ExitCodes Compute(const IGeometricObject& polyhedron, const bool& computeInverse = true);

      /// Map from the reference element to the generic cell
      Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const { result = (j*x).colwise() + b; return Output::Success; }
      /// Map from the generic cell to reference element
      Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const { result = jInverse*(x.colwise() - b); return Output::Success; }

      MatrixXd J() const { return j; }
      MatrixXd JInverse() const { return jInverse; }
      VectorXd DetJ() const { return MatrixXd::Constant(1, 1, detJ); }
  };
}

#endif // __MAPPING3D_H
