#ifndef POLYHEDRON_HPP
#define POLYHEDRON_HPP

#include "Polygon.hpp"
#include "Mapping3D.hpp"
#include "MappingPolyhedron.hpp"
#include "MappingBoundaryPolyhedron.hpp"
#include "unordered_map"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  /*!
    \brief Class representing a polyhedron.
    \author Andrea Borio, Alessandro D'Auria.
    \copyright See top level LICENSE file for details.
  */
  class Polyhedron : public GeometricObject
  {
    protected:

      vector< const Segment* > edges; ///< list of the edges in the polyhedron
      vector< const Polygon* > faces; ///< list of the faces in the polyhedron

    public:
      /// Default constructor.
      /// \param _id The id of the polyhedron.
      explicit Polyhedron(const unsigned int& _id = 0);
      /// Copy constructor.
      /// \param polyhedron The polyhedron that is copied to *this.
      Polyhedron(const Polyhedron& polyhedron) : Polyhedron() { *this = polyhedron; }
      /// Move constructor.
      /// \param polyhedron The polyhedron that is moved to *this.
      Polyhedron(Polyhedron&& polyhedron) : Polyhedron() { *this = move(polyhedron); }
      Polyhedron& operator=(Polyhedron&& polyhedron);
      Polyhedron& operator=(const Polyhedron& polyhedron);
      /// Default destructor.
      virtual ~Polyhedron();
      inline GeometricObject* Clone() const { return new Polyhedron(); }
      ///
      /// \brief Initialize
      /// \param numberOfVertices
      /// \param numberOfEdges
      /// \param numberOfFaces
      ///
      inline void Initialize(const unsigned int& numberOfVertices, const unsigned int& numberOfEdges, const unsigned int& numberOfFaces)
      { InitializeVertices(numberOfVertices); InitializeEdges(numberOfEdges); InitializeFaces(numberOfFaces); }
      ///
      /// \brief Allocate
      /// \param numberOfVertices
      /// \param numberOfEdges
      /// \param numberOfFaces
      ///
      inline void Allocate(const unsigned int& numberOfVertices, const unsigned int& numberOfEdges, const unsigned int& numberOfFaces)
      { AllocateVertices(numberOfVertices); AllocateEdges(numberOfEdges); AllocateFaces(numberOfFaces); }
      inline void InitializeEdges(const unsigned int& numberOfEdges) { edges.reserve(numberOfEdges); }
      ///
      /// \brief AllocateEdges
      /// \param numberOfEdges
      ///
      inline void AllocateEdges(const unsigned int& numberOfEdges) { edges.resize(numberOfEdges); }
      ///
      /// \brief AddEdge
      /// \param edge
      ///
      void AddEdge(const Segment& edge) { edges.push_back(&edge); }
      ///
      /// \brief InsertEdge
      /// \param edge
      /// \param position
      ///
      void InsertEdge(const Segment& edge, const unsigned int& position) { edges[position] = &edge; }
      ///
      /// \brief AddEdges
      /// \param _edges
      ///
      inline void AddEdges(const vector< const Segment* >& _edges) {edges = _edges; }
      /// @return The number of segments of the polyhedron
      inline unsigned int NumberOfEdges() const  { return edges.size(); }
      /// @return The segment at position p of the polygon
      inline const Segment* Edge(const unsigned int& position) const  { return edges[position]; }
      /// @return The list of segments of the polygon
      const vector<const Segment*>& Edges() const { return edges; }
      ///
      /// \brief CreateEdge
      /// \param firstPoint
      /// \param secondPoint
      /// \param id
      /// \return
      ///
      Segment CreateEdge(const unsigned int& firstPoint, const unsigned int& secondPoint, const unsigned int& id = 0);
      /// Add a new faces in the polyhedron
      inline void InitializeFaces(const unsigned int& numberOfFaces) { faces.reserve(numberOfFaces); }
      ///
      /// \brief AllocateFaces
      /// \param numberOfFaces
      ///
      inline void AllocateFaces(const unsigned int& numberOfFaces) { faces.resize(numberOfFaces); }
      ///
      /// \brief AddFace
      /// \param face
      ///
      void AddFace(const Polygon& face) { faces.push_back(&face); }
      ///
      /// \brief InsertFace
      /// \param face
      /// \param position
      ///
      void InsertFace(const Polygon& face, const unsigned int& position) { faces[position] = &face; }
      ///
      /// \brief AddFaces
      /// \param _faces
      ///
      inline void AddFaces(const vector< const Polygon* >& _faces) {faces = _faces; }
      /// @return The number of faces of the polyhedron
      inline unsigned int NumberOfFaces() const  { return faces.size(); }
      /// @return The face at position p of the polyhedron
      inline const Polygon* Face(const unsigned int& position) const  { return faces[position]; }
      /// @return The list of faces of the polyhedron
      const vector<const Polygon*>& Faces() const { return faces;}
      ///
      /// \brief CreateFace
      /// \param vectorPoints
      /// \param vectorEdges
      /// \param id
      /// \return
      ///
      Polygon CreateFace(const vector<unsigned int>& vectorPoints, const vector<unsigned int>& vectorEdges , const unsigned int& id = 0);
      ///
      /// \brief Dimension
      /// \return
      ///
      inline unsigned int Dimension() const { return 3; }
      ///Compute Centroid
      /// \brief Compute the centroid of the polygon and save inside the object
      inline Output::ExitCodes ComputeCentroid() { if(centroid == NULL) centroid = new Point(); return ComputeCentroid(*centroid); }
      ///
      /// \brief ComputeCentroid
      /// \param _centroid
      /// \return
      ///
      Output::ExitCodes ComputeCentroid(Point& _centroid) const ;
      ///Compute Measure
      /// \brief Compute the length of the edge and save inside the object
      inline Output::ExitCodes ComputeMeasure() { return ComputeMeasure(measure); }
      ///
      /// \brief ComputeMeasure
      /// \param _measure
      /// \return
      ///
      Output::ExitCodes ComputeMeasure(double& _measure) const;
      ///Compute radius
      /// \brief Compute the radius of the edge and save inside the object
      inline Output::ExitCodes ComputeSquaredRadius() { return ComputeSquaredRadius(squaredRadius); }
      ///
      /// \brief ComputeSquaredRadius
      /// \param _squaredRadius
      /// \return
      ///
      Output::ExitCodes ComputeSquaredRadius(double& _squaredRadius) const;
      ///
      /// \brief ComputeAspectRatio
      /// \return
      ///
      inline Output::ExitCodes ComputeAspectRatio() { return ComputeAspectRatio(aspectRatio);}
      ///
      /// \brief ComputeAspectRatio
      /// \param _aspectRatio
      /// \return
      ///
      Output::ExitCodes ComputeAspectRatio(double& _aspectRatio) const;
      ///
      /// \brief ComputePrincipalAxis
      /// \param axis
      /// \param position
      /// \return
      ///
      Output::ExitCodes ComputePrincipalAxis(Vector3d& axis, const unsigned int& position = 0);
      ///
      /// \brief ComputeInertiaTensor
      /// \param inertiaTensor
      /// \return
      ///
      Output::ExitCodes ComputeInertiaTensor(Matrix3d& inertiaTensor, const bool& boundary = false);
      Output::ExitCodes ComputeMap(IMapping*& map,
                                   const bool& computeInverse = true) const
      {
        switch (typeObj)
        {
          case Tetrahedron:
          case Hexahedron:
          case Cube:
            map = new Mapping3D();
          break;
          default:
            map = new MappingPolyhedron();
        }

        return map->Compute(*this, computeInverse);
      }
      ///
      /// \brief ComputeMapBoundary
      /// \param map
      /// \param computeInverse
      /// \return
      ///
      Output::ExitCodes ComputeMapBoundary(IMapping*& map,
                                   const bool& computeInverse = true) const
      {
        map = new MappingBoundaryPolyhedron();

        return map->Compute(*this, computeInverse);
      }
  };
}
#endif //POLYHEDRON_HPP
