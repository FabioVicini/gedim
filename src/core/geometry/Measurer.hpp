#ifndef MEASURER_HPP
#define MEASURER_HPP

#include "Output.hpp"
#include "Eigen"

using namespace std;
using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
  class Measurer;
  class Measurer
  {
    public:
      enum TypeMeasurer
      {
        PointToPoint = 0,
        PointToLine = 1,
      };

    private:

      double coordinateProjectionOnLine;
      double distanceSquared;

      TypeMeasurer typeMeasurer;

    public:
      Measurer(const TypeMeasurer type);
      ~Measurer();

      Output::ExitCodes ComputeSquaredDistancePoints(const Vector3d& firstPoint,
                                                     const Vector3d secondPoint);
      Output::ExitCodes ComputeDistancePointLineAndProj(const Vector3d& point,
                                                        const Vector3d& startPointLine,
                                                        const Vector3d& tangentVector,
                                                        const Vector3d& normalVector);

      double DistanceSquared() { return distanceSquared; }
      double CoordinateProjectionOnLine() { return coordinateProjectionOnLine; }
  };

}

#endif // MEASURER_HPP
