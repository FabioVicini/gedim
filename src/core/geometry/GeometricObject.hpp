#ifndef GEOMETRICOBJECT_HPP
#define GEOMETRICOBJECT_HPP

#include <vector>
#include "Output.hpp"
#include "IGeometricObject.hpp"
#include "Point.hpp"
#include "unordered_map"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  class GeometricObject : public IGeometricObject
  {
    protected:
      GeometricType typeObj;
      unsigned int id;
      unsigned int globalId;

      double measure; ///< Measure of the geometric object.
      double diameter; ///< Diameter of the geometric object (max distance between vertices).
      double squaredRadius; ///< Radius of the geometric object (max distance between centroid and vertices).
      double aspectRatio; ///Aspect ratio of the geometric object.

      Point* centroid; ///< Centroid of the geometric object.
      Point* barycenter; ///< Barycenter of the geometric object.

      unordered_map<unsigned int, unsigned int> idPositionPoint; ///< map of the id position of the point
      vector< const Point* > vertices; ///< Array of geometric object vertices
    public:
      /*!
      \brief Constructor specifying the id of the object.
      \details Also \ref globalId is set to the given value.
      \param _id The value to which \ref id and \ref globalId are set. Defaults to 0.
    */
      explicit GeometricObject(const unsigned int& _id = 0, const GeometricType& _typeObj = GeometricType::Unknown)
      {
        typeObj = _typeObj;
        id = _id;
        globalId = _id;
        measure = -1.0;
        diameter = -1.0;
        squaredRadius = -1.0;
        aspectRatio = -1.0;
        centroid = nullptr;
        barycenter = nullptr;
      }

      /*!
      \brief Class destructor.
      \details Deletes centroid and barycenter.
    */
      virtual ~GeometricObject() { FreeCentroid(); FreeBarycenter(); }

      GeometricObject& operator=(const GeometricObject& object)
      {
        if(object.centroid != NULL)
          centroid = new Point(*object.centroid);
        else
          centroid = nullptr;
        if(object.barycenter != NULL)
          barycenter = new Point(*object.barycenter);
        else
          barycenter = nullptr;
        measure = object.measure;
        diameter = object.diameter;
        squaredRadius = object.squaredRadius;
        aspectRatio = object.aspectRatio;
        id = object.id;
        globalId = object.globalId;
        typeObj = object.typeObj;
        return *this;
      }

      GeometricObject& operator=(GeometricObject&& object)
      {
        if(this != &object)
        {
          delete barycenter;
          delete centroid;
          centroid = object.centroid;
          barycenter = object.barycenter;
          measure = object.measure;
          diameter = object.diameter;
          squaredRadius = object.squaredRadius;
          aspectRatio = object.aspectRatio;
          id = object.id;
          globalId = object.globalId;
          typeObj = object.typeObj;
          object.centroid = nullptr;
          object.barycenter = nullptr;
          object.id = 0;
          object.globalId = 0;
        }
        return *this;
      }

      inline void SetGlobalId(const unsigned int& _globalId) override { globalId = _globalId; }
      inline void SetId(const unsigned int& _id) override { id = _id; }

      inline const unsigned int& GlobalId() const override { return globalId; }
      inline const unsigned int& Id() const override { return id; }

      inline void SetType(const GeometricType& type) override { typeObj = type; }
      inline GeometricType Type() const override { return typeObj; }

      inline double Measure() const override { return measure; }
      inline double Diameter() const override { return diameter; }
      inline const Point* Centroid() const override { return centroid; }
      inline double SquaredRadius() const override { return squaredRadius; }
      inline const Point* Barycenter() const override { return barycenter; }
      inline double AspectRatio() const override { return aspectRatio; }
      /*!
     * \brief InitializeVertices
     * \param numberOfVertices
     */
      inline void InitializeVertices(const unsigned int& numberOfVertices) { vertices.reserve(numberOfVertices); }
      /*!
     * \brief AllocateVertices
     * \param numberOfVertices
     */
      inline void AllocateVertices(const unsigned int& numberOfVertices) { vertices.resize(numberOfVertices); }
      /*!
     * \brief AddVertex
     * \param vertex
     */
      inline void AddVertex(const Point& vertex) { vertices.push_back(&vertex); }
      /*!
     * \brief AddVertices
     * \param _vertices
     */
      inline void AddVertices(const vector<const Point*>& _vertices) { vertices = _vertices; }
      /*!
     * \brief InsertVertex
     * \param vertex
     * \param position
     */
      inline void InsertVertex(const Point& vertex, const unsigned int& position) { vertices[position] = &vertex; }
      /*!
     * \brief NumberOfVertices
     * \return
     */
      inline unsigned int NumberOfVertices() const { return vertices.size(); }
      /*!
     * \brief Vertex
     * \param position
     * \return
     */
      inline const Point* Vertex(const unsigned int& position) const { return vertices[position]; }
      /*!
     * \brief Vertices
     * \return
     */
      inline const vector<const Point*>& Vertices() const { return vertices; }
      inline vector<const Point*>& Vertices() { return vertices; }
      ///
      /// \brief ComputeBarycenter
      /// \return
      ///
      inline Output::ExitCodes ComputeBarycenter() { if(barycenter == NULL) barycenter = new Point(); return ComputeBarycenter(*barycenter); }
      ///
      /// \brief ComputeBarycenter
      /// \param _barycenter
      /// \return
      ///
      Output::ExitCodes ComputeBarycenter(Point& _barycenter) const;
      ///
      /// \brief ComputeDiameter
      /// \return
      ///
      inline Output::ExitCodes ComputeDiameter() override { return ComputeDiameter(diameter); }
      ///
      /// \brief ComputeDiameter
      /// \param _diameter
      /// \return
      ///
      Output::ExitCodes ComputeDiameter(double& _diameter) const override;
      ///
      /// \brief PositionPoint
      /// \param point
      /// \return
      ///
      unsigned int PositionPoint(const Point& point) const { return (idPositionPoint.size() != 0)? idPositionPoint.at(point.Id()) : point.Id(); }
      ///
      /// \brief ComputePositionPoint
      /// \return
      ///
      Output::ExitCodes ComputePositionPoint();

      Output::ExitCodes FreeCentroid();
      Output::ExitCodes FreeBarycenter();
      friend ostream& operator<<(ostream& out, const GeometricObject& obj);
  };
}

#endif // GENERICOBJECT_HPP
