#ifndef __MAPPINGPOLYHEDRON_H
#define __MAPPINGPOLYHEDRON_H

#include "Output.hpp"
#include "IGeometricObject.hpp"
#include "IMapping.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MappingPolyhedron : public IMapping
  {
    protected:

      MatrixXd j; ///< Gradient of the transformation F, J
      MatrixXd jInverse; ///< Gradient of the transformation F, J
      VectorXd b; ///< Constant member of the function F
      VectorXd detJ; ///< Determinant of the J
      unsigned int numberOfSubTetrahedra; ///< Number of SubTetrahedra in the Polyhedron

    public:
      MappingPolyhedron() {}
      ~MappingPolyhedron() {}

      unsigned int Dimension() const { return 3; }

      Output::ExitCodes Compute(const IGeometricObject& polyhedron, const bool& computeInverse = true);

      /// Map from the reference element to the generic cell
      Output::ExitCodes F(const MatrixXd& x, MatrixXd& result) const;
      /// Map from the generic cell to reference element
      Output::ExitCodes FInverse(const MatrixXd& x, MatrixXd& result) const;

      MatrixXd J() const { return j; }
      MatrixXd JInverse() const { return jInverse; }
      VectorXd DetJ() const { return detJ; }
      unsigned int NumberOfSubTetrahedra() const { return numberOfSubTetrahedra; }
  };


}

#endif // __MAPPING_H
