#include "Segment.hpp"

namespace GeDiM
{

  // ***************************************************************************
  Segment::Segment(const unsigned int& _id) : GeometricObject(_id, GeometricType::SegmentObj)
  {
    tangent = nullptr;
    normal = nullptr;
    vertices.reserve(2);
    aspectRatio = 1.0;
    typeObj = SegmentObj;
  }

  Segment& Segment::operator=(Segment&& segment)
  {
    if(this != &segment)
    {
      this->GeometricObject::operator =(move(segment));

      delete normal;
      delete tangent;
      vertices.clear();

      unsigned int numVertices = segment.vertices.size();
      vertices.resize(numVertices);
      for (unsigned int v = 0; v < numVertices; v++)
        vertices[v] = segment.vertices[v];

      tangent = segment.tangent;
      normal = segment.normal;
      segment.tangent = nullptr;
      segment.normal = nullptr;
      segment.vertices.clear();
    }
    return *this;
  }

  Segment& Segment::operator=(const Segment& segment)
  {
    this->GeometricObject::operator =(segment);
    delete normal;
    delete tangent;
    vertices.clear();

    unsigned int numVertices = segment.vertices.size();
    vertices.resize(numVertices);
    for (unsigned int v = 0; v < numVertices; v++)
      vertices[v] = segment.vertices[v];

    if (segment.tangent != nullptr)
      tangent = new Vector3d(*segment.tangent);
    if (segment.normal != nullptr)
      normal = new Vector3d(*segment.normal);
    return *this;
  }

  Segment::~Segment()
  {
    if(tangent != NULL)
      delete tangent;

    if(normal != NULL)
      delete normal;

    vertices.clear();
  }
  // ***************************************************************************
  Output::ExitCodes Segment::ComputeMeasure(double& _measure) const
  {
    _measure = (*vertices[1] - *vertices[0]).norm();
    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Segment::ComputeTangent(Vector3d& _tangent, const bool& normalize) const
  {
    _tangent = *vertices[1] - *vertices[0];
    if(normalize)
      _tangent.normalize();
    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes Segment::ComputeNormal(Vector3d& _normal, const bool& normalize) const
  {
    _normal.setZero();
    _normal = *vertices[1] - *vertices[0];
    if(normalize)
      _normal.normalize();
    double tmp = _normal[0];
    _normal[0] = -_normal[1];
    _normal[1] = tmp;

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Segment::ComputeSquaredRadius(double& _squaredRadius) const
  {
    _squaredRadius = (*vertices[1] - *vertices[0]).squaredNorm() * 0.25;
    return Output::Success;
  }

  // ***************************************************************************
  Segment::Position Segment::PointPosition(const Point& point, const double& tollerance) const
  {
    Vector3d tangentVectorEdge;
    if(tangent == NULL)
      tangentVectorEdge = End() - Origin();
    else
      tangentVectorEdge = *tangent * measure;

    Vector3d tangentVectorDiff = point - Origin();
    double crossProd = tangentVectorEdge.x() * tangentVectorDiff.y() - tangentVectorDiff.x() * tangentVectorEdge.y();

    double coordinateCurvilinear = (tangentVectorEdge.dot(tangentVectorDiff)) / tangentVectorEdge.squaredNorm();

    if(crossProd > tollerance)
      return Position::AtTheLeft;
    if(crossProd < -tollerance)
      return Position::AtTheRight;
    if(coordinateCurvilinear < -tollerance)
      return Position::Behind;
    if(coordinateCurvilinear > 1.0 + tollerance)
      return Position::Beyond;
    if(abs(coordinateCurvilinear) < tollerance)
      return Position::AtTheOrigin;
    if(abs(coordinateCurvilinear) > 1.0 - tollerance)
      return Position::AtTheEnd;
    return Position::Between;
  }
}
