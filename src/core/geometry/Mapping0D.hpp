#ifndef __MAPPING0D_H
#define __MAPPING0D_H

#include "IMapping.hpp"

namespace GeDiM
{
  typedef Matrix<double, 1, 1> Matrix1d;

  /// \brief Class for mapping 0D objects
	/// \copyright See top level LICENSE file for details.
  class Mapping0D final : public IMapping
	{
		public:
      Mapping0D() {}
      ~Mapping0D() {}

      /// \return The dimension of the map
      virtual unsigned int Dimension() const { return 0; }

			/// \brief Gradient of the transformation F
      virtual MatrixXd J() const { return Matrix1d::Ones(); }
			/// \brief Gradient of the inverse transformation F^-1
      virtual MatrixXd JInverse() const { return Matrix1d::Ones(); }
      /// \brief Determinant of the gradient of the transfromation F
      virtual VectorXd DetJ() const { return Matrix1d::Ones(); }

			/// \brief Compute of the Map with the original geometry
			/// \param geometry The geometry of the map
			/// \return The result of the initialization
      virtual Output::ExitCodes Compute(const IGeometricObject&,
                                        const bool& = true) { return Output::Success; }
			/// \brief Map from the geometry to reference element
      /// \param x The points of the geometry to be mapped. Size is Dimension x NumberPoints
      /// \param result The points of the reference element mapped. Size is Dimension x NumberPoints
			/// \return The result of the transformation
      virtual Output::ExitCodes FInverse(const MatrixXd& x,
                                         MatrixXd& result) const { result = x; return Output::Success; }
			/// \brief Map from the reference element to geometry
      /// \param x The points of the reference element to be mapped. Size is Dimension x NumberPoints
      /// \param result The points of the geometry mapped. Size is Dimension x NumberPoints
			/// \return The result of the transformation
      virtual Output::ExitCodes F(const MatrixXd& x,
                                  MatrixXd& result) const { result = x; return Output::Success; }
	};
}

#endif // __MAPPING0D_H
