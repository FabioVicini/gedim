#ifndef LEASTSQUAREPLANE_HPP
#define LEASTSQUAREPLANE_HPP
#include "Output.hpp"
#include "Eigen"

using namespace std;
using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
  class LeastSquarePlane;

  class LeastSquarePlane
  {
    public:
      ///
      /// \brief ComputeLeastSquarePlane
      /// \param vertices
      /// \param normal
      /// \param planeTranslation
      ///
      static void ComputeLeastSquarePlane(const vector<Vector3d>& vertices,
                              Vector3d& normal,
                              double& planeTranslation);
  };

}

#endif // LEASTSQUAREPLANE_HPP
