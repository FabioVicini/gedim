#include "MappingPolyhedron.hpp"
#include "Polyhedron.hpp"

namespace GeDiM
{
  Output::ExitCodes MappingPolyhedron::Compute(const IGeometricObject& polyhedron, const bool& computeInverse)
  {
    Point centroid;
    centroid.setZero();
    const Point* centroidConst = &centroid;
    if(polyhedron.Centroid() == NULL)
      polyhedron.ComputeCentroid(centroid);
    else
      centroidConst = polyhedron.Centroid();

    unsigned int numberOfFaces = polyhedron.NumberOfFaces();
    numberOfSubTetrahedra = 0;

    vector<Point> centroidFaces(numberOfFaces);
    for(unsigned int numFac = 0; numFac < numberOfFaces; numFac++)
    {
      const Polygon& face = *polyhedron.Face(numFac);
      if(face.Centroid() == NULL)
        face.ComputeCentroid(centroidFaces[numFac]);
      else
        centroidFaces[numFac] = *face.Centroid();

      numberOfSubTetrahedra += face.NumberOfEdges();
    }

    j.resize(3*numberOfSubTetrahedra, 3);
    if(computeInverse)
      jInverse.resize(3, 3*numberOfSubTetrahedra);

    detJ.resize(numberOfSubTetrahedra);
    detJ.setZero();
    b.resize(3*numberOfSubTetrahedra);

    Matrix3d tempMatrix;
    unsigned int counterTetra = 0;
    for(unsigned int numFac = 0; numFac < numberOfFaces; numFac++)
    {
      const Polygon& face = *polyhedron.Face(numFac);
      for(unsigned int numEdg = 0; numEdg < face.NumberOfEdges(); numEdg++)
      {
        unsigned int numEdgNext = (numEdg+1)%face.NumberOfEdges();

        tempMatrix.col(0) = *face.Vertex(numEdgNext) - *face.Vertex(numEdg);
        tempMatrix.col(1) = centroidFaces[numFac] - *face.Vertex(numEdg);
        tempMatrix.col(2) = *centroidConst - *face.Vertex(numEdg);

        detJ(counterTetra) = tempMatrix.determinant();
        if(detJ(counterTetra)  < 0)
        {
          tempMatrix.col(0).swap(tempMatrix.col(1));
          detJ(counterTetra) = -detJ(counterTetra);
        }

        j.block<3,3>(counterTetra*3, 0) = tempMatrix;
        b.segment<3>(counterTetra*3) = *face.Vertex(numEdg);

        if(computeInverse)
          jInverse.block<3,3>(0,counterTetra*3) = tempMatrix.inverse();

        counterTetra++;
        tempMatrix.setZero();
      }
    }

    return Output::Success;
  }

  Output::ExitCodes MappingPolyhedron::F(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    result.resize(3, numCols * numberOfSubTetrahedra);

    MatrixXd tempResult = (j*x).colwise() + b;
    for(unsigned int numTetra = 0; numTetra < numberOfSubTetrahedra; numTetra++)
    {
      result.block(0, numCols*numTetra, 3, numCols) = tempResult.block(numTetra*3, 0, 3, numCols);
    }
    return Output::Success;
  }

  Output::ExitCodes MappingPolyhedron::FInverse(const MatrixXd& x, MatrixXd& result) const
  {
    unsigned int numCols = x.cols();
    unsigned int newNumCols = numCols/numberOfSubTetrahedra;
    MatrixXd reshapeX(3*numberOfSubTetrahedra, newNumCols);
    for(unsigned int numTetra = 0; numTetra < numberOfSubTetrahedra; numTetra++)
    {
      reshapeX.block(numTetra*3, 0, 3, newNumCols) = x.block(0, newNumCols*numTetra, 3, newNumCols);
    }
    result.setZero();
    reshapeX.colwise() -= b;
    result = jInverse.block(0, 0, 3, 3)*(reshapeX.block(0, 0, 3, newNumCols));
    return Output::Success;
  }

  //============================================================================

}
