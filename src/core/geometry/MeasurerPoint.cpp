#include "MeasurerPoint.hpp"
#include "Projectors.hpp"

namespace GeDiM
{
  double MeasurerPoint::DistancePointLine(const Vector3d& point, const Vector3d& pointLine, const Vector3d& tangentLine)
  {
    Vector3d normal(-tangentLine(1), tangentLine(0), 0.0);
    const Vector3d tangentVectorDifference = point - pointLine;
    double norm = normal.norm();
    return tangentVectorDifference.dot(normal) / norm;
  }

  double MeasurerPoint::DistancePointLine3D(const Vector3d& point, const Vector3d& pointLine, const Vector3d& tangentLine, double& coordinateCurvilinear, Vector3d& projectedPoint)
  {
    const Vector3d tangentVectorDifference = point - pointLine;
    Projectors::OrthogonalProjectionPointLine(point, pointLine, tangentLine, coordinateCurvilinear, projectedPoint);
    Vector3d normal = point-projectedPoint;
    double norm = normal.norm();
    return tangentVectorDifference.dot(normal) / norm;
  }
}
