#ifndef MEASURERPOINT_HPP
#define MEASURERPOINT_HPP
#include "Output.hpp"
#include "Eigen"

using namespace std;
using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
  class MeasurerPoint;

  class MeasurerPoint
  {
    public:
      ///
      /// \brief SquaredDistancePoints
      /// \param firstPoint
      /// \param secondPoint
      /// \return
      ///
      static inline double SquaredDistancePoints(const Vector3d& firstPoint,
                                                 const Vector3d& secondPoint)
      {return (firstPoint - secondPoint).squaredNorm(); }
      ///
      /// \brief DistancePointLine
      /// \param point
      /// \param pointLine
      /// \param tangentLine
      /// \return
      ///
      static double DistancePointLine(const Vector3d& point,
                                      const Vector3d& pointLine,
                                      const Vector3d& tangentLine);
      ///
      /// \brief DistancePointLine3D
      /// \param point
      /// \param pointLine
      /// \param tangentLine
      /// \param coordinateCurvilinear
      /// \param projectedPoint
      /// \return
      ///
      static double DistancePointLine3D(const Vector3d& point,
                                      const Vector3d& pointLine,
                                      const Vector3d& tangentLine, double& coordinateCurvilinear, Vector3d& projectedPoint);
      ///
      /// \brief DistancePointLineNormal
      /// \param point
      /// \param pointLine
      /// \param normalLine
      /// \return
      ///
      static double DistancePointLineNormal(const Vector3d& point,
                                            const Vector3d& pointLine,
                                            const Vector3d& normalizedNormalLine)
      { return (point - pointLine).dot(normalizedNormalLine); }

      ///
      /// \brief DistancePointPlane
      /// \param point
      /// \param planeNormal
      /// \param planeTranslation
      /// \return
      ///
      static double DistancePointPlane(const Vector3d& point,
                                       const Vector3d& normalizedPlaneNormal,
                                       const double& planeTranslation)
      { return (normalizedPlaneNormal.dot(point) - planeTranslation); }

  };

}

#endif // MEASURER_HPP
