#ifndef __IFEMVALUES_H
#define __IFEMVALUES_H

#include "IGeometricObject.hpp"
#include "ITreeNode.hpp"
#include "IMapping.hpp"
#include "IFemReferenceElement.hpp"
#include "IQuadrature.hpp"
#include "Output.hpp"
#include "Eigen"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Interface used to FEM Values computation
  /// \author Andrea Borio, Fabio Vicini.
  /// \copyright See top level LICENSE file for details.
  class IFemValues
  {
    public:
      virtual ~IFemValues() { }

      /// \return The number of points of the internal quadrature formula
      virtual unsigned int NumberInternalQuadraturePoints() const = 0;
      /// \return The number of basis fucntions
      virtual unsigned int NumberBasisFunctions() const = 0;
      /// \return The geometric dimension
      virtual unsigned int Dimension() const = 0;

      /// \brief Initialize the values
      /// \param referenceElement the FEM basis chosen to take the order and the basis function dimension
      /// \param internalQuadrature the quadrature on the cell
      /// \param boundaryQuadrature the quadrature on the boundaries of cell
      virtual Output::ExitCodes Initialize(const IFemReferenceElement& referenceElement,
                                           const IQuadrature& internalQuadrature) = 0;

      /// \brief Compute the quadrature weights on the geometry
      /// \param referenceElementMap The reference element cell map
      /// \param quadratureWeights The mapped quadrature weights
      /// \note quadratureWeights dimension is NumQuadraturePoints
      /// \return The result of the computation
      virtual Output::ExitCodes ComputeInternalQuadratureWeights(const IMapping& referenceElementMap,
                                                                 VectorXd& quadratureWeights) const = 0;

      /// \brief Compute the quadrature points on the geometry
      /// \param referenceElementMap The reference element cell map
      /// \param quadraturePoints The mapped quadrature points
      /// \note quadraturePoints dimension is Dimension x NumQuadraturePoints
      /// \return The result of the computation
      virtual Output::ExitCodes ComputeInternalQuadraturePoints(const IMapping& referenceElementMap,
                                                                MatrixXd& quadraturePoints) const = 0;

      /// \brief Compute the basis function values on internal quadrature points on the geometry
      /// \param basisFunctionValues The basis function evaluated on the cell
      /// \note basisFunctionValues dimension is NumQuadraturePoints x NumCellDofs
      /// \return The result of the computation
      virtual Output::ExitCodes ComputeBasisFunctionValues(MatrixXd& basisFunctionValues) const = 0;
      /// \brief Compute the basis function derivatives values on internal quadrature points on the geometry
      /// \param basisFunctionsDerivativeValues The basis function derivatives evaluated on the cell
      /// \note Dimension of the vector is Dimension of the geometry
      /// \note Dimension of each element of the vector NumQuadraturePoints x NumCellDofs
      /// \return The result of the computation
      virtual Output::ExitCodes ComputeBasisFunctionDerivativeValues(const IMapping& referenceElementMap,
                                                                     vector<MatrixXd>& basisFunctionsDerivativeValues) const = 0;


      /// \brief Compute the basis function values on generic points
      /// \param referenceElementMap The reference element cell map
      /// \param points The points on which evaluate. Size is geometry Dimension x NumPoints
      /// \param basisFunctionValues The basis function evaluated on the cell
      /// \note basisFunctionValues dimension is NumPoints x NumCellDofs
      /// \return The result of the computation
      virtual Output::ExitCodes ComputeBasisFunctionValues(const IMapping& referenceElementMap,
                                                           const MatrixXd& points,
                                                           MatrixXd& basisFunctionValues) const = 0;

      /// \brief Compute the basis function derivatives values on generic points
      /// \param points The points on which evaluate. Size is geometry Dimension x NumPoints
      /// \param basisFunctionsDerivativeValues The basis function derivatives evaluated on the cell
      /// \note Dimension of the vector is Dimension of the geometry
      /// \note Dimension of each element of the vector NumPoints x NumCellDofs
      /// \return The result of the computation
      virtual Output::ExitCodes ComputeBasisFunctionDerivativeValues(const IMapping& referenceElementMap,
                                                                     const MatrixXd& points,
                                                                     vector<MatrixXd>& basisFunctionsDerivativeValues) const = 0;

  };
}

#endif // __IFEMVALUES_H
