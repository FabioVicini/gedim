#include "FemEllipticEquation.hpp"
#include "Mapping1D2D.hpp"

namespace GeDiM
{
  // ***************************************************************************
  FemEllipticEquation::FemEllipticEquation(const IFemValues& femValues,
                                           const IDofHandler& dofHandler,
                                           IMapping& referenceElementMap)
  {
    _femValues = &femValues;
    _dofHandler = &dofHandler;
    _referenceElementMap = &referenceElementMap;

    unsigned int dimension = femValues.Dimension();

    _diffusionTerm.resize(dimension);
    for (unsigned int i = 0; i < dimension; i++)
      _diffusionTerm[i].resize(dimension, nullptr);

    _transportTerm.resize(dimension, nullptr);

    _reactionTerm = nullptr;
    _forcingTerm = nullptr;
    _dirichletBoundaryConditions = nullptr;
    _neumannBoundaryConditions = nullptr;
  }
  FemEllipticEquation::~FemEllipticEquation()
  {
    _femValues = nullptr;
    _dofHandler = nullptr;

    for (unsigned int i = 0; i < _diffusionTerm.size(); i++)
      _diffusionTerm[i].clear();
    _diffusionTerm.clear();

    _transportTerm.clear();
    _reactionTerm = nullptr;
    _forcingTerm = nullptr;
    _dirichletBoundaryConditions = nullptr;
    _neumannBoundaryConditions = nullptr;
  }
  // ***************************************************************************
  Output::ExitCodes FemEllipticEquation::BuildLocalSystem(const IMesh& mesh,
                                                          const unsigned int& cellPostionId,
                                                          MatrixXd& cellMatrix,
                                                          VectorXd& cellRightHandSide,
                                                          VectorXd& cellDirichletTermValues) const
  {
    /// <ul>

    if (_femValues == nullptr || _dofHandler == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return  Output::GenericError;
    }

    const IGeometricObject& cellGeometry = *mesh.GetMaximalGeometricObject(cellPostionId);

    const IFemValues& femValues = *_femValues;
    const IDofHandler& dofHandler = *_dofHandler;

    /// <li> Check of input parameters
    unsigned int numReferenceElementDofs = femValues.NumberBasisFunctions();
    unsigned int dimension = femValues.Dimension();
    unsigned int solutionDimension = dofHandler.SolutionDimension();
    const unsigned int numMaxDofsPerCell = dofHandler.NumMaximalCellDofs(cellPostionId);

    if (dimension != cellGeometry.Dimension())
    {
      Output::PrintErrorMessage("%s: Values geometric dimension %d differs from cell geometric dimension %d", true, __func__, dimension, cellGeometry.Dimension());
      return Output::GenericError;
    }

    if (numReferenceElementDofs * solutionDimension != numMaxDofsPerCell)
    {
      Output::PrintErrorMessage("%s: Values numMaxDofsPerCell %d differs from numReferenceElementDofs * solutionDimension %d * %d = %d", true, __func__, numMaxDofsPerCell, numReferenceElementDofs, solutionDimension, numReferenceElementDofs * solutionDimension);
      return Output::GenericError;
    }

    IMapping& referenceElementMap = *_referenceElementMap;
    Output::ExitCodes result = referenceElementMap.Compute(cellGeometry, true);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: cell %d map not computed", true, __func__, cellGeometry.Id());
      return result;
    }

    VectorXd quadratureWeights;
    MatrixXd quadraturePoints;
    MatrixXd basisFunctionValues;
    vector<MatrixXd> basisFunctionDerivativeValues;

    /// <li> Compute Mapped Quadrature Weights
    result = femValues.ComputeInternalQuadratureWeights(referenceElementMap,
                                                        quadratureWeights);
    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: ComputeQuadratureWeights failed", false, __func__);
      return result;
    }

    /// <li> Compute Mapped Quadrature Points
    result = femValues.ComputeInternalQuadraturePoints(referenceElementMap,
                                                       quadraturePoints);
    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: ComputeQuadraturePoints failed", false, __func__);
      return result;
    }

    /// <li> Compute Mapped Basis Function Values
    result = femValues.ComputeBasisFunctionValues(basisFunctionValues);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: ComputeBasisFunctionValues failed", false, __func__);
      return result;
    }

    /// <li> Compute Mapped Basis Function Derivatives
    result = femValues.ComputeBasisFunctionDerivativeValues(referenceElementMap,
                                                            basisFunctionDerivativeValues);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: ComputeBasisFunctionDerivativeValues failed", false, __func__);
      return result;
    }

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: ComputeDofPoints failed", false, __func__);
      return result;
    }

    /// <li> Create single solution cellMatrix and cellRightHandSide
    MatrixXd cellMatrixSingleSolution;
    VectorXd cellRightHandSideSingleSolution;

    cellMatrixSingleSolution.setZero(numReferenceElementDofs, numReferenceElementDofs);
    cellRightHandSideSingleSolution.setZero(numReferenceElementDofs);

    /// <li> Compute stiffness matrix
    if (!_diffusionTerm.empty())
    {
      if (_diffusionTerm.size() == dimension)
      {
        for (unsigned int i = 0; i < dimension; i++)
        {
          if (_diffusionTerm[i].size() != dimension)
          {
            Output::PrintErrorMessage("%s: computing stiffness matrix. Diffusion term is not valid",	true, __func__);
            return Output::GenericError;
          }

          if (_diffusionTerm[i][i] != nullptr)
          {
            const IPhysicalParameter& diffusionTermComponentII = *_diffusionTerm[i][i];

            VectorXd diffusionTermValuesII;
            diffusionTermComponentII.Eval(quadraturePoints, diffusionTermValuesII);

            cellMatrixSingleSolution += basisFunctionDerivativeValues[i].transpose() * quadratureWeights.cwiseProduct(diffusionTermValuesII).asDiagonal() * basisFunctionDerivativeValues[i];
          }

          for(unsigned int j = 0; j < i; j++)
          {
            if (_diffusionTerm[i][j] != nullptr)
            {
              const IPhysicalParameter& diffusionTermComponentIJ = *_diffusionTerm[i][j];

              VectorXd diffusionTermValuesIJ;
              diffusionTermComponentIJ.Eval(quadraturePoints, diffusionTermValuesIJ);

              cellMatrixSingleSolution += basisFunctionDerivativeValues[i].transpose() * quadratureWeights.cwiseProduct(diffusionTermValuesIJ).asDiagonal() * basisFunctionDerivativeValues[j];
            }

            if (_diffusionTerm[j][i] != nullptr)
            {
              const IPhysicalParameter& diffusionTermComponentJI = *_diffusionTerm[j][i];

              VectorXd diffusionTermValuesJI;
              diffusionTermComponentJI.Eval(quadraturePoints, diffusionTermValuesJI);

              cellMatrixSingleSolution += basisFunctionDerivativeValues[j].transpose() * quadratureWeights.cwiseProduct(diffusionTermValuesJI).asDiagonal() * basisFunctionDerivativeValues[i];
            }
          }
        }
      }
      else
      {
        Output::PrintErrorMessage("%s: computing stiffness matrix. Diffusion term is not valid",	true, __func__);
        return Output::GenericError;
      }
    }

    /// <li> Compute Reaction matrix
    if (_reactionTerm != nullptr)
    {
      const IPhysicalParameter& reactionTerm = *_reactionTerm;

      VectorXd reactionTermValues;
      reactionTerm.Eval(quadraturePoints, reactionTermValues);
      cellMatrixSingleSolution += basisFunctionValues.transpose() * (quadratureWeights.cwiseProduct(reactionTermValues)).asDiagonal() * basisFunctionValues;
    }

    /// <li> Compute Transport matrix
    if (!_transportTerm.empty())
    {
      if (_transportTerm.size() == dimension)
      {
        for (unsigned int i = 0; i < dimension; i++)
        {
          if (_transportTerm[i] == nullptr)
            continue;

          const IPhysicalParameter& transportTermComponent = *_transportTerm[i];
          VectorXd transportTermValues;

          transportTermComponent.Eval(quadraturePoints, transportTermValues);
          cellMatrixSingleSolution += basisFunctionValues.transpose() * quadratureWeights.cwiseProduct(transportTermValues).asDiagonal() * basisFunctionDerivativeValues[i];
        }
      }
      else
      {
        Output::PrintErrorMessage("%s: size of transport term not valid: %d", true, __func__, _transportTerm.size());
        return Output::GenericError;
      }
    }

    /// <li> Compute Right-hand side
    if (_forcingTerm != nullptr)
    {
      const IPhysicalParameter& forcingTerm = *_forcingTerm;

      VectorXd forcingTermValues;
      forcingTerm.Eval(quadraturePoints, forcingTermValues);
      cellRightHandSideSingleSolution += basisFunctionValues.transpose() * quadratureWeights.asDiagonal() * forcingTermValues;
    }

    /// <li> Fill the cell local matrix and right hand side
    cellMatrix.setZero(numMaxDofsPerCell, numMaxDofsPerCell);
    cellRightHandSide.setZero(numMaxDofsPerCell);

    for (unsigned int i = 0; i < solutionDimension; i++)
    {
      cellMatrix.block(i * numReferenceElementDofs,
                       i * numReferenceElementDofs,
                       numReferenceElementDofs,
                       numReferenceElementDofs) = cellMatrixSingleSolution;

      cellRightHandSide.segment(i * numReferenceElementDofs,
                                numReferenceElementDofs) = cellRightHandSideSingleSolution;
    }

    /// <li> Compute Neumann conditions
    if (_neumannBoundaryConditions != nullptr && _neumannQuadrature != nullptr)
    {
      VectorXd neumannTermValues;

      result = ComputeNeumann(mesh,
                              cellPostionId,
                              dofHandler,
                              referenceElementMap,
                              femValues,
                              *_neumannBoundaryConditions,
                              *_neumannQuadrature,
                              neumannTermValues);

      if (result != Output::Success)
      {
        Output::PrintErrorMessage("%s: Error on ComputeNeumann", false);
        return result;
      }

      cellRightHandSide += neumannTermValues;
    }

    /// <li> Compute Dirichlet conditions
    if (_dirichletBoundaryConditions != nullptr)
    {
      result =  ComputeDirichlet(cellPostionId,
                                 dofHandler,
                                 *_dirichletBoundaryConditions,
                                 cellDirichletTermValues);

      if (result != Output::Success)
      {
        Output::PrintErrorMessage("%s: Error on ComputeDirichlet", false);
        return result;
      }
    }

    return Output::Success;

    /// </ul>
  }
  // ***************************************************************************
  void FemEllipticEquation::SetDiffusionTerm(const IPhysicalParameter& diffusionTerm)
  {
    if (_femValues == nullptr ||
        _dofHandler == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return;
    }

    const IFemValues& femValues = *_femValues;
    unsigned int dimension = femValues.Dimension();

    for (unsigned int i = 0; i < dimension; i++)
      _diffusionTerm[i][i] = &diffusionTerm;
  }
  // ***************************************************************************
  void FemEllipticEquation::SetTransportTerm(const IPhysicalParameter& transportTerm)
  {
    if (_femValues == nullptr ||
        _dofHandler == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return;
    }

    const IFemValues& femValues = *_femValues;
    unsigned int dimension = femValues.Dimension();

    for (unsigned int i = 0; i < dimension; i++)
      _transportTerm[i] = &transportTerm;
  }
  // ***************************************************************************
  Output::ExitCodes FemEllipticEquation::ComputeNeumann(const IMesh& mesh,
                                                        const unsigned int& cellPostionId,
                                                        const IDofHandler& dofHandler,
                                                        const IMapping& referenceElementMap,
                                                        const IFemValues& femValues,
                                                        const IBoundaryCondition& neumannBoundaryConditions,
                                                        const IQuadrature& neumannQuadrature,
                                                        VectorXd& neumannTermValues) const
  {
    unsigned int dimension = femValues.Dimension();

    const IGeometricObject& cellGeometry = *mesh.GetMaximalGeometricObject(cellPostionId);

    unsigned int numMaxDofsPerCell = dofHandler.NumMaximalCellDofs(cellPostionId);

    /// <li> Compute single solution dirichlet term
    neumannTermValues.setZero(numMaxDofsPerCell);

    switch (dimension)
    {
      case 1:
      {
        for (unsigned int m = 0; m < numMaxDofsPerCell; m++)
        {
          if (!dofHandler.IsNeumannDof(cellPostionId, m))
            continue;

          VectorXd neumannTermValue;
          neumannBoundaryConditions.Eval(dofHandler.Marker(cellPostionId, m),
                                         dofHandler.DofPosition(cellPostionId, m),
                                         neumannTermValue);

          neumannTermValues.segment(m, 1) = neumannTermValue;
        }
      }
      break;
      case 2:
      {
        unsigned int solutionDimension = dofHandler.SolutionDimension();

        for(unsigned int e = 0; e < cellGeometry.NumberOfEdges(); e++)
        {
          const Cell1D& edge = *static_cast<const Cell1D*>(cellGeometry.Edge(e));

          if (!edge.IsNeumann())
            continue;

          MatrixXd quadraturePoints;
          MatrixXd basisFunctionValues;


          IMapping& edgeMap = *_neumannBorderMap;
          Output::ExitCodes result = edgeMap.Compute(edge, false);

          if (result != Output::Success)
          {
            Output::PrintErrorMessage("%s: edge %d map not computed", true, __func__, edge.Id());
            return result;
          }

          result = edgeMap.F(neumannQuadrature.Points(), quadraturePoints);

          if (result != Output::Success)
          {
            Output::PrintErrorMessage("%s: edge %d points map not computed", true, __func__, edge.Id());
            return result;
          }

          VectorXd quadratureWeights = neumannQuadrature.Weights() * edgeMap.DetJ();

          femValues.ComputeBasisFunctionValues(referenceElementMap,
                                               quadraturePoints,
                                               basisFunctionValues);

          for (unsigned int d = 0; d < solutionDimension; d++)
          {
            unsigned int numMaxDofsPerCellPerSolution = dofHandler.NumMaximalCellDofsPerSolution(cellPostionId, d);

            VectorXd neumannTermValue;
            neumannBoundaryConditions.Eval(edge.Marker(d),
                                           quadraturePoints,
                                           neumannTermValue);

            neumannTermValues.segment(d*numMaxDofsPerCellPerSolution,
                                      numMaxDofsPerCellPerSolution) +=
                basisFunctionValues.transpose() *
                quadratureWeights.asDiagonal() * neumannTermValue;
          }
        }
      }
      break;
      case 3:
      {
        unsigned int solutionDimension = dofHandler.SolutionDimension();

        for(unsigned int i = 0; i < cellGeometry.NumberOfFaces(); i++)
        {
          const Cell2D& face = *static_cast<const Cell2D*>(cellGeometry.Face(i));

          if (!face.IsNeumann())
            continue;

          MatrixXd quadraturePoints;
          MatrixXd basisFunctionValues;

          IMapping& faceMap = *_neumannBorderMap;
          Output::ExitCodes result = faceMap.Compute(face, false);

          if (result != Output::Success)
          {
            Output::PrintErrorMessage("%s: face %d map not computed", true, __func__, face.Id());
            return result;
          }

          result = faceMap.F(neumannQuadrature.Points(), quadraturePoints);

          if (result != Output::Success)
          {
            Output::PrintErrorMessage("%s: face %d points map not computed", true, __func__, face.Id());
            return result;
          }

          VectorXd quadratureWeights = neumannQuadrature.Weights() * faceMap.DetJ();

          femValues.ComputeBasisFunctionValues(referenceElementMap,
                                               quadraturePoints,
                                               basisFunctionValues);

          for (unsigned int d = 0; d < solutionDimension; d++)
          {
            unsigned int numMaxDofsPerCellPerSolution = dofHandler.NumMaximalCellDofsPerSolution(cellPostionId, d);

            VectorXd neumannTermValue;
            neumannBoundaryConditions.Eval(face.Marker(d),
                                           quadraturePoints,
                                           neumannTermValue);

            neumannTermValues.segment(d*numMaxDofsPerCellPerSolution,
                                      numMaxDofsPerCellPerSolution) +=
                basisFunctionValues.transpose() *
                quadratureWeights.asDiagonal() * neumannTermValue;
          }
        }
      }
      break;
      default:
        Output::PrintErrorMessage("%s: Dimension %d not implemented yet", false, dimension);
      return Output::GenericError;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes FemEllipticEquation::ComputeDirichlet(const unsigned int& cellPostionId,
                                                          const IDofHandler& dofHandler,
                                                          const IBoundaryCondition& dirichletBoundaryConditions,
                                                          VectorXd& dirichletTermValues) const
  {
    /// <ul>

    unsigned int numMaxDofsPerCell = dofHandler.NumMaximalCellDofs(cellPostionId);

    /// <li> Compute single solution dirichlet term
    dirichletTermValues.setZero(numMaxDofsPerCell);

    for (unsigned int m = 0; m < numMaxDofsPerCell; m++)
    {
      if (!dofHandler.IsDirichletDof(cellPostionId, m))
        continue;

      VectorXd dirichletTermValue;
      dirichletBoundaryConditions.Eval(dofHandler.Marker(cellPostionId, m),
                                       dofHandler.DofPosition(cellPostionId, m),
                                       dirichletTermValue);

      dirichletTermValues.segment(m, 1) = dirichletTermValue;
    }

    return Output::Success;
  }
  // ***************************************************************************
}
