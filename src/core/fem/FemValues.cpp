#include "FemValues.hpp"
#include "EigenExtension.hpp"

namespace GeDiM
{
  // ***************************************************************************
  FemValues::FemValues()
  {
    _referenceElement = nullptr;
    _internalQuadrature = nullptr;
  }
  FemValues::~FemValues()
  {
    _referenceElementBasisFunctionValues.resize(0, 0);

    for (MatrixXd& _basisFunctionsDerivativeValue : _referenceElementBasisFunctionsDerivativeValues)
      _basisFunctionsDerivativeValue.resize(0, 0);

    _referenceElementBasisFunctionsDerivativeValues.clear();

    _referenceElement = nullptr;
    _internalQuadrature = nullptr;
  }
  // ***************************************************************************
  Output::ExitCodes FemValues::Initialize(const IFemReferenceElement& referenceElement,
                                          const IQuadrature& internalQuadrature)
  {
    _referenceElement = &referenceElement;
    _internalQuadrature = &internalQuadrature;

    Output::ExitCodes result;

    result = referenceElement.EvaluateBasisFunctionDerivatives(internalQuadrature.Points(),
                                                               _referenceElementBasisFunctionsDerivativeValues);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: error on EvaluateBasisFunctionDerivatives", true, __func__);
      return result;
    }

    result = referenceElement.EvaluateBasisFunctions(internalQuadrature.Points(),
                                                     _referenceElementBasisFunctionValues);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: error on EvaluateBasisFunctions", true, __func__);
      return result;
    }

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: error on EvaluateDofPoints", true, __func__);
      return result;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes FemValues::ComputeInternalQuadraturePoints(const IMapping& referenceElementMap,
                                                               MatrixXd& quadraturePoints) const
  {
    if (_internalQuadrature == nullptr || _referenceElement == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    const IQuadrature& internalQuadrature = *_internalQuadrature;

    referenceElementMap.F(internalQuadrature.Points(), quadraturePoints);

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes FemValues::ComputeInternalQuadratureWeights(const IMapping& referenceElementMap,
                                                                VectorXd& quadratureWeights) const
  {
    if (_internalQuadrature == nullptr || _referenceElement == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    const IQuadrature& internalQuadrature = *_internalQuadrature;

    quadratureWeights = internalQuadrature.Weights() * referenceElementMap.DetJ();

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes FemValues::ComputeBasisFunctionValues(MatrixXd& basisFunctionValues) const
  {
    if (_internalQuadrature == nullptr || _referenceElement == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    basisFunctionValues = _referenceElementBasisFunctionValues;

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes FemValues::ComputeBasisFunctionDerivativeValues(const IMapping& referenceElementMap,
                                                                    vector<MatrixXd>& basisFunctionsDerivativeValues) const
  {
    if (_internalQuadrature == nullptr || _referenceElement == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    const IReferenceElement& referenceElement = *_referenceElement;

    MatrixXd mapGradientInverse = referenceElementMap.JInverse();
    basisFunctionsDerivativeValues.resize(referenceElement.Dimension());

    // The transposition of J^-1 is performed directly in the computation taking the suitable values
    // example:
    // basisFunctionsDerivativeValues[i] += mapGradientInverse(j,i)
    // instead of
    // basisFunctionsDerivativeValues[i] += mapGradientInverse(i,j)
    for(unsigned int i = 0; i < referenceElement.Dimension(); i++)
    {
      basisFunctionsDerivativeValues[i] = mapGradientInverse(i,i) * _referenceElementBasisFunctionsDerivativeValues[i];
      for(unsigned int j = 0; j < i; j++)
      {
        basisFunctionsDerivativeValues[i] += mapGradientInverse(j,i) * _referenceElementBasisFunctionsDerivativeValues[j];
        basisFunctionsDerivativeValues[j] += mapGradientInverse(i,j) * _referenceElementBasisFunctionsDerivativeValues[i];
      }
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes FemValues::ComputeBasisFunctionValues(const IMapping& referenceElementMap,
                                                          const MatrixXd& points,
                                                          MatrixXd& basisFunctionValues) const
  {
    if (_internalQuadrature == nullptr || _referenceElement == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    const IFemReferenceElement& referenceElement = *_referenceElement;

    MatrixXd referenceElementPoints;
    Output::ExitCodes result = referenceElementMap.FInverse(points, referenceElementPoints);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: FInverse error", false, __func__);
      return Output::GenericError;
    }

    return referenceElement.EvaluateBasisFunctions(referenceElementPoints, basisFunctionValues);
  }
  // ***************************************************************************
  Output::ExitCodes FemValues::ComputeBasisFunctionDerivativeValues(const IMapping& referenceElementMap,
                                                                    const MatrixXd& points,
                                                                    vector<MatrixXd>& basisFunctionsDerivativeValues) const
  {
    if (_internalQuadrature == nullptr || _referenceElement == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    const IFemReferenceElement& referenceElement = *_referenceElement;

    MatrixXd referenceElementPoints;
    Output::ExitCodes result =  referenceElementMap.FInverse(points, referenceElementPoints);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: FInverse error", false, __func__);
      return Output::GenericError;
    }

    vector<MatrixXd> basisFunctionsDerivativeValuesReference;
    result = referenceElement.EvaluateBasisFunctionDerivatives(referenceElementPoints, basisFunctionsDerivativeValuesReference);

    if (result != Output::Success)
    {
      Output::PrintErrorMessage("%s: EvaluateBasisFunctionDerivatives error", false, __func__);
      return Output::GenericError;
    }

    MatrixXd mapGradientInverse = referenceElementMap.JInverse();
    basisFunctionsDerivativeValues.resize(referenceElement.Dimension());
    for(unsigned int i = 0; i < referenceElement.Dimension(); i++)
    {
      basisFunctionsDerivativeValues[i] = mapGradientInverse(i,i) * basisFunctionsDerivativeValuesReference[i];
      for(unsigned int j = 0; j < i; j++)
      {
        basisFunctionsDerivativeValues[i] += mapGradientInverse(j,i) * basisFunctionsDerivativeValuesReference[j];
        basisFunctionsDerivativeValues[j] += mapGradientInverse(i,j) * basisFunctionsDerivativeValuesReference[i];
      }
    }

    return Output::Success;
  }
  // ***************************************************************************
}
