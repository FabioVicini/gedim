#ifndef __FEMVALUES_H
#define __FEMVALUES_H

#include "IFemValues.hpp"
#include "EigenExtension.hpp"

namespace GeDiM
{
  /// \brief Interface used to FEM Values computation
  /// \author Andrea Borio, Fabio Vicini.
  /// \copyright See top level LICENSE file for details.
  class FemValues final : public IFemValues
  {
    private:
      const IFemReferenceElement* _referenceElement; ///< class cell reference element
      const IQuadrature* _internalQuadrature; ///< class cell internal quadratures
      MatrixXd _referenceElementBasisFunctionValues; ///< reference element basis function values
      vector<MatrixXd> _referenceElementBasisFunctionsDerivativeValues; ///< reference element basis function derivate values

    public:
      FemValues();
      ~FemValues();

      unsigned int NumberInternalQuadraturePoints() const { return (_internalQuadrature == nullptr) ? 0 : _internalQuadrature->NumPoints(); }
      unsigned int NumberBasisFunctions() const { return (_referenceElement == nullptr) ? 0 : _referenceElement->NumberBasisFunctions(); }
      unsigned int Dimension() const { return (_referenceElement == nullptr) ? 0 : _referenceElement->Dimension(); }

      Output::ExitCodes Initialize(const IFemReferenceElement& referenceElement,
                                   const IQuadrature& internalQuadrature);

      Output::ExitCodes ComputeInternalQuadraturePoints(const IMapping& referenceElementMap,
                                                        MatrixXd& quadraturePoints) const;

      Output::ExitCodes ComputeInternalQuadratureWeights(const IMapping& referenceElementMap,
                                                         VectorXd& quadratureWeights) const;

      Output::ExitCodes ComputeBasisFunctionValues(MatrixXd& basisFunctionValues) const;

      Output::ExitCodes ComputeBasisFunctionDerivativeValues(const IMapping& referenceElementMap,
                                                             vector<MatrixXd>& basisFunctionsDerivativeValues) const;

      Output::ExitCodes ComputeBasisFunctionValues(const IMapping& referenceElementMap,
                                                   const MatrixXd& points,
                                                   MatrixXd& basisFunctionValues) const;

      Output::ExitCodes ComputeBasisFunctionDerivativeValues(const IMapping& referenceElementMap,
                                                             const MatrixXd& points,
                                                             vector<MatrixXd>& basisFunctionsDerivativeValues) const;
  };
}

#endif // __FEMVALUES_H
