#include "Fem1DLagrangePCC.hpp"

namespace GeDiM
{
  // ***************************************************************************
  void Fem1DLagrangePCC::EvaluateLambda(const MatrixXd& points,
                                        MatrixXd& lambda) const
  {
    lambda.setZero(points.cols(), 2);

    lambda.col(0) = 1.0 - points.row(0).array();
    lambda.col(1) = points.row(0);
  }
  // ***************************************************************************
  void Fem1DLagrangePCC::EvaluateGradLambda(const MatrixXd& points,
                                            vector<MatrixXd>& gradLambda) const
  {
    gradLambda.resize(1);

    gradLambda[0].setZero(points.cols(), 2);

    gradLambda[0].col(0).setConstant(-1.0);

    gradLambda[0].col(1).setOnes();
  }
  // ***************************************************************************
  Fem1DLagrangePCC::Fem1DLagrangePCC(unsigned int& order)
  {
    _order = order;

    _numberBasisFunctions = 0;
    _numberDofs0D = 0;
    _numberDofs1D = 0;
  }
  // ***************************************************************************
  Output::ExitCodes Fem1DLagrangePCC::Initialize()
  {
    /// <ul>

    /// <li> Computing DOFs on the reference element

    unsigned int vertexCount = 0;
    vector<unsigned int> nodeDofs; ///< The degrees of freedom of the element on nodes
    vector<unsigned int> cellDofs; ///< The degrees of freedom of the element on cell
    vector<unsigned int> nodeDofsIndex(3, 0); ///< Index of DOFS on nodes

    _numberDofs0D = 0;
    _numberDofs1D = 0;
    _numberBasisFunctions = _order + 1;

    MatrixXd dofPositions(Dimension(), _numberBasisFunctions);

    for(unsigned int i = 0; i < _numberBasisFunctions; i++)
    {
      if(_order > 0)
        dofPositions.col(vertexCount) << (double) i / _order;
      else
        dofPositions.col(vertexCount) << 0.5;

      if(dofPositions(0, vertexCount) == 0.0)
      {
        nodeDofs.push_back(vertexCount);
        nodeDofsIndex[1]++;
      }
      else if(dofPositions(0, vertexCount) == 1.0)
      {
        nodeDofs.push_back(vertexCount);
        nodeDofsIndex[2]++;
      }
      else
        cellDofs.push_back(vertexCount);

      vertexCount++;
    }

    for(unsigned int i = 1; i < nodeDofsIndex.size(); i++)
      nodeDofsIndex[i] = nodeDofsIndex[i] + nodeDofsIndex[i - 1];

    if(vertexCount != _numberBasisFunctions ||
       nodeDofs.size() != nodeDofsIndex.back())
    {
      Output::PrintErrorMessage("%s: Wrong initialization in FE reference element. Number of DOFs found is not correct.", false, __func__);
      return Output::GenericError;
    }

    /// <li> Reordering Dofs using convention [point, cell]
    _numberDofs0D = nodeDofs.size() / 2;
    _numberDofs1D = cellDofs.size();
    _referenceElementDofPositions.resize(Dimension(), _numberBasisFunctions);

    unsigned int dofCounter = 0;
    for (unsigned int n = 0; n < 2; n++)
    {
      for (unsigned int i = nodeDofsIndex[n]; i < nodeDofsIndex[n + 1]; i++)
        _referenceElementDofPositions.col(dofCounter++) = dofPositions.col(nodeDofs[i]);
    }

    for (unsigned int c = 0; c < cellDofs.size(); c++)
      _referenceElementDofPositions.col(dofCounter++) = dofPositions.col(cellDofs[c]);


    return Output::Success;

    /// </ul>
  }
  // ***************************************************************************
  Output::ExitCodes Fem1DLagrangePCC::EvaluateDofPositions(const IMapping& referenceElementMap,
                                                           MatrixXd& dofPositions) const
  {
    if (_referenceElementDofPositions.size() == 0)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    return referenceElementMap.F(_referenceElementDofPositions, dofPositions);
  }
  // ***************************************************************************
  Output::ExitCodes Fem1DLagrangePCC::EvaluateBasisFunctions(const MatrixXd& points,
                                                             MatrixXd& values) const
  {
    values.resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    switch (_order)
    {
      case 1:
        values.col(0) = lambda.col(0);
        values.col(1) = lambda.col(1);
      break;
      case 2:
        values.col(0) = 2.0 * lambda.col(0).array() * (lambda.col(0).array() - 0.5);
        values.col(1) = 2.0 * lambda.col(1).array() * (lambda.col(1).array() - 0.5);
        values.col(2) = 4.0 * lambda.col(0).array() * lambda.col(1).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Fem1DLagrangePCC::EvaluateBasisFunctionDerivatives(const MatrixXd& points,
                                                                       vector<MatrixXd>& values) const
  {
    values.resize(Dimension());
    for (unsigned int d = 0; d < Dimension(); d++)
      values[d].resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    vector<MatrixXd> gradLambda;
    EvaluateGradLambda(points, gradLambda);

    switch (_order)
    {
      case 1:
        values[0].col(0) = gradLambda[0].col(0);
        values[0].col(1) = gradLambda[0].col(1);
      break;
      case 2:
        values[0].col(0) = 2.0 * gradLambda[0].col(0).array() * (lambda.col(0).array() - 0.5) + 2.0 * lambda.col(0).array() * gradLambda[0].col(0).array();
        values[0].col(1) = 2.0 * gradLambda[0].col(1).array() * (lambda.col(1).array() - 0.5) + 2.0 * lambda.col(1).array() * gradLambda[0].col(1).array();
        values[0].col(2) = 4.0 * gradLambda[0].col(0).array() * lambda.col(1).array() + 4.0 * lambda.col(0).array() * gradLambda[0].col(1).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
}
