#ifndef __IFEMELLIPTICEQUATION_H
#define __IFEMELLIPTICEQUATION_H

#include "IDifferentialEquation.hpp"
#include "IBoundaryCondition.hpp"
#include "IPhysicalParameter.hpp"
#include "IQuadrature.hpp"

using namespace std;
using namespace Eigen;

namespace GeDiM
{
  /// \brief Interface used to FEM Elliptic Differential Equation
  /// \copyright See top level LICENSE file for details.
  class IFemEllipticEquation : public IDifferentialEquation
  {
    public:
      ~IFemEllipticEquation() {}

      /// \brief Function representing the diffusion term for isotropic case
      /// \note Size is the geometric Dimesion x geometric Dimesion
      virtual void SetDiffusionTerm(const IPhysicalParameter& diffusionTerm) = 0;
      /// \brief Function representing the diffusion term for anisotropic case
      /// \param row The Diffusion Term row position in tensor
      /// \param col The Diffusion Term col position in tensor
      virtual void SetDiffusionTerm(const IPhysicalParameter& diffusionTerm,
                                    const unsigned int& row,
                                    const unsigned int& col) = 0;
      /// \brief Function representing the transport velocity vector for isotropic case
      /// \note Size is the geometric Dimesion
      virtual void SetTransportTerm(const IPhysicalParameter& transportTerm) = 0;
      /// \brief Function representing the transport velocity vector for anisotropic case
      /// \brief position The position on the vector
      /// \note Size is the geometric Dimesion
      virtual void SetTransportTerm(const IPhysicalParameter& transportTerm,
                                    const unsigned int& position) = 0;
      /// \brief Function representing the reaction coefficient
      virtual void SetReactionTerm(const IPhysicalParameter& reactionTerm) = 0;
      /// \brief Functions representing the forcing term.
      virtual void SetForcingTerm(const IPhysicalParameter& forcingTerm) = 0;
      /// \brief Dirichlet conditions. Inputs of each one: marker, points, output.
      virtual void SetDirichletBoundaryConditions(const IBoundaryCondition& dirichletBoundaryConditions) = 0;
      /// \brief Neumann conditions. Inputs of each one: marker, points, output.
      /// \param neumannBoundaryConditions The condition
      /// \param neumannQuadrature The quadrature formula to be used
      /// \param neumannBorderMap The map for boundary geometry (empty for dimension 1)
      virtual void SetNeumannBoundaryConditions(const IBoundaryCondition& neumannBoundaryConditions,
                                                const IQuadrature& neumannQuadrature,
                                                IMapping& neumannBorderMap) = 0;
  };
}

#endif // __IFEMELLIPTICEQUATION_H
