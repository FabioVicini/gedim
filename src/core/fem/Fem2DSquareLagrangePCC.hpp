#ifndef __FEM2DSQUARELAGRANGEPCC_H
#define __FEM2DSQUARELAGRANGEPCC_H

#include "Eigen"
#include "Output.hpp"
#include "IFemReferenceElement.hpp"

using namespace Eigen;
using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief 2D Primal Conforming Constant Lagrange Element Degree variable
  /// \copyright See top level LICENSE file for details.
  class Fem2DSquareLagrangePCC final : public IFemReferenceElement
  {
    private:
      unsigned int _order; ///< Degree of the basis functions
      unsigned int _numberBasisFunctions; ///< Number of total basis functions
      unsigned int _numberDofs0D; ///< Number of Dofs 0 D
      unsigned int _numberDofs1D; ///< Number of Dofs 1 D
      unsigned int _numberDofs2D; ///< Number of Dofs 2 D
      MatrixXd _referenceElementDofPositions; ///< reference element dof points

      /// \brief evaluate the lambda function for basis functions (Barycentric coordinate system)
      void EvaluateLambda(const MatrixXd& points,
                          MatrixXd& lambda) const;
      /// \brief evaluate the lambda gradient function for basis functions
      void EvaluateGradLambda(const MatrixXd& points,
                              vector<MatrixXd>& gradLambda) const;

    public:
      Fem2DSquareLagrangePCC(unsigned int& order);
      ~Fem2DSquareLagrangePCC() {}

      inline unsigned int Dimension() const { return 2; }
      inline unsigned int Order() const { return _order; }
      inline unsigned int NumDofs0D(const Cell0D&) const { return _numberDofs0D; }
      inline unsigned int NumDofs1D(const Cell1D&) const { return _numberDofs1D; }
      inline unsigned int NumDofs2D(const Cell2D&) const { return _numberDofs2D; }
      inline unsigned int NumDofs3D(const Cell3D&) const { return 0; }
      inline unsigned int NumberBasisFunctions() const { return _numberBasisFunctions; }

      Output::ExitCodes Initialize();

      Output::ExitCodes EvaluateDofPositions(const IMapping& referenceElementMap,
                                             MatrixXd& dofPositions) const;

      Output::ExitCodes EvaluateBasisFunctions(const MatrixXd& points,
                                               MatrixXd& values) const;

      Output::ExitCodes EvaluateBasisFunctionDerivatives(const MatrixXd& points,
                                                         vector<MatrixXd>& values) const;
  };
}

#endif // __FEM2DSQUARELAGRANGEPCC_H
