#include "Fem3DHexahedronLagrangePCC.hpp"

namespace GeDiM
{
  // ***************************************************************************
  void Fem3DHexahedronLagrangePCC::EvaluateLambda(const MatrixXd& points,
                                                  MatrixXd& lambda) const
  {
    lambda.setZero(points.cols(), 6);

    lambda.col(0) = 1.0 - points.row(0).array();
    lambda.col(1) = 1.0 - points.row(1).array();
    lambda.col(2) = 1.0 - points.row(2).array();
    lambda.col(3) = points.row(0);
    lambda.col(4) = points.row(1);
    lambda.col(5) = points.row(2);
  }
  // ***************************************************************************
  void Fem3DHexahedronLagrangePCC::EvaluateGradLambda(const MatrixXd& points,
                                                      vector<MatrixXd>& gradLambda) const
  {
    gradLambda.resize(3);

    gradLambda[0].setZero(points.cols(), 6);
    gradLambda[1].setZero(points.cols(), 6);
    gradLambda[2].setZero(points.cols(), 6);

    gradLambda[0].col(0).setConstant(-1.0);
    gradLambda[1].col(0).setZero();
    gradLambda[2].col(0).setZero();

    gradLambda[0].col(1).setZero();
    gradLambda[1].col(1).setConstant(-1.0);
    gradLambda[2].col(1).setZero();

    gradLambda[0].col(2).setZero();
    gradLambda[1].col(2).setZero();
    gradLambda[2].col(2).setConstant(-1.0);

    gradLambda[0].col(3).setOnes();
    gradLambda[1].col(3).setZero();
    gradLambda[2].col(3).setZero();

    gradLambda[0].col(4).setZero();
    gradLambda[1].col(4).setOnes();
    gradLambda[2].col(4).setZero();

    gradLambda[0].col(5).setZero();
    gradLambda[1].col(5).setZero();
    gradLambda[2].col(5).setOnes();
  }
  // ***************************************************************************
  Fem3DHexahedronLagrangePCC::Fem3DHexahedronLagrangePCC(unsigned int& order)
  {
    _order = order;

    _numberBasisFunctions = 0;
    _numberDofs0D = 0;
    _numberDofs1D = 0;
    _numberDofs2D = 0;
    _numberDofs3D = 0;
  }
  // ***************************************************************************
  Output::ExitCodes Fem3DHexahedronLagrangePCC::Initialize()
  {
    /// <ul>

    /// <li> Computing DOFs on the reference element

    switch (_order)
    {
      case 1:
        _numberDofs0D = 1;
        _numberDofs1D = 0;
        _numberDofs2D = 0;
        _numberDofs3D = 0;
        _numberBasisFunctions = 8;

        _referenceElementDofPositions.resize(Dimension(), _numberBasisFunctions);
        _referenceElementDofPositions.col(0)<< 0.0, 0.0, 0.0;
        _referenceElementDofPositions.col(1)<< 1.0, 0.0, 0.0;
        _referenceElementDofPositions.col(2)<< 1.0, 1.0, 0.0;
        _referenceElementDofPositions.col(3)<< 0.0, 1.0, 0.0;
        _referenceElementDofPositions.col(4)<< 0.0, 0.0, 1.0;
        _referenceElementDofPositions.col(5)<< 1.0, 0.0, 1.0;
        _referenceElementDofPositions.col(6)<< 1.0, 1.0, 1.0;
        _referenceElementDofPositions.col(7)<< 0.0, 1.0, 1.0;
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;

    /// </ul>
  }
  // ***************************************************************************
  Output::ExitCodes Fem3DHexahedronLagrangePCC::EvaluateDofPositions(const IMapping& referenceElementMap,
                                                                     MatrixXd& dofPositions) const
  {
    if (_referenceElementDofPositions.size() == 0)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    return referenceElementMap.F(_referenceElementDofPositions, dofPositions);
  }
  // ***************************************************************************
  Output::ExitCodes Fem3DHexahedronLagrangePCC::EvaluateBasisFunctions(const MatrixXd& points,
                                                                       MatrixXd& values) const
  {
    values.resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    switch (_order)
    {
      case 1:
        values.col(0) = lambda.col(0).array() * lambda.col(1).array() * lambda.col(2).array();
        values.col(1) = lambda.col(3).array() * lambda.col(1).array() * lambda.col(2).array();
        values.col(2) = lambda.col(3).array() * lambda.col(4).array() * lambda.col(2).array();
        values.col(3) = lambda.col(0).array() * lambda.col(4).array() * lambda.col(2).array();
        values.col(4) = lambda.col(0).array() * lambda.col(1).array() * lambda.col(5).array();
        values.col(5) = lambda.col(3).array() * lambda.col(1).array() * lambda.col(5).array();
        values.col(6) = lambda.col(3).array() * lambda.col(4).array() * lambda.col(5).array();
        values.col(7) = lambda.col(0).array() * lambda.col(4).array() * lambda.col(5).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Fem3DHexahedronLagrangePCC::EvaluateBasisFunctionDerivatives(const MatrixXd& points,
                                                                                 vector<MatrixXd>& values) const
  {
    values.resize(Dimension());
    for (unsigned int d = 0; d < Dimension(); d++)
      values[d].resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    vector<MatrixXd> gradLambda;
    EvaluateGradLambda(points, gradLambda);

    switch (_order)
    {
      case 1:
        values[0].col(0) =
            gradLambda[0].col(0).array() * lambda.col(1).array() * lambda.col(2).array() +
            lambda.col(0).array() * gradLambda[0].col(1).array() * lambda.col(2).array() +
            lambda.col(0).array() * lambda.col(1).array() * gradLambda[0].col(2).array();
        values[1].col(0) =
            gradLambda[1].col(0).array() * lambda.col(1).array() * lambda.col(2).array() +
            lambda.col(0).array() * gradLambda[1].col(1).array() * lambda.col(2).array() +
            lambda.col(0).array() * lambda.col(1).array() * gradLambda[1].col(2).array();
        values[2].col(0) =
            gradLambda[2].col(0).array() * lambda.col(1).array() * lambda.col(2).array() +
            lambda.col(0).array() * gradLambda[2].col(1).array() * lambda.col(2).array() +
            lambda.col(0).array() * lambda.col(1).array() * gradLambda[2].col(2).array();

        values[0].col(1) =
            gradLambda[0].col(3).array() * lambda.col(1).array() * lambda.col(2).array() +
            lambda.col(3).array() * gradLambda[0].col(1).array() * lambda.col(2).array() +
            lambda.col(3).array() * lambda.col(1).array() * gradLambda[0].col(2).array();
        values[1].col(1) =
            gradLambda[1].col(3).array() * lambda.col(1).array() * lambda.col(2).array() +
            lambda.col(3).array() * gradLambda[1].col(1).array() * lambda.col(2).array() +
            lambda.col(3).array() * lambda.col(1).array() * gradLambda[1].col(2).array();
        values[2].col(1) =
            gradLambda[2].col(3).array() * lambda.col(1).array() * lambda.col(2).array() +
            lambda.col(3).array() * gradLambda[2].col(1).array() * lambda.col(2).array() +
            lambda.col(3).array() * lambda.col(1).array() * gradLambda[2].col(2).array();

        values[0].col(2) =
            gradLambda[0].col(3).array() * lambda.col(4).array() * lambda.col(2).array() +
            lambda.col(3).array() * gradLambda[0].col(4).array() * lambda.col(2).array() +
            lambda.col(3).array() * lambda.col(4).array() * gradLambda[0].col(2).array();
        values[1].col(2) =
            gradLambda[1].col(3).array() * lambda.col(4).array() * lambda.col(2).array() +
            lambda.col(3).array() * gradLambda[1].col(4).array() * lambda.col(2).array() +
            lambda.col(3).array() * lambda.col(4).array() * gradLambda[1].col(2).array();
        values[2].col(2) =
            gradLambda[2].col(3).array() * lambda.col(4).array() * lambda.col(2).array() +
            lambda.col(3).array() * gradLambda[2].col(4).array() * lambda.col(2).array() +
            lambda.col(3).array() * lambda.col(4).array() * gradLambda[2].col(2).array();

        values[0].col(3) =
            gradLambda[0].col(0).array() * lambda.col(4).array() * lambda.col(2).array() +
            lambda.col(0).array() * gradLambda[0].col(4).array() * lambda.col(2).array() +
            lambda.col(0).array() * lambda.col(4).array() * gradLambda[0].col(2).array();
        values[1].col(3) =
            gradLambda[1].col(0).array() * lambda.col(4).array() * lambda.col(2).array() +
            lambda.col(0).array() * gradLambda[1].col(4).array() * lambda.col(2).array() +
            lambda.col(0).array() * lambda.col(4).array() * gradLambda[1].col(2).array();
        values[2].col(3) =
            gradLambda[2].col(0).array() * lambda.col(4).array() * lambda.col(2).array() +
            lambda.col(0).array() * gradLambda[2].col(4).array() * lambda.col(2).array() +
            lambda.col(0).array() * lambda.col(4).array() * gradLambda[2].col(2).array();

        values[0].col(4) =
            gradLambda[0].col(0).array() * lambda.col(1).array() * lambda.col(5).array() +
            lambda.col(0).array() * gradLambda[0].col(1).array() * lambda.col(5).array() +
            lambda.col(0).array() * lambda.col(1).array() * gradLambda[0].col(5).array();
        values[1].col(4) =
            gradLambda[1].col(0).array() * lambda.col(1).array() * lambda.col(5).array() +
            lambda.col(0).array() * gradLambda[1].col(1).array() * lambda.col(5).array() +
            lambda.col(0).array() * lambda.col(1).array() * gradLambda[1].col(5).array();
        values[2].col(4) =
            gradLambda[2].col(0).array() * lambda.col(1).array() * lambda.col(5).array() +
            lambda.col(0).array() * gradLambda[2].col(1).array() * lambda.col(5).array() +
            lambda.col(0).array() * lambda.col(1).array() * gradLambda[2].col(5).array();

        values[0].col(5) =
            gradLambda[0].col(3).array() * lambda.col(1).array() * lambda.col(5).array() +
            lambda.col(3).array() * gradLambda[0].col(1).array() * lambda.col(5).array() +
            lambda.col(3).array() * lambda.col(1).array() * gradLambda[0].col(5).array();
        values[1].col(5) =
            gradLambda[1].col(3).array() * lambda.col(1).array() * lambda.col(5).array() +
            lambda.col(3).array() * gradLambda[1].col(1).array() * lambda.col(5).array() +
            lambda.col(3).array() * lambda.col(1).array() * gradLambda[1].col(5).array();
        values[2].col(5) =
            gradLambda[2].col(3).array() * lambda.col(1).array() * lambda.col(5).array() +
            lambda.col(3).array() * gradLambda[2].col(1).array() * lambda.col(5).array() +
            lambda.col(3).array() * lambda.col(1).array() * gradLambda[2].col(5).array();

        values[0].col(6) =
            gradLambda[0].col(3).array() * lambda.col(4).array() * lambda.col(5).array() +
            lambda.col(3).array() * gradLambda[0].col(4).array() * lambda.col(5).array() +
            lambda.col(3).array() * lambda.col(4).array() * gradLambda[0].col(5).array();
        values[1].col(6) =
            gradLambda[1].col(3).array() * lambda.col(4).array() * lambda.col(5).array() +
            lambda.col(3).array() * gradLambda[1].col(4).array() * lambda.col(5).array() +
            lambda.col(3).array() * lambda.col(4).array() * gradLambda[1].col(5).array();
        values[2].col(6) =
            gradLambda[2].col(3).array() * lambda.col(4).array() * lambda.col(5).array() +
            lambda.col(3).array() * gradLambda[2].col(4).array() * lambda.col(5).array() +
            lambda.col(3).array() * lambda.col(4).array() * gradLambda[2].col(5).array();

        values[0].col(7) =
            gradLambda[0].col(0).array() * lambda.col(4).array() * lambda.col(5).array() +
            lambda.col(0).array() * gradLambda[0].col(4).array() * lambda.col(5).array() +
            lambda.col(0).array() * lambda.col(4).array() * gradLambda[0].col(5).array();
        values[1].col(7) =
            gradLambda[1].col(0).array() * lambda.col(4).array() * lambda.col(5).array() +
            lambda.col(0).array() * gradLambda[1].col(4).array() * lambda.col(5).array() +
            lambda.col(0).array() * lambda.col(4).array() * gradLambda[1].col(5).array();
        values[2].col(7) =
            gradLambda[2].col(0).array() * lambda.col(4).array() * lambda.col(5).array() +
            lambda.col(0).array() * gradLambda[2].col(4).array() * lambda.col(5).array() +
            lambda.col(0).array() * lambda.col(4).array() * gradLambda[2].col(5).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
}
