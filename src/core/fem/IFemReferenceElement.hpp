#ifndef __IFEMREFERENCEELEMENT_H
#define __IFEMREFERENCEELEMENT_H

#include "Eigen"
#include "Output.hpp"
#include "IReferenceElement.hpp"

using namespace Eigen;
using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Interface used to FEM computation on the reference element
  /// \author Fabio Vicini.
  /// \copyright See top level LICENSE file for details.
  class IFemReferenceElement : public IReferenceElement
  {
    public:
      virtual ~IFemReferenceElement() {}

      /// \return The number of basis fucntions
      virtual unsigned int NumberBasisFunctions() const = 0;

      /// \brief Evaluate the basis function on points
      /// \param points The points on which evaluate the basis.
      /// Its size is geometric dimension x number points
      /// \param values The evaluation result. Its size is number points x NumberBasisFunctions
      /// \return The computation result
      virtual Output::ExitCodes EvaluateBasisFunctions(const MatrixXd& points,
                                                       MatrixXd& values) const = 0;
      /// \brief Evaluate the basis function derivatives on points
      /// \param points The points on which evaluate the basis
      /// \param values The evaluation result. Its size is the geometric dimension of the element.
      /// Each element has size number points x NumberBasisFunctions
      /// \return The computation result
      virtual Output::ExitCodes EvaluateBasisFunctionDerivatives(const MatrixXd& points,
                                                                 vector<MatrixXd>& values) const = 0;
  };
}

#endif // __IFEMREFERENCEELEMENT_H
