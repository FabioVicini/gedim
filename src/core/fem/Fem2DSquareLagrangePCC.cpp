#include "Fem2DSquareLagrangePCC.hpp"

namespace GeDiM
{
  // ***************************************************************************
  void Fem2DSquareLagrangePCC::EvaluateLambda(const MatrixXd& points,
                                              MatrixXd& lambda) const
  {
    lambda.setZero(points.cols(), 4);

    lambda.col(0) = 1.0 - points.row(0).array();
    lambda.col(1) = 1.0 - points.row(1).array();
    lambda.col(2) = points.row(0);
    lambda.col(3) = points.row(1);
  }
  // ***************************************************************************
  void Fem2DSquareLagrangePCC::EvaluateGradLambda(const MatrixXd& points,
                                                  vector<MatrixXd>& gradLambda) const
  {
    gradLambda.resize(2);

    gradLambda[0].setZero(points.cols(), 4);
    gradLambda[1].setZero(points.cols(), 4);

    gradLambda[0].col(0).setConstant(-1.0);
    gradLambda[1].col(0).setZero();

    gradLambda[0].col(1).setZero();
    gradLambda[1].col(1).setConstant(-1.0);

    gradLambda[0].col(2).setOnes();
    gradLambda[1].col(2).setZero();

    gradLambda[0].col(3).setZero();
    gradLambda[1].col(3).setOnes();
  }
  // ***************************************************************************
  Fem2DSquareLagrangePCC::Fem2DSquareLagrangePCC(unsigned int& order)
  {
    _order = order;

    _numberBasisFunctions = 0;
    _numberDofs0D = 0;
    _numberDofs1D = 0;
    _numberDofs2D = 0;
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DSquareLagrangePCC::Initialize()
  {
    /// <ul>

    /// <li> Computing DOFs on the reference element

    switch (_order)
    {
      case 1:
        _numberDofs0D = 1;
        _numberDofs1D = 0;
        _numberDofs2D = 0;
        _numberBasisFunctions = 4;

        _referenceElementDofPositions.resize(Dimension(), _numberBasisFunctions);
        _referenceElementDofPositions.col(0)<< 0.0, 0.0;
        _referenceElementDofPositions.col(1)<< 1.0, 0.0;
        _referenceElementDofPositions.col(2)<< 1.0, 1.0;
        _referenceElementDofPositions.col(3)<< 0.0, 1.0;
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;

    /// </ul>
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DSquareLagrangePCC::EvaluateDofPositions(const IMapping& referenceElementMap,
                                                                 MatrixXd& dofPositions) const
  {
    if (_referenceElementDofPositions.size() == 0)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    return referenceElementMap.F(_referenceElementDofPositions, dofPositions);
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DSquareLagrangePCC::EvaluateBasisFunctions(const MatrixXd& points,
                                                                   MatrixXd& values) const
  {
    values.resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    switch (_order)
    {
      case 1:
        values.col(0) = lambda.col(0).array() * lambda.col(1).array();
        values.col(1) = lambda.col(2).array() * lambda.col(1).array();
        values.col(2) = lambda.col(2).array() * lambda.col(3).array();
        values.col(3) = lambda.col(0).array() * lambda.col(3).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DSquareLagrangePCC::EvaluateBasisFunctionDerivatives(const MatrixXd& points,
                                                                             vector<MatrixXd>& values) const
  {
    values.resize(Dimension());
    for (unsigned int d = 0; d < Dimension(); d++)
      values[d].resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    vector<MatrixXd> gradLambda;
    EvaluateGradLambda(points, gradLambda);

    switch (_order)
    {
      case 1:
        values[0].col(0) = gradLambda[0].col(0).array() * lambda.col(1).array() +
            lambda.col(0).array() * gradLambda[0].col(1).array();
        values[1].col(0) = gradLambda[1].col(0).array() * lambda.col(1).array() +
            lambda.col(0).array() * gradLambda[1].col(1).array();

        values[0].col(1) = gradLambda[0].col(2).array() * lambda.col(1).array() +
            lambda.col(2).array() * gradLambda[0].col(1).array();
        values[1].col(1) = gradLambda[1].col(2).array() * lambda.col(1).array() +
            lambda.col(2).array() * gradLambda[1].col(1).array();

        values[0].col(2) = gradLambda[0].col(2).array() * lambda.col(3).array() +
            lambda.col(2).array() * gradLambda[0].col(3).array();
        values[1].col(2) = gradLambda[1].col(2).array() * lambda.col(3).array() +
            lambda.col(2).array() * gradLambda[1].col(3).array();

        values[0].col(3) = gradLambda[0].col(0).array() * lambda.col(3).array() +
            lambda.col(0).array() * gradLambda[0].col(3).array();
        values[1].col(3) = gradLambda[1].col(0).array() * lambda.col(3).array() +
            lambda.col(0).array() * gradLambda[1].col(3).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
}
