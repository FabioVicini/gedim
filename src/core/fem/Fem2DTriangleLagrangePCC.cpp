#include "Fem2DTriangleLagrangePCC.hpp"

namespace GeDiM
{
  // ***************************************************************************
  void Fem2DTriangleLagrangePCC::EvaluateLambda(const MatrixXd& points,
                                        MatrixXd& lambda) const
  {
    lambda.setZero(points.cols(), 3);

    lambda.col(0) = 1.0 - points.row(0).array() - points.row(1).array();
    lambda.col(1) = points.row(0);
    lambda.col(2) = points.row(1);
  }
  // ***************************************************************************
  void Fem2DTriangleLagrangePCC::EvaluateGradLambda(const MatrixXd& points,
                                            vector<MatrixXd>& gradLambda) const
  {
    gradLambda.resize(2);

    gradLambda[0].setZero(points.cols(), 3);
    gradLambda[1].setZero(points.cols(), 3);

    gradLambda[0].col(0).setConstant(-1.0);
    gradLambda[1].col(0).setConstant(-1.0);

    gradLambda[0].col(1).setOnes();
    gradLambda[1].col(1).setZero();

    gradLambda[0].col(2).setZero();
    gradLambda[1].col(2).setOnes();
  }
  // ***************************************************************************
  Fem2DTriangleLagrangePCC::Fem2DTriangleLagrangePCC(unsigned int& order)
  {
    _order = order;

    _numberBasisFunctions = 0;
    _numberDofs0D = 0;
    _numberDofs1D = 0;
    _numberDofs2D = 0;
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DTriangleLagrangePCC::Initialize()
  {
    /// <ul>

    /// <li> Computing DOFs on the reference element

    unsigned int vertexCount = 0;
    vector<unsigned int> nodeDofs; ///< The degrees of freedom of the element on nodes
    vector<unsigned int> edgeDofs; ///< The degrees of freedom of the element on edges
    vector<unsigned int> cellDofs; ///< The degrees of freedom of the element on cell
    vector<unsigned int> nodeDofsIndex(4, 0); ///< Index of DOFS on nodes
    vector<unsigned int> edgeDofsIndex(4, 0); ///< Index of DOFS on edges
    vector<int> edgeOneTempDofs;
    vector<int> edgeTwoTempDofs;

    _numberDofs0D = 0;
    _numberDofs1D = 0;
    _numberDofs2D = 0;
    _numberBasisFunctions = (_order + 1) * (_order + 2) / 2;
    MatrixXd dofPositions(Dimension(), _numberBasisFunctions);

    for(unsigned int i = 0; i < _order + 1; i++)
    {
      for(unsigned int j = 0; j < _order - i + 1; j++)
      {
        if(_order > 0)
          dofPositions.col(vertexCount) << (double) j / _order, (double) i / _order;
        else
          dofPositions.col(vertexCount) << 0.5, 0.5;

        // Set dof Type
        if(dofPositions(0, vertexCount) == 0.0 && dofPositions(1, vertexCount) == 0.0)
        {
          nodeDofs.push_back(vertexCount);
          nodeDofsIndex[1]++;
        }
        else if(dofPositions(0, vertexCount) == 1.0 && dofPositions(1, vertexCount) == 0.0)
        {
          nodeDofs.push_back(vertexCount);
          nodeDofsIndex[2]++;
        }
        else if(dofPositions(0, vertexCount) == 0.0 && dofPositions(1, vertexCount) == 1.0)
        {
          nodeDofs.push_back(vertexCount);
          nodeDofsIndex[3]++;
        }
        else if(dofPositions(1, vertexCount) == 0.0)
        {
          edgeDofs.push_back(vertexCount);
          edgeDofsIndex[1]++;
        }
        else if(dofPositions(0, vertexCount) == 0.0)
        {
          edgeTwoTempDofs.push_back(vertexCount);
          edgeDofsIndex[3]++;
        }
        else if(dofPositions(0, vertexCount) + dofPositions(1, vertexCount) == 1.0)
        {
          edgeOneTempDofs.push_back(vertexCount);
          edgeDofsIndex[2]++;
        }
        else
          cellDofs.push_back(vertexCount);

        vertexCount++;
      }
    }

    for(unsigned int i = 0; i < edgeDofsIndex[2]; i++)
      edgeDofs.push_back(edgeOneTempDofs[i]);
    for(unsigned int i = 0; i < edgeDofsIndex[3]; i++)
      edgeDofs.push_back(edgeTwoTempDofs[i]);
    for(unsigned int i = 1; i < nodeDofsIndex.size(); i++)
      nodeDofsIndex[i] = nodeDofsIndex[i] + nodeDofsIndex[i - 1];
    for(unsigned int i = 1; i < edgeDofsIndex.size(); i++)
      edgeDofsIndex[i] = edgeDofsIndex[i] + edgeDofsIndex[i - 1];

    if(vertexCount != _numberBasisFunctions ||
       nodeDofs.size() != nodeDofsIndex.back() ||
       edgeDofs.size() != edgeDofsIndex.back())
    {
      Output::PrintErrorMessage("%s: Wrong initialization in FE reference element. Number of DOFs found is not correct.", false, __func__);
      return Output::GenericError;
    }

    /// <li> Reordering Dofs using convention [point, edge, cell]

    _numberDofs0D = nodeDofs.size() / 3;
    _numberDofs1D = edgeDofs.size() / 3;
    _numberDofs2D = cellDofs.size() / 3;
    _referenceElementDofPositions.resize(Dimension(), _numberBasisFunctions);

    unsigned int dofCounter = 0;
    for (unsigned int n = 0; n < 3; n++)
    {
      for (unsigned int i = nodeDofsIndex[n]; i < nodeDofsIndex[n + 1]; i++)
        _referenceElementDofPositions.col(dofCounter++) = dofPositions.col(nodeDofs[i]);
    }

    for (unsigned int e = 0; e < 3; e++)
    {
      for (unsigned int i = edgeDofsIndex[e]; i < edgeDofsIndex[e + 1]; i++)
        _referenceElementDofPositions.col(dofCounter++) = dofPositions.col(edgeDofs[i]);
    }

    for (unsigned int c = 0; c < cellDofs.size(); c++)
      _referenceElementDofPositions.col(dofCounter++) = dofPositions.col(cellDofs[c]);

    return Output::Success;

    /// </ul>
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DTriangleLagrangePCC::EvaluateDofPositions(const IMapping& referenceElementMap,
                                                           MatrixXd& dofPositions) const
  {
    if (_referenceElementDofPositions.size() == 0)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return Output::GenericError;
    }

    return referenceElementMap.F(_referenceElementDofPositions, dofPositions);
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DTriangleLagrangePCC::EvaluateBasisFunctions(const MatrixXd& points,
                                                             MatrixXd& values) const
  {
    values.resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    switch (_order)
    {
      case 1:
        values.col(0) = lambda.col(0);
        values.col(1) = lambda.col(1);
        values.col(2) = lambda.col(2);
      break;
      case 2:
        values.col(0) = 2.0 * lambda.col(0).array() * (lambda.col(0).array() - 0.5);
        values.col(1) = 2.0 * lambda.col(1).array() * (lambda.col(1).array() - 0.5);
        values.col(2) = 2.0 * lambda.col(2).array() * (lambda.col(2).array() - 0.5);
        values.col(3) = 4.0 * lambda.col(0).array() * lambda.col(1).array();
        values.col(4) = 4.0 * lambda.col(1).array() * lambda.col(2).array();
        values.col(5) = 4.0 * lambda.col(2).array() * lambda.col(0).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes Fem2DTriangleLagrangePCC::EvaluateBasisFunctionDerivatives(const MatrixXd& points,
                                                                       vector<MatrixXd>& values) const
  {
    values.resize(Dimension());
    for (unsigned int d = 0; d < Dimension(); d++)
      values[d].resize(points.cols(), NumberBasisFunctions());

    MatrixXd lambda;
    EvaluateLambda(points, lambda);

    vector<MatrixXd> gradLambda;
    EvaluateGradLambda(points, gradLambda);

    switch (_order)
    {
      case 1:
        values[0].col(0) = gradLambda[0].col(0);
        values[1].col(0) = gradLambda[1].col(0);

        values[0].col(1) = gradLambda[0].col(1);
        values[1].col(1) = gradLambda[1].col(1);

        values[0].col(2) = gradLambda[0].col(2);
        values[1].col(2) = gradLambda[1].col(2);
      break;
      case 2:
        values[0].col(0) = 2.0 * gradLambda[0].col(0).array() * (lambda.col(0).array() - 0.5) + 2.0 * lambda.col(0).array() * gradLambda[0].col(0).array();
        values[1].col(0) = 2.0 * gradLambda[1].col(0).array() * (lambda.col(0).array() - 0.5) + 2.0 * lambda.col(0).array() * gradLambda[1].col(0).array();

        values[0].col(1) = 2.0 * gradLambda[0].col(1).array() * (lambda.col(1).array() - 0.5) + 2.0 * lambda.col(1).array() * gradLambda[0].col(1).array();
        values[1].col(1) = 2.0 * gradLambda[1].col(1).array() * (lambda.col(1).array() - 0.5) + 2.0 * lambda.col(1).array() * gradLambda[1].col(1).array();

        values[0].col(2) = 2.0 * gradLambda[0].col(2).array() * (lambda.col(2).array() - 0.5) + 2.0 * lambda.col(2).array() * gradLambda[0].col(2).array();
        values[1].col(2) = 2.0 * gradLambda[1].col(2).array() * (lambda.col(2).array() - 0.5) + 2.0 * lambda.col(2).array() * gradLambda[1].col(2).array();

        values[0].col(3) = 4.0 * gradLambda[0].col(0).array() * lambda.col(1).array() + 4.0 * lambda.col(0).array() * gradLambda[0].col(1).array();
        values[1].col(3) = 4.0 * gradLambda[1].col(0).array() * lambda.col(1).array() + 4.0 * lambda.col(0).array() * gradLambda[1].col(1).array();

        values[0].col(4) = 4.0 * gradLambda[0].col(1).array() * lambda.col(2).array() + 4.0 * lambda.col(1).array() * gradLambda[0].col(2).array();
        values[1].col(4) = 4.0 * gradLambda[1].col(1).array() * lambda.col(2).array() + 4.0 * lambda.col(1).array() * gradLambda[1].col(2).array();

        values[0].col(5) = 4.0 * gradLambda[0].col(2).array() * lambda.col(0).array() + 4.0 * lambda.col(2).array() * gradLambda[0].col(0).array();
        values[1].col(5) = 4.0 * gradLambda[1].col(2).array() * lambda.col(0).array() + 4.0 * lambda.col(2).array() * gradLambda[1].col(0).array();
      break;
      default:
        Output::PrintErrorMessage("%s: order %d not supported yet", false, __func__, _order);
      return Output::UnimplementedMethod;
    }

    return Output::Success;
  }
  // ***************************************************************************
}
