#ifndef __FEMELLIPTICEQUATION_H
#define __FEMELLIPTICEQUATION_H

#include "IFemEllipticEquation.hpp"
#include "IFemValues.hpp"
#include "IDofHandler.hpp"

namespace GeDiM
{
  /// \brief Example of FEM Elliptic Differential Equation
  /// \copyright See top level LICENSE file for details.
  class FemEllipticEquation final : public IFemEllipticEquation
  {
    private:
      const IFemValues* _femValues; ///< FEM Values
      const IDofHandler* _dofHandler; ///< DofHandler Values
      IMapping* _referenceElementMap; ///< Reference element map

      vector< vector<const IPhysicalParameter*> > _diffusionTerm; ///< Local Diffusion term
      vector<const IPhysicalParameter*> _transportTerm; ///< Local Transport term
      const IPhysicalParameter* _reactionTerm; ///< Local Reaction term
      const IPhysicalParameter* _forcingTerm; ///< Local Forcing term
      const IBoundaryCondition* _dirichletBoundaryConditions; ///< Local Dirichlet Boundary Condition term
      const IBoundaryCondition* _neumannBoundaryConditions; ///< Local Neumann Boundary Condition term
      const IQuadrature* _neumannQuadrature; ///< Neumann Boundary Condition quadrature formula
      IMapping* _neumannBorderMap; ///< Neumann Boundary Condition border mapping

      /// \brief Compute the Neumann conditions
      Output::ExitCodes ComputeNeumann(const IMesh& mesh,
                                       const unsigned int& cellPostionId,
                                       const IDofHandler& dofHandler,
                                       const IMapping& referenceElementMap,
                                       const IFemValues& femValues,
                                       const IBoundaryCondition& neumannBoundaryConditions,
                                       const IQuadrature& neumannQuadrature,
                                       VectorXd& neumannTermValues) const;

      /// \brief Compute the Dirichlet conditions
      Output::ExitCodes ComputeDirichlet(const unsigned int& cellPostionId,
                                         const IDofHandler& dofHandler,
                                         const IBoundaryCondition& dirichletBoundaryConditions,
                                         VectorXd& dirichletTermValues) const;
    public:
      FemEllipticEquation(const IFemValues& femValues,
                          const IDofHandler& dofHandler,
                          IMapping& referenceElementMap);
      ~FemEllipticEquation();

      Output::ExitCodes Initialize() { return Output::Success; }

      Output::ExitCodes BuildLocalSystem(const IMesh& mesh,
                                         const unsigned int& cellPostionId,
                                         MatrixXd& cellMatrix,
                                         VectorXd& cellRightHandSide,
                                         VectorXd& cellDirichletTermValues) const;

      void SetDiffusionTerm(const IPhysicalParameter& diffusionTerm);
      void SetDiffusionTerm(const IPhysicalParameter& diffusionTerm,
                            const unsigned int& row,
                            const unsigned int& col)
      {
        _diffusionTerm[row][col] = &diffusionTerm;
      }
      void SetTransportTerm(const IPhysicalParameter& transportTerm);
      void SetTransportTerm(const IPhysicalParameter& transportTerm,
                            const unsigned int& position)
      {
        _transportTerm[position] = &transportTerm;
      }
      void SetReactionTerm(const IPhysicalParameter& reactionTerm) { _reactionTerm = &reactionTerm; }
      void SetForcingTerm(const IPhysicalParameter& forcingTerm) { _forcingTerm = &forcingTerm; }
      void SetDirichletBoundaryConditions(const IBoundaryCondition& dirichletBoundaryConditions)
      {
        _dirichletBoundaryConditions = &dirichletBoundaryConditions;
      }
      void SetNeumannBoundaryConditions(const IBoundaryCondition& neumannBoundaryConditions,
                                        const IQuadrature& neumannQuadrature,
                                        IMapping& neumannBorderMap)
      {
        _neumannBoundaryConditions = &neumannBoundaryConditions;
        _neumannQuadrature = &neumannQuadrature;
        _neumannBorderMap = &neumannBorderMap;
      }
  };

}

#endif
