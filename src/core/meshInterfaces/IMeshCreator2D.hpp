#ifndef __IMESHCREATOR2D_H
#define __IMESHCREATOR2D_H

#include "Polygon.hpp"
#include "IMesh.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Interface used to create mesh 2D
  /// \copyright See top level LICENSE file for details.
  class IMeshCreator2D
  {
    public:
      virtual ~IMeshCreator2D() { }

      /// \brief Set the dimension of the Marker
      virtual void SetMarkerDimension(const unsigned int& _markerDimension = 1) = 0;
      /// \brief Set the maximum cell size in the mesh
      virtual void SetMaximumCellSize(const double& _maximumCellSize) = 0;
      /// \brief Set the minimum number of cell in the mesh
      virtual void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells) = 0;
      /// \brief Set the contrained points of the mesh
      /// \param points the points ???
      virtual void SetContrainedPoints(const MatrixXd& points) = 0;
      /// \brief Set the Boundary Conditions for the mesh 2D
      /// \param _vertexMarkers the vertex markers
      /// \param _edgeMarkers the edge markers
      /// \param position the position of the _minimumNumberOfCells
      virtual void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                         const vector<unsigned int>& _edgeMarkers,
                                         const unsigned int& position = 0) = 0;
      /// \brief Create the Mesh from the segment domain
      /// \param domain the domain
      /// \param mesh the mesh created
      /// \return the result of the creation
      virtual Output::ExitCodes CreateMesh(const Polygon& domain,
                                           IMesh& mesh) = 0;
  };
}

#endif // __IMESHCREATOR2D_H
