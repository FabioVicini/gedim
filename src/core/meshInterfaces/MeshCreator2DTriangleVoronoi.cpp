#include <math.h>
#include <iomanip>
#include <sys/stat.h>

#include "MeshCreator2DTriangleVoronoi.hpp"
#include "Output.hpp"

using namespace MainApplication;

namespace GeDiM
{
  // ***************************************************************************
  MeshCreator2DTriangleVoronoi::MeshCreator2DTriangleVoronoi() : MeshCreator2DTriangleBase()
  {
    outputMeshPointer = NULL;
    voronoiMeshPointer = NULL;
    triangleOptions = "-Qzpqneva";
  }
  MeshCreator2DTriangleVoronoi::~MeshCreator2DTriangleVoronoi()
  {
    if (outputMeshPointer != NULL)
    {
      free(outputMeshPointer->pointlist);
      free(outputMeshPointer->pointattributelist);
      free(outputMeshPointer->pointmarkerlist);
      free(outputMeshPointer->trianglelist);
      free(outputMeshPointer->triangleattributelist);
      free(outputMeshPointer->trianglearealist);
      free(outputMeshPointer->neighborlist);
      free(outputMeshPointer->segmentlist);
      free(outputMeshPointer->segmentmarkerlist);
      free(outputMeshPointer->edgelist);
      free(outputMeshPointer->edgemarkerlist);
      delete outputMeshPointer; outputMeshPointer = NULL;
    }
    if(voronoiMeshPointer != NULL)
    {
      free(voronoiMeshPointer->pointlist);
      free(voronoiMeshPointer->pointattributelist);
      free(voronoiMeshPointer->segmentlist);
      free(voronoiMeshPointer->edgelist);
      free(voronoiMeshPointer->normlist);
      delete voronoiMeshPointer; voronoiMeshPointer = NULL;
    }
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator2DTriangleVoronoi::CreateTriangleOutput(const Polygon& domain)
  {
    if (minimumNumberOfCells == 0 && maximumCellSize <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the minimumNumberOfCells or minimumCellSize", false, __func__);
      return Output::GenericError;
    }

    if (inputMeshPointer == NULL)
    {
      Output::PrintErrorMessage("%s: No Triangle input in domain %d", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    if (minimumNumberOfCells > 0 && domain.Measure() <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the domain %d, no measure computed", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    const double& domainArea = domain.Measure();
    double cellArea = minimumNumberOfCells == 0 ? maximumCellSize : domainArea / (double)minimumNumberOfCells;

    ostringstream options;
    options.precision(16);
    options<< triangleOptions;
    options<< cellArea;
    size_t sizeOptions = options.str().size();
    char* optionPointer = new char[sizeOptions + 1];
    options.str().copy(optionPointer, sizeOptions);
    optionPointer[sizeOptions] = '\0';

    delete outputMeshPointer; outputMeshPointer = NULL;
    outputMeshPointer = new triangulateio();

    delete voronoiMeshPointer; voronoiMeshPointer = NULL;
    voronoiMeshPointer = new triangulateio();

    triangulate(optionPointer, inputMeshPointer, outputMeshPointer, voronoiMeshPointer);

    delete[] optionPointer;
    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator2DTriangleVoronoi::CreateMesh(const Polygon& domain,
                                                             IMesh&)
  {
    CreateTriangleInput(domain);
    CreateTriangleOutput(domain);

    /// <li>	Fill mesh structures
    ///
//    unsigned int numberOfCellMesh = voronoiMeshPointer->numberoftriangles;
    unsigned int numberOfEgdesMesh = voronoiMeshPointer->numberofedges;
    unsigned int numberOfSegmentMesh = voronoiMeshPointer->numberofsegments;
    unsigned int numberOfPointsMesh = voronoiMeshPointer->numberofpoints;
//    unsigned int numberOfRegions = voronoiMeshPointer->numberofregions;
    for (unsigned int p = 0; p < numberOfPointsMesh; p++)
    {
      Vector3d point2d(voronoiMeshPointer->pointlist[2 * p], voronoiMeshPointer->pointlist[2 * p + 1], 0.0);
    }
    for (unsigned int ed = 0; ed < numberOfEgdesMesh; ed++)
    {
//      unsigned int idZero = voronoiMeshPointer->edgelist[2 * ed];
//      unsigned int idOne = voronoiMeshPointer->edgelist[2 * ed + 1];

    }
    for (unsigned int seg = 0; seg < numberOfSegmentMesh; seg++)
    {
//      unsigned int idZero = voronoiMeshPointer->segmentlist[2 * seg];
//      unsigned int idOne = voronoiMeshPointer->segmentlist[2 * seg + 1];
      Vector3d point2d(voronoiMeshPointer->normlist[2 * seg], voronoiMeshPointer->normlist[2 * seg + 1], 0.0);

    }

    return Output::Success;
  }
  // ***************************************************************************
}
