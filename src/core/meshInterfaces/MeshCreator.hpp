#ifndef __MESHCREATOR_H
#define __MESHCREATOR_H

#include "Output.hpp"
#include "Eigen"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreator
  {
    protected:
      double maximumCellSize; ///< Size of the minimum cell of the mesh
      unsigned int minimumNumberOfCells; ///< Minimum number of cell of the mesh
      unsigned int markerDimension; ///< Minimum number of cell of the mesh

      vector<vector<unsigned int> > vertexMarkers; ///< Vector of boundary conditions of domain vertices per equation
      vector<vector<unsigned int> > edgeMarkers; ///< Vector of boundary conditions of domain edges per equation
      vector<vector<unsigned int> > faceMarkers; ///< Vector of boundary conditions of domain faces per equation

      vector<int> unidimensionalVertexMarkers;
      vector<int> unidimensionalEdgeMarkers;
      vector<int> unidimensionalFaceMarkers;
      vector< vector<unsigned int> > mappedMarkers;
      MatrixXd constrainedPoints;

      Output::ExitCodes CreateUniDimMarkers();
      Output::ExitCodes CreateMappedMarkers();

      const vector<unsigned int>& VertexMarkers(const unsigned int& position = 0) const { return vertexMarkers[position]; }
      const vector<unsigned int>& EdgeMarkers(const unsigned int& position = 0) const { return edgeMarkers[position]; }
      const vector<unsigned int>& FaceMarkers(const unsigned int& position = 0) const { return faceMarkers[position]; }

      const vector<unsigned int> MarkersPerVertex(const unsigned int& position) const;
      const vector<unsigned int> MarkersPerEdge(const unsigned int& position) const;
      const vector<unsigned int> MarkersPerFace(const unsigned int& position) const;

      void MC_SetContrainedPoints(const MatrixXd& points) { constrainedPoints = points; }

      void MC_SetMarkerDimension(const unsigned int& _markerDimension = 1);
      void MC_SetMaximumCellSize(const double& _maximumCellSize) { maximumCellSize = _maximumCellSize; }
      void MC_SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells) { minimumNumberOfCells = _minimumNumberOfCells; }

      void MC_SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                    const unsigned int& position = 0);
      void MC_SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                    const vector<unsigned int>& _edgeMarkers,
                                    const unsigned int& position = 0);
      void MC_SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                    const vector<unsigned int>& _edgeMarkers,
                                    const vector<unsigned int>& _faceMarkers,
                                    const unsigned int& position = 0);

    public:
      MeshCreator();
      virtual ~MeshCreator();
  };
}

#endif // __MESHCREATOR_H
