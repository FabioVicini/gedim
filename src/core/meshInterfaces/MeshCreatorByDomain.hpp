#ifndef MESHCREATORBYDOMAIN_H
#define MESHCREATORBYDOMAIN_H

#include "Polyhedron.hpp"
#include "Mesh.hpp"
#include "IMeshCreator1D.hpp"
#include "IMeshCreator2D.hpp"
#include "IMeshCreator3D.hpp"
#include "MeshCreator.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreatorByDomain;

  class MeshCreatorByDomain : public MeshCreator, public IMeshCreator1D, public IMeshCreator2D, public IMeshCreator3D
	{
		protected:
      Output::ExitCodes CreateMesh1D(const Segment& domain, IMesh& mesh) const;
      Output::ExitCodes CreateMesh2D(const Polygon& domain, IMesh& mesh) const;
      Output::ExitCodes CreateMesh3D(const Polyhedron& domain, IMesh& mesh) const;

    public:
      MeshCreatorByDomain();
      virtual ~MeshCreatorByDomain();

      void SetMarkerDimension(const unsigned int& _markerDimension) { return MC_SetMarkerDimension(_markerDimension); }
      void SetMaximumCellSize(const double& _maximumCellSize) { return MC_SetMaximumCellSize(_maximumCellSize); }
      void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells) { return MC_SetMinimumNumberOfCells(_minimumNumberOfCells); }
      void SetContrainedPoints(const MatrixXd& points) { return MC_SetContrainedPoints(points); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const unsigned int& position) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                 position); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const vector<unsigned int>& _edgeMarkers,
                                 const unsigned int& position) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                 _edgeMarkers,
                                                                                                 position); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const vector<unsigned int>& _edgeMarkers,
                                 const vector<unsigned int>& _faceMarkers,
																 const unsigned int& position) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                 _edgeMarkers,
                                                                                                 _faceMarkers,
                                                                                                 position); }
      Output::ExitCodes CreateMesh(const Polyhedron& domain, IMesh& mesh);

      Output::ExitCodes CreateMesh(const Polygon& domain, IMesh& mesh);

      Output::ExitCodes CreateMesh(const Segment& domain, IMesh& mesh);
  };
}
#endif // MESHCREATORBYDOMAIN_H
