#ifndef __MESHCREATOR1DEQUISPACED_H
#define __MESHCREATOR1DEQUISPACED_H

#include "MeshCreator.hpp"
#include "IMeshCreator1D.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreator1DEquispaced : public MeshCreator, public IMeshCreator1D
  {
    public:
      MeshCreator1DEquispaced();
      virtual ~MeshCreator1DEquispaced();

      void SetMarkerDimension(const unsigned int& _markerDimension) { return MC_SetMarkerDimension(_markerDimension); }
      void SetMaximumCellSize(const double& _maximumCellSize) { return MC_SetMaximumCellSize(_maximumCellSize); }
      void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells) { return MC_SetMinimumNumberOfCells(_minimumNumberOfCells); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const unsigned int& position = 0) { return MC_SetBoundaryConditions(_vertexMarkers, position); }

      Output::ExitCodes CreateMesh(const Segment& domain,
                                   IMesh& mesh);
  };
}

#endif // __MESHCREATOR1DEQUISPACED_H
