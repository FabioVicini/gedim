#include <math.h>

#include "MeshCreator.hpp"

using namespace MainApplication;

namespace GeDiM
{
  // ***************************************************************************
  MeshCreator::MeshCreator()
  {
    maximumCellSize = 0.0;
    minimumNumberOfCells = 0;
    markerDimension = 1;
    vertexMarkers.resize(1);
    edgeMarkers.resize(1);
    faceMarkers.resize(1);
  }
  MeshCreator::~MeshCreator()
  {
  }
  // ***************************************************************************
  void MeshCreator::MC_SetMarkerDimension(const unsigned int& _markerDimension)
  {
    markerDimension = _markerDimension;
    vertexMarkers.resize(_markerDimension);
    edgeMarkers.resize(_markerDimension);
    faceMarkers.resize(_markerDimension);
  }
  // ***************************************************************************
  void MeshCreator::MC_SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers, const unsigned int& position)
  {
    if (_vertexMarkers.size() > 0)
    {
      vertexMarkers[position].resize(_vertexMarkers.size());
      memcpy(&vertexMarkers[position][0], &_vertexMarkers[0], _vertexMarkers.size() * sizeof(unsigned int));
    }

    edgeMarkers.clear();
    faceMarkers.clear();

    if(position == 0)
      CreateUniDimMarkers();
  }
  // ***************************************************************************
  void MeshCreator::MC_SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers, const vector<unsigned int>& _edgeMarkers, const unsigned int& position)
  {
    if (_vertexMarkers.size() > 0)
    {
      vertexMarkers[position].resize(_vertexMarkers.size());
      memcpy(&vertexMarkers[position][0], &_vertexMarkers[0], _vertexMarkers.size() * sizeof(unsigned int));
    }

    if (_edgeMarkers.size() > 0)
    {
      edgeMarkers[position].resize(_edgeMarkers.size());
      memcpy(&edgeMarkers[position][0], &_edgeMarkers[0], _edgeMarkers.size() * sizeof(unsigned int));
    }
    faceMarkers.clear();

    if(position == 0)
      CreateUniDimMarkers();
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator::CreateUniDimMarkers()
  {

    unsigned int sizeVertexMarker = vertexMarkers[0].size();
    unsigned int sizeVertexMarkerConstrained = constrainedPoints.rows();
    unsigned int totalSizeVertexMarker = sizeVertexMarker + sizeVertexMarkerConstrained;

    unsigned int sizeEdgeMarker = edgeMarkers[0].size();

    unsigned int sizeFaceMarker = faceMarkers[0].size();

    unidimensionalVertexMarkers.resize(totalSizeVertexMarker);
    for(unsigned int val = 0; val < sizeVertexMarker; val++)
      unidimensionalVertexMarkers[val] = val + 1;
    for(unsigned int val = 0; val < sizeVertexMarkerConstrained; val++)
      unidimensionalVertexMarkers[val + sizeVertexMarker] = val + sizeVertexMarker + 1;

    unidimensionalEdgeMarkers.resize(sizeEdgeMarker);
    for(unsigned int val = 0; val < sizeEdgeMarker; val++)
      unidimensionalEdgeMarkers[val] = val + 1 + totalSizeVertexMarker;


    unidimensionalFaceMarkers.resize(sizeFaceMarker);
    for(unsigned int val = 0; val < sizeFaceMarker; val++)
      unidimensionalFaceMarkers[val] = val + 1 + totalSizeVertexMarker + sizeEdgeMarker;

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator::CreateMappedMarkers()
  {
    unsigned int sizeVertexMarker = vertexMarkers[0].size();
    unsigned int sizeVertexMarkerConstrained = constrainedPoints.rows();
    unsigned int totalSizeVertexMarker = sizeVertexMarker + sizeVertexMarkerConstrained;

    unsigned int sizeEdgeMarker = edgeMarkers[0].size();

    unsigned int sizeFaceMarker = faceMarkers[0].size();

    unsigned int sizeTotalMarkers = sizeEdgeMarker + sizeFaceMarker + totalSizeVertexMarker;

    mappedMarkers.resize(sizeTotalMarkers);
    for(unsigned int val = 0; val < sizeTotalMarkers; val++)
      mappedMarkers[val] = vector<unsigned int>(markerDimension, 0);

    for(unsigned int val = 0; val < sizeVertexMarker; val++)
      mappedMarkers[val] = MarkersPerVertex(val);

    for(unsigned int val = 0; val < sizeEdgeMarker; val++)
      mappedMarkers[val + sizeVertexMarker + sizeVertexMarkerConstrained] = MarkersPerEdge(val);

    for(unsigned int val = 0; val < sizeFaceMarker; val++)
      mappedMarkers[val + sizeVertexMarker  + sizeVertexMarkerConstrained + sizeEdgeMarker] = MarkersPerFace(val);


    return Output::Success;
  }
  // ***************************************************************************
  void MeshCreator::MC_SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers, const vector<unsigned int>& _edgeMarkers, const vector<unsigned int>& _faceMarkers, const unsigned int& position)
  {
    if (_vertexMarkers.size() > 0)
    {
      vertexMarkers[position].resize(_vertexMarkers.size());
      memcpy(&vertexMarkers[position][0], &_vertexMarkers[0], _vertexMarkers.size() * sizeof(unsigned int));
    }

    if (_edgeMarkers.size() > 0)
    {
      edgeMarkers[position].resize(_edgeMarkers.size());
      memcpy(&edgeMarkers[position][0], &_edgeMarkers[0], _edgeMarkers.size() * sizeof(unsigned int));
    }

    if (_faceMarkers.size() > 0)
    {
      faceMarkers[position].resize(_faceMarkers.size());
      memcpy(&faceMarkers[position][0], &_faceMarkers[0], _faceMarkers.size() * sizeof(unsigned int));
    }

    if(position == 0)
      CreateUniDimMarkers();
  }
  // ***************************************************************************
  const vector<unsigned int> MeshCreator::MarkersPerVertex(const unsigned int& position) const
  {
    vector<unsigned int> temp(markerDimension, 0);
    for(unsigned int numMark = 0; numMark < markerDimension; numMark++)
      temp[numMark] = vertexMarkers[numMark][position];

    return temp;
  }
  // ***************************************************************************
  const vector<unsigned int> MeshCreator::MarkersPerEdge(const unsigned int& position) const
  {
    vector<unsigned int> temp(markerDimension, 0);
    for(unsigned int numMark = 0; numMark < markerDimension; numMark++)
      temp[numMark] = edgeMarkers[numMark][position];

    return temp;
  }
  // ***************************************************************************
  const vector<unsigned int> MeshCreator::MarkersPerFace(const unsigned int& position) const
  {
    vector<unsigned int> temp(markerDimension, 0);
    for(unsigned int numMark = 0; numMark < markerDimension; numMark++)
      temp[numMark] = faceMarkers[numMark][position];

    return temp;
  }
  // ***************************************************************************
}
