#ifndef __MESHCREATOR3DTETGEN_H
#define __MESHCREATOR3DTETGEN_H

#include "MeshCreator.hpp"
#include "IMeshCreator3D.hpp"
#include "tetgen.h"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreator3DTetgen : public MeshCreator, public IMeshCreator3D
  {
    protected:
      tetgenio* inputMeshPointer;
      tetgenio* outputMeshPointer;

      string tetgenOptions;

      // http://wias-berlin.de/software/tetgen/files/tetcall.cxx
      void SetInputMesh(tetgenio* _inputMeshPointer) { inputMeshPointer = _inputMeshPointer; }
      void SetOutputMesh(tetgenio* _outputMeshPointer) { outputMeshPointer = _outputMeshPointer; }
      void SetTetgenOptions(const string& _tetgenOptions) { tetgenOptions = _tetgenOptions; }

      Output::ExitCodes CreateTetgenInput(const Polyhedron& domain);
      Output::ExitCodes CreateTetgenOutput(const Polyhedron& domain);

    public:
      MeshCreator3DTetgen();
      virtual ~MeshCreator3DTetgen();

      void SetMarkerDimension(const unsigned int& _markerDimension) { return MC_SetMarkerDimension(_markerDimension); }
      void SetMaximumCellSize(const double& _maximumCellSize) { return MC_SetMaximumCellSize(_maximumCellSize); }
      void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells)  { return MC_SetMinimumNumberOfCells(_minimumNumberOfCells); }
      void SetConstrainedPoints(const MatrixXd& points) { return MC_SetContrainedPoints(points); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const vector<unsigned int>& _edgeMarkers,
                                 const vector<unsigned int>& _faceMarkers,
                                 const unsigned int& position = 0) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                     _edgeMarkers,
                                                                                                     _faceMarkers,
                                                                                                     position); }
      Output::ExitCodes CreateMesh(const Polyhedron& domain,
                                   IMesh& mesh);

      Output::ExitCodes ExportTetgenMesh(const string& nameFolder, const string& nameFile) const;
  };

}

#endif // __MESHCREATOR3DTETGEN_H
