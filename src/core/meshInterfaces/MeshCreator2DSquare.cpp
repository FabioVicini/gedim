#include "MeshCreator2DSquare.hpp"
#include "Output.hpp"
#include "Utilities.hpp"

using namespace MainApplication;

namespace GeDiM
{
  // ***************************************************************************
  GeDiM::MeshCreator2DSquare::MeshCreator2DSquare()
  {

  }
  MeshCreator2DSquare::~MeshCreator2DSquare()
  {

  }
  // ***************************************************************************
  void MeshCreator2DSquare::SetPointMarker(Cell0D& point,
                                           unsigned int numPoints1D,
                                           unsigned int i,
                                           unsigned int j)
  {
    if (!vertexMarkers.empty() && !edgeMarkers.empty())
    {
      vector<unsigned int> pointMarkers(markerDimension, 0);

      if (i == 0 && j == 0)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = vertexMarkers[d][0];
      }
      else if (i == 0 && j == numPoints1D - 1)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = vertexMarkers[d][1];
      }
      else if (i == numPoints1D - 1 && j == numPoints1D - 1)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = vertexMarkers[d][2];
      }
      else if (i == numPoints1D - 1 && j == 0)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = vertexMarkers[d][3];
      }
      else if (i == 0 && j > 0 && j < numPoints1D - 1)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = edgeMarkers[d][0];
      }
      else if (i > 0 && i < numPoints1D - 1 && j == numPoints1D - 1)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = edgeMarkers[d][1];
      }
      else if (i == numPoints1D - 1 && j > 0 && j < numPoints1D - 1)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = edgeMarkers[d][2];
      }
      else if (i > 0 && i < numPoints1D - 1 && j == 0)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          pointMarkers[d] = edgeMarkers[d][3];
      }

      point.SetMarkers(pointMarkers);
    }
    else
      point.SetMarkers(vector<unsigned int>(markerDimension, 0));
  }

  void MeshCreator2DSquare::SetEdgeMarker(Cell1D& edge,
                                          unsigned int numPoints1D,
                                          unsigned int originI,
                                          unsigned int originJ,
                                          unsigned int endI,
                                          unsigned int endJ)
  {
    if (!edgeMarkers.empty())
    {
      vector<unsigned int> edgeMarker(markerDimension, 0);

      if (originI == 0 && endI == 0)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          edgeMarker[d] = edgeMarkers[d][0];
      }
      else if (originJ == numPoints1D - 1 && endJ == numPoints1D - 1)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          edgeMarker[d] = edgeMarkers[d][1];
      }
      else if (originI == numPoints1D - 1 && endI == numPoints1D - 1)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          edgeMarker[d] = edgeMarkers[d][2];
      }
      else if (originJ == 0 && endJ == 0)
      {
        for (unsigned int d = 0; d < markerDimension; d++)
          edgeMarker[d] = edgeMarkers[d][3];
      }

      edge.SetMarkers(edgeMarker);
    }
    else
      edge.SetMarkers(vector<unsigned int>(markerDimension, 0));
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator2DSquare::CreateMesh(const Polygon& domain,
                                                    IMesh& mesh)
  {
    if (domain.Type() != Polygon::Square)
    {
      Output::PrintErrorMessage("%s: domain shall be a square", false, __func__);
      return Output::GenericError;
    }

    if (minimumNumberOfCells == 0 && maximumCellSize <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the minimumNumberOfCells or minimumCellSize", false, __func__);
      return Output::GenericError;
    }

    if (minimumNumberOfCells > 0 && domain.Measure() <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the domain %d, no measure computed", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    const double& domainArea = domain.Measure();
    double numCell1D = minimumNumberOfCells == 0 ? ceil(sqrt(domainArea / maximumCellSize)) : ceil(sqrt(minimumNumberOfCells));
    double domainEdgeLenght = sqrt(domainArea);
    double meshEdgeLenght = domainEdgeLenght / (double)numCell1D;

    list<double> mesh1DList;
    double totLenght = 0.0;
    while (totLenght < domainEdgeLenght - meshEdgeLenght / 2.0)
    {
      mesh1DList.push_back(totLenght);
      totLenght += meshEdgeLenght;
    }
    mesh1DList.push_back(domainEdgeLenght);
    unsigned int numPoints1D = mesh1DList.size();
    vector<double> mesh1D(mesh1DList.begin(), mesh1DList.end());
    mesh1DList.clear();

    unsigned int numPoints2D = numPoints1D * numPoints1D;
    unsigned int numEdges2D = 2 * (numPoints1D - 1) * numPoints1D;
    unsigned int numCells2D = (numPoints1D - 1) * (numPoints1D - 1);

    mesh.SetDimension(2);
    mesh.InitializeCells0D(numPoints2D);
    mesh.InitializeCells1D(numEdges2D);
    mesh.InitializeCells2D(numCells2D);

    vector<Cell0D*> meshPoints(numPoints2D, nullptr);
    vector<Cell1D*> meshEdges(numEdges2D, nullptr);
    vector<Cell2D*> meshCells(numCells2D, nullptr);

    const Point& squareOrigin = *domain.Vertex(0);

    unsigned int counterEdge = 0, edgeStartingIndex = 0, counterCell = 0;
    for (unsigned int i = 0; i < numPoints1D; i++)
    {
      const double y = mesh1D[i] + squareOrigin.x();
      for (unsigned int j = 0; j < numPoints1D; j++)
      {
        const unsigned int counterPoint = i * numPoints1D + j;
        const double x = mesh1D[j] + squareOrigin.y();

        meshPoints[counterPoint] = mesh.CreateCell0D();
        mesh.AddCell0D(meshPoints[counterPoint]);

        Cell0D& point = *meshPoints[counterPoint];
        point.SetCoordinates(x, y);

        SetPointMarker(point, numPoints1D, i, j);

        if (j > 0)
        {
          Cell0D& origin = *meshPoints[counterPoint - 1];
          Cell0D& end = point;
          meshEdges[counterEdge] = mesh.CreateCell1D();
          mesh.AddCell1D(meshEdges[counterEdge]);

          Cell1D& edge = *meshEdges[counterEdge++];
          edge.AddVertex(origin);
          edge.AddVertex(end);
          SetEdgeMarker(edge, numPoints1D, i, j - 1, i, j);
        }

        if (i > 0)
        {
          Cell0D& origin = point;
          Cell0D& end = *meshPoints[(i - 1) * numPoints1D + j];
          meshEdges[counterEdge] = mesh.CreateCell1D();
          mesh.AddCell1D(meshEdges[counterEdge]);

          Cell1D& edge = *meshEdges[counterEdge++];
          edge.AddVertex(origin);
          edge.AddVertex(end);
          SetEdgeMarker(edge, numPoints1D, i, j, i - 1, j);
        }

        if (i > 0 && j > 0)
        {
          Cell0D& point1 = *meshPoints[(i - 1) * numPoints1D + (j - 1)];
          Cell0D& point2 = *meshPoints[(i - 1) * numPoints1D + j];
          Cell0D& point3 = point;
          Cell0D& point4 = *meshPoints[i * numPoints1D + (j - 1)];
          Cell1D& edge1 = *meshEdges[edgeStartingIndex + (j - 1)];
          Cell1D& edge2 = *meshEdges[counterEdge - 1];
          Cell1D& edge3 = *meshEdges[counterEdge - 2];
          Cell1D& edge4 = *meshEdges[counterEdge - 3];

          meshCells[counterCell] = mesh.CreateCell2D();
          mesh.AddCell2D(meshCells[counterCell]);

          Cell2D& cell = *meshCells[counterCell++];

          cell.Set2DPolygon();
          cell.SetType(Polygon::Square);
          cell.SetMarkers(vector<unsigned int>(markerDimension, 0));

          cell.AllocateVertices(4);
          cell.InsertVertex(point1, 0);
          cell.InsertVertex(point2, 1);
          cell.InsertVertex(point3, 2);
          cell.InsertVertex(point4, 3);

          cell.AllocateEdges(4);
          cell.InsertEdge(edge1, 0);
          cell.InsertEdge(edge2, 1);
          cell.InsertEdge(edge3, 2);
          cell.InsertEdge(edge4, 3);
          cell.SetNormalSign(true, 0);
          cell.SetNormalSign(true, 1);
          cell.SetNormalSign(false, 2);
          cell.SetNormalSign(false, 3);

          cell.Compute2DPolygonProperties();
        }
      }
      edgeStartingIndex = counterEdge - (numPoints1D - 1) - (numPoints1D - 1) * (i > 0);
    }

    return Output::Success;
  }
  // ***************************************************************************
}
