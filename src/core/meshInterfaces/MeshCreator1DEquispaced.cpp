#include <math.h>

#include "MeshCreator1DEquispaced.hpp"

using namespace MainApplication;

namespace GeDiM
{
  // ***************************************************************************
  MeshCreator1DEquispaced::MeshCreator1DEquispaced()
  {
  }
  MeshCreator1DEquispaced::~MeshCreator1DEquispaced()
  {
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator1DEquispaced::CreateMesh(const Segment& domain,
                                              IMesh& mesh)
  {
    /// <ul>
    if (minimumNumberOfCells == 0 && maximumCellSize <= 0)
    {
      Output::PrintErrorMessage("Wrong initialization of the minimumNumberOfCells or minimumCellSize", false);
      return Output::GenericError;
    }

    if (minimumNumberOfCells > 0 && domain.Measure() == 0)
    {
      Output::PrintErrorMessage("Wrong initialization of the domain %d", false, domain.GlobalId());
      return Output::GenericError;
    }

    const double& domainLength = domain.Measure();

    if (domainLength <= 0.0)
    {
      Output::PrintErrorMessage("%s: domain %d length not computed", false, __func__, domain.Id());
      return Output::GenericError;
    }

    double cellLength = minimumNumberOfCells == 0 ? maximumCellSize : domainLength / (double)minimumNumberOfCells;
    unsigned int numberOfCells = ceil(domainLength / cellLength);
    double coordinateCurvilinear = cellLength;
    unsigned int numberOfPoints = numberOfCells + 1;

    /// <li>	Fill mesh structures
    mesh.SetDimension(1);
    mesh.InitializeCells1D(numberOfCells);
    mesh.InitializeCells0D(numberOfPoints);

    vector<Cell1D*> cells(numberOfCells, NULL);
    vector<Cell0D*> points(numberOfPoints, NULL);

    Vector3d normalizedTangent;
    domain.ComputeTangent(normalizedTangent);
    /// <li> Set Points
    for (unsigned int p = 0; p < numberOfPoints; p++)
    {
      points[p] = mesh.CreateCell0D();

      Cell0D* point = points[p];
      double pointCoordinate = (p * coordinateCurvilinear > domainLength) ? domainLength : p * coordinateCurvilinear;
      point->SetCoordinates(domain.Origin() + pointCoordinate*normalizedTangent );
      point->AllocateNeighbourhood1D(2);

      if (unidimensionalVertexMarkers.size() > 0 && p == 0)
        point->SetMarkers(MarkersPerVertex(0));
      else if (unidimensionalVertexMarkers.size() > 1 && p == (numberOfPoints - 1))
        point->SetMarkers(MarkersPerVertex(1));
      else
        point->SetMarkers(vector<unsigned int>(markerDimension, 0));

      mesh.AddCell0D(point);
    }

    /// <li> Set Cells
    for (unsigned int c = 0; c < numberOfCells; c++)
    {
      cells[c] = mesh.CreateCell1D();

      Cell1D* cell = cells[c];
      cell->AllocateVertices(2);
      cell->AllocateNeighbourhood1D(2);
      cell->SetMarkers(vector<unsigned int>(markerDimension, 0));

      Cell0D& point = *points[c];
      cell->InsertVertex(point, 0);
      point.InsertNeighCell1D(*cell, 1);

      Cell0D& point2 = *points[c + 1];
      cell->InsertVertex(point2, 1);
      point2.InsertNeighCell1D(*cell, 0);

      mesh.AddCell1D(cell);
    }

    /// <li> Insert Cell Neighbour
    if (numberOfCells > 1)
    {
      for (unsigned int c = 0; c < numberOfCells; c++)
      {
        Cell1D* cell = cells[c];

        if (c == 0)
        {
          cell->InsertNeighCell1D(*cells[1], 1);
        }
        else if (c == (numberOfCells - 1))
        {
          cell->InsertNeighCell1D(*cells[numberOfCells - 2], 0);
        }
        else
        {
          cell->InsertNeighCell1D(*cells[c - 1], 0);
          cell->InsertNeighCell1D(*cells[c + 1], 1);
        }
      }
    }

    return Output::Success;
  }
  // ***************************************************************************
}
