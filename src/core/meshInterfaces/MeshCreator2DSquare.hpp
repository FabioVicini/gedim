#ifndef __MESHCREATOR2DSQUARE_H
#define __MESHCREATOR2DSQUARE_H

#include "IMeshCreator2D.hpp"
#include "MeshCreator.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreator2DSquare : public MeshCreator, public IMeshCreator2D
  {
    protected:

      void SetPointMarker(Cell0D& point,
                          unsigned int numPoints1D,
                          unsigned int i,
                          unsigned int j);
      void SetEdgeMarker(Cell1D& edge,
                         unsigned int numPoints1D,
                         unsigned int originI,
                         unsigned int originJ,
                         unsigned int endI,
                         unsigned int endJ);
    public:
      MeshCreator2DSquare();
      virtual ~MeshCreator2DSquare();

      void SetMarkerDimension(const unsigned int& _markerDimension) { return MC_SetMarkerDimension(_markerDimension); }
      void SetMaximumCellSize(const double& _maximumCellSize) { return MC_SetMaximumCellSize(_maximumCellSize); }
      void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells)  { return MC_SetMinimumNumberOfCells(_minimumNumberOfCells); }
      void SetContrainedPoints(const MatrixXd& points) { return MC_SetContrainedPoints(points); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const vector<unsigned int>& _edgeMarkers,
                                 const unsigned int& position = 0) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                     _edgeMarkers,
                                                                                                     position); }

      Output::ExitCodes CreateMesh(const Polygon& domain,
                                   IMesh& mesh);
  };
}

#endif // __MESHCREATOR2DSQUARE_H

