#include <math.h>
#include <iomanip>
#include <sys/stat.h>

#include "MeshCreator2DTriangle.hpp"
#include "Output.hpp"

using namespace MainApplication;

namespace GeDiM
{
  MeshCreator2DTriangleBase::MeshCreator2DTriangleBase() : MeshCreator()
  {
    inputMeshPointer = NULL;
  }

  MeshCreator2DTriangleBase::~MeshCreator2DTriangleBase()
  {
    if (inputMeshPointer != NULL)
    {
      delete[] inputMeshPointer->pointlist; inputMeshPointer->pointlist = NULL;
      delete[] inputMeshPointer->pointmarkerlist; inputMeshPointer->pointmarkerlist = NULL;
      delete[] inputMeshPointer->segmentlist; inputMeshPointer->segmentlist = NULL;
      delete[] inputMeshPointer->segmentmarkerlist; inputMeshPointer->segmentmarkerlist = NULL;
    }
    delete inputMeshPointer; inputMeshPointer = NULL;
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator2DTriangleBase::CreateTriangleInput(const Polygon& domain)
  {
    delete inputMeshPointer; inputMeshPointer = NULL;

    const unsigned int& numberOfRotatedVertices = domain.NumberOfRotatedVertices();
    const unsigned int& numberOfConstrainedPoints = constrainedPoints.rows();
    const unsigned int& numberOfEdges = domain.NumberOfEdges();

    if (numberOfRotatedVertices == 0 || numberOfEdges == 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the domain %d, no rotated vertices or edges", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    inputMeshPointer = new triangulateio();

    inputMeshPointer->pointlist = new double[2 * (numberOfRotatedVertices + numberOfConstrainedPoints)];
    inputMeshPointer->pointattributelist = NULL;
    inputMeshPointer->pointmarkerlist = new int[numberOfRotatedVertices + numberOfConstrainedPoints];
    inputMeshPointer->numberofpoints = numberOfRotatedVertices + numberOfConstrainedPoints;
    inputMeshPointer->numberofpointattributes = 0;
    inputMeshPointer->numberofsegments = numberOfRotatedVertices;
    inputMeshPointer->trianglelist = NULL;
    inputMeshPointer->triangleattributelist = NULL;
    inputMeshPointer->trianglearealist = NULL;
    inputMeshPointer->neighborlist = NULL;
    inputMeshPointer->numberoftriangles = 0;
    inputMeshPointer->numberofcorners = 0;
    inputMeshPointer->numberoftriangleattributes = 0;
    inputMeshPointer->segmentlist = new int[2 * numberOfRotatedVertices];
    inputMeshPointer->segmentmarkerlist = new int[numberOfRotatedVertices];
    inputMeshPointer->holelist = NULL;
    inputMeshPointer->numberofholes = 0;
    inputMeshPointer->regionlist = NULL;
    inputMeshPointer->numberofregions = 0;
    inputMeshPointer->edgelist = NULL;
    inputMeshPointer->edgemarkerlist = NULL;
    inputMeshPointer->normlist = NULL;
    inputMeshPointer->numberofedges = 0;

    double* point_list = inputMeshPointer->pointlist;
    int* point_markerlist = inputMeshPointer->pointmarkerlist;
    int* segment_list = inputMeshPointer->segmentlist;
    int* segment_markerlist = inputMeshPointer->segmentmarkerlist;

    for (unsigned int j = 0; j < numberOfRotatedVertices; j++)
    {
      point_list[2 * j] = domain.RotatedVertex(j)->x();
      point_list[2 * j + 1] = domain.RotatedVertex(j)->y();

      point_markerlist[j] = 2;
    }

    for (unsigned int j = 0; j < numberOfEdges; j++)
    {
      segment_list[2 * j] = j;
      segment_list[2 * j + 1] = (j+1) % numberOfEdges;

      segment_markerlist[j]= 2;
    }

    if (!unidimensionalVertexMarkers.empty())
      memcpy(point_markerlist, unidimensionalVertexMarkers.data(), numberOfRotatedVertices * sizeof(int));
    if (!unidimensionalEdgeMarkers.empty())
    {
      memcpy(segment_markerlist, unidimensionalEdgeMarkers.data(), numberOfEdges * sizeof(int));
      CreateMappedMarkers();
    }

    if(numberOfConstrainedPoints > 0)
    {
      MatrixXd pointsCoordinate = constrainedPoints.leftCols(3);
      pointsCoordinate.transposeInPlace();
      MatrixXd pointsRotatedCoordinate = domain.RotationMatrix() * pointsCoordinate;
      for (unsigned int j = 0; j < numberOfConstrainedPoints; j++)
      {
        point_list[2 * (numberOfRotatedVertices + j)] = pointsRotatedCoordinate(0, j);
        point_list[2 * (numberOfRotatedVertices + j) + 1] = pointsRotatedCoordinate(1, j);

        unsigned int marker = constrainedPoints(j, 3);
        point_markerlist[numberOfRotatedVertices + j] = marker;
      }
    }

    return Output::Success;
  }
  // ***************************************************************************
  MeshCreator2DTriangle::MeshCreator2DTriangle() : MeshCreator2DTriangleBase()
  {
    outputMeshPointer = NULL;
    triangleOptions = "-Qzpqnea";
  }
  MeshCreator2DTriangle::~MeshCreator2DTriangle()
  {
    if (outputMeshPointer != NULL)
    {
      free(outputMeshPointer->pointlist);
      free(outputMeshPointer->pointattributelist);
      free(outputMeshPointer->pointmarkerlist);
      free(outputMeshPointer->trianglelist);
      free(outputMeshPointer->triangleattributelist);
      free(outputMeshPointer->trianglearealist);
      free(outputMeshPointer->neighborlist);
      free(outputMeshPointer->segmentlist);
      free(outputMeshPointer->segmentmarkerlist);
      free(outputMeshPointer->edgelist);
      free(outputMeshPointer->edgemarkerlist);
    }

    delete outputMeshPointer; outputMeshPointer = NULL;
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator2DTriangle::CreateTriangleOutput(const Polygon& domain)
  {
    if (minimumNumberOfCells == 0 && maximumCellSize <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the minimumNumberOfCells or minimumCellSize", false, __func__);
      return Output::GenericError;
    }

    if (inputMeshPointer == NULL)
    {
      Output::PrintErrorMessage("%s: No Triangle input in domain %d", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    if (minimumNumberOfCells > 0 && domain.Measure() <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the domain %d, no measure computed", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    const double& domainArea = domain.Measure();
    double cellArea = minimumNumberOfCells == 0 ? maximumCellSize : domainArea / (double)minimumNumberOfCells;

    ostringstream options;
    options.precision(16);
    options<< triangleOptions;
    options<< cellArea;
    size_t sizeOptions = options.str().size();
    char* optionPointer = new char[sizeOptions + 1];
    options.str().copy(optionPointer, sizeOptions);
    optionPointer[sizeOptions] = '\0';

    delete outputMeshPointer; outputMeshPointer = NULL;
    outputMeshPointer = new triangulateio();

    triangulate(optionPointer, inputMeshPointer, outputMeshPointer, (struct triangulateio*) NULL);

    delete[] optionPointer;

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator2DTriangle::CreateMesh(const Polygon& domain,
                                                      IMesh& mesh)
  {
    /// <ul>
    CreateTriangleInput(domain);
    CreateTriangleOutput(domain);

    if (outputMeshPointer == NULL)
    {
      Output::PrintErrorMessage("%s: No Triangle ouput in domain %d", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    /// <li>	Fill mesh structures
    unsigned int numberOfCellMesh = outputMeshPointer->numberoftriangles;
    unsigned int numberOfEgdesMesh = outputMeshPointer->numberofedges;
    unsigned int numberOfPointsMesh = outputMeshPointer->numberofpoints;

    mesh.SetDimension(2);
    mesh.InitializeCells2D(numberOfCellMesh);
    mesh.InitializeCells1D(numberOfEgdesMesh);
    mesh.InitializeCells0D(numberOfPointsMesh);

    vector<Cell2D*> cells(numberOfCellMesh);
    vector<Cell1D*> edges(numberOfEgdesMesh);
    vector<Cell0D*> points(numberOfPointsMesh);

    /// <li> Set Points
    for (unsigned int p = 0; p < numberOfPointsMesh; p++)
    {
      points[p] = mesh.CreateCell0D();

      Cell0D* point = points[p];
      point->SetCoordinates(outputMeshPointer->pointlist[2 * p], outputMeshPointer->pointlist[2 * p + 1], 0.0);

      if(!unidimensionalVertexMarkers.empty())
      {
        int position = outputMeshPointer->pointmarkerlist[p];
        if(position == 0)
          point->SetMarkers(vector<unsigned int>(markerDimension, 0));
        else
          point->SetMarkers(mappedMarkers[position - 1]);
      }
      else
      {
        point->SetMarkers(vector<unsigned int>(markerDimension, 0));
      }

      mesh.AddCell0D(point);
    }

    /// <li> Set Edges
    SparseMatrix<int> connectivityPoints(numberOfPointsMesh,numberOfPointsMesh);
    vector< Triplet<int> > triplets;
    triplets.reserve(numberOfEgdesMesh);

    for(unsigned int ed = 0; ed < numberOfEgdesMesh; ed++)
    {
      edges[ed] = mesh.CreateCell1D();

      Cell1D* edge = edges[ed];
      edge->AllocateNeighbourhood2D();

      if(!unidimensionalEdgeMarkers.empty())
      {
        int position = outputMeshPointer->edgemarkerlist[ed];
        if(position == 0)
          edge->SetMarkers(vector<unsigned int>(markerDimension, 0));
        else
          edge->SetMarkers(mappedMarkers[position - 1]);
      }
      else
      {
        edge->SetMarkers(vector<unsigned int>(markerDimension, 0));
      }

      Cell0D& point = *points[outputMeshPointer->edgelist[2 * ed]];
      edge->AddVertex(point);
      point.AddNeighCell1D(*edge);

      Cell0D& point2 = *points[outputMeshPointer->edgelist[2 * ed + 1]];
      edge->AddVertex(point2);
      point2.AddNeighCell1D(*edge);

      mesh.AddCell1D(edge);

      triplets.push_back(Triplet<int>(outputMeshPointer->edgelist[2 * ed],outputMeshPointer->edgelist[2 * ed + 1], ed + 1));
    }
    connectivityPoints.setFromTriplets(triplets.begin(), triplets.end());

    /// <li> Set Cells
    for (unsigned int c = 0; c < numberOfCellMesh; c++)
    {
      cells[c] = mesh.CreateCell2D();

      Cell2D& cell = *cells[c];

      cell.SetMarkers(vector<unsigned int>(markerDimension, 0));
      cell.AllocateNeighCell2D(3);
      cell.AllocateEdges(3);
      cell.AllocateVertices(3);
      cell.Set2DPolygon();
      cell.SetType(Polygon::Triangle);

      for (int i = 0; i < 3; i++)
      {
        Cell0D& point = *points[outputMeshPointer->trianglelist[3 * c + i]];

        cell.InsertVertex(point,i);
        point.AddNeighCell2D(cell);
      }
      mesh.AddCell2D(&cell);

      cell.Compute2DPolygonProperties();
    }

    vector<int> cellVertceIds(3, -1);
    for (unsigned int c = 0; c < numberOfCellMesh; c++)
    {
      Cell2D& cell = *cells[c];
      cell.AllocateNormalSign();

      for (int i = 0; i < 3; i++)
      {
        if (outputMeshPointer->neighborlist[3 * c + i] > -1)
        {
          Cell2D& cellNeigh = *cells[outputMeshPointer->neighborlist[3 * c + i]];
          cell.InsertNeighCell2D(cellNeigh, (i+1)%3);
        }
      }

      cellVertceIds[0] = outputMeshPointer->trianglelist[3 * c];
      cellVertceIds[1] = outputMeshPointer->trianglelist[3 * c + 1];
      cellVertceIds[2] = outputMeshPointer->trianglelist[3 * c + 2];

      // CheckTrianglePosition
      for (int e = 0; e < 3; e++)
      {
        int& value = connectivityPoints.coeffRef(cellVertceIds[e], cellVertceIds[(e + 1) % 3]);

        if (value > 0) // left cell
        {
          Cell1D& edge = *edges[value - 1];
          edge.InsertNeighCell2D(cell, 1) ; // left
          cell.InsertEdge(edge, e);
          cell.SetNormalSign(true, e);
        }
        else // right cell
        {
          value = connectivityPoints.coeffRef(cellVertceIds[(e + 1) % 3], cellVertceIds[e]);
          Cell1D& edge = *edges[value - 1];

          edge.InsertNeighCell2D(cell, 0) ; // right
          cell.InsertEdge(edge, e);
          cell.SetNormalSign(false, e);
        }
      }
    }
    return Output::Success;

    /// </ul>
  }

  // ***************************************************************************
  int MeshCreator2DTriangle::CheckTrianglePosition(const vector<int>& edgePointIds,
                                                   const vector<int>& cellPointIds)
  {
    if(edgePointIds[0]==cellPointIds[2] && edgePointIds[1]==cellPointIds[1])
      return 11;  //right, edge 1
    else if(edgePointIds[0]==cellPointIds[1] && edgePointIds[1]==cellPointIds[0])
      return 10;  //right, edge 0
    else if(edgePointIds[0]==cellPointIds[0] && edgePointIds[1]==cellPointIds[2])
      return 12;  //right, edge 2
    else if(edgePointIds[0]==cellPointIds[0] && edgePointIds[1]==cellPointIds[1])
      return 20; //left, edge 0
    else if(edgePointIds[0]==cellPointIds[1] && edgePointIds[1]== cellPointIds[2])
      return 21; //left, edge 1
    else if(edgePointIds[0]==cellPointIds[2] && edgePointIds[1]==cellPointIds[0])
      return 22; //left, edge 2

    return 0; // edge not adjacent to cell
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator2DTriangle::ExportTriangleMesh(const string& nameFolder,
                                                              const string& nameFile) const
  {
    if (inputMeshPointer == NULL || outputMeshPointer == NULL)
      return Output::GenericError;

    ostringstream nameFolderStream, nameFileStream;

    nameFolderStream<< nameFolder<< "/";
    nameFolderStream<< "Triangle/";

    Output::CreateFolder(nameFolderStream.str());

    nameFileStream<< nameFolderStream.str()<< nameFile;

    const struct triangulateio& input = *inputMeshPointer;
    const struct triangulateio& triangulation = *outputMeshPointer;


    ofstream file;
    char basefilename[50];
    char filename[50];
    strcpy(basefilename, nameFileStream.str().c_str());

    file<< setprecision(16);

    strcpy (filename, basefilename);
    strcat (filename, ".poly");
    file.open(filename);
    file<< input.numberofpoints<< " "<< "2"<< " "<< "0"<< " "<< "1"<< endl;
    for(int i = 0; i < input.numberofpoints; i++)
      file<< i + 1<< " "
          << input.pointlist[2 * i]<< " "
          << input.pointlist[2 * i + 1]<< " "
          << input.pointmarkerlist[i]<< endl;

    file<< input.numberofsegments<< " "<< " 0 "<< endl;

    for(int i = 0; i < input.numberofsegments; i++)
      file<< i + 1<< " "
          << input.segmentlist[2 * i] + 1<< " "
          << input.segmentlist[2 * i + 1] + 1<< " "
          << input.segmentmarkerlist[i]<< endl;

    file<< "0";
    file.close();

    strcpy (filename, basefilename);
    strcat (filename, ".1.node");
    file.open(filename);
    file<< triangulation.numberofpoints<< " "<< "2"<< " "<< "0"<< " "<< "1"<< endl;
    for(int i = 0; i < triangulation.numberofpoints; i++)
      file<< i + 1<< " "<< triangulation.pointlist[2 * i]<< " "
          << triangulation.pointlist[2 * i + 1]<< " "
          << triangulation.pointmarkerlist[i]<< endl;
    file.close();

    strcpy (filename, basefilename);
    strcat (filename, ".1.neigh");
    file.open(filename);
    file<< triangulation.numberoftriangles<< " "<< "3"<< endl;
    for(int i = 0; i < triangulation.numberoftriangles; i++)
      file<< i + 1<< " "
          << ((triangulation.neighborlist[3 * i] != -1) ? (triangulation.neighborlist[3 * i] + 1) : -1)<< " "
        << ((triangulation.neighborlist[3 * i + 1] != -1) ? (triangulation.neighborlist[3 * i + 1] + 1) : -1)<< " "
      << ((triangulation.neighborlist[3 * i + 2] != -1) ? (triangulation.neighborlist[3 * i + 2] + 1) : -1)<< endl;
    file.close();

    strcpy (filename,basefilename);
    strcat (filename,".1.edge");
    file.open(filename);
    file<< triangulation.numberofedges<< " "<< "1"<< endl;
    for(int i = 0; i < triangulation.numberofedges; i++)
      file<< i + 1<< " "<< triangulation.edgelist[2 * i] + 1
          << " "<< triangulation.edgelist[2 * i + 1] + 1
          << " "<< triangulation.edgemarkerlist[i]<< endl;
    file.close();

    strcpy (filename,basefilename);
    strcat (filename,".1.ele");
    file.open(filename);
    file<< triangulation.numberoftriangles<< " "<< "3"<< " "<< "0"<< endl;
    for(int i = 0; i < triangulation.numberoftriangles; i++)
      file<< i + 1<< " "
          << triangulation.trianglelist[3 * i] + 1<< " "
          << triangulation.trianglelist[3 * i + 1] + 1<< " "
          << triangulation.trianglelist[3 * i + 2] + 1<< endl;
    file.close();

    strcpy (filename,basefilename);
    strcat (filename,".1.poly");
    file.open(filename);
    file<< " 0 "<< "2 "<< "0 "<< "1"<< endl;
    file<< triangulation.numberofsegments<< " 1"<< endl;
    for(int i = 0; i < triangulation.numberofsegments; i++)
    {
      file<< i + 1<< " "
          << triangulation.segmentlist[2 * i] + 1<< " "
          << triangulation.segmentlist[2 * i + 1] + 1<< " "
          << triangulation.segmentmarkerlist[i] << endl;
    }

    file<< "0";
    file.close();

    return Output::Success;
  }
  // ***************************************************************************
}
