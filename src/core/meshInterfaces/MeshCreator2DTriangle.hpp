#ifndef __MESHCREATOR2DTRIANGLE_H
#define __MESHCREATOR2DTRIANGLE_H

#include "MeshCreator.hpp"
#include "IMeshCreator2D.hpp"

#include "mytriangle.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreator2DTriangleBase : public MeshCreator
  {
    protected:
      struct triangulateio* inputMeshPointer;

      void SetInputMesh(struct triangulateio* _inputMeshPointer) { inputMeshPointer = _inputMeshPointer; }

      Output::ExitCodes CreateTriangleInput(const Polygon& domain);

    public:
      MeshCreator2DTriangleBase();
      virtual ~MeshCreator2DTriangleBase();
  };

  class MeshCreator2DTriangle : public MeshCreator2DTriangleBase, public IMeshCreator2D
  {
    protected:
      struct triangulateio* outputMeshPointer;
      string triangleOptions;

      static int CheckTrianglePosition(const vector<int>& edgePointIds, const vector<int>& cellPointIds);
      Output::ExitCodes CreateTriangleOutput(const Polygon& domain);
    public:
      MeshCreator2DTriangle();
      virtual ~MeshCreator2DTriangle();

      void SetMarkerDimension(const unsigned int& _markerDimension) { return MC_SetMarkerDimension(_markerDimension); }
      void SetMaximumCellSize(const double& _maximumCellSize) { return MC_SetMaximumCellSize(_maximumCellSize); }
      void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells)  { return MC_SetMinimumNumberOfCells(_minimumNumberOfCells); }
      void SetContrainedPoints(const MatrixXd& points) { return MC_SetContrainedPoints(points); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const vector<unsigned int>& _edgeMarkers,
                                 const unsigned int& position = 0) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                 _edgeMarkers,
                                                                                                 position); }

      Output::ExitCodes CreateMesh(const Polygon& domain,
                                   IMesh& mesh);

      Output::ExitCodes ExportTriangleMesh(const string& nameFolder, const string& nameFile) const;
  };
}

#endif // __MESHCREATOR2DTRIANGLE_H

