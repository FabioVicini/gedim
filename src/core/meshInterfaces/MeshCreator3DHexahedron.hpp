#ifndef __MESHCREATOR3DHEXAHEDRON_H
#define __MESHCREATOR3DHEXAHEDRON_H

#include "MeshCreator.hpp"
#include "IMeshCreator3D.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreator3DHexahedron : public MeshCreator, public IMeshCreator3D
  {
    public:
      MeshCreator3DHexahedron();
      virtual ~MeshCreator3DHexahedron();

      void SetMarkerDimension(const unsigned int& _markerDimension) { return MC_SetMarkerDimension(_markerDimension); }
      void SetMaximumCellSize(const double& _maximumCellSize) { return MC_SetMaximumCellSize(_maximumCellSize); }
      void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells)  { return MC_SetMinimumNumberOfCells(_minimumNumberOfCells); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const vector<unsigned int>& _edgeMarkers,
                                 const vector<unsigned int>& _faceMarkers,
                                 const unsigned int& position = 0) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                     _edgeMarkers,
                                                                                                     _faceMarkers,
                                                                                                     position); }
      Output::ExitCodes CreateMesh(const Polyhedron& domain,
                                   IMesh& mesh);
  };

}

#endif // __MESHCREATOR3DHEXAHEDRON_H
