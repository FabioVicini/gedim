#include "MeshCreatorByDomain.hpp"

#include "Output.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{	
  MeshCreatorByDomain::MeshCreatorByDomain()
  {
  }
  MeshCreatorByDomain::~MeshCreatorByDomain()
  {
  }
  //****************************************************************************************************
  Output::ExitCodes MeshCreatorByDomain::CreateMesh(const Polyhedron& domain, IMesh& mesh)
  {
    return CreateMesh3D(domain, mesh);
  }
  //****************************************************************************************************
  Output::ExitCodes MeshCreatorByDomain::CreateMesh(const Polygon& domain, IMesh& mesh)
  {
    return CreateMesh2D(domain, mesh);
  }
  //****************************************************************************************************
  Output::ExitCodes MeshCreatorByDomain::CreateMesh(const Segment& domain, IMesh& mesh)
  {
    return CreateMesh1D(domain, mesh);
  }
  //****************************************************************************************************
  Output::ExitCodes MeshCreatorByDomain::CreateMesh1D(const Segment& domain, IMesh& mesh) const
  {
    /// <ul>
    unsigned int numPoints = 2;

    vector<Cell1D*> cells(1, NULL);
    vector<Cell0D*> points(numPoints, NULL);
    mesh.SetDimension(1);

    bool boundaryConditionVertices = !unidimensionalVertexMarkers.empty();

    /// <li> Add points
    for(unsigned int ver = 0; ver < numPoints; ver++)
    {
      points[ver] = mesh.CreateCell0D();

      Cell0D* point = points[ver];
      point->SetCoordinates(*domain.Vertex(ver));
      if(boundaryConditionVertices)
      {
        point->SetMarkers(MarkersPerVertex(ver));
      }
      else
      {
        for(unsigned int numMark = 0; numMark < markerDimension; numMark++)
          point->SetMarker(2, numMark);
      }
      mesh.AddCell0D(point);
      point->InitializeNeighbourhood1D(1);
    }

    /// <li> Add cell
    cells[0] = mesh.CreateCell1D();

    Cell1D* cell = cells[0];
    cell->InitializeVertices(numPoints);
    cell->AllocateNeighbourhood1D(numPoints);
    mesh.AddCell1D(cell);

    for (unsigned int p = 0; p < numPoints; p++)
    {
      Cell0D* point = points[p];

      cell->AddVertex(*point);
      point->AddNeighCell1D(*cell);
    }

    return Output::Success;
  }
  //****************************************************************************************************
  Output::ExitCodes MeshCreatorByDomain::CreateMesh2D(const Polygon& domain, IMesh& mesh) const
  {
    /// <ul>
    unsigned int numPoints = domain.NumberOfVertices();
    unsigned int numEdges = domain.NumberOfEdges();
    mesh.SetDimension(2);

    vector<Cell2D*> cells(1, NULL);
    vector<Cell1D*> edges(numEdges, NULL);
    vector<Cell0D*> points(numPoints, NULL);

    bool boundaryConditionVertices = !unidimensionalVertexMarkers.empty();
    bool boundaryConditionEdges = !unidimensionalEdgeMarkers.empty();

    /// <li> Add points
    for(unsigned int ver = 0; ver < numPoints; ver++)
    {
      points[ver] = mesh.CreateCell0D();

      Cell0D* point = points[ver];
      point->SetCoordinates(*domain.Vertex(ver));
      if(boundaryConditionVertices)
      {
        point->SetMarkers(MarkersPerVertex(ver));
      }
      else
      {
        point->SetMarkers(vector<unsigned int>(markerDimension, 2));
      }
      mesh.AddCell0D(point);
      point->InitializeNeighbourhood2D(1);
			point->InitializeNeighbourhood1D(2);
    }

    /// <li> Add edges
    for(unsigned int edg = 0; edg < numEdges; edg++)
    {
      edges[edg] = mesh.CreateCell1D();
      Cell1D* edge = edges[edg];
      edge->AllocateNeighbourhood2D(2);

      if(boundaryConditionEdges)
      {
        edge->SetMarkers(MarkersPerEdge(edg));
      }
      else
      {
        edge->SetMarkers(vector<unsigned int>(markerDimension, 2));
      }

      Cell0D* pointOrigin = points[edg];
      Cell0D* pointEnd = points[(edg+1)%numEdges];
      edge->AddVertex(*pointOrigin);
      edge->AddVertex(*pointEnd);
      pointOrigin->AddNeighCell1D(*edge);
      pointEnd->AddNeighCell1D(*edge);

      mesh.AddCell1D(edge);
    }

    /// <li> Add cell
    cells[0] = mesh.CreateCell2D();

    Cell2D* cell = cells[0];
    cell->InitializeVertices(numPoints);
    cell->InitializeEdges(numEdges);
		cell->AllocateNeighCell2D(numEdges);
    mesh.AddCell2D(cell);

    for (unsigned int p = 0; p < numPoints; p++)
    {
      Cell0D* point = points[p];

      cell->AddVertex(*point);
      point->AddNeighCell2D(*cell);
    }

    for (unsigned int e = 0; e < numEdges; e++)
    {
      Cell1D* edge = edges[e];

      cell->AddEdge(*edge);
      edge->InsertNeighCell2D(*cell, 1);
    }

    Vector3d normalPlane;
    cell->ComputeNormalPlane(normalPlane);
    if((normalPlane - Vector3d(0,0,1)).squaredNorm() < 1.0E-14)
      cell->Set2DPolygon();
    else if((normalPlane - Vector3d(0,0,-1)).squaredNorm() < 1.0E-14)
    {
      Output::PrintWarningMessage("Domain with clock wise vertices, cell has been created with counterclockwise vertices", true);
      cell->ChangeOrientation();
      cell->Set2DPolygon();
    }


    return Output::Success;
  }
  //****************************************************************************************************
  Output::ExitCodes MeshCreatorByDomain::CreateMesh3D(const Polyhedron& domain, IMesh& mesh) const
  {
    /// <ul>
    unsigned int numPoints = domain.NumberOfVertices();
    unsigned int numEdges = domain.NumberOfEdges();
    unsigned int numFaces = domain.NumberOfFaces();
    mesh.SetDimension(3);

    vector<Cell3D*> cells(1, NULL);
    vector<Cell2D*> faces(numFaces, NULL);
    vector<Cell1D*> edges(numEdges, NULL);
    vector<Cell0D*> points(numPoints, NULL);

    bool boundaryConditionVertices = !unidimensionalVertexMarkers.empty();
    bool boundaryConditionEdges = !unidimensionalEdgeMarkers.empty();
    bool boundaryConditionFaces = !unidimensionalFaceMarkers.empty();

    /// <li> Add points
    for(unsigned int ver = 0; ver < numPoints; ver++)
    {
      points[ver] = mesh.CreateCell0D();

      Cell0D* point = points[ver];
      point->SetCoordinates(*domain.Vertex(ver));
      if(boundaryConditionVertices)
      {
        point->SetMarkers(MarkersPerVertex(ver));
      }
      else
      {
        point->SetMarkers(vector<unsigned int>(markerDimension, 2));
      }
      mesh.AddCell0D(point);
      point->InitializeNeighbourhood3D(1);
    }

    /// <li> Add edges
    for(unsigned int edg = 0; edg < numEdges; edg++)
    {
      edges[edg] = mesh.CreateCell1D();
      Cell1D* edge = edges[edg];
      edge->InitializeNeighbourhood3D(10);

      if(boundaryConditionEdges)
      {
        edge->SetMarkers(MarkersPerEdge(edg));
      }
      else
      {
        edge->SetMarkers(vector<unsigned int>(markerDimension, 2));
      }

      const Segment& segment = *domain.Edge(edg);
      Cell0D* pointOrigin = points[segment.Vertex(0)->Id()];
      Cell0D* pointEnd = points[segment.Vertex(1)->Id()];
      edge->AddVertex(*pointOrigin);
      edge->AddVertex(*pointEnd);
      pointOrigin->AddNeighCell1D(*edge);
      pointEnd->AddNeighCell1D(*edge);

      mesh.AddCell1D(edge);
    }

    /// <li> Add faces
    for(unsigned int fc = 0; fc < numFaces; fc++)
    {
      faces[fc] = mesh.CreateCell2D();

			Cell2D* face = faces[fc];

      const Polygon& facePolyhedron = *domain.Face(fc);
      const unsigned int facePoints = facePolyhedron.NumberOfVertices();
      const unsigned int faceEdges = facePolyhedron.NumberOfEdges();

      face->InitializeVertices(facePoints);
      face->InitializeEdges(faceEdges);
			face->AllocateNeighCell3D(2);

      if(boundaryConditionFaces)
      {
        face->SetMarkers(MarkersPerFace(fc));
      }
      else
      {
        face->SetMarkers(vector<unsigned int>(markerDimension, 2));
      }

      for (unsigned int p = 0; p < facePoints; p++)
      {
        Cell0D* point = points[facePolyhedron.Vertex(p)->Id()];
        face->AddVertex(*point);
        point->AddNeighCell2D(*face);
      }

      for (unsigned int e = 0; e < faceEdges; e++)
      {
        Cell1D* edge = edges[facePolyhedron.Edge(e)->Id()];
        face->AddEdge(*edge);
        edge->AddNeighCell2D(*face);
      }

      face->Compute3DPolygonProperties();
      mesh.AddCell2D(face);
    }

    /// <li> Add cell
    cells[0] = mesh.CreateCell3D();

    Cell3D* cell = cells[0];
    cell->InitializeVertices(numPoints);
    cell->InitializeEdges(numEdges);
    cell->InitializeFaces(numFaces);
		cell->AllocateNeighCell3D(numFaces);
    mesh.AddCell3D(cell);

    for (unsigned int p = 0; p < numPoints; p++)
    {
      Cell0D* point = points[p];

      cell->AddVertex(*point);
      point->AddNeighCell3D(*cell);
    }

    for (unsigned int e = 0; e < numEdges; e++)
    {
      Cell1D* edge = edges[e];

      cell->AddEdge(*edge);
      edge->AddNeighCell3D(*cell);
    }

    for (unsigned int f = 0; f < numFaces; f++)
    {
      Cell2D* face = faces[f];

      cell->AddFace(*face);
      face->InsertNeighCell3D(*cell, 1);
    }

    return Output::Success;
  }
  //****************************************************************************************************
}
