#ifndef __MESHCREATOR2DTRIANGLEVORONOI_H
#define __MESHCREATOR2DTRIANGLEVORONOI_H



#include "MeshCreator2DTriangle.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class MeshCreator2DTriangleVoronoi : public MeshCreator2DTriangleBase, public IMeshCreator2D
  {
    protected:
      struct triangulateio* outputMeshPointer;
      struct triangulateio* voronoiMeshPointer;
      string triangleOptions;

      Output::ExitCodes CreateTriangleOutput(const Polygon& domain);

    public:
      MeshCreator2DTriangleVoronoi();
      virtual ~MeshCreator2DTriangleVoronoi();

      void SetMarkerDimension(const unsigned int& _markerDimension) { return MC_SetMarkerDimension(_markerDimension); }
      void SetMaximumCellSize(const double& _maximumCellSize) { return MC_SetMaximumCellSize(_maximumCellSize); }
      void SetMinimumNumberOfCells(const unsigned int& _minimumNumberOfCells)  { return MC_SetMinimumNumberOfCells(_minimumNumberOfCells); }
      void SetContrainedPoints(const MatrixXd& points) { return MC_SetContrainedPoints(points); }
      void SetBoundaryConditions(const vector<unsigned int>& _vertexMarkers,
                                 const vector<unsigned int>& _edgeMarkers,
                                 const unsigned int& position) { return MC_SetBoundaryConditions(_vertexMarkers,
                                                                                                 _edgeMarkers,
                                                                                                 position); }

      virtual Output::ExitCodes CreateMesh(const Polygon& domain,
                                           IMesh& mesh);
  };

}

#endif // __MESHCREATOR2DTRIANGLEVORONOI_H

