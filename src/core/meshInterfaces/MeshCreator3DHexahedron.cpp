#include "MeshCreator3DHexahedron.hpp"
#include "Output.hpp"

using namespace MainApplication;

namespace GeDiM
{
  // ***************************************************************************
  MeshCreator3DHexahedron::MeshCreator3DHexahedron() : MeshCreator()
  {
  }
  MeshCreator3DHexahedron::~MeshCreator3DHexahedron()
  {
  }
  // ***************************************************************************
  Output::ExitCodes MeshCreator3DHexahedron::CreateMesh(const Polyhedron& domain,
                                                        IMesh& mesh)
  {
    /// <ul>

    if (domain.Type() != Polyhedron::Cube)
    {
      Output::PrintErrorMessage("%s: domain shall be a cube", false, __func__);
      return Output::GenericError;
    }

    if (minimumNumberOfCells == 0 && maximumCellSize <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the minimumNumberOfCells or minimumCellSize", false, __func__);
      return Output::GenericError;
    }

    if (minimumNumberOfCells > 0 && domain.Measure() <= 0)
    {
      Output::PrintErrorMessage("%s: Wrong initialization of the domain %d, no measure computed", false, __func__, domain.GlobalId());
      return Output::GenericError;
    }

    /// <li> Compute number cell per edge
    const double& domainVolume = domain.Measure();
    double numCell1D = minimumNumberOfCells == 0 ? ceil(sqrt(domainVolume / maximumCellSize)) : ceil(sqrt(minimumNumberOfCells));
    double domainEdgeLenght = sqrt(domainVolume);
    double meshEdgeLenght = domainEdgeLenght / (double)numCell1D;

    list<double> mesh1DList;
    double totLenght = 0.0;
    while (totLenght < domainEdgeLenght - meshEdgeLenght / 2.0)
    {
      mesh1DList.push_back(totLenght);
      totLenght += meshEdgeLenght;
    }
    mesh1DList.push_back(domainEdgeLenght);
    vector<double> mesh1D(mesh1DList.begin(), mesh1DList.end());
    mesh1DList.clear();

    const unsigned int numCellsX = mesh1D.size();
    const unsigned int numCellsY = mesh1D.size();
    const unsigned int numCellsZ = mesh1D.size();

    const Vector3d& originCoords = *domain.Vertex(0);

    const double sizeX = (*domain.Vertex(1) - originCoords).norm() / numCellsX; //dimensione lato "x" di un cubetto
    const double sizeY = (*domain.Vertex(3) - originCoords).norm() / numCellsY; //dimensione lato "y" di un cubetto
    const double sizeZ = (*domain.Vertex(4) - originCoords).norm() / numCellsZ; //dimensione lato "z" di un cubetto

    mesh.SetDimension(3);
    const unsigned int numVertices = (numCellsX + 1) *
                                     (numCellsY + 1) *
                                     (numCellsZ + 1);
    mesh.InitializeCells0D(numVertices);
    const unsigned int numCells = numCellsX *
                                  numCellsY *
                                  numCellsZ;
    mesh.InitializeCells3D(numCells);
    const unsigned int numFaces = numCellsX * numCellsY * (numCellsZ + 1) +
                                  numCellsX * (numCellsY + 1) * numCellsZ +
                                  (numCellsX + 1) * numCellsY * numCellsZ;
    mesh.InitializeCells2D(numFaces);
    const unsigned int numEdges = numCellsX * (numCellsY + 1) * (numCellsZ + 1) +
                                  (numCellsX+1) * (numCellsY+1) * numCellsZ +
                                  (numCellsX+1) * numCellsY * (numCellsZ+1);
    mesh.InitializeCells1D(numEdges);

    unsigned int nbofMarkers = 1;
    //points
    for(unsigned int k = 0; k < numCellsZ+1; k++)
    {
      for(unsigned int j = 0; j < numCellsY+1; j++)
      {
        for(unsigned int i = 0; i < numCellsX+1; i++)
        {
          Cell0D* point = mesh.CreateCell0D();
          Vector3d pointCoords = originCoords + (Vector3d() << i*sizeX, j*sizeY, k*sizeZ).finished();
          point->SetCoordinates(pointCoords);
          point->InitializeNeighbourhood3D(8);
          point->InitializeNeighbourhood2D(12);
          point->InitializeNeighbourhood1D(6);
          if(k==0) // z==0
          {
            point->SetSizeMarkers(nbofMarkers);
            point->SetMarker(faceMarkers[0][0]);  // faceMarkers è un vector<vector > dove il vector esterno corrisponde agli ordini dei marker (e.g. x,y,z)
          }
          else if(k==numCellsZ) // z==L_z
          {
            point->SetSizeMarkers(nbofMarkers);
            point->SetMarker(faceMarkers[0][1]);
          }
          else
          {
            if(i==0) // x==0
            {
              point->SetSizeMarkers(nbofMarkers);
              point->SetMarker(faceMarkers[0][2]);
            }
            else if(i==numCellsX) // x==L_x
            {
              point->SetSizeMarkers(nbofMarkers);
              point->SetMarker(faceMarkers[0][3]);
            }
            else
            {
              if(j==0) //y==0
              {
                point->SetSizeMarkers(nbofMarkers);
                point->SetMarker(faceMarkers[0][4]);
              }
              else if(j==numCellsY) //y==L_y
              {
                point->SetSizeMarkers(nbofMarkers);
                point->SetMarker(faceMarkers[0][5]);
              }
            }
          }
          mesh.AddCell0D(point);
        }
      }
    }

    //edges
    map< vector<unsigned int>, unsigned int > endpointsToEdge;
    vector<unsigned int> endpoints(2);

    //ciclo edges paralleli ox
    for(unsigned int k = 0; k < numCellsZ+1; k++)
    {
      for(unsigned int j = 0; j < numCellsY+1; j++)
      {
        for(unsigned int i = 0; i < numCellsX; i++)
        {
          Cell1D* edge = mesh.CreateCell1D();
          Cell0D* point1 = mesh.GetCell0D(i+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1));
          Cell0D* point2 = mesh.GetCell0D(i+1+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1));
          edge->AddVertex(*point1);
          edge->AddVertex(*point2);
          edge->InitializeNeighbourhood3D(4);
          edge->InitializeNeighbourhood2D(4);
          //marker
          if(k==0)
          {
            if (j==0) //z==0,y==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][0]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][0]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][0]);
            }
            else if (j==numCellsY) //z==0,y==Ly
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][2]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][2]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][2]);
            }
            else // z==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][0]);
            }

          }
          else if (k==numCellsZ)
          {
            if (j==0) //z==Lz y==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][4]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][4]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][4]);
            }
            else if (j==numCellsY) //z==Lz y==Ly
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][6]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][6]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][6]);
            }
            else //z==Lz
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][1]);
            }
          }
          else if (j==0) // y==0
          {
            edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][4]);
          }
          else if(j==numCellsY) //y==Ly
          {
            edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][5]);
          }
          point1->AddNeighCell1D(*edge);
          point2->AddNeighCell1D(*edge);
          endpoints[0] = i+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          endpoints[1] = i+1+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          sort(endpoints.begin(), endpoints.end());
          endpointsToEdge.insert(std::pair< vector<unsigned int>, unsigned int>(endpoints, edge->Id()));
          mesh.AddCell1D(edge);
        }
      }
    }

    //ciclo edges paralleli oy
    for(unsigned int k = 0; k < numCellsZ+1; k++)
    {
      for(unsigned int j = 0; j < numCellsX+1; j++)
      {
        for(unsigned int i = 0; i < numCellsY; i++)
        {
          Cell1D* edge = mesh.CreateCell1D();
          Cell0D* point1 = mesh.GetCell0D(j+i*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1));
          Cell0D* point2 = mesh.GetCell0D(j+(numCellsX+1)+i*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1));
          edge->AddVertex(*point1);
          edge->AddVertex(*point2);
          edge->InitializeNeighbourhood3D(4);
          edge->InitializeNeighbourhood2D(4);
          //marker
          if(k==0)
          {
            if (j==0) //x==0 z==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][3]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][3]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][3]);
            }
            else if (j==numCellsX) //x==Lx z==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][1]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][1]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][1]);
            }
            else // z==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][0]);
            }

          }
          else if (k==numCellsZ)
          {
            if (j==0) //x==0 z==Lz
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][7]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][7]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][7]);
            }
            else if (j==numCellsX) //x==Lx z==Lz
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][5]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][5]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][5]);
            }
            else //z==Lz
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][1]);
            }

          }
          else if (j==0) //x==0
          {
            edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][2]);
          }
          else if(j==numCellsX) //x==Lx
          {
            edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][3]);
          }

          point1->AddNeighCell1D(*edge);
          point2->AddNeighCell1D(*edge);
          endpoints[0] = j+i*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          endpoints[1] = j+(numCellsX+1)+i*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          sort(endpoints.begin(), endpoints.end());
          endpointsToEdge.insert(std::pair< vector<unsigned int>, unsigned int>(endpoints, edge->Id()));
          mesh.AddCell1D(edge);
        }
      }
    }
    //ciclo edges paralleli oz
    for(unsigned int k = 0; k < numCellsY+1; k++)
    {
      for(unsigned int j = 0; j < numCellsX+1; j++)
      {
        for(unsigned int i = 0; i < numCellsZ; i++)
        {
          Cell1D* edge = mesh.CreateCell1D();
          Cell0D* point1 =mesh.GetCell0D(j+k*(numCellsX+1)+i*(numCellsX+1)*(numCellsY+1)) ;
          Cell0D* point2 =mesh.GetCell0D(j+(numCellsX+1)*(numCellsY+1)+k*(numCellsX+1)+i*(numCellsX+1)*(numCellsY+1));
          edge->AddVertex(*point1);
          edge->AddVertex(*point2);
          edge->InitializeNeighbourhood3D(4);
          edge->InitializeNeighbourhood2D(4);
          //marker
          if(k==0)
          {
            if (j==0) // y==0 x==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][8]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][8]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][8]);
            }
            else if (j==numCellsX) //y==0 x==Lx
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][9]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][9]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][9]);
            }
            else // y==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][4]);
            }

          }
          else if (k==numCellsY)
          {
            if (j==0) //y==Ly x==0
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][11]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][11]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][11]);
            }
            else if (j==numCellsX) //y==Ly x==Lx
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(edgeMarkers[0][10]);
              point1->SetSizeMarkers(nbofMarkers);point1->SetMarker(edgeMarkers[0][10]);
              point2->SetSizeMarkers(nbofMarkers);point2->SetMarker(edgeMarkers[0][10]);
            }
            else //y==Ly
            {
              edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][5]);
            }

          }
          else if (j==0) //x==0
          {
            edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][2]);
          }
          else if(j==numCellsX) //x==Lx
          {
            edge->SetSizeMarkers(nbofMarkers);edge->SetMarker(faceMarkers[0][3]);
          }
          point1->AddNeighCell1D(*edge);
          point2->AddNeighCell1D(*edge);
          endpoints[0] = j+k*(numCellsX+1)+i*(numCellsX+1)*(numCellsY+1);
          endpoints[1] = j+(numCellsX+1)*(numCellsY+1)+k*(numCellsX+1)+i*(numCellsX+1)*(numCellsY+1);
          sort(endpoints.begin(), endpoints.end());
          endpointsToEdge.insert(std::pair< vector<unsigned int>, unsigned int>(endpoints, edge->Id()));
          mesh.AddCell1D(edge);
        }
      }
    }
    //faces
    map< vector<unsigned int> , unsigned int > verticesToFace;
    vector<unsigned int> vertices(4);
    vector<unsigned int> edgeEndPoints(2);
    //ciclo facce parallele oxz
    for(unsigned int k = 0; k < numCellsY+1; k++)
    {
      for(unsigned int j = 0; j < numCellsZ; j++)
      {
        for(unsigned int i = 0; i < numCellsX; i++)
        {
          Cell2D* face = mesh.CreateCell2D();
          face->InitializeNeighCell3D(2);
          face->InitializeVertices(4);
          face->InitializeEdges(4);
          face->Set2DPolygon(false);
          face->SetType(Polygon::Parallelogram);

          vertices[0] = i+k*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);
          vertices[1] = i+1+k*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);
          vertices[2] = i+1+(numCellsX+1)*(numCellsY+1)+k*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);
          vertices[3] = i+(numCellsX+1)*(numCellsY+1)+k*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);

          //marker
          if(k==0) //faccia y==0
          {
            face->SetSizeMarkers(nbofMarkers);face->SetMarker(faceMarkers[0][4]);
          }
          else if (k==numCellsY) //faccia y==L_y
          {
            face->SetSizeMarkers(nbofMarkers);face->SetMarker(faceMarkers[0][5]);
          }

          for (unsigned int p=0;p<4;p++)
          {
            Cell0D* point = mesh.GetCell0D(vertices[p]);
            face->AddVertex(*point);
            point->AddNeighCell2D(*face);
          }

          sort(vertices.begin(),vertices.end());
          verticesToFace.insert(std::pair< vector<unsigned int>, unsigned int >(vertices, face->Id()));
          for (unsigned int j = 0; j < 4; j++)
          {
            edgeEndPoints[0] = face->GetCell0D(j)->Id();
            edgeEndPoints[1] = face->GetCell0D((j + 1) % 4)->Id();

            sort(edgeEndPoints.begin(),edgeEndPoints.end());

            map< vector<unsigned int>, unsigned int>::iterator edgeFound = endpointsToEdge.find(edgeEndPoints);
            if (edgeFound == endpointsToEdge.end())
            {
              Output::PrintErrorMessage("Error retrieving an edge when adding edges to face %d in domain %d", true, face->GlobalId(), domain.GlobalId());
              return Output::GenericError;
            }

            face->AddEdge(*mesh.GetCell1D(edgeFound->second));
            mesh.GetCell1D(edgeFound->second)->AddNeighCell2D(*face);
          }
          mesh.AddCell2D(face);
        }
      }
    }
    //ciclo facce parallele oxy
    for(unsigned int k = 0; k < numCellsZ+1; k++)
    {
      for(unsigned int j = 0; j < numCellsY; j++)
      {
        for(unsigned int i = 0; i < numCellsX; i++)
        {
          Cell2D* face = mesh.CreateCell2D();
          face->InitializeNeighCell3D(2);
          face->InitializeVertices(4);
          face->InitializeEdges(4);
          face->Set2DPolygon(false);
          face->SetType(Polygon::Parallelogram);

          vertices[0]=i+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          vertices[1]=i+1+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          vertices[2]=i+1+(numCellsX+1)+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          vertices[3]=i+(numCellsX+1)+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          if(k==0) //faccia z==0
          {
            face->SetSizeMarkers(nbofMarkers);face->SetMarker(faceMarkers[0][0]);
          }
          else if (k==numCellsZ) //faccia z==L_z
          {
            face->SetSizeMarkers(nbofMarkers);face->SetMarker(faceMarkers[0][1]);
          }

          for (unsigned int p=0;p<4;p++)
          {
            Cell0D* point = mesh.GetCell0D(vertices[p]);
            face->AddVertex(*point);
            point->AddNeighCell2D(*face);
          }
          sort(vertices.begin(),vertices.end());
          verticesToFace.insert(std::pair< vector<unsigned int>, unsigned int >(vertices, face->Id()));
          for (unsigned int j = 0; j < 4; j++)
          {
            edgeEndPoints[0] = face->GetCell0D(j)->Id();
            edgeEndPoints[1] = face->GetCell0D((j + 1) % 4)->Id();

            sort(edgeEndPoints.begin(),edgeEndPoints.end());

            map< vector<unsigned int>, unsigned int>::iterator edgeFound = endpointsToEdge.find(edgeEndPoints);
            if (edgeFound == endpointsToEdge.end())
            {
              Output::PrintErrorMessage("Error retrieving an edge when adding edges to face %d in domain %d", true, face->GlobalId(), domain.GlobalId());
              return Output::GenericError;
            }

            face->AddEdge(*mesh.GetCell1D(edgeFound->second));
            mesh.GetCell1D(edgeFound->second)->AddNeighCell2D(*face);
          }
          mesh.AddCell2D(face);
        }
      }
    }
    //ciclo facce parallele oyz

    for(unsigned int k = 0; k < numCellsX+1; k++)
    {
      for(unsigned int j = 0; j < numCellsZ; j++)
      {
        for(unsigned int i = 0; i < numCellsY; i++)
        {
          Cell2D* face = mesh.CreateCell2D();
          face->InitializeNeighCell3D(2);
          face->InitializeVertices(4);
          face->InitializeEdges(4);
          face->Set2DPolygon(false);
          face->SetType(Polygon::Parallelogram);

          vertices[0]=k+i*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);
          vertices[1]=k+(numCellsX+1)*(numCellsY+1)+i*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);
          vertices[2]=k+(numCellsX+1)*(numCellsY+1)+(numCellsX+1)+i*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);
          vertices[3]=k+(numCellsX+1)+i*(numCellsX+1)+j*(numCellsX+1)*(numCellsY+1);
          if(k==0) //faccia x==0
          {
            face->SetSizeMarkers(nbofMarkers);face->SetMarker(faceMarkers[0][2]);
          }
          else if (k==numCellsX) //faccia x==L_z
          {
            face->SetSizeMarkers(nbofMarkers);face->SetMarker(faceMarkers[0][3]);
          }
          for (unsigned int p=0;p<4;p++)
          {
            Cell0D* point = mesh.GetCell0D(vertices[p]);
            face->AddVertex(*point);
            point->AddNeighCell2D(*face);
          }
          sort(vertices.begin(),vertices.end());
          verticesToFace.insert(std::pair< vector<unsigned int>, unsigned int >(vertices, face->Id()));
          for (unsigned int j = 0; j < 4; j++)
          {
            edgeEndPoints[0] = face->GetCell0D(j)->Id();
            edgeEndPoints[1] = face->GetCell0D((j + 1) % 4)->Id();

            sort(edgeEndPoints.begin(),edgeEndPoints.end());

            map< vector<unsigned int>, unsigned int>::iterator edgeFound = endpointsToEdge.find(edgeEndPoints);
            if (edgeFound == endpointsToEdge.end())
            {
              Output::PrintErrorMessage("Error retrieving an edge when adding edges to face %d in domain %d", true, face->GlobalId(), domain.GlobalId());
              return Output::GenericError;
            }

            face->AddEdge(*mesh.GetCell1D(edgeFound->second));
            mesh.GetCell1D(edgeFound->second)->AddNeighCell2D(*face);
          }
          mesh.AddCell2D(face);
        }
      }
    }
    //celle
    vector<unsigned int> addpoints= {1,
                                     numCellsX+2,
                                     numCellsX+1,
                                     (numCellsX+1)*(numCellsY+1),
                                     (numCellsX+1)*(numCellsY+1)+1,
                                     (numCellsX+1)*(numCellsY+1)+2+numCellsX,
                                     (numCellsX+1)*(numCellsY+1)+1+numCellsX};
    vector<unsigned int> cellVertices(8);
    vector<vector<unsigned int>> matCellFaces;
    matCellFaces.resize(6);
    matCellFaces[0] = {0,1,2,3};
    matCellFaces[1] = {0,1,5,4};
    matCellFaces[2] = {1,2,5,6};
    matCellFaces[3] = {3,2,6,7};
    matCellFaces[4] = {0,3,4,7};
    matCellFaces[5] = {4,5,6,7};
    vector<unsigned int> faceVertices(4);
    for(unsigned int k = 0; k < numCellsZ; k++)
    {
      for(unsigned int j = 0; j < numCellsY; j++)
      {
        for(unsigned int i = 0; i < numCellsX; i++)
        {
          Cell3D* cell = mesh.CreateCell3D();
          cell->InitializeVertices(8);
          cell->InitializeFaces(6);
          cell->InitializeEdges(12);
          cell->SetType(Polyhedron::Hexahedron);
          cell->InitializeNeighCell3D(6);
          cellVertices[0]=i+j*(numCellsX+1)+k*(numCellsX+1)*(numCellsY+1);
          for(unsigned int p=0;p<7;p++)
            cellVertices[p+1]=cellVertices[0]+addpoints[p];
          for (unsigned int p=0;p<8;p++)
          {
            Cell0D* point = mesh.GetCell0D(cellVertices[p]);
            cell->AddVertex(*point);
            point->AddNeighCell3D(*cell);
          }
          //ciclo sulle facce della cella
          for (unsigned int f=0;f<6;f++)
          {
            for(unsigned int pf=0;pf<4;pf++)
              faceVertices[pf]=cell->GetCell0D(matCellFaces[f][pf])->Id();
            sort(faceVertices.begin(),faceVertices.end());
            map< vector<unsigned int>, unsigned int>::iterator verticesToFaceFound = verticesToFace.find(faceVertices);
            if (verticesToFaceFound == verticesToFace.end())
            {
              Output::PrintErrorMessage("Error retrieving faces when adding faces to cell %d in domain %d", true, cell->GlobalId(), domain.GlobalId());
              return Output::GenericError;
            }
            unsigned int faceId = verticesToFaceFound->second;

            cell->AddFace(*mesh.GetCell2D(faceId));
            if(mesh.GetCell2D(faceId)->NumberOfNeighCell3D() == 0)
              mesh.GetCell2D(faceId)->InsertNeighCell3D(*cell, 0);
            else
              mesh.GetCell2D(faceId)->InsertNeighCell3D(*cell, 1);
            if (f<5) //sono suff le prime 5 facce per toccare tutti gli edges
            {
              for(unsigned int ed = 0; ed < 4; ed++) // cella-edge
              {
                bool flag = true;
                unsigned int pos = 0;

                while (pos < cell->NumberOfEdges() && flag)
                  flag = cell->Face(f)->Edge(ed) != cell->Edge(pos++);

                if(flag)
                {
                  cell->AddEdge(*cell->GetCell2D(f)->GetCell1D(ed));
                  mesh.GetCell1D(cell->GetCell2D(f)->GetCell1D(ed)->Id())->AddNeighCell3D(*cell);
                }
              }
            }

          }
          mesh.AddCell3D(cell);
        }
      }
    }
    //marker dei vertici del parallelepipedo
    mesh.GetCell0D(0)->SetMarker(vertexMarkers[0][0]);
    mesh.GetCell0D(numCellsX)->SetMarker(vertexMarkers[0][1]);
    unsigned int pos_3 = (numCellsX+1)*numCellsY;
    mesh.GetCell0D(numCellsX+pos_3)->SetMarker(vertexMarkers[0][2]);
    mesh.GetCell0D(pos_3)->SetMarker(vertexMarkers[0][3]);
    unsigned int pos_4 = (numCellsX+1)*(numCellsY+1)*numCellsZ;
    mesh.GetCell0D(pos_4)->SetMarker(vertexMarkers[0][4]);
    mesh.GetCell0D(pos_4+numCellsX)->SetMarker(vertexMarkers[0][5]);
    mesh.GetCell0D(pos_4+pos_3+numCellsX)->SetMarker(vertexMarkers[0][6]);
    mesh.GetCell0D(pos_4+pos_3)->SetMarker(vertexMarkers[0][7]);

    //vicini
    for(unsigned int c=0; c<numCells;c++)
    {
      Cell3D* cell = mesh.GetCell3D(c);
      for (unsigned int f=0;f<6;f++)
      {
        const Cell2D* face = cell->GetCell2D(f);
        if (face->GetNeighCell3D(0)->Id() == cell->Id())
        {
          if(face->GetNeighCell3D(1))
          {
            cell->InsertNeighCell3D(*face->GetNeighCell3D(1), (f+1)%6);
          }

        }
        else
          cell->InsertNeighCell3D(*face->GetNeighCell3D(0), (f+1)%6);
      }
    }

    /// <ul>

    return Output::Success;
  }
  // ***************************************************************************
}
