#include "AdjacencyMatrix.hpp"

#include "MacroDefinitions.hpp"
#include "algorithm"

namespace GeDiM
{
  // ***************************************************************************
  // AdjacencyMatrix Implementation
  // ***************************************************************************
  AdjacencyMatrix::AdjacencyMatrix()
  {
    numberDomains = 0;
    numberConnections = 0;
  }
  AdjacencyMatrix::~AdjacencyMatrix()
  {
    Reset();
  }
  // ***************************************************************************
  Output::ExitCodes AdjacencyMatrix::Reset()
  {
    numberDomains = 0;
    numberConnections = 0;

    adjacencyRows.clear();
    adjacencyCols.clear();

    for (unsigned int i = 0; i < connections.size(); i++)
      connections[i].clear();
    connections.clear();

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes AdjacencyMatrix::Initialize(const unsigned int& _numberDomains, const unsigned int& _numberConnections)
  {
    Output::ExitCodes result = Reset();

    if (result !=	Output::Success)
      return result;

    numberDomains = _numberDomains;
    numberConnections = _numberConnections;

    adjacencyRows.resize(numberDomains + 1, 0);
    adjacencyCols.reserve(numberConnections);
    connections.resize(numberDomains);

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes AdjacencyMatrix::InitializeDomainConnections(const unsigned int& domainPosition, const unsigned int& numberDomainConnections)
  {
    if (domainPosition >= numberDomains)
      return Output::GenericError;

    if (numberDomainConnections > numberConnections)
      return Output::GenericError;

    connections.reserve(numberDomainConnections);

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes AdjacencyMatrix::AddDomainConnection(const unsigned int& domainOnePosition, const unsigned int& connectedDomainPosition)
  {
    if (domainOnePosition >= numberDomains || connectedDomainPosition >= numberDomains)
      return Output::GenericError;

    connections[domainOnePosition].push_back(connectedDomainPosition);

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes AdjacencyMatrix::Compress()
  {
    for (unsigned int d = 0; d < numberDomains; d++)
    {
      vector<unsigned int>& domainConnections = connections[d];
      adjacencyRows[d + 1] = adjacencyRows[d] + domainConnections.size();

      const unsigned int& numberDomainConnections = domainConnections.size();

      for (unsigned int c = 0; c < numberDomainConnections; c++)
        adjacencyCols.push_back(domainConnections[c]);
    }

    adjacencyCols.shrink_to_fit();

    for (unsigned int i = 0; i < connections.size(); i++)
      connections[i].clear();
    connections.clear();

    return Output::Success;
  }
  // ***************************************************************************
  // Export Adjacency Matrix in Matlab Format
  // ***************************************************************************
  Output::ExitCodes AdjacencyMatrix::ExportInMatlabFormat(ofstream& file)
  {
    file << "["<< endl ;
    int numberCol= 0;
    vector<int> row(numberDomains,0);
    for (unsigned int d = 0; d < adjacencyRows.size()-1; d++)
    {
      row.at(d)=1;
      int n = adjacencyRows.at(d+1)-adjacencyRows.at(d);
      if(n>0){
        for(int c = 0; c < n; c++){
          row.at(adjacencyCols.at(numberCol))=1;
          numberCol++;
        }
      }
      for(unsigned int k=0; k<numberDomains;k++)
        file << row.at(k)<< " ";
      file << " ;"<< endl;
      row.assign(numberDomains, 0);
    }
    file << "]";

    return Output::Success;
  }
  // ***************************************************************************
}
