#include "GeometryNetwork.hpp"

namespace GeDiM
{ 
  template class GeometryNetwork<1>;
  template class GeometryNetwork<2>;
  template class GeometryNetwork<3>;
  // ***************************************************************************
  template<unsigned short maxDimension>
  GeometryNetwork<maxDimension>::GeometryNetwork()
  {
    domains.resize(maxDimension);
    domainIds.resize(maxDimension);
    interfaces.resize(maxDimension);
    interfaceIds.resize(maxDimension);
  }
  template<unsigned short maxDimension>
  GeometryNetwork<maxDimension>::~GeometryNetwork()
  {
    Reset();
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  Output::ExitCodes GeometryNetwork<maxDimension>::Reset()
  {
    for (unsigned int d = 0; d < maxDimension; d++)
    {
      domains[d].clear();
      domainIds[d].clear();

      for (unsigned int i = 0; i < interfaces[d].size(); i++)
        delete interfaces[d][i];

      interfaces[d].clear();
      interfaceIds[d].clear();
    }

    return Output::Success;
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  bool GeometryNetwork<maxDimension>::ContainsDomainById(const unsigned int& domainId,
                                                         const unsigned int& dimension) const
  {
    unordered_map<unsigned int, unsigned int>::const_iterator domainIt = domainIds[dimension - 1].find(domainId);
    if (domainIt == domainIds[dimension - 1].end())
      return false;

    unsigned int position = domainIt->second;
    return ContainsDomainByPosition(position, dimension);
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  bool GeometryNetwork<maxDimension>::ContainsInterfaceById(const unsigned int& interfaceId,
                                                            const unsigned int& dimension) const
  {
    unordered_map<unsigned int, unsigned int>::const_iterator interfaceIt = interfaceIds[dimension - 1].find(interfaceId);
    if (interfaceIt == interfaceIds[dimension - 1].end())
      return false;

    unsigned int position = interfaceIt->second;
    return ContainsInterfaceByPosition(position, dimension);
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  bool GeometryNetwork<maxDimension>::ContainsDomainByPosition(const unsigned int& position,
                                                               const unsigned int& dimension) const
  {
    return domains[dimension - 1].size() > position && domains[dimension - 1][position] != nullptr;
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  bool GeometryNetwork<maxDimension>::ContainsInterfaceByPosition(const unsigned int& position,
                                                                  const unsigned int& dimension) const
  {
    return interfaces[dimension - 1].size() > position;
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  const IGeometricObject& GeometryNetwork<maxDimension>::DomainById(const unsigned int& domainId,
                                                                    const unsigned int& dimension) const
  {
    unsigned int position = domainIds[dimension - 1].at(domainId);
    return DomainByPosition(position, dimension);
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  IGeometricObject& GeometryNetwork<maxDimension>::DomainById(const unsigned int& domainId,
                                                              const unsigned int& dimension)
  {
    unsigned int position = domainIds[dimension - 1].at(domainId);
    return DomainByPosition(position, dimension);
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  Output::ExitCodes GeometryNetwork<maxDimension>::InitializeDomains(const unsigned int& numberDomains,
                                                                     const unsigned int& dimension)
  {
    domains[dimension - 1].reserve(numberDomains);
    return Output::Success;
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  Output::ExitCodes GeometryNetwork<maxDimension>::InitializeInterfaces(const unsigned int& numberInterfaces,
                                                                        const unsigned int& dimension)
  {
    interfaces[dimension - 1].reserve(numberInterfaces);
    return Output::Success;
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  Output::ExitCodes GeometryNetwork<maxDimension>::AddDomain(IGeometricObject& domain)
  {
    unsigned int domainId = domain.GlobalId();
    unsigned int dimension = domain.Dimension();

    if (ContainsDomainById(domainId, dimension))
    {
      Output::PrintErrorMessage("%s: domain %d already inserted", false, __func__, domainId);
      return Output::GenericError;
    }

    int position = domains[dimension - 1].size();
    domains[dimension - 1].push_back(&domain);
    domainIds[dimension - 1].insert(pair<unsigned int, unsigned int>(domainId, position));

    return Output::Success;
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  Output::ExitCodes GeometryNetwork<maxDimension>::CreateInterface(const unsigned int& interfaceId,
                                                                   const unsigned int& dimension,
                                                                   const list<unsigned int>& connectedDomainIds)
  {
    if (!ContainsDomainById(interfaceId, dimension))
    {
      Output::PrintErrorMessage("%s: interface domain %d not found", false, __func__, interfaceId);
      return Output::GenericError;
    }

    int position = interfaces[dimension - 1].size();
    switch (dimension)
    {
      case 0:
        interfaces[dimension - 1].push_back(new GeometryNetworkInterface<0>(interfaceId, connectedDomainIds));
      break;
      case 1:
        interfaces[dimension - 1].push_back(new GeometryNetworkInterface<1>(interfaceId, connectedDomainIds));
      break;
      case 2:
        interfaces[dimension - 1].push_back(new GeometryNetworkInterface<2>(interfaceId, connectedDomainIds));
      break;
      default:
        Output::PrintErrorMessage("%s: interface %dD not supported", false, __func__, dimension);
      return Output::GenericError;
    }

    interfaceIds[dimension - 1].insert(pair<unsigned int, unsigned int>(interfaceId, position));

    return Output::Success;
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  const INetworkInterface& GeometryNetwork<maxDimension>::InterfaceById(const unsigned int& interfaceId,
                                                                        const unsigned int& dimension) const
  {
    unsigned int position = interfaceIds[dimension - 1].at(interfaceId);
    return InterfaceByPosition(position, dimension);
  }
  // ***************************************************************************
  template<unsigned short maxDimension>
  INetworkInterface& GeometryNetwork<maxDimension>::InterfaceById(const unsigned int& interfaceId,
                                                                  const unsigned int& dimension)
  {
    unsigned int position = interfaceIds[dimension - 1].at(interfaceId);
    return InterfaceByPosition(position, dimension);
  }
  // ***************************************************************************
}
