#ifndef __INETWORK_H
#define __INETWORK_H

#include "Output.hpp"
#include "IGeometricObject.hpp"
#include "INetworkInterface.hpp"

using namespace std;

namespace GeDiM
{
  class INetwork;

  /// \brief Interface for Network, set of domains.
  /// \copyright See top level LICENSE file for details.
  class INetwork
  {
    public:
      virtual ~INetwork() {}

      /// \brief Reset the network
      virtual Output::ExitCodes Reset() = 0;
      /// \returns The network max dimension
      virtual unsigned short MaxDimension() const = 0;

      /// \return The number of Domains per dimension
      /// \param dimension: the dimension requested
      virtual size_t NumberOfDomains(const unsigned int& dimension) const = 0;
      /// \return The number of Interfaces per dimension
      virtual size_t NumberOfInterfaces(const unsigned int& dimension) const = 0;

      /// \brief Check if the domain per dimension by Id exists
      /// \param domainId: the id of the domain
      /// \param dimension: the dimension requested
      /// \return true if exists
      virtual bool ContainsDomainById(const unsigned int& domainId,
                                      const unsigned int& dimension) const = 0;
      /// \brief Check if the interface per dimension by Id exists
      /// \param domainId: the id of the domain
      /// \param dimension: the dimension requested
      /// \return true if exists
      virtual bool ContainsInterfaceById(const unsigned int& interfaceId,
                                         const unsigned int& dimension) const = 0;
      /// \brief Check if the domain per dimension by position exists
      /// \param domainId: the id of the domain
      /// \param dimension: the dimension requested
      /// \return true if exists
      virtual bool ContainsDomainByPosition(const unsigned int& position,
                                            const unsigned int& dimension) const = 0;
      /// \brief Check if the interface per dimension by position exists
      /// \param domainId: the id of the domain
      /// \param dimension: the dimension requested
      /// \return true if exists
      virtual bool ContainsInterfaceByPosition(const unsigned int& position,
                                               const unsigned int& dimension) const = 0;

      /// \return The domain per dimension filtered by Id
      /// \param domainId: the id of the domain
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual const IGeometricObject& DomainById(const unsigned int& domainId,
                                                 const unsigned int& dimension) const = 0;
      /// \return The domain per dimension filtered by Id
      /// \param domainId: the id of the domain
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual IGeometricObject& DomainById(const unsigned int& domainId,
                                           const unsigned int& dimension) = 0;
      /// \return The domain per dimension filtered by position
      /// \param position: the position of the domain
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual const IGeometricObject& DomainByPosition(const unsigned int& position,
                                                       const unsigned int& dimension) const = 0;
      /// \return The domain per dimension filtered by position
      /// \param position: the position of the domain
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual IGeometricObject& DomainByPosition(const unsigned int& position,
                                                 const unsigned int& dimension) = 0;
      /// \return The domain position per dimension filtered by Id
      /// \param domainId: the id of the domain
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual unsigned int DomainPosition(const unsigned int& domainId,
                                          const unsigned int& dimension) const = 0;

      /// \return The interface per dimension filtered by Id
      /// \param interfaceId: the domain id of the interface
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual const INetworkInterface& InterfaceById(const unsigned int& interfaceId,
                                                     const unsigned int& dimension) const = 0;
      /// \return The interface per dimension filtered by Id
      /// \param interfaceId: the domain id of the interface
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual INetworkInterface& InterfaceById(const unsigned int& interfaceId,
                                               const unsigned int& dimension) = 0;
      /// \return The interface per dimension filtered by position
      /// \param position: the position of the interface
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual const INetworkInterface& InterfaceByPosition(const unsigned int& position,
                                                           const unsigned int& dimension) const = 0;
      /// \return The interface per dimension filtered by position
      /// \param position: the position of the interface
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual INetworkInterface& InterfaceByPosition(const unsigned int& position,
                                                     const unsigned int& dimension) = 0;
      /// \return The interface position per dimension filtered by Id
      /// \param interfaceId: the domain id of the interface
      /// \param dimension: the dimension requested
      /// \note No check of the existence is performed
      virtual unsigned int InterfacePosition(const unsigned int& interfaceId,
                                             const unsigned int& dimension) const = 0;


      /// \brief Initialize the number of Domains per dimension
      /// \param numberDomains the number of domains
      /// \param dimension: the dimension requested
      /// \return the result of initialization
      virtual Output::ExitCodes InitializeDomains(const unsigned int& numberDomains,
                                                  const unsigned int& dimension) = 0;
      /// \brief Initialize the number of Interfaces per dimension
      /// \param numberInterfaces the number of interfaces
      /// \param dimension: the dimension requested
      /// \return the result of initialization
      virtual Output::ExitCodes InitializeInterfaces(const unsigned int& numberInterfaces,
                                                     const unsigned int& dimension) = 0;

      /// \brief Add a new Domain of dimension to the network
      /// \param domain: the domain to add
      /// \param dimension: the dimension requested
      /// \return the result of the add
      /// \note the function checks if the domain is already contained
      virtual Output::ExitCodes AddDomain(IGeometricObject& domain) = 0;

      /// \brief Create an Interface in the network
      /// \param interfaceId: the interface domain id
      /// \param dimension: the dimension of the interface
      /// \param connectedDomainIds: the domain Ids list connected by the interface
      /// \return the result of the create
      /// \note the function checks if the domains are already contained
      /// \note the dimesion of the connected domains is the interface dimension + 1
      virtual Output::ExitCodes CreateInterface(const unsigned int& interfaceId,
                                                const unsigned int& dimension,
                                                const list<unsigned int>& connectedDomainIds) = 0;
  };
}

#endif // __INETWORK_H
