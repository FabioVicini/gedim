#ifndef __NETWORKIMPORTERFAB_H
#define __NETWORKIMPORTERFAB_H

#include "INetworkImporter.hpp"
#include "IShapeCreator.hpp"
#include "IFileReader.hpp"

using namespace std;

namespace GeDiM
{
  class NetworkImporterFabConfig
  {
    public:
      bool ImportTessDomains = false; ///< Import tess domains
      bool ComputeLeastSquarePlane = false; ///< Compute the least square plane genereted by the fracture points
  };

  /// \brief Interface for Network Import
  /// \copyright See top level LICENSE file for details.
  class NetworkImporterFab : public INetworkImporter
  {
    protected:
      IFileReader& _file; ///< the Fab file
      IShapeCreator& _shapeCreator; ///< shape creator
      NetworkImporterFabConfig _configuration; ///< configuration

    public:
      NetworkImporterFab(IFileReader& file,
                         IShapeCreator& shapeCreator,
                         const NetworkImporterFabConfig& configuration = NetworkImporterFabConfig());
      virtual ~NetworkImporterFab();

      Output::ExitCodes Import(INetwork& network);
  };
}

#endif // __NETWORKIMPORTERFAB_H
