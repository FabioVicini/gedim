#ifndef __INETWORKINTERFACE_H
#define __INETWORKINTERFACE_H

#include "Output.hpp"

using namespace std;

namespace GeDiM
{
  class INetworkInterface;

  /// \brief Interface for Network Interface.
  /// \copyright See top level LICENSE file for details.
  class INetworkInterface
  {
    public:
      virtual ~INetworkInterface() {}

      /// \returns The network interface id
      virtual unsigned short Id() const = 0;
      /// \returns The network interface dimension
      virtual unsigned short Dimension() const = 0;

      /// \return The number of the connected Domains
      /// \param dimension: the dimension requested
      virtual size_t NumberOfConnectedDomains() const = 0;

      /// \brief Check if the domain by Id exists
      /// \param domainId: the id of the domain
      /// \return true if exists
      virtual bool ContainsDomainById(const unsigned int& domainId) const = 0;
      /// \brief Check if the domain by position exists
      /// \param domainId: the id of the domain
      /// \return true if exists
      virtual bool ContainsDomainByPosition(const unsigned int& position) const = 0;

      /// \return The domain id by position
      /// \param position: the position of the domain
      /// \note No check of the existence is performed
      virtual unsigned int DomainByPosition(const unsigned int& position) const = 0;
      /// \return The domain position filtered by Id
      /// \param domainId: the id of the domain
      /// \note No check of the existence is performed
      virtual unsigned int DomainPosition(const unsigned int& domainId) const = 0;
  };
}

#endif // __INETWORKINTERFACE_H
