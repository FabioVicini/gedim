#ifndef __GEOMETRYNETWORK_H
#define __GEOMETRYNETWORK_H

#include "Output.hpp"
#include "INetwork.hpp"
#include "GeometryNetworkInterface.hpp"
#include <unordered_map>
#include <unordered_set>

using namespace std;

namespace GeDiM
{
  /// \brief Interface for Network, set of domains.
  /// \copyright See top level LICENSE file for details.
  template<unsigned short maxDimension>
  class GeometryNetwork : public INetwork
  {
    protected:
      vector<unordered_map<unsigned int, unsigned int>> domainIds; ///< domain ids vs position
      vector<unordered_map<unsigned int, unsigned int>> interfaceIds; ///< interface ids vs position
      vector<vector<IGeometricObject*>> domains; ///< network domains
      vector<vector<INetworkInterface*>> interfaces; ///< network interfaces
    public:
      GeometryNetwork();
      virtual ~GeometryNetwork();

      Output::ExitCodes Reset();
      unsigned short MaxDimension() const { return maxDimension; }
      size_t NumberOfDomains(const unsigned int& dimension) const { return domains[dimension - 1].size(); }
      size_t NumberOfInterfaces(const unsigned int& dimension) const { return interfaces[dimension - 1].size(); }

      bool ContainsDomainById(const unsigned int& domainId,
                              const unsigned int& dimension) const;
      bool ContainsInterfaceById(const unsigned int& interfaceId,
                                 const unsigned int& dimension) const;
      bool ContainsDomainByPosition(const unsigned int& position,
                                    const unsigned int& dimension) const;
      bool ContainsInterfaceByPosition(const unsigned int& position,
                                       const unsigned int& dimension) const;
      const IGeometricObject& DomainById(const unsigned int& domainId,
                                         const unsigned int& dimension) const;
      IGeometricObject& DomainById(const unsigned int& domainId,
                                   const unsigned int& dimension);
      const IGeometricObject& DomainByPosition(const unsigned int& position,
                                               const unsigned int& dimension) const { return *domains[dimension - 1][position]; }
      IGeometricObject& DomainByPosition(const unsigned int& position,
                                         const unsigned int& dimension) { return *domains[dimension - 1][position]; }
      unsigned int DomainPosition(const unsigned int& domainId,
                                  const unsigned int& dimension) const { return domainIds[dimension - 1].at(domainId); }
      Output::ExitCodes InitializeDomains(const unsigned int& numberDomains,
                                          const unsigned int& dimension);
      Output::ExitCodes InitializeInterfaces(const unsigned int& numberInterfaces,
                                             const unsigned int& dimension);
      Output::ExitCodes AddDomain(IGeometricObject& domain);

      Output::ExitCodes CreateInterface(const unsigned int& interfaceId,
                                        const unsigned int& dimension,
                                        const list<unsigned int>& connectedDomainIds);
      const INetworkInterface& InterfaceById(const unsigned int& interfaceId,
                                             const unsigned int& dimension) const;
      INetworkInterface& InterfaceById(const unsigned int& interfaceId,
                                       const unsigned int& dimension);
      const INetworkInterface& InterfaceByPosition(const unsigned int& position,
                                                   const unsigned int& dimension) const { return *interfaces[dimension - 1][position]; }
      INetworkInterface& InterfaceByPosition(const unsigned int& position,
                                             const unsigned int& dimension) { return *interfaces[dimension - 1][position]; }
      unsigned int InterfacePosition(const unsigned int& interfaceId,
                                     const unsigned int& dimension) const { return interfaceIds[dimension - 1].at(interfaceId); }
  };
}

#endif // __GEOMETRYNETWORK_H
