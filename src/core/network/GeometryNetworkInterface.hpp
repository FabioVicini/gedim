#ifndef __GEOMETRYNETWORKINTERFACE_H
#define __GEOMETRYNETWORKINTERFACE_H

#include "Output.hpp"
#include "INetworkInterface.hpp"
#include <unordered_map>
#include <unordered_set>

using namespace std;

namespace GeDiM
{
  template<unsigned short dimension>
  class GeometryNetworkInterface : public INetworkInterface {
    protected:
      unsigned int _id; ///< the domain id of the interface
      vector<unsigned int> _connectedDomains; ///< the ids of the connected domains
      unordered_map<unsigned int, unsigned int> _connectedDomainIds; ///< the map ids vs position

    public:
      GeometryNetworkInterface(const unsigned int& id,
                               const list<unsigned int>& connectedDomainIds);
      virtual ~GeometryNetworkInterface() { }

      unsigned short Id() const { return _id; };
      unsigned short Dimension() const { return dimension; };

      size_t NumberOfConnectedDomains() const { return _connectedDomains.size(); }
      bool ContainsDomainById(const unsigned int& domainId) const;
      bool ContainsDomainByPosition(const unsigned int& position) const;
      unsigned int DomainByPosition(const unsigned int& position) const { return _connectedDomains[position]; }
      unsigned int DomainPosition(const unsigned int& domainId) const { return _connectedDomainIds.at(domainId); }
  };
}

#endif // __GEOMETRYNETWORKINTERFACE_H
