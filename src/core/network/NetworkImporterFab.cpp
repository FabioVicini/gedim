#include "NetworkImporterFab.hpp"
#include "Projectors.hpp"

namespace GeDiM
{
  // ***************************************************************************
  NetworkImporterFab::NetworkImporterFab(IFileReader& file,
                                         IShapeCreator& shapeCreator,
                                         const NetworkImporterFabConfig& configuration) :
    _file(file),
    _shapeCreator(shapeCreator)
  {
    _configuration = configuration;
  }
  NetworkImporterFab::~NetworkImporterFab()
  {
  }
  // ***************************************************************************
  Output::ExitCodes NetworkImporterFab::Import(INetwork& network)
  {
    unsigned int numReadDomains = 0, numNormalDomains = 0, numTessDomains = 0;

    if (!_file.Open())
    {
      Output::PrintErrorMessage("%s: File '%s' cannot be opened", false, __func__, _file.Path().c_str());
      return Output::GenericError;
    }

    vector<string> lines;
    char tempValue[255];
    unsigned int domainId, tempInt, numVert;
    //    int marker;
    double x,y,z, diffusionTerm, maxDiffusionTerm = 0.0;

    _file.GetAllLines(lines);

    int lineCounter = 4;
    istringstream convertNumDomains(lines[lineCounter++]);
    convertNumDomains >> tempValue;
    convertNumDomains >> tempValue;
    convertNumDomains >> numNormalDomains;

    istringstream convertNumTessDomains(lines[lineCounter++]);
    convertNumTessDomains >> tempValue;
    convertNumTessDomains >> tempValue;
    convertNumTessDomains >> numTessDomains;

    numReadDomains = numTessDomains + numNormalDomains;

    if (numReadDomains == 0)
    {
      Output::PrintWarningMessage("%s: Error in file: number of domains is 0", false, __func__);
      return Output::GenericError;
    }

    network.InitializeDomains(_configuration.ImportTessDomains ? numReadDomains : numNormalDomains,
                              2);

    while (lines[lineCounter].compare(0, 14, "BEGIN FRACTURE", 14))
      lineCounter++;
    lineCounter++;

    for (unsigned int i = 0; i < numNormalDomains; i++)
    {
      istringstream convertDomainId(lines[lineCounter]);

      convertDomainId >> domainId>> numVert;
      convertDomainId >> diffusionTerm;

      if (numVert <= 0)
      {
        Output::PrintWarningMessage("Error in file: domain %d has no vertexes", false, domainId);
        continue;
      }

      lineCounter++;

      vector<Vector3d> vertices;
      vertices.reserve(numVert);
      for (unsigned int j = 0; j < numVert; j++)
      {
        istringstream convertDomainVertex(lines[lineCounter]);

        convertDomainVertex >> tempInt>> x>> y>> z;
        vertices.push_back(Vector3d(x, y, z));

        lineCounter++;
      }

      if(_configuration.ComputeLeastSquarePlane)
      {
        Projectors::ProjectPointsOnLeastSquarePlane(vertices);
      }

      Polygon& domain = _shapeCreator.CreatePolygon(vertices);
      domain.SetGlobalId(domainId);

      network.AddDomain(domain);

      if (maxDiffusionTerm < diffusionTerm)
        maxDiffusionTerm = diffusionTerm;

      // TODO: import other properties
      //      domain.InitializeProperty("DiffusionTerm", InputPropertySupportedTypes::Double);
      //      domain.AddProperty("DiffusionTerm", new double(diffusionTerm));

      //      domain.InitializeProperty("Type", InputPropertySupportedTypes::String);
      //      domain.AddProperty("Type", new string("Normal"));

      lineCounter++;
    }

    if(_configuration.ImportTessDomains && numTessDomains > 0)
    {
      while (lines[lineCounter].compare(0, 18, "BEGIN TESSFRACTURE", 18))
        lineCounter++;
      lineCounter++;

      //      int normalDirection;
      for (unsigned int i = numNormalDomains; i < numReadDomains;i++)
      {
        Output::ExitCodes result = Output::Success;

        istringstream convertDomainId(lines[lineCounter]);
        convertDomainId >> domainId;
        convertDomainId >> numVert;

        if (numVert <= 0)
        {
          Output::PrintWarningMessage("Error in file: domain %d has no vertexes", false, domainId);
          continue;
        }

        if (domainId <= numNormalDomains)
          domainId = numNormalDomains + domainId;

        lineCounter++;

        vector<Vector3d> vertices;
        vertices.reserve(numVert);
        for (unsigned int j = 0; j < numVert; j++)
        {
          istringstream convertDomainVertex(lines[lineCounter]);

          convertDomainVertex >> tempInt>> x>> y>> z;
          vertices.push_back(Vector3d(x, y, z));

          lineCounter++;
        }

        if(_configuration.ComputeLeastSquarePlane)
        {
          Projectors::ProjectPointsOnLeastSquarePlane(vertices);
        }

        Polygon& domain = _shapeCreator.CreatePolygon(vertices);
        domain.SetGlobalId(domainId);

        network.AddDomain(domain);

        if (result != Output::Success)
        {
          Output::PrintGenericMessage("Domain %d failed initialize function", false, domainId);
          continue;
        }

        // TODO: import other properties
        //        domain.InitializeProperty("DiffusionTerm", InputPropertySupportedTypes::Double);
        //        domain.AddProperty("DiffusionTerm", new double(1e3 * maxDiffusionTerm));

        //        domain.InitializeProperty("Type", InputPropertySupportedTypes::String);
        //        domain.AddProperty("Type", new string("Tess"));

        //        file >> marker >> normalDirection;

        //        domain.InitializeProperty("Marker", InputPropertySupportedTypes::UInt);
        //        domain.AddProperty("Marker", new unsigned int(marker));

        //        domain.InitializeProperty("NormalDirection", InputPropertySupportedTypes::Int);
        //        domain.AddProperty("NormalDirection", new int(normalDirection));

        lineCounter++;
      }
      lineCounter++;
    }

    _file.Close();

    return Output::Success;
  }
  // ***************************************************************************
}
