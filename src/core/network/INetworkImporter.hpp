#ifndef __INETWORKIMPORTER_H
#define __INETWORKIMPORTER_H

#include "Output.hpp"
#include "INetwork.hpp"

using namespace std;

namespace GeDiM
{
  /// \brief Interface for Network Import
  /// \copyright See top level LICENSE file for details.
  class INetworkImporter
  {
    public:
      virtual ~INetworkImporter() {}

      /// \brief Import the network
      /// \param network the network imported
      /// \return the result of the import
      virtual Output::ExitCodes Import(INetwork& network) = 0;
  };
}

#endif // __INETWORKIMPORTER_H
