#ifndef __ADJACENCYMATRIX_H
#define __ADJACENCYMATRIX_H

#include <vector>
#include <string>

#include "Output.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
	class NetworkBase;
	class AdjacencyMatrix;

	class AdjacencyMatrix
	{
		protected:
			unsigned int numberDomains;
			unsigned int numberConnections;
			vector<unsigned int> adjacencyRows;
			vector<unsigned int> adjacencyCols;
			vector< vector<unsigned int> > connections;

		public:
			AdjacencyMatrix();
			~AdjacencyMatrix();

			const unsigned int& NumberDomains() const { return numberDomains; }
      unsigned int NumberConnections() const { return numberConnections; }
			const vector<unsigned int>& AdjacencyRows() const { return adjacencyRows; }
			const vector<unsigned int>& AdjacencyCols() const { return adjacencyCols; }
      unsigned int NumberDomainConnections(const unsigned int& domainPosition) const { return  adjacencyRows[domainPosition + 1] - adjacencyRows[domainPosition]; }
      const unsigned int& DomainConnection(const unsigned int& domainPosition,
                                           const unsigned int& connectionPosition) const { return adjacencyCols[adjacencyRows[domainPosition] + connectionPosition]; }

			Output::ExitCodes Reset();
			const vector<unsigned int>& Connections(const unsigned int& position) {return connections[position];}

			Output::ExitCodes Initialize(const unsigned int& _numberDomains, const unsigned int& _numberConnections);
			Output::ExitCodes InitializeDomainConnections(const unsigned int& domainPosition, const unsigned int& numberDomainConnections);
			Output::ExitCodes AddDomainConnection(const unsigned int& domainPosition, const unsigned int& connectedDomainPosition);
			Output::ExitCodes Compress();
			Output::ExitCodes ExportInMatlabFormat(ofstream &file);
	};
}

#endif // __ADJACENCYMATRIX_H
