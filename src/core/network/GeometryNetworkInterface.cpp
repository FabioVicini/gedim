#include "GeometryNetworkInterface.hpp"

namespace GeDiM
{ 
  template class GeometryNetworkInterface<0>;
  template class GeometryNetworkInterface<1>;
  template class GeometryNetworkInterface<2>;
  // ***************************************************************************
  template<unsigned short dimension>
  GeometryNetworkInterface<dimension>::GeometryNetworkInterface(const unsigned int& id,
                                                                const list<unsigned int>& connectedDomainIds)
  {
    _id = id;
    _connectedDomains.reserve(connectedDomainIds.size());
    for (list<unsigned int>::const_iterator it = connectedDomainIds.begin(); it != connectedDomainIds.end(); it++)
    {
      _connectedDomainIds.insert(pair<unsigned int, unsigned int>(*it, _connectedDomains.size()));
      _connectedDomains.push_back(*it);
    }
  }
  // ***************************************************************************
  template<unsigned short dimension>
  bool GeometryNetworkInterface<dimension>::ContainsDomainById(const unsigned int& domainId) const
  {
    unordered_map<unsigned int, unsigned int>::const_iterator domainIt = _connectedDomainIds.find(domainId);
    if (domainIt == _connectedDomainIds.end())
      return false;

    unsigned int position = domainIt->second;
    return ContainsDomainByPosition(position);
  }
  // ***************************************************************************
  template<unsigned short dimension>
  bool GeometryNetworkInterface<dimension>::ContainsDomainByPosition(const unsigned int& position) const
  {
    return _connectedDomains.size() > position;
  }
  // ***************************************************************************
}
