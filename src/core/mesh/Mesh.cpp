#include "Mesh.hpp"
#include "Output.hpp"
#include <set>
#include "DefineNumbers.hpp"
#include "CellsReader.hpp"
#include "MeasurerPoint.hpp"

using namespace MainApplication;

namespace GeDiM
{
  Output::ExitCodes Mesh::UpdateCell2D_2D(Cell2D& cell)
  {
    Cell2D& cellChild = *CreateCell2D();

    cell.InitializeChildren(1);
    cell.AddChild(&cellChild);
    cell.SetState(false);
    cellChild.SetFather(&cell);
    cellChild.Set2DPolygon(cell.Is2DPolygon());
    cellChild.InheritPropertiesByFather();

    unsigned int numberEdges = cell.NumberOfEdges(); //equal number points
    unsigned int numberEdgesChild = 0;

    vector<unsigned int> pointsIdCutEdge(2,0); //id dei punti del lato tagliato
    vector<unsigned int> newIdPoint; // nuovo punto da aggiungere

    for(unsigned int numEd = 0; numEd < numberEdges; numEd++)
    {
      const unsigned int& idEdgeCut = cell.Edge(numEd)->Id();
      Cell1D* edge = cells1D[idEdgeCut];
      if(edge->HasChildren())
      {
        pointsIdCutEdge[0] = edge->Vertex(0)->Id();
        pointsIdCutEdge[1] = edge->Vertex(1)->Id();
        numberEdgesChild = edge->NumberOfChildren();
        break;
      }
    }


    for(unsigned int numPoint = 0; numPoint < numberEdges; numPoint++)
    {
      const unsigned int& idPoint = cell.Vertex(numPoint)->Id();
      const unsigned int& idPointNext = cell.Vertex((numPoint+1)%numberEdges)->Id();
      if(idPoint == pointsIdCutEdge[1] && idPointNext == pointsIdCutEdge[0])
      {
        pointsIdCutEdge[0] = idPoint;
        pointsIdCutEdge[1] = idPointNext;
        break;
      }
    }

    unsigned int numEdgesCellChild = (numberEdgesChild == 0)? numberEdges : numberEdges - 1 + numberEdgesChild;
    cellChild.InitializeEdges(numEdgesCellChild);
    cellChild.InitializeVertices(numEdgesCellChild);

    AddCell2D(&cellChild);

    newIdPoint.reserve(numEdgesCellChild-1);

    //CICLO SUI LATI DELLA CELLA
    for(unsigned int numEd = 0; numEd < numberEdges; numEd++)
    {
      const unsigned int& idEdge = cell.Edge(numEd)->Id();
      Cell1D* edge = cells1D[idEdge];
      //SE NON HA FIGLI IL LATO:
      //1) AGGIUNGO IL LATO ALLA CELLA
      //2) AGGIORNO LA CELLA DEL LATO PADRE CON LA CELLA FIGLIO
      if(!edge->HasChildren())
      {
        cellChild.AddEdge(*edge);
        for(unsigned int neigCell = 0; neigCell < edge->NumberOfNeighCell2D(); neigCell++)
          if(edge->GetNeighCell2D(neigCell) != NULL)
            if(edge->GetNeighCell2D(neigCell)->Id() == cell.Id())
              edge->InsertNeighCell2D(cellChild, neigCell);
      }
      //SE HA FIGLI IL LATO:
      //1) SALVO GLI ID DEI PUNTI DEL LATO PADRE CHE E' STATO TAGLIATO
      //2) AGGIUNGO ALLA CELLA I FIGLI DEL LATO
      //3) AI FIGLI DEL LATO AGGIORNO LA CELLA PADRE CON LA CELLA FIGLIO
      //4) TROVO L'ID DEL NUOVO PUNTO DA AGGIUNGERE
      //5) AGGIORNO GLI ID DEI PUNTI DEL LATO PADRE CON QUELLI DEL FIGLIO TEMPORANEAMENTE
      //   PER TROVARE L'EVENTUALE NUOVO ID DEL PUNTO SE NE SONO DUE
      //6) SALVO GLI ID DEI PUNTI DEL LATO PADRE CHE E' STATO TAGLIATO
      else
      {
        bool inversePushBack = false;
        const ITreeNode* child = edge->Child(0);
        const Cell1D* cell1DChild = static_cast<const Cell1D*>(child);

        unsigned int idEdge = cell1DChild->Id();
        Cell1D* edgeChild = cells1D[idEdge];
        if(edgeChild->Vertex(0)->Id() == pointsIdCutEdge[1])
          inversePushBack = true;

        unsigned int counter = 0;
        if(inversePushBack)
        {
          for(int numChild = edge->NumberOfChildren() - 1; numChild >= 0 ; numChild--)
          {
            const ITreeNode* child = edge->Child(numChild);
            const Cell1D* cell1DChild = static_cast<const Cell1D*>(child);
            unsigned int idEdge = cell1DChild->Id();
            Cell1D* edgeChild = cells1D[idEdge];
            cellChild.AddEdge(*edgeChild);

            for(unsigned int neigCell = 0; neigCell < edge->NumberOfNeighCell2D(); neigCell++)
              if(edge->GetNeighCell2D(neigCell) != NULL)
                if(edge->GetNeighCell2D(neigCell)->Id() == cell.Id())
                  edgeChild->InsertNeighCell2D(cellChild, neigCell);

            if(counter < edge->NumberOfChildren() - 1)
            {
              if(edgeChild->Vertex(0)->Id() != pointsIdCutEdge[0] && edgeChild->Vertex(0)->Id() != pointsIdCutEdge[1])
                newIdPoint.push_back(edgeChild->Vertex(0)->Id());
              else
                newIdPoint.push_back(edgeChild->Vertex(1)->Id());
            }
            pointsIdCutEdge[0] = edgeChild->Vertex(0)->Id();
            pointsIdCutEdge[1] = edgeChild->Vertex(1)->Id();
            counter++;
          }
          pointsIdCutEdge[0] = edge->Vertex(0)->Id();
          pointsIdCutEdge[1] = edge->Vertex(1)->Id();
        }
        else
        {
          for(unsigned int numChild =  0; numChild < edge->NumberOfChildren() ; numChild++)
          {
            const ITreeNode* child = edge->Child(numChild);
            const Cell1D* cell1DChild = static_cast<const Cell1D*>(child);
            const unsigned int& idEdge = cell1DChild->Id();
            Cell1D* edgeChild = cells1D[idEdge];
            cellChild.AddEdge(*edgeChild);

            for(unsigned int neigCell = 0; neigCell < edge->NumberOfNeighCell2D(); neigCell++)
              if(edge->GetNeighCell2D(neigCell) != NULL)
                if(edge->GetNeighCell2D(neigCell)->Id() == cell.Id())
                  edgeChild->InsertNeighCell2D(cellChild, neigCell);

            if(counter < edge->NumberOfChildren() - 1)
            {
              if(edgeChild->Vertex(0)->Id() != pointsIdCutEdge[0] && edgeChild->Vertex(0)->Id() != pointsIdCutEdge[1])
                newIdPoint.push_back(edgeChild->Vertex(0)->Id());
              else
                newIdPoint.push_back(edgeChild->Vertex(1)->Id());
            }
            pointsIdCutEdge[0] = edgeChild->Vertex(0)->Id();
            pointsIdCutEdge[1] = edgeChild->Vertex(1)->Id();
            counter++;
          }
        }
        pointsIdCutEdge[0] = edge->Vertex(1)->Id();
        pointsIdCutEdge[1] = edge->Vertex(0)->Id();
      }
    }


    for(unsigned int numPoint = 0; numPoint < numberEdges; numPoint++)
    {
      const unsigned int& idPoint = cell.Vertex(numPoint)->Id();
      const unsigned int& idPointNext = cell.Vertex((numPoint+1)%numberEdges)->Id();
      Cell0D* point = cells0D[idPoint];
      cellChild.AddVertex(*point);
      for(unsigned int cellPosition = 0; cellPosition < point->NumberOfNeighCell2D(); cellPosition++)
      {
        //Tolgo il padre dai punti ed inserisco il figlio
        if(cell.Id() == point->GetNeighCell2D(cellPosition)->Id())
          point->InsertNeighCell2D(cellChild,cellPosition);
      }
      if((idPoint == pointsIdCutEdge[0] && idPointNext == pointsIdCutEdge[1]) || (idPoint == pointsIdCutEdge[1] && idPointNext == pointsIdCutEdge[0]))
      {
        for(unsigned int newId = 0; newId < newIdPoint.size() ; newId++)
        {
          Cell0D* newPoint = cells0D[newIdPoint[newId]];
          cellChild.AddVertex(*newPoint);
          for(unsigned int cellPosition = 0; cellPosition < newPoint->NumberOfNeighCell2D(); cellPosition++)
          {
            //Tolgo il padre dai punti ed inserisco il figlio
            if(cell.Id() == newPoint->GetNeighCell2D(cellPosition)->Id())
              newPoint->InsertNeighCell2D(cellChild,cellPosition);
          }
        }
      }
    }


    if(dimension == 2)
    {
      cellChild.AllocateNeighCell2D(numEdgesCellChild);
      for(unsigned int numEd = 0; numEd < numEdgesCellChild; numEd++)
      {
        const Cell1D& edge = static_cast<const Cell1D&>(*cellChild.Edge(numEd));
        for(unsigned int numNeig = 0; numNeig < 2; numNeig++)
        {
          if(edge.GetNeighCell2D(numNeig) != NULL)
          {
            Cell2D& cellNeigh = *cells2D[edge.GetNeighCell2D(numNeig)->Id()];
            if(cellNeigh.Id() != cellChild.Id())
              cellChild.InsertNeighCell2D(cellNeigh, numEd);

            for(unsigned int neig = 0; neig < cellNeigh.NumberOfNeighCell2D(); neig++)
              if(cellNeigh.GetNeighCell2D(neig) != NULL && cell.Id() == cellNeigh.GetNeighCell2D(neig)->Id())
                cellNeigh.InsertNeighCell2D(cellChild, neig);
          }
        }
      }
    }
    else if(dimension == 3)
    {
      cellChild.AllocateNeighCell3D(2);
      for(unsigned int numCell = 0; numCell < 2; numCell++)
      {
        if(cell.GetNeighCell3D(numCell) != NULL)
        {
          cellChild.InsertNeighCell3D(*cell.GetNeighCell3D(numCell), numCell);
        }
      }
    }

    if(cellChild.NumberOfEdges() != cellChild.NumberOfVertices())
    {
      Output::PrintErrorMessage("Failed Update Cell %d", false, cell.Id());
      return Output::GenericError;
    }

    cellChild.ComputeNormalSign();

    return Output::Success;
  }

  Output::ExitCodes Mesh::UpdateCell2D_3D(Cell2D& face)
  {
    unsigned int counterEdges = 0;
    for(unsigned int numEdg = 0; numEdg < face.NumberOfEdges(); numEdg++)
      if(face.GetCell1D(numEdg)->IsActive())
        counterEdges++;

    unsigned int edgesToUpdate = face.NumberOfEdges() - counterEdges;

    if(edgesToUpdate == 0)
      return Output::Success;

    Cell2D& faceChild = *CreateCell2D();
    face.InitializeChildren(1);
    face.AddChild(&faceChild);
    face.SetState(false);
    faceChild.SetFather(&face);
    faceChild.Set2DPolygon(face.Is2DPolygon());
    faceChild.InheritPropertiesByFather();

    unsigned int numberEdges = face.NumberOfEdges(); //equal number points
    unsigned int numberEdgesChild = 0;

    vector<unsigned int> pointsIdCutEdge(2,0); //id dei punti del lato tagliato
    vector<unsigned int> newIdPoint; // nuovo punto da aggiungere

    for(unsigned int numEd = 0; numEd < numberEdges; numEd++)
    {
      unsigned idEdgeCut = face.Edge(numEd)->Id();
      Cell1D* edge = cells1D[idEdgeCut];
      if(edge->HasChildren())
      {
        pointsIdCutEdge[0] = edge->Vertex(0)->Id();
        pointsIdCutEdge[1] = edge->Vertex(1)->Id();
        numberEdgesChild = edge->NumberOfChildren();
        break;
      }
    }

    for(unsigned int numPoint = 0; numPoint < numberEdges; numPoint++)
    {
      const unsigned int& idPoint = face.Vertex(numPoint)->Id();
      const unsigned int& idPointNext = face.Vertex((numPoint+1)%numberEdges)->Id();
      if(idPoint == pointsIdCutEdge[1] && idPointNext == pointsIdCutEdge[0])
      {
        pointsIdCutEdge[0] = idPoint;
        pointsIdCutEdge[1] = idPointNext;
        break;
      }
    }

    unsigned int numEdgesFaceChild = numberEdges - 1 + numberEdgesChild;
    faceChild.InitializeEdges(numEdgesFaceChild);
    faceChild.InitializeVertices(numEdgesFaceChild);
    faceChild.AllocateNeighCell3D(2);
    AddCell2D(&faceChild);

    newIdPoint.reserve(numEdgesFaceChild-1);
    //CICLO SUI LATI DELLA FACCIA
    for(unsigned int numEd = 0; numEd < numberEdges; numEd++)
    {
      const unsigned int& idEdge = face.Edge(numEd)->Id();
      Cell1D* edge = cells1D[idEdge];
      //SE NON HA FIGLI IL LATO:
      //1) AGGIUNGO IL LATO ALLA FACCIA
      //2) AGGIORNO LA FACCIA DEL LATO PADRE CON LA FACCIA FIGLIO
      if(!edge->HasChildren())
      {
        faceChild.AddEdge(*edge);
        for(unsigned int neigFace = 0; neigFace < edge->NumberOfNeighCell2D(); neigFace++)
          if(edge->GetNeighCell2D(neigFace) != NULL)
            if(edge->GetNeighCell2D(neigFace)->Id() == face.Id())
              edge->InsertNeighCell2D(faceChild, neigFace);
      }
      //SE HA FIGLI IL LATO:
      //1) SALVO GLI ID DEI PUNTI DEL LATO PADRE CHE E' STATO TAGLIATO
      //2) AGGIUNGO ALLA FACCIA I FIGLI DEL LATO
      //3) AI FIGLI DEL LATO AGGIORNO LA FACCIA PADRE CON LA FACCIA FIGLIO
      //4) TROVO L'ID DEL NUOVO PUNTO DA AGGIUNGERE
      //5) AGGIORNO GLI ID DEI PUNTI DEL LATO PADRE CON QUELLI DEL FIGLIO TEMPORANEAMENTE
      //   PER TROVARE L'EVENTUALE NUOVO ID DEL PUNTO SE NE SONO DUE
      //6) SALVO GLI ID DEI PUNTI DEL LATO PADRE CHE E' STATO TAGLIATO
      else
      {
        bool inversePushBack = false;
        unsigned int idEdgeChild = static_cast<const Cell1D*>(edge->Child(0))->Id();
        Cell1D* edgeChild = cells1D[idEdgeChild];
        if(edgeChild->Vertex(0)->Id() == pointsIdCutEdge[1])
          inversePushBack = true;

        unsigned int counter = 0;
        if(inversePushBack)
        {
          for(int numChild = edge->NumberOfChildren() - 1; numChild >= 0 ; numChild--)
          {
            const unsigned int& id = static_cast<const Cell1D*>(edge->Child(numChild))->Id();
            Cell1D* edgeChildToAdd = cells1D[id];
            faceChild.AddEdge(*edgeChildToAdd);

            for(unsigned int neigFace = 0; neigFace < edgeChildToAdd->NumberOfNeighCell2D(); neigFace++)
              if(edgeChildToAdd->GetNeighCell2D(neigFace) != NULL)
                if(edgeChildToAdd->GetNeighCell2D(neigFace)->Id() == face.Id())
                  edgeChildToAdd->InsertNeighCell2D(faceChild, neigFace);

            if(counter < edge->NumberOfChildren() - 1)
            {
              if(edgeChildToAdd->Vertex(0)->Id() != pointsIdCutEdge[0] && edgeChildToAdd->Vertex(0)->Id() != pointsIdCutEdge[1])
                newIdPoint.push_back(edgeChildToAdd->Vertex(0)->Id());
              else
                newIdPoint.push_back(edgeChildToAdd->Vertex(1)->Id());
            }
            pointsIdCutEdge[0] = edgeChildToAdd->Vertex(0)->Id();
            pointsIdCutEdge[1] = edgeChildToAdd->Vertex(1)->Id();
            counter++;
          }
        }
        else
        {
          for(unsigned int numChild =  0; numChild < edge->NumberOfChildren() ; numChild++)
          {
            unsigned int id = static_cast<const Cell1D*>(edge->Child(numChild))->Id();
            Cell1D* edgeChildToAdd = cells1D[id];
            faceChild.AddEdge(*edgeChildToAdd);

            for(unsigned int neigFace = 0; neigFace < edgeChildToAdd->NumberOfNeighCell2D(); neigFace++)
              if(edgeChildToAdd->GetNeighCell2D(neigFace) != NULL)
                if(edgeChildToAdd->GetNeighCell2D(neigFace)->Id() == face.Id())
                  edgeChildToAdd->InsertNeighCell2D(faceChild, neigFace);

            if(counter < edge->NumberOfChildren() - 1)
            {
              if(edgeChildToAdd->Vertex(0)->Id() != pointsIdCutEdge[0] && edgeChildToAdd->Vertex(0)->Id() != pointsIdCutEdge[1])
                newIdPoint.push_back(edgeChildToAdd->Vertex(0)->Id());
              else
                newIdPoint.push_back(edgeChildToAdd->Vertex(1)->Id());
            }
            pointsIdCutEdge[0] = edgeChildToAdd->Vertex(0)->Id();
            pointsIdCutEdge[1] = edgeChildToAdd->Vertex(1)->Id();
            counter++;
          }
        }
        pointsIdCutEdge[0] = edge->Vertex(1)->Id();
        pointsIdCutEdge[1] = edge->Vertex(0)->Id();
      }
    }

    for(unsigned int numPoint = 0; numPoint < numberEdges; numPoint++)
    {
      const unsigned int& idPoint = face.Vertex(numPoint)->Id();
      const unsigned int& idPointNext = face.Vertex((numPoint+1)%numberEdges)->Id();
      Cell0D* point = cells0D[idPoint];
      faceChild.AddVertex(*point);
      for(unsigned int facePosition = 0; facePosition < point->NumberOfFaces(); facePosition++)
      {
        //Tolgo il padre dai punti ed inserisco il figlio
        if(face.Id() == point->GetNeighCell2D(facePosition)->Id())
        {
          point->InsertNeighCell2D(faceChild, facePosition);
          break;
        }
      }
      if((idPoint == pointsIdCutEdge[0] && idPointNext == pointsIdCutEdge[1]) || (idPoint == pointsIdCutEdge[1] && idPointNext == pointsIdCutEdge[0]))
      {
        for(unsigned int newId = 0; newId < newIdPoint.size() ; newId++)
        {
          Cell0D* newPoint = cells0D[newIdPoint[newId]];
          faceChild.AddVertex(*newPoint);
          for(unsigned int facePosition = 0; facePosition < newPoint->NumberOfNeighCell2D(); facePosition++)
          {
            //Tolgo il padre dai punti ed inserisco il figlio
            if(face.Id() == newPoint->GetNeighCell2D(facePosition)->Id())
              newPoint->InsertNeighCell2D(faceChild,facePosition);
          }
        }
      }
    }

    for(unsigned int numCell = 0; numCell < 2; numCell++)
    {
      if(face.GetNeighCell3D(numCell) != NULL)
      {
        faceChild.InsertNeighCell3D(*face.GetNeighCell3D(numCell), numCell);
      }
    }

    if(faceChild.NumberOfEdges() != faceChild.NumberOfVertices())
    {
      Output::PrintErrorMessage("Failed Update Cell %d", false, face.Id());
      return Output::GenericError;
    }

    for(unsigned int numPoint = 0; numPoint < numberEdges; numPoint++)
    {
      const Cell1D& edge = *faceChild.GetCell1D(numPoint);
      if(!edge.IsActive())
      {
        Output::PrintErrorMessage("In the face there is an edge not active with id %d", true, edge.Id());
        return Output::GenericError;
      }
    }

    for(unsigned int numPoint = 0; numPoint < numberEdges; numPoint++)
    {
      const Cell1D& edge = *faceChild.GetCell1D(numPoint);
      bool found = false;
      for(unsigned int neigh2D = 0; neigh2D < edge.NumberOfNeighCell2D(); neigh2D++)
        if(edge.GetNeighCell2D(neigh2D) == &faceChild)
          found = true;

      if(!found)
      {
        Output::PrintErrorMessage("In edge %d there is not the updated face", true, edge.Id());
        return Output::GenericError;
      }
    }

    Vector3d normalFaceFather;
    faceChild.Compute3DPolygonProperties();
    const Vector3d& normalFace = *faceChild.Normal();
    face.ComputeNormalPlane(normalFaceFather);

    if(normalFace.dot(normalFaceFather) < 1.0E-7)
      faceChild.ChangeOrientation();

    faceChild.ComputeNormalSign();

    return Output::Success;
  }

  Output::ExitCodes Mesh::UpdateCell3D(Cell3D& cell)
  {
    unsigned int counterFaces = 0;
    unsigned int counterEdges = 0;
    for(unsigned int numFace = 0; numFace < cell.NumberOfFaces(); numFace++)
    {
      const Cell2D* face = static_cast<const Cell2D*>(cell.Face(numFace));
      if(face->IsActive())
        counterFaces++;
    }
    for(unsigned int numEdg = 0; numEdg < cell.NumberOfEdges(); numEdg++)
    {
      const Cell1D* edge = static_cast<const Cell1D*>(cell.Edge(numEdg));
      if(edge->IsActive())
        counterEdges++;
    }

    Cell3D& cellChild = *CreateCell3D();

    cell.InitializeChildren(1);
    cell.AddChild(&cellChild);
    cell.SetState(false);
    cellChild.SetFather(&cell);
    cellChild.InheritPropertiesByFather();

    set<unsigned int> pointsSetChild;
    set<unsigned int> edgesSetChild;
    set<unsigned int> facesSetChild;
    for(unsigned int fac = 0; fac < cell.NumberOfFaces(); fac++)
    {
      //in caso di cvx devono essere le ultime foglie dell'albero
      const Cell2D& face = static_cast<const Cell2D&>(*cell.Face(fac));
      if(!face.HasChildren())
        facesSetChild.insert(face.Id());
      else
      {
        list<unsigned int> idChildrenToVisit;
        idChildrenToVisit.push_back(face.Id());
        while(!idChildrenToVisit.empty())
        {
          unsigned int id = idChildrenToVisit.front();
          idChildrenToVisit.pop_front();
          MeshTreeNode* faceVisit = cells2D[id];
          for(unsigned int numChild = 0; numChild < faceVisit->NumberOfChildren(); numChild++)
          {
            const Cell2D* child = static_cast<const Cell2D*>(faceVisit->Child(numChild));
            if(child->IsActive())
            {
              facesSetChild.insert(child->Id());
            }
            else
              idChildrenToVisit.push_back(child->Id());
          }
        }
      }
    }
    for(set<unsigned int>::iterator fc = facesSetChild.begin(); fc != facesSetChild.end(); ++fc)
    {
      Cell2D& faceReference = *cells2D[*fc];
      for(unsigned int pt = 0; pt < faceReference.NumberOfVertices(); pt++)
        pointsSetChild.insert(faceReference.Vertex(pt)->Id());

      for(unsigned int pt = 0; pt < faceReference.NumberOfEdges(); pt++)
      {
        const unsigned int& idEdge =faceReference.Edge(pt)->Id();
        edgesSetChild.insert(idEdge);
      }
    }

    cellChild.AllocateNeighCell3D(facesSetChild.size());
    cellChild.InitializeFaces(facesSetChild.size());
    cellChild.InitializeEdges(edgesSetChild.size());
    cellChild.InitializeVertices(pointsSetChild.size());
    AddCell3D(&cellChild);

    for(set<unsigned int>::iterator it=pointsSetChild.begin(); it!=pointsSetChild.end(); ++it)
    {
      Cell0D& cell0D = *cells0D[*it];
      cellChild.AddVertex(cell0D);
      for(unsigned int pointCell = 0; pointCell < cell0D.NumberOfNeighCell3D(); pointCell++)
      {
        if(cell0D.GetNeighCell3D(pointCell)->Id() == cell.Id())
          cell0D.InsertNeighCell3D(cellChild, pointCell);
      }
    }

    for(set<unsigned int>::iterator it = edgesSetChild.begin(); it != edgesSetChild.end(); ++it)
    {
      Cell1D& cell1D = *cells1D[*it];
      cellChild.AddEdge(cell1D);
      if(cell1D.IsActive())
      {
        for(unsigned int edgeCell = 0; edgeCell < cell1D.NumberOfNeighCell3D(); edgeCell++)
        {
          if(cell1D.GetNeighCell3D(edgeCell)->Id() == cell.Id())
            cell1D.InsertNeighCell3D(cellChild,edgeCell);
        }
      }
      else
      {
        Output::PrintWarningMessage("All the faces are active than all the edges have to be active ", true);
        Output::PrintWarningMessage("Edge %d is not active ", true, cell1D.Id());
        return Output::GenericError;
      }
    }

    unsigned int positionNeigCell = 0;
    for(set<unsigned int>::iterator fc = facesSetChild.begin(); fc != facesSetChild.end(); ++fc)
    {
      Cell2D& faceReference = *cells2D[*fc];

      if(!faceReference.HasChildren())
      {
        cellChild.AddFace(faceReference);
        for(unsigned int neigCell = 0; neigCell < 2; neigCell++)
        {
          if(faceReference.GetNeighCell3D(neigCell) != NULL)
          {
            Cell3D& cellNeigh = *cells3D[faceReference.GetNeighCell3D(neigCell)->Id()];
            if(cellNeigh.Id() == cell.Id())
              faceReference.InsertNeighCell3D(cellChild, neigCell);
            else
              cellChild.InsertNeighCell3D(cellNeigh, positionNeigCell);

            for(unsigned int neig = 0; neig < cellNeigh.NumberOfNeighCell3D(); neig++)
              if(cellNeigh.GetNeighCell3D(neig) != NULL && cell.Id() == cellNeigh.GetNeighCell3D(neig)->Id())
                cellNeigh.InsertNeighCell3D(cellChild, neig);
          }
        }
        positionNeigCell++;
      }
      else
      {
        Output::PrintWarningMessage("There are some faces not active", true);
        Output::PrintWarningMessage("Face %d is not active", true, faceReference.Id());
        return Output::GenericError;
      }
    }

    for(unsigned int numEdg = 0; numEdg < cellChild.NumberOfEdges(); numEdg++)
    {
      const Cell1D& edge = *cellChild.GetCell1D(numEdg);
      bool found = false;
      for(unsigned int neigh3D = 0; neigh3D < edge.NumberOfNeighCell3D(); neigh3D++)
        if(edge.GetNeighCell3D(neigh3D) == &cellChild)
          found = true;

      if(!found)
      {
        Output::PrintErrorMessage("In edge %d there is not the updated face", true, edge.Id());
        return Output::GenericError;
      }
    }
    return Output::Success;
  }

  Mesh::~Mesh()
  {
    Reset();
  }

  Output::ExitCodes Mesh::SetDimension(const unsigned short& _dimension)
  {
    Output::ExitCodes exitCode = Output::Success;
    delete maximalCellsReaderPtr;
    switch(_dimension)
    {
      case 0:
        maximalCellsReaderPtr = new CellsReader<Cell0D>(cells0D);
        dimension = _dimension;
      break;
      case 1:
        maximalCellsReaderPtr = new CellsReader<Cell1D>(cells1D);
        dimension = _dimension;
      break;
      case 2:
        maximalCellsReaderPtr = new CellsReader<Cell2D>(cells2D);
        dimension = _dimension;
      break;
      case 3:
        maximalCellsReaderPtr = new CellsReader<Cell3D>(cells3D);
        dimension = _dimension;
      break;
      default:
        maximalCellsReaderPtr = nullptr;
        Output::PrintErrorMessage("Error in setting mesh dimension. Dimension %d not supported.", false, _dimension);
        exitCode = Output::GenericError;
    }
    return exitCode;
  }

  Output::ExitCodes Mesh::AddCell3D(Cell3D* block)
  {
    if (block == NULL)
      return Output::GenericError;

    cells3D.push_back(block);

    return Output::Success;
  }

  Output::ExitCodes Mesh::AddCell2D(Cell2D* polygon)
  {
    if (polygon == NULL)
      return Output::GenericError;

    cells2D.push_back(polygon);

    return Output::Success;
  }

  Output::ExitCodes Mesh::AddCell1D(Cell1D* edge)
  {
    if (edge == NULL)
      return Output::GenericError;

    cells1D.push_back(edge);

    return Output::Success;
  }

  Output::ExitCodes Mesh::AddCell0D(Cell0D* point)
  {
    if (point == NULL)
      return Output::GenericError;

    cells0D.push_back(point);

    return Output::Success;
  }

  Output::ExitCodes Mesh::ExportInfoMesh3D(const string& pathFile) const
  {

    ofstream fileInfoMeshFaces;
    string infoMeshFileFaces = pathFile + "/Faces.csv";
    fileInfoMeshFaces.open(infoMeshFileFaces.c_str());

    if(fileInfoMeshFaces.fail())
    {
      Output::PrintErrorMessage("File '%s' cannot be exported.", false, infoMeshFileFaces.c_str());
      return Output::GenericError;
    }
    fileInfoMeshFaces << "IdFace" <<  "," << "AspectRatio"<< ","
                      << "MaxRatioEdgeMaxMin" << ","	<< "NumberVertices"
                      << endl;

    for(unsigned int numCell = 0; numCell < NumberOfCells2D(); numCell++)
    {
      Cell2D& cell = *cells2D[numCell];
      double aspectRatio = 0.0;
      cell.ComputeCentroid();
      if(cell.Normal() == NULL)
        cell.ComputePlane();
      cell.ComputeRotationMatrix();
      cell.ComputeRotatedVertices();
      cell.ComputeAspectRatio(aspectRatio);

      double edgeMinCell = numeric_limits<double>::max();
      double edgeMaxCell = 0.0;
      double length = 0.0;
      for(unsigned int numEdge = 0; numEdge < cell.NumberOfEdges(); numEdge++)
      {
        const Segment& edge = *cell.Edge(numEdge);
        edge.ComputeMeasure(length);
        if(length < edgeMinCell)
          edgeMinCell = length;
        if(length > edgeMaxCell)
          edgeMaxCell = length;
      }
      double ratioEdge = edgeMaxCell/edgeMinCell;
      fileInfoMeshFaces << cell.Id() << "," << aspectRatio << ","
                        << ratioEdge << "," << cell.NumberOfVertices() << endl;
    }

    fileInfoMeshFaces.close();

    ofstream fileInfoMeshCells;
    string infoMeshFileCells = pathFile + "/Cells.csv";
    fileInfoMeshCells.open(infoMeshFileCells.c_str());

    if(fileInfoMeshCells.fail())
    {
      Output::PrintErrorMessage("File '%s' cannot be exported.", false, infoMeshFileCells.c_str());
      return Output::GenericError;
    }
    fileInfoMeshCells << "IdCell" <<  "," << "AspectRatio"<< ","
                      << "RatioEdgeMaxMin" << "," << "NumberVertices" << ","
                      << endl;

    for(unsigned int numCell = 0; numCell < NumberOfCells3D(); numCell++)
    {
      Cell3D& cell = *cells3D[numCell];
      double aspectRatio = 0.0;
      cell.ComputeAspectRatio(aspectRatio);
      double edgeMinCell = numeric_limits<double>::max();
      double edgeMaxCell = 0.0;
      for(unsigned int numEdge = 0; numEdge < cell.NumberOfEdges(); numEdge++)
      {
        const Segment& edge = *cell.Edge(numEdge);
        double length = 0.0;
        edge.ComputeMeasure(length);
        if(length < edgeMinCell)
          edgeMinCell = length;
        if(length > edgeMaxCell)
          edgeMaxCell = length;
      }
      double ratioEdge = edgeMaxCell/edgeMinCell;
      fileInfoMeshCells << cell.Id() << "," << aspectRatio << ","
                        << ratioEdge << "," << cell.NumberOfVertices() << endl;
    }
    fileInfoMeshCells.close();

    return Output::Success;

  }

  void Mesh::Reset()
  {
    for (vector<Cell0D*>::iterator pointPtr = cells0D.begin(); pointPtr != cells0D.end(); pointPtr++)
      delete *pointPtr;

    for (vector<Cell1D*>::iterator edgePtr = cells1D.begin(); edgePtr != cells1D.end(); edgePtr++)
      delete *edgePtr;

    for (vector<Cell2D*>::iterator polyPtr = cells2D.begin(); polyPtr != cells2D.end(); polyPtr++)
      delete *polyPtr;

    for (vector<Cell3D*>::iterator blockPtr = cells3D.begin(); blockPtr != cells3D.end(); blockPtr++)
      delete *blockPtr;

    cells0D.clear();
    cells1D.clear();
    cells2D.clear();
    cells3D.clear();

    delete maximalCellsReaderPtr;
  }

  Output::ExitCodes Mesh::CheckDoublePoints(const double& toll)

  {
    double squaredToll = toll*toll;
    for(unsigned int pnt = 0; pnt < cells0D.size() - 1; pnt++)
    {
      Cell0D& point = *cells0D[pnt];
      for(unsigned int pnt2 = pnt+1; pnt2 < cells0D.size(); pnt2++)
      {
        Cell0D& point2 = *cells0D[pnt2];
        Vector3d diff = point2 - point;
        if(diff.squaredNorm() < squaredToll)
        {
          Output::PrintWarningMessage("%s :Point %d and Point %d have the same coordinates", true, __func__, point.Id(), point2.Id());
        }
      }
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckPointsInCells3D()
  {
    set<unsigned int> pointsInCells3D;
    vector<bool> pointsChecked(cells0D.size(), false);
    for(unsigned int cel = 0; cel < cells3D.size(); cel++)
    {
      Cell3D& cell = *cells3D[cel];
      for(unsigned int pnt = 0; pnt < cell.NumberOfVertices(); pnt++)
      {
        if(pointsChecked[cell.Vertex(pnt)->Id()])
          continue;

        pointsInCells3D.insert(cell.Vertex(pnt)->Id());
        pointsChecked[cell.Vertex(pnt)->Id()] = true;
      }
    }
    if(pointsInCells3D.size() != cells0D.size())
    {
      for(unsigned int numPnt = 0; numPnt < cells0D.size(); numPnt++)
      {
        if(pointsChecked[numPnt])
          continue;
        Output::PrintErrorMessage("%s : There is point with id %d not used in the cells", true, __func__, numPnt);
      }
      return Output::GenericError;
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckPointsInCells2D()
  {
    set<unsigned int> pointsInCells2D;
    vector<bool> pointsChecked2D(cells0D.size(), false);
    for(unsigned int cel = 0; cel < cells2D.size(); cel++)
    {
      Cell2D& cell = *cells2D[cel];
      for(unsigned int pnt = 0; pnt < cell.NumberOfVertices(); pnt++)
      {
        if(pointsChecked2D[cell.Vertex(pnt)->Id()])
          continue;

        pointsInCells2D.insert(cell.Vertex(pnt)->Id());
        pointsChecked2D[cell.Vertex(pnt)->Id()] = true;
      }
    }
    if(pointsInCells2D.size() != cells0D.size())
    {
      for(unsigned int numPnt = 0; numPnt < cells2D.size(); numPnt++)
      {
        if(pointsChecked2D[numPnt])
          continue;
        Output::PrintErrorMessage("%s : There is point with id %d not used in the cells", true, __func__, numPnt);
      }
      return Output::GenericError;
    }

    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckPointsEqualsEdgesInCells2D()
  {
    for(unsigned int cel = 0; cel < cells2D.size(); cel++)
    {
      Cell2D& cell = *cells2D[cel];
      if(cell.NumberOfVertices() - cell.NumberOfEdges() > 0)
      {
        Output::PrintErrorMessage("%s :There are %d point/s and %d edges in the face %d", true, __func__, cell.NumberOfVertices(), cell.NumberOfEdges(), cell.Id());
        return Output::GenericError;
      }
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckEdgesInCells2D()
  {
    for(unsigned int fac = 0; fac < cells2D.size(); fac++)
    {
      Cell2D& face = *cells2D[fac];
      for(unsigned int edg = 0; edg < face.NumberOfEdges(); edg++)
      {
        Cell1D& edge = *cells1D[face.Edge(edg)->Id()];
        bool foundFace = false;
        for(unsigned int facEdg = 0; facEdg < edge.NumberOfNeighCell2D(); facEdg++)
        {
          if(edge.GetNeighCell2D(facEdg) == &face)
          {
            foundFace = true;
            break;
          }
        }
        if(!foundFace)
        {
          Output::PrintErrorMessage("In edge %d there is not the cell2D %d ", true, edge.Id(), face.Id());
          return Output::GenericError;
        }
      }
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckEdgesInCells3D()
  {
    for(unsigned int fac = 0; fac < cells3D.size(); fac++)
    {
      Cell3D& cell3D = *cells3D[fac];
      for(unsigned int edg = 0; edg < cell3D.NumberOfEdges(); edg++)
      {
        Cell1D& edge = *cells1D[cell3D.Edge(edg)->Id()];
        bool foundFace = false;
        for(unsigned int facEdg = 0; facEdg < edge.NumberOfNeighCell2D(); facEdg++)
        {
          if(edge.GetNeighCell3D(facEdg) == &cell3D)
          {
            foundFace = true;
            break;
          }
        }
        if(!foundFace)
        {
          Output::PrintErrorMessage("In edge %d there is not the cell3D %d ", true, edge.Id(), cell3D.Id());
          return Output::GenericError;
        }
      }
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckDoubleCells3D()
  {
    set<unsigned int> firstCell;
    for(unsigned int cel = 0; cel < cells3D.size()-1; cel++)
    {
      Cell3D& cell = *cells3D[cel];
      unsigned int numberFaces = cell.NumberOfFaces();
      if(numberFaces < 4)
      {
        Output::PrintErrorMessage("%s :In cell %d there are %d faces ", true, __func__, cell.Id(), numberFaces);
        return Output::GenericError;
      }

      for(unsigned int fac = 0; fac < numberFaces; fac++)
        firstCell.insert(cell.Face(fac)->Id());

      if(firstCell.size() != numberFaces)
      {
        Output::PrintErrorMessage("%s :In cell %d there are doubled faces", true, __func__, cell.Id());
        return Output::GenericError;
      }

      set<unsigned int> secondCell;
      for(unsigned int cel2 = cel+1; cel2 < cells3D.size(); cel2++)
      {
        Cell3D& cell2 = *cells3D[cel2];
        if(cell2.NumberOfFaces() != numberFaces)
          continue;

        for(unsigned int fac2 = 0; fac2 < numberFaces; fac2++)
          secondCell.insert(cell2.Face(fac2)->Id());

        if(secondCell.size() != numberFaces)
        {
          Output::PrintErrorMessage("%s :In cell %d there are doubled faces ", true, __func__, cell2.Id());
          return Output::GenericError;
        }

        set<unsigned int>::iterator it = firstCell.begin();
        set<unsigned int>::iterator it2 = secondCell.begin();
        bool cont = true;
        for(unsigned int i = 0; i < numberFaces; i++)
        {
          if(*it != *it2)
          {
            cont = false;
            break;
          }
          it++;
          it2++;
        }
        if(cont)
        {
          Output::PrintErrorMessage("%s :There is a doubled cell with id %d and %d ", true, __func__, cell.Id(), cell2.Id());
          return Output::GenericError;
        }
        secondCell.clear();
      }
      firstCell.clear();
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckDoubleCells2D()
  {
    set<unsigned int> firstFace;
    for(unsigned int fac = 0; fac < cells2D.size()-1; fac++)
    {
      Cell2D& face = *cells2D[fac];
      unsigned int numEdges = face.NumberOfEdges();
      if(numEdges < 3)
      {
        Output::PrintErrorMessage("%s :In face %d there are %d edges ", true, __func__, face.Id(), numEdges);
        return Output::GenericError;
      }

      for(unsigned int edg = 0; edg < numEdges; edg++)
        firstFace.insert(face.Edge(edg)->Id());

      if(firstFace.size() != numEdges)
      {
        Output::PrintErrorMessage("%s :In face %d there are doubled edges", true, __func__, face.Id());
        return Output::GenericError;
      }

      set<unsigned int> secondFace;
      for(unsigned int fac2 = fac+1; fac2 < cells2D.size(); fac2++)
      {
        Cell2D& face2 = *cells2D[fac2];
        if(face2.NumberOfFaces() != numEdges)
          continue;

        for(unsigned int edg2 = 0; edg2 < numEdges; edg2++)
          secondFace.insert(face2.Edge(edg2)->Id());

        if(secondFace.size() != numEdges)
        {
          Output::PrintErrorMessage("%s :In face %d there are doubled edges ", true, __func__, face2.Id());
          return Output::GenericError;
        }

        set<unsigned int>::iterator it = firstFace.begin();
        set<unsigned int>::iterator it2 = secondFace.begin();
        bool cont = true;
        for(unsigned int i = 0; i < numEdges; i++)
        {
          if(*it != *it2)
          {
            cont = false;
            break;
          }
          it++;
          it2++;
        }
        if(cont)
        {
          Output::PrintErrorMessage("%s :There is a doubled face with id %d and %d ", true, __func__, face.Id(), face2.Id());
          return Output::GenericError;
        }
        secondFace.clear();
      }
      firstFace.clear();
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckDoubleCells1D()
  {
    set<unsigned int> firstEdge;
    for(unsigned int edg = 0; edg < cells1D.size()-1; edg++)
    {
      Cell1D& edge = *cells1D[edg];
      unsigned int numPoints = edge.NumberOfVertices();
      if(numPoints != 2)
      {
        Output::PrintErrorMessage("%s :In edge %d there are %d points ", true, edge.Id(), __func__, numPoints);
        return Output::GenericError;
      }

      for(unsigned int edg = 0; edg < 2; edg++)
        firstEdge.insert(edge.Vertex(edg)->Id());

      if(firstEdge.size() != 2)
      {
        Output::PrintErrorMessage("%s :In edge %d there are doubled points", true, __func__, edge.Id());
        return Output::GenericError;
      }

      set<unsigned int> secondEdge;
      for(unsigned int edg2 = edg+1; edg2 < cells1D.size(); edg2++)
      {
        Cell1D& edge2 = *cells1D[edg2];

        for(unsigned int edg3 = 0; edg3 < 2; edg3++)
          secondEdge.insert(edge2.Vertex(edg3)->Id());

        if(secondEdge.size() != numPoints)
        {
          Output::PrintErrorMessage("%s :In edge %d there are doubled point ", true, __func__, edge2.Id());
          return Output::GenericError;
        }

        set<unsigned int>::iterator it = firstEdge.begin();
        set<unsigned int>::iterator it2 = secondEdge.begin();
        bool cont = true;
        for(unsigned int i = 0; i < numPoints; i++)
        {
          if(*it != *it2)
          {
            cont = false;
            break;
          }
          it++;
          it2++;
        }
        if(cont)
        {
          Output::PrintErrorMessage("%s :There is a doubled edge with id %d and %d ", true, __func__, edge.Id(), edge2.Id());
          return Output::GenericError;
        }
        secondEdge.clear();
      }
      firstEdge.clear();
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckNeigs()
  {
    switch(dimension)
    {
      case 3:
      {
        for(unsigned int cel = 0; cel < cells3D.size()-1; cel++)
        {
          Cell3D& cell = *cells3D[cel];
          unsigned int idCell = cell.Id();
          unsigned int idCellToControl = 0;
          for(unsigned int fac = 0; fac < cell.NumberOfFaces(); fac++)
          {
            const Cell2D& face = *cell.GetCell2D(fac);
            if(face.GetNeighCell3D(0) == NULL )
            {
              if(cell.GetNeighCell3D(fac) == NULL)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            else if(face.GetNeighCell3D(0) != NULL && face.GetNeighCell3D(0)->Id() != idCell)
            {
              idCellToControl = face.GetNeighCell3D(0)->Id();
              if(cell.GetNeighCell3D(fac)->Id() == idCellToControl)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            if(face.GetNeighCell3D(1) == NULL )
            {
              if(cell.GetNeighCell3D(fac) == NULL)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            else if(face.GetNeighCell3D(1) != NULL && face.GetNeighCell3D(1)->Id() != idCell)
            {
              idCellToControl = face.GetNeighCell3D(1)->Id();
              if(cell.GetNeighCell3D(fac)->Id() == idCellToControl)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
          }
        }
      }
      break;
      case 2:
      {
        for(unsigned int cel = 0; cel < cells2D.size()-1; cel++)
        {
          Cell2D& cell = *cells2D[cel];
          const unsigned int& idCell = cell.Id();
          unsigned int idCellToControl = 0;
          for(unsigned int edg = 0; edg < cell.NumberOfEdges(); edg++)
          {
            const Cell1D& edge = *cell.GetCell1D(edg);
            if(edge.GetNeighCell2D(0) == NULL )
            {
              if(cell.GetNeighCell2D(edg) == NULL)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            else if(edge.GetNeighCell2D(0) != NULL && edge.GetNeighCell2D(0)->Id() != idCell)
            {
              idCellToControl = edge.GetNeighCell2D(0)->Id();
              if(cell.GetNeighCell2D(edg)->Id() == idCellToControl)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            if(edge.GetNeighCell2D(1) == NULL )
            {
              if(cell.GetNeighCell2D(edg) == NULL)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            else if(edge.GetNeighCell2D(1) != NULL && edge.GetNeighCell2D(1)->Id() != idCell)
            {
              idCellToControl = edge.GetNeighCell2D(1)->Id();
              if(cell.GetNeighCell2D(edg)->Id() == idCellToControl)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
          }
        }
      }
      break;
      case 1:
      {
        for(unsigned int cel = 0; cel < cells1D.size()-1; cel++)
        {
          Cell1D& cell = *cells1D[cel];
          const unsigned int& idCell = cell.Id();
          unsigned int idCellToControl = 0;
          for(unsigned int pnt = 0; pnt < cell.NumberOfVertices(); pnt++)
          {
            const Cell0D& point = *cell.GetCell0D(pnt);
            if(point.GetNeighCell1D(0) == NULL )
            {
              if(cell.GetNeighCell1D(pnt) == NULL)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            else if(point.GetNeighCell1D(0) != NULL && point.GetNeighCell1D(0)->Id() != idCell)
            {
              idCellToControl = point.GetNeighCell1D(0)->Id();
              if(cell.GetNeighCell1D(pnt)->Id() == idCellToControl)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            if(point.GetNeighCell1D(1) == NULL )
            {
              if(cell.GetNeighCell1D(pnt) == NULL)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
            else if(point.GetNeighCell1D(1) != NULL && point.GetNeighCell1D(1)->Id() != idCell)
            {
              idCellToControl = point.GetNeighCell1D(1)->Id();
              if(cell.GetNeighCell1D(pnt)->Id() == idCellToControl)
                continue;
              else
              {
                Output::PrintErrorMessage("%s :Position neigs not right", true, __func__);
                return Output::GenericError;
              }
            }
          }
        }
      }
      break;
      default:
        Output::PrintErrorMessage("%s : Dimension higher than 3 is not supported", true, __func__);
      break;
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CheckMarkers()
  {

    return Output::Success;
  }

  Output::ExitCodes Mesh::UpdateCell(const unsigned int& idCell, const unsigned int& dimensionCell)
  {
    switch(dimensionCell)
    {
      case 2:
      {
        Cell2D& cell = *cells2D[idCell];
        if(!cell.IsActive())
          return Output::Success;

        return ((dimension == 2)? UpdateCell2D_2D(cell) : UpdateCell2D_3D(cell));
      }
      break;
      case 3:
      {
        Cell3D& cell = *cells3D[idCell];
        if(!cell.IsActive())
          return Output::Success;

        return UpdateCell3D(cell);
      }
      break;
      default:
      {
        Output::PrintErrorMessage("Dimensiont %d does not supported", true);
        return Output::GenericError;
      }
      break;
    }
    return Output::Success;
  }

  Output::ExitCodes Mesh::CleanInactiveTreeNode()
  {
    vector<Cell0D*> pointsTemp;
    vector<Cell1D*> edgesTemp;
    vector<Cell2D*> facesTemp;
    vector<Cell3D*> cellsTemp;

    pointsTemp.reserve(NumberOfCells0D());
    edgesTemp.reserve(NumberOfCells1D());
    facesTemp.reserve(NumberOfCells2D());
    cellsTemp.reserve(NumberOfCells3D());

    unsigned int pointId = 0;
    unsigned int edgeId = 0;
    unsigned int faceId = 0;
    unsigned int cellId = 0;

    for (vector<Cell0D*>::iterator pointPtr = cells0D.begin(); pointPtr != cells0D.end(); pointPtr++)
    {
      Cell0D& point = **pointPtr;
      if(point.IsActive())
      {
        point.SetId(pointId++);
        pointsTemp.push_back(*pointPtr);
        for(int numEdge = point.NumberOfNeighCell1D() - 1; numEdge >= 0; numEdge--)
          if(!point.GetNeighCell1D(numEdge)->IsActive())
            point.EraseNeighCell1D(numEdge);
        for(int numFace = point.NumberOfNeighCell2D() - 1; numFace >= 0; numFace--)
          if(!point.GetNeighCell2D(numFace)->IsActive())
            point.EraseNeighCell2D(numFace);
        for(int numCell = point.NumberOfNeighCell3D() - 1; numCell >= 0 ; numCell--)
          if(!point.GetNeighCell3D(numCell)->IsActive())
            point.EraseNeighCell3D(numCell);
      }
      else
        delete &point;
    }

    for (vector<Cell1D*>::iterator edgePtr = cells1D.begin(); edgePtr != cells1D.end(); edgePtr++)
    {
      Cell1D& edge = **edgePtr;
      if(edge.IsActive())
      {
        edge.SetId(edgeId++);
        edgesTemp.push_back(&edge);

        for(int numPnt = edge.NumberOfVertices() - 1; numPnt >= 0; numPnt--)
          if(!edge.GetCell0D(numPnt)->IsActive())
            edge.EraseCell0D(numPnt);
        for(int numEdge = edge.NumberOfNeighCell1D() - 1; numEdge >= 0; numEdge--)
          if(!edge.GetNeighCell1D(numEdge)->IsActive())
            edge.EraseNeighCell1D(numEdge);
        for(int numFace = edge.NumberOfNeighCell2D() - 1; numFace >= 0; numFace--)
          if((edge.GetNeighCell2D(numFace) != NULL) && !edge.GetNeighCell2D(numFace)->IsActive())
            edge.EraseNeighCell2D(numFace);
        for(int numCell = edge.NumberOfNeighCell3D() - 1; numCell >= 0 ; numCell--)
          if((edge.GetNeighCell3D(numCell) != NULL) && !edge.GetNeighCell3D(numCell)->IsActive())
            edge.EraseNeighCell3D(numCell);

        edge.SetFather(NULL);
      }
      else
        delete &edge;
    }

    for (vector<Cell2D*>::iterator facePtr = cells2D.begin(); facePtr != cells2D.end(); facePtr++)
    {
      Cell2D& face = **facePtr;
      if(face.IsActive())
      {
        face.SetId(faceId++);
        facesTemp.push_back(*facePtr);

        for(int numPnt = face.NumberOfVertices() - 1; numPnt >= 0; numPnt--)
          if(!face.GetCell0D(numPnt)->IsActive())
            face.EraseCell0D(numPnt);
        for(int numEdge = face.NumberOfEdges() - 1; numEdge >= 0; numEdge--)
          if(!face.GetCell1D(numEdge)->IsActive())
            face.EraseCell1D(numEdge);
        for(int numFace = face.NumberOfNeighCell2D() - 1; numFace >= 0; numFace--)
          if((face.GetNeighCell2D(numFace) != NULL) && !face.GetNeighCell2D(numFace)->IsActive())
            face.EraseNeighCell2D(numFace);
        for(int numCell = face.NumberOfNeighCell3D() - 1; numCell >= 0 ; numCell--)
          if((face.GetNeighCell3D(numCell) != NULL) && !face.GetNeighCell3D(numCell)->IsActive())
            face.EraseNeighCell3D(numCell);

        face.SetFather(NULL);
      }
      else
        delete &face;
    }

    for (vector<Cell3D*>::iterator cellPtr = cells3D.begin(); cellPtr != cells3D.end(); cellPtr++)
    {
      Cell3D& cell = **cellPtr;
      if(cell.IsActive())
      {
        cell.SetId(cellId++);
        cellsTemp.push_back(*cellPtr);

        for(int numPnt = cell.NumberOfVertices() - 1; numPnt >= 0; numPnt--)
          if(!cell.GetCell0D(numPnt)->IsActive())
            cell.EraseCell0D(numPnt);
        for(int numEdge = cell.NumberOfEdges() - 1; numEdge >= 0; numEdge--)
          if(!cell.GetCell1D(numEdge)->IsActive())
            cell.EraseCell1D(numEdge);
        for(int numFace = cell.NumberOfFaces() - 1; numFace >= 0; numFace--)
          if(!cell.GetCell2D(numFace)->IsActive())
            cell.EraseCell2D(numFace);
        for(int numCell = cell.NumberOfNeighCell3D() - 1; numCell >= 0 ; numCell--)
          if((cell.GetNeighCell3D(numCell) != NULL) && !cell.GetNeighCell3D(numCell)->IsActive())
            cell.EraseNeighCell3D(numCell);

        cell.SetFather(NULL);
      }
      else
        delete &cell;
    }

    unsigned int numPoints = pointsTemp.size();
    unsigned int numEdges = edgesTemp.size();
    unsigned int numFaces = facesTemp.size();
    unsigned int numCells = cellsTemp.size();

    cells0D.resize(numPoints, NULL);
    cells1D.resize(numEdges, NULL);
    cells2D.resize(numFaces, NULL);
    cells3D.resize(numCells, NULL);

    for(unsigned int pnt = 0 ; pnt < numPoints; pnt++)
      cells0D[pnt] = pointsTemp[pnt];

    for(unsigned int edg = 0 ; edg < numEdges; edg++)
      cells1D[edg] = edgesTemp[edg];

    for(unsigned int fac = 0 ; fac < numFaces; fac++)
      cells2D[fac] = facesTemp[fac];

    for(unsigned int cel = 0 ; cel < numCells; cel++)
      cells3D[cel] = cellsTemp[cel];

    return Output::Success;
  }

  double Mesh::ComputeEnergyNorm()
  {
    double energyNorm = 0.0;
    Point centroid;
    double measure;
    switch(dimension)
    {
      case 1:
      {
        for(unsigned int numCell = 0; numCell < NumberOfCells1D(); numCell++)
        {
          const Cell1D& cell = *cells1D[numCell];
          double distanceFromCentroid = 0.0;
          cell.ComputeMeasure(measure);
          cell.ComputeCentroid(centroid);
          for(unsigned int numVert = 0; numVert < cell.NumberOfVertices(); numVert++)
            distanceFromCentroid += MeasurerPoint::SquaredDistancePoints(centroid, *cell.Vertex(numVert));

          energyNorm += measure*measure*distanceFromCentroid;
        }
      }
      break;
      case 2:
      {
        for(unsigned int numCell = 0; numCell < NumberOfCells2D(); numCell++)
        {
          const Cell2D& cell = *cells2D[numCell];
          double distanceFromCentroid = 0.0;
          cell.ComputeMeasure(measure);
          cell.ComputeCentroid(centroid);
          for(unsigned int numVert = 0; numVert < cell.NumberOfVertices(); numVert++)
            distanceFromCentroid += MeasurerPoint::SquaredDistancePoints(centroid, *cell.Vertex(numVert));

          energyNorm += measure*measure*distanceFromCentroid;
        }
      }
      break;
      case 3:
      {
        for(unsigned int numCell = 0; numCell < NumberOfCells3D(); numCell++)
        {
          const Cell3D& cell = *cells3D[numCell];
          double distanceFromCentroid = 0.0;
          cell.ComputeMeasure(measure);
          cell.ComputeCentroid(centroid);
          for(unsigned int numVert = 0; numVert < cell.NumberOfVertices(); numVert++)
            distanceFromCentroid += MeasurerPoint::SquaredDistancePoints(centroid, *cell.Vertex(numVert));

          energyNorm += measure*measure*distanceFromCentroid;
        }
      }
      break;
    }
    return sqrt(energyNorm);
  }
}
