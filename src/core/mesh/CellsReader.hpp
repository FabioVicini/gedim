#ifndef CELLSREADER_H
#define CELLSREADER_H

#include "ICellsReader.hpp"
#include "ITreeNode.hpp"
#include "IGeometricObject.hpp"

namespace GeDiM
{
    /// \brief Class used to navigate a vector of cells.
    /// \details This class extends \ref ICellsReader.
    /// \param CellsType The type of cell. Must be convertible to types
    /// \ref TreeNode and \ref IGeometricObject.
    /// \author Andrea Borio.
    /// \copyright See top level LICENSE file for details.
    /// \sa Cell1D, Cell2D, Cell3D, Mesh.
    template< typename CellsType >
    class CellsReader : public ICellsReader
    {
        protected:
            /// Pointer to the vector of cells to be read.
            const vector<CellsType*>* cellsVectorPtr;
        public:
            /// \brief Default class constructor.
            /// \details Sets \ref cellsVectorPtr to nullptr.
            CellsReader() : cellsVectorPtr(nullptr) {}
            /// \brief Class constructor setting \ref cellsVectorPtr.
            /// \param cellsVector The vector that will be pointed by \ref cellsVectorPtr.
            CellsReader(const vector<CellsType*>& cellsVector) { cellsVectorPtr = &cellsVector; }
            virtual ~CellsReader() {} ///< Class destructor.

            /// \brief Sets \ref cellsVectorPtr to point to given vector.
            /// \param value The vector that will be pointed by \ref cellsVectorPtr.
            void SetCellsVectorPtr(const vector<CellsType*>& value) { cellsVectorPtr = &value; }

            virtual size_t NumberOfCells() const { return cellsVectorPtr->size(); }
            virtual const ITreeNode* CellAsTreeNode(const unsigned int& position) const
            { return cellsVectorPtr->at(position); }
            virtual const IGeometricObject* CellAsGeometricObject(const unsigned int& position) const
            { return cellsVectorPtr->at(position); }
            virtual ITreeNode* CellAsTreeNode(const unsigned int& position)
            { return cellsVectorPtr->at(position); }
            virtual IGeometricObject* CellAsGeometricObject(const unsigned int& position)
            { return cellsVectorPtr->at(position); }
    };
}

#endif // CELLSREADER_H
