#ifndef CELL3D_HPP
#define CELL3D_HPP

#include "MeshTreeNode.hpp"
#include "Polyhedron.hpp"
#include "Cell0D.hpp"
#include "Cell1D.hpp"
#include "Cell2D.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
    class Cell0D;
    class Cell1D;
    class Cell2D;
    class Cell3D;

    class Cell3D : public MeshTreeNode, public Polyhedron
    {
        private:
            vector<bool> normalSign;
            vector<const Cell3D*> cells3D;

        public:
            explicit Cell3D(const unsigned int& id) : MeshTreeNode(), Polyhedron(id) {}
            Cell3D(const Cell3D& cell3D) : MeshTreeNode(cell3D), Polyhedron(cell3D) { *this = cell3D; }
            Cell3D(Cell3D&& cell3D) noexcept : MeshTreeNode(move(cell3D)), Polyhedron(move(cell3D)) { *this = move(cell3D); }
            Cell3D& operator=(const Cell3D& cell3D);
            Cell3D& operator=(Cell3D&& cell3D);
            virtual ~Cell3D() { cells3D.clear(); }

            void AllocateNormalSign() { normalSign.resize(NumberOfFaces()); }
            void SetNormalSign(const bool& sign, const unsigned int& position) { normalSign[position] = sign; }
            void ComputeNormalSign();
            bool NormalSign(const unsigned int& position) { return normalSign[position]; }

            void ShrinkNeighCells0D() { vertices.shrink_to_fit(); }
            void ShrinkNeighCells1D() { edges.shrink_to_fit(); }
            void ShrinkNeighCells2D() { faces.shrink_to_fit(); }
            void ShrinkNeighCells3D() { cells3D.shrink_to_fit(); }

            void AllocateNeighCell3D(const unsigned int& numberOfFaces) { cells3D.resize(numberOfFaces, nullptr); }
            void ReAllocateNeighCell3D(const unsigned int& numberOfFaces) { cells3D.assign(numberOfFaces, NULL); }
            void InitializeNeighCell3D(const unsigned int& numberOfFaces) { cells3D.reserve(numberOfFaces); }

            void EraseCell0D(const unsigned int& position) {vertices.erase(vertices.begin() + position);}
            void EraseCell1D(const unsigned int& position) {edges.erase(edges.begin() + position);}
            void EraseCell2D(const unsigned int& position) {faces.erase(faces.begin() +position);}
            void EraseNeighCell3D(const unsigned int& position) {cells3D.erase(cells3D.begin() + position);}

            void AddNeighCell3D(const Cell3D& cell3D) { cells3D.push_back(&cell3D);}
            void InsertNeighCell3D(const Cell3D& cell3D, const unsigned int& position) { cells3D[position] = &cell3D; }

            const Cell0D* GetCell0D(const unsigned int& position) const { return static_cast<const Cell0D*>(vertices[position]);}
            const Cell1D* GetCell1D(const unsigned int& position) const { return static_cast<const Cell1D*>(edges[position]);}
            const Cell2D* GetCell2D(const unsigned int& position) const { return static_cast<const Cell2D*>(faces[position]);}
            const Cell3D* GetNeighCell3D(const unsigned int& position) const { return cells3D[position];}

            unsigned int NumberOfNeighCell3D() const { return cells3D.size(); }
    };
}

#endif //CELL3D_HPP
