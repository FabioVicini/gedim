#ifndef CELL0D_HPP
#define CELL0D_HPP

#include "MeshTreeNode.hpp"
#include "Point.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
    class Cell0D;
    class Cell1D;
    class Cell2D;
    class Cell3D;

    class Cell0D : public MeshTreeNode, public Point
    {
        private:
            vector<const Cell1D*> cells1D;
            vector<const Cell2D*> cells2D;
            vector<const Cell3D*> cells3D;

        public:
            explicit Cell0D(const unsigned int& id) : MeshTreeNode(), Point(id) {}
            explicit Cell0D(const double& x, const unsigned int& id) : MeshTreeNode(), Point(x, id) {}
            explicit Cell0D(const double& x, const double& y, const unsigned int& id) : MeshTreeNode(), Point(x, y, id) {}
            explicit Cell0D(const double& x, const double& y, const double& z, const unsigned int& id) : MeshTreeNode(), Point(x, y, z, id) {}
            Cell0D(const Cell0D& cell0D) : MeshTreeNode(cell0D), Point(cell0D) { *this = cell0D; }
            Cell0D(Cell0D&& cell0D) noexcept : MeshTreeNode(move(cell0D)), Point(move(cell0D)) { *this = move(cell0D); }
            Cell0D& operator=(const Cell0D& cell0D);
            Cell0D& operator=(Cell0D&& cell0D);
            virtual ~Cell0D() { cells1D.clear(); cells2D.clear(); cells3D.clear();}

            void AllocateNeighbourhood1D(const unsigned int& numNeighs = 2) { cells1D.resize(numNeighs, nullptr); }
            void AllocateNeighbourhood2D(const unsigned int& numNeighs) { cells2D.resize(numNeighs, nullptr); }
            void AllocateNeighbourhood3D(const unsigned int& numNeighs) { cells3D.resize(numNeighs, nullptr);}

            void InitializeNeighbourhood1D(const unsigned int& numNeighs  = 2) { cells1D.reserve(numNeighs); }
            void InitializeNeighbourhood2D(const unsigned int& numNeighs) { cells2D.reserve(numNeighs); }
            void InitializeNeighbourhood3D(const unsigned int& numNeighs) { cells3D.reserve(numNeighs); }

            void InsertNeighCell1D(const Cell1D& cell1D, const unsigned int& position) { cells1D[position] = &cell1D; }
            void InsertNeighCell2D(const Cell2D& cell2D, const unsigned int& position) { cells2D[position] = &cell2D; }
            void InsertNeighCell3D(const Cell3D& cell3D, const unsigned int& position) { cells3D[position] = &cell3D; }

            void AddNeighCell1D(const Cell1D& cell1D) { cells1D.push_back(&cell1D); }
            void AddNeighCell2D(const Cell2D& cell2D) { cells2D.push_back(&cell2D); }
            void AddNeighCell3D(const Cell3D& cell3D) { cells3D.push_back(&cell3D); }

            const Cell1D* GetNeighCell1D(const unsigned int& position) const { return cells1D[position];}
            const Cell2D* GetNeighCell2D(const unsigned int& position) const { return cells2D[position];}
            const Cell3D* GetNeighCell3D(const unsigned int& position) const { return cells3D[position];}

            unsigned int NumberOfNeighCell1D() const { return cells1D.size(); }
            unsigned int NumberOfNeighCell2D() const { return cells2D.size(); }
            unsigned int NumberOfNeighCell3D() const { return cells3D.size(); }

            void EraseNeighCell1D(const unsigned int& position) {cells1D.erase(cells1D.begin() + position);}
            void EraseNeighCell2D(const unsigned int& position) {cells2D.erase(cells2D.begin() +position);}
            void EraseNeighCell3D(const unsigned int& position) {cells3D.erase(cells3D.begin() + position);}

            void ShrinkNeighCells1D() { cells1D.shrink_to_fit(); }
            void ShrinkNeighCells2D() { cells2D.shrink_to_fit(); }
            void ShrinkNeighCells3D() { cells3D.shrink_to_fit(); }
    };
}

#endif //CELL1D_HPP
