#ifndef __ICELLSREADER_H
#define __ICELLSREADER_H

#include "ITreeNode.hpp"
#include "IGeometricObject.hpp"

namespace GeDiM
{
    /// \brief Interface to methods used to navigate a vector of cells.
    /// \details This class contains interfaces to the methods implemented
    /// by \ref CellsReader, that are intended to be used to navigate a
    /// generic vector of cells that can be converted to \ref ITreeNode
    /// objects and \ref IGeometricObject objects.
    /// \author Andrea Borio.
    /// \copyright See top level LICENSE file for details.
    /// \sa Cell0D, Cell1D, Cell2D, Cell3D, Mesh.
    class ICellsReader
    {
        public:
            virtual ~ICellsReader() {} ///< Default destructor.

            virtual size_t NumberOfCells() const = 0; ///< \returns The number of cells.
            /// \brief Get const pointer to a cell seen as a \ref TreeNode object.
            /// \param position The position of the cell to be read.
            /// \returns A const pointer to the \ref TreeNode.
            virtual const ITreeNode* CellAsTreeNode(const unsigned int& position) const = 0;
            /// \brief Get pointer to a cell seen as a \ref TreeNode object.
            /// \param position The position of the cell to be read.
            /// \returns A non-const pointer to the \ref TreeNode.
            virtual ITreeNode* CellAsTreeNode(const unsigned int& position) = 0;
            /// \brief Get const pointer to a cell seen as a \ref GeometricObject object.
            /// \param position The position of the cell to be read.
            /// \returns A const pointer to the \ref GeometricObject.
            virtual const IGeometricObject* CellAsGeometricObject(const unsigned int& position) const = 0;
            /// \brief Get pointer to a cell seen as a \ref GeometricObject object.
            /// \param position The position of the cell to be read.
            /// \returns A non-const pointer to the \ref GeometricObject.
            virtual IGeometricObject* CellAsGeometricObject(const unsigned int& position) = 0;
    };
}

#endif // __ICELLSREADER_H
