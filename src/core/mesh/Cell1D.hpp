#ifndef CELL1D_HPP
#define CELL1D_HPP

#include "MeshTreeNode.hpp"
#include "Segment.hpp"
#include "Cell0D.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
    class Cell0D;
    class Cell1D;
    class Cell2D;
    class Cell3D;

    class Cell1D : public MeshTreeNode, public Segment
    {
        private:
            vector<bool> normalSign;

            vector<const Cell1D*> cells1D;
            vector<const Cell2D*> cells2D;
            vector<const Cell3D*> cells3D;

        public:
            explicit Cell1D(const unsigned int& id) : MeshTreeNode(), Segment(id) {}
            Cell1D(const Cell1D& cell1D) : MeshTreeNode(cell1D), Segment(cell1D) { *this = cell1D; }
            Cell1D(Cell1D&& cell1D) noexcept  : MeshTreeNode(move(cell1D)), Segment(move(cell1D)) { *this = cell1D; }
            Cell1D& operator=(const Cell1D& cell1D);
            Cell1D& operator=(Cell1D&& cell1D);
            virtual ~Cell1D() { cells1D.clear(); cells2D.clear(); cells3D.clear(); }

            void AllocateNormalSign() { normalSign.resize(NumberOfVertices()); }
            void SetNormalSign(const bool& sign, const unsigned int& position) { normalSign[position] = sign; }
            bool NormalSign(const unsigned int& position) { return normalSign[position]; }

            void AllocateNeighbourhood1D(const unsigned int& numNeighs) { cells1D.resize(numNeighs, nullptr); }
            void AllocateNeighbourhood2D(const unsigned int& numNeighs = 2) { cells2D.resize(numNeighs, nullptr); }
            void AllocateNeighbourhood3D(const unsigned int& numNeighs) { cells3D.resize(numNeighs, nullptr);}

            void InitializeNeighbourhood1D(const unsigned int& numNeighs) { cells1D.reserve(numNeighs); }
            void InitializeNeighbourhood2D(const unsigned int& numNeighs = 2) { cells2D.reserve(numNeighs); }
            void InitializeNeighbourhood3D(const unsigned int& numNeighs) { cells3D.reserve(numNeighs); }

            void InsertNeighCell1D(const Cell1D& cell1D, const unsigned int& position) { cells1D[position] = &cell1D; }
            void InsertNeighCell2D(const Cell2D& cell2D, const unsigned int& position) { cells2D[position] = &cell2D; }
            void InsertNeighCell3D(const Cell3D& cell3D, const unsigned int& position) { cells3D[position] = &cell3D; }

            void AddNeighCell1D(const Cell1D& cell1D) { cells1D.push_back(&cell1D); }
            void AddNeighCell2D(const Cell2D& cell2D) { cells2D.push_back(&cell2D); }
            void AddNeighCell3D(const Cell3D& cell3D) { cells3D.push_back(&cell3D); }

            const Cell0D* GetCell0D(const unsigned int& position) const {return static_cast<const Cell0D*>(vertices[position]); }
            const Cell1D* GetNeighCell1D(const unsigned int& position) const { return cells1D[position];}
            const Cell2D* GetNeighCell2D(const unsigned int& position) const { return cells2D[position];}
            const Cell3D* GetNeighCell3D(const unsigned int& position) const { return cells3D[position];}

            unsigned int NumberOfNeighCell1D() const { return cells1D.size(); }
            unsigned int NumberOfNeighCell2D() const { return cells2D.size(); }
            unsigned int NumberOfNeighCell3D() const { return cells3D.size(); }

            void ShrinkNeighCells0D() { vertices.shrink_to_fit(); }
            void ShrinkNeighCells1D() { cells1D.shrink_to_fit(); }
            void ShrinkNeighCells2D() { cells2D.shrink_to_fit(); }
            void ShrinkNeighCells3D() { cells3D.shrink_to_fit(); }

            void EraseCell0D(const unsigned int& position) {vertices.erase(vertices.begin() + position);}
            void EraseNeighCell1D(const unsigned int& position) {cells1D.erase(cells1D.begin() + position);}
            void EraseNeighCell2D(const unsigned int& position) {cells2D.erase(cells2D.begin() +position);}
            void EraseNeighCell3D(const unsigned int& position) {cells3D.erase(cells3D.begin() + position);}

    };
}

#endif //CELL1D_HPP
