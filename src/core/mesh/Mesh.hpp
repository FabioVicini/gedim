#ifndef MESH_H
#define MESH_H

#include <vector>
#include "Eigen"
#include "IMesh.hpp"
#include "Output.hpp"
#include "Cell0D.hpp"
#include "Cell1D.hpp"
#include "Cell2D.hpp"
#include "Cell3D.hpp"
#include "ICellsReader.hpp"
#include "IGeometryFactory.hpp"

using namespace std;
using namespace Eigen;

namespace GeDiM
{
	class Mesh : public IMesh, public IGeometryFactory
	{
		protected:
			/// \brief The dimension of the mesh.
			/// \details Defaults to 0.
			unsigned short dimension;
			vector<Cell3D*> cells3D; ///< Cells of the mesh.
			vector<Cell2D*> cells2D; ///< Faces of the mesh.
			vector<Cell1D*> cells1D; ///< Edges of the mesh.
			vector<Cell0D*> cells0D; ///< Points of the mesh.
			/// \brief Pointer to a \ref ICellsReader object.
			/// \details This is initialized by the \ref SetDimension method and
			/// is used to get cells of maximal dimensions.
			ICellsReader* maximalCellsReaderPtr;

      Output::ExitCodes UpdateCell2D_2D(Cell2D& cell);
      Output::ExitCodes UpdateCell2D_3D(Cell2D& face);
      Output::ExitCodes UpdateCell3D(Cell3D& cell);

		public:
			Mesh(const unsigned short& _dimension = 0) : maximalCellsReaderPtr(nullptr) { SetDimension(_dimension); }
      Mesh(const Mesh&) {}
			virtual ~Mesh();

			size_t NumberOfCells3D() const { return cells3D.size(); }
			size_t NumberOfCells2D() const { return cells2D.size(); }
			size_t NumberOfCells1D() const { return cells1D.size(); }
			size_t NumberOfCells0D() const { return cells0D.size(); }
			size_t NumberOfMaximalCells() const { return maximalCellsReaderPtr->NumberOfCells(); }
			const Cell3D* GetCell3D(const unsigned int& position) const { return cells3D[position]; }
			const Cell2D* GetCell2D(const unsigned int& position) const { return cells2D[position]; }
			const Cell1D* GetCell1D(const unsigned int& position) const { return cells1D[position]; }
			const Cell0D* GetCell0D(const unsigned int& position) const { return cells0D[position]; }

			Cell3D* GetCell3D(const unsigned int& position) { return cells3D[position]; }
			Cell2D* GetCell2D(const unsigned int& position) { return cells2D[position]; }
			Cell1D* GetCell1D(const unsigned int& position) { return cells1D[position]; }
			Cell0D* GetCell0D(const unsigned int& position) { return cells0D[position]; }
			/// \brief Get const pointer to given maximal \ref TreeNode object.
			/// \param position The position of the object to be read within the corresponding vector.
			/// \returns A const pointer to the object.
			const ITreeNode* GetMaximalTreeNode(const unsigned int& position) const
			{ return maximalCellsReaderPtr->CellAsTreeNode(position); }
			/// \brief Get const pointer to given maximal \ref IGeometricObject object.
			/// \param position The position of the object to be read within the corresponding vector.
			/// \returns A const pointer to the object.
			const IGeometricObject* GetMaximalGeometricObject(const unsigned int& position) const
			{ return maximalCellsReaderPtr->CellAsGeometricObject(position); }
			/// \brief Get non-const pointer to given maximal \ref TreeNode object.
			/// \param position The position of the object to be read within the corresponding vector.
			/// \returns A non-const pointer to the object.
			ITreeNode* GetMaximalTreeNode(const unsigned int& position)
			{ return maximalCellsReaderPtr->CellAsTreeNode(position); }
			/// \brief Get non-const pointer to given maximal \ref IGeometricObject object.
			/// \param position The position of the object to be read within the corresponding vector.
			/// \returns A non-const pointer to the object.
			IGeometricObject* GetMaximalGeometricObject(const unsigned int& position)
			{ return maximalCellsReaderPtr->CellAsGeometricObject(position); }
			/// \brief Get mesh dimension.
			/// \returns A const reference to \ref dimension.
			const unsigned short& Dimension() const { return dimension; }
			/// \brief Set mesh dimension.
			/// \details This method also allocates \ref maximalCellsReaderPtr
			/// according to \ref dimension.
			/// \param _dimension The value to which \ref dimension will be
			/// set. Must be one of 0, 1, 2 or 3.
			/// \returns \ref MainApplication::Output::Success if the method was
			/// succesful.\n MainApplication::Output::GenericError if the value of
			/// _dimension is invalid.
			Output::ExitCodes SetDimension(const unsigned short& _dimension);
			/// Rotate the mesh using the rotation in IRotation
      Output::ExitCodes Rotate(const bool& = false) { return Output::UnimplementedMethod; }
			/// Rotate the mesh using matrix Rotation in Input
      Output::ExitCodes RotateWithInput(const Matrix3d&,
                                        const Vector3d&,
                                        const bool& = false) { return Output::UnimplementedMethod; }
      Output::ExitCodes RotateCellWithInput(const unsigned int&,
                                            const Matrix3d&,
                                            const Vector3d&,
                                            const bool& = false) { return Output::UnimplementedMethod; }
      Output::ExitCodes RotateFaceWithInput(const unsigned int&,
                                            const Matrix3d&,
                                            const Vector3d&,
                                            const bool& = false) { return Output::UnimplementedMethod; }

			Cell3D* CreateCell3D() { return new Cell3D(cells3D.size()); }
			Cell2D* CreateCell2D() { return new Cell2D(cells2D.size()); }
			Cell1D* CreateCell1D() { return new Cell1D(cells1D.size()); }
			Cell0D* CreateCell0D() { return new Cell0D(cells0D.size()); }

			void InitializeCells3D(const size_t numberOfCells) { cells3D.reserve(numberOfCells); }
			void InitializeCells2D(const size_t numberOfFaces) { cells2D.reserve(numberOfFaces); }
			void InitializeCells1D(const size_t numberOfEdges) { cells1D.reserve(numberOfEdges); }
			void InitializeCells0D(const size_t numberOfPoints) { cells0D.reserve(numberOfPoints); }

			Output::ExitCodes AddCell3D(Cell3D* block);
			Output::ExitCodes AddCell2D(Cell2D* polygon);
			Output::ExitCodes AddCell1D(Cell1D* edge);
			Output::ExitCodes AddCell0D(Cell0D* point);

			Output::ExitCodes ExportInfoMesh3D(const string& pathFile) const;
			//			const bool FindCoordinates(const Vector3d& coordinates, unsigned int& idPoint, const double& toll = 1.0E-5);

      Output::ExitCodes CheckDoublePoints(const double& toll = 1.0E-7);
      Output::ExitCodes CheckPointsInCells3D();
      Output::ExitCodes CheckPointsInCells2D();
      Output::ExitCodes CheckPointsEqualsEdgesInCells2D();
      Output::ExitCodes CheckEdgesInCells2D();
      Output::ExitCodes CheckEdgesInCells3D();
      Output::ExitCodes CheckDoubleCells3D();
      Output::ExitCodes CheckDoubleCells2D();
      Output::ExitCodes CheckDoubleCells1D();
      Output::ExitCodes CheckNeigs();
      Output::ExitCodes CheckMarkers();

			//			void MovePointMesh(const unsigned int& position,const Vector3d& newCoordinate) { points[position]->SetCoordinates(newCoordinate); }
			//			const Output::ExitCodes CutEdgeWithPoints(const unsigned int& idEdge, const vector<Vector3d >& coordinatesPoints, const bool& inheritProperty = true);
			//			const Output::ExitCodes CutEdgeWithCoordinateCurvilinears(const unsigned int& idEdge, const vector<double>& coordinatesCurvilinear, const bool& inheritProperty = true);

      Output::ExitCodes UpdateCell(const unsigned int& idCell, const unsigned int& dimensionCell);
			//			const Output::ExitCodes CreateFaceChild(Cell2D& face, Cell2D& faceFather, const list<unsigned int>& idEdgesFace, const list<unsigned int>& idPointFace, const bool& property = true);

			//			const Output::ExitCodes UpdateBlock(const unsigned int& idCell, const int& idEdge = -1);
			//			const Output::ExitCodes CreateCellChild2D(Cell2D& cell, Cell2D& cellFather, const list<unsigned int>& idEdgesCell, const list<unsigned int>& idPointCell, const bool& property = true);

			//			const Output::ExitCodes ComputeGeometricalProperties();

			//			const Output::ExitCodes ActivateFatherNodes();
			//			const Output::ExitCodes ActivateChildrenNodes();

			//			virtual const Output::ExitCodes CleanInactiveTreeNode();
      virtual Output::ExitCodes CleanInactiveTreeNode();
      double ComputeEnergyNorm();
			// IGeometryFactory interface
			void Reset();
			unsigned int NumberPoints() const { return NumberOfCells0D(); }
			unsigned int NumberSegments() const { return NumberOfCells1D(); }
			unsigned int NumberPolygons() const { return NumberOfCells2D(); }
			unsigned int NumberPolyhedrons() const { return NumberOfCells3D(); }
			Point& GetPoint(const unsigned int& id) { return *GetCell0D(id); }
			const Point& GetPoint(const unsigned int& id) const { return *GetCell0D(id); }
			Segment& GetSegment(const unsigned int& id) { return *GetCell1D(id); }
			const Segment& GetSegment(const unsigned int& id) const { return *GetCell1D(id); }
			Polygon& GetPolygon(const unsigned int& id) { return *GetCell2D(id); }
			const Polygon& GetPolygon(const unsigned int& id) const { return *GetCell2D(id); }
			Polyhedron& GetPolyhedron(const unsigned int& id) { return *GetCell3D(id); }
			const Polyhedron& GetPolyhedron(const unsigned int& id) const { return *GetCell3D(id); }
			bool ExistPoint(const unsigned int& id) const { return id < NumberPoints(); }
			bool ExistSegment(const unsigned int& id) const { return id < NumberSegments(); }
			bool ExistPolygon(const unsigned int& id) const { return id < NumberPolygons(); }
			bool ExistPolyhedron(const unsigned int& id) const { return id < NumberPolyhedrons(); }
			Point& CreatePoint() { AddCell0D(CreateCell0D()); return *cells0D.back(); }
			Segment& CreateSegment() { AddCell1D(CreateCell1D()); return *cells1D.back(); }
			Polygon& CreatePolygon() { AddCell2D(CreateCell2D()); return *cells2D.back(); }
			Polyhedron& CreatePolyhedron() { AddCell3D(CreateCell3D()); return *cells3D.back(); }
	};
}
#endif // MESH_H
