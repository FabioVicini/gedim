#ifndef __IMESH_H
#define __IMESH_H

#include "Eigen"
#include "Output.hpp"
#include "ITreeNode.hpp"
#include "Cell0D.hpp"
#include "Cell1D.hpp"
#include "Cell2D.hpp"
#include "Cell3D.hpp"

using namespace std;
using namespace Eigen;

namespace GeDiM
{
  /// \brief Interface used to implement the Mesh
  /// \copyright See top level LICENSE file for details.
  class IMesh
  {
    public:
      virtual ~IMesh() { }

      /// \return The number of Cells3D
      virtual size_t NumberOfCells3D() const = 0;
      /// \return The number of Cells2D
      virtual size_t NumberOfCells2D() const = 0;
      /// \return The number of Cells1D
      virtual size_t NumberOfCells1D() const = 0;
      /// \return The number of Cells0D
      virtual size_t NumberOfCells0D() const = 0;
      /// \return The number of Maximal Cells
      virtual size_t NumberOfMaximalCells() const = 0;
      /// \return The Cell3D at position
      virtual const Cell3D* GetCell3D(const unsigned int& position) const = 0;
      /// \return The Cell2D at position
      virtual const Cell2D* GetCell2D(const unsigned int& position) const = 0;
      /// \return The Cell1D at position
      virtual const Cell1D* GetCell1D(const unsigned int& position) const = 0;
      /// \return The Cell0D at position
      virtual const Cell0D* GetCell0D(const unsigned int& position) const = 0;

      /// \brief Get const pointer to given maximal \ref TreeNode object.
      /// \param position The position of the object to be read within the corresponding vector.
      /// \returns A const pointer to the object.
      virtual const ITreeNode* GetMaximalTreeNode(const unsigned int& position) const = 0;

      /// \brief Get const pointer to given maximal \ref IGeometricObject object.
      /// \param position The position of the object to be read within the corresponding vector.
      /// \returns A const pointer to the object.
      virtual const IGeometricObject* GetMaximalGeometricObject(const unsigned int& position) const = 0;

      /// \return The Cell3D at position
      virtual Cell3D* GetCell3D(const unsigned int& position) = 0;
      /// \return The Cell2D at position
      virtual Cell2D* GetCell2D(const unsigned int& position) = 0;
      /// \return The Cell1D at position
      virtual Cell1D* GetCell1D(const unsigned int& position) = 0;
      /// \return The Cell0D at position
      virtual Cell0D* GetCell0D(const unsigned int& position) = 0;

      /// \brief Get non-const pointer to given maximal \ref TreeNode object.
      /// \param position The position of the object to be read within the corresponding vector.
      /// \returns A non-const pointer to the object.
      virtual ITreeNode* GetMaximalTreeNode(const unsigned int& position) = 0;

      /// \brief Get non-const pointer to given maximal \ref IGeometricObject object.
      /// \param position The position of the object to be read within the corresponding vector.
      /// \returns A non-const pointer to the object.
      virtual IGeometricObject* GetMaximalGeometricObject(const unsigned int& position) = 0;

      /// \brief Get mesh dimension.
      /// \returns A const reference to \ref dimension.
      virtual const unsigned short& Dimension() const = 0;

      /// \brief Set mesh dimension.
      /// \details This method also allocates \ref maximalCellsReaderPtr according to \ref dimension.
      /// \param _dimension The value to which \ref dimension will be set. Must be one of 0, 1, 2 or 3.
      /// \returns \ref MainApplication::Output::Success if the method was
      /// succesful.\n MainApplication::Output::GenericError if the value of
      /// _dimension is invalid.
      virtual Output::ExitCodes SetDimension(const unsigned short& _dimension) = 0;

      /// Rotate the mesh using the rotation in IRotation
      virtual Output::ExitCodes Rotate(const bool& inverse = false) = 0;
      /// Rotate the mesh using matrix Rotation in Input
      virtual Output::ExitCodes RotateWithInput(const Matrix3d& rotationMatrix,
                                                const Vector3d& translationVector,
                                                const bool& inverse = false) = 0;
      virtual Output::ExitCodes RotateCellWithInput(const unsigned int& idCell,
                                                    const Matrix3d& rotationMatrix,
                                                    const Vector3d& translationVector,
                                                    const bool& inverse = false) = 0;
      virtual Output::ExitCodes RotateFaceWithInput(const unsigned int& idFace,
                                                    const Matrix3d& rotationMatrix,
                                                    const Vector3d& translationVector,
                                                    const bool& inverse = false) = 0;

      /// \return A New Cell3D
      virtual Cell3D* CreateCell3D() = 0;
      /// \return A New Cell2D
      virtual Cell2D* CreateCell2D() = 0;
      /// \return A New Cell1D
      virtual Cell1D* CreateCell1D() = 0;
      /// \return A New Cell0D
      virtual Cell0D* CreateCell0D() = 0;

      /// \return Initialize the number of Cells3D
      virtual void InitializeCells3D(const size_t numberOfCells) = 0;
      /// \return Initialize the number of Cells2D
      virtual void InitializeCells2D(const size_t numberOfFaces) = 0;
      /// \return Initialize the number of Cells1D
      virtual void InitializeCells1D(const size_t numberOfEdges) = 0;
      /// \return Initialize the number of Cells0D
      virtual void InitializeCells0D(const size_t numberOfPoints) = 0;

      /// \brief Add a Cell3D to the Mesh
      /// \return The result of the add
      virtual Output::ExitCodes AddCell3D(Cell3D* block) = 0;
      /// \brief Add a Cell2D to the Mesh
      /// \return The result of the add
      virtual Output::ExitCodes AddCell2D(Cell2D* polygon) = 0;
      /// \brief Add a Cell1D to the Mesh
      /// \return The result of the add
      virtual Output::ExitCodes AddCell1D(Cell1D* edge) = 0;
      /// \brief Add a Cell0D to the Mesh
      /// \return The result of the add
      virtual Output::ExitCodes AddCell0D(Cell0D* point) = 0;
  };
}
#endif // __IMESH_H
