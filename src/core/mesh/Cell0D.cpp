#include "Cell0D.hpp"

namespace GeDiM
{

	Cell0D& Cell0D::operator=(const Cell0D& cell0D)
	{
    this->MeshTreeNode::operator =(cell0D);
		this->Point::operator =(cell0D);

		cells1D.reserve(cell0D.cells1D.size());
		for(unsigned int cel1D = 0; cel1D < cell0D.cells1D.size(); cel1D++)
			cells1D.push_back(cell0D.cells1D[cel1D]);

		cells2D.reserve(cell0D.cells2D.size());
		for(unsigned int cel2D = 0; cel2D < cell0D.cells2D.size(); cel2D++)
			cells2D.push_back(cell0D.cells2D[cel2D]);

		cells3D.reserve(cell0D.cells3D.size());
		for(unsigned int cel3D = 0; cel3D < cell0D.cells3D.size(); cel3D++)
			cells3D.push_back(cell0D.cells3D[cel3D]);

		return *this;
	}

	Cell0D& Cell0D::operator=(Cell0D&& cell0D)
	{
		if(this != &cell0D)
		{
			this->Point::operator =(move(cell0D));
      this->MeshTreeNode::operator =(move(cell0D));
			cells1D.reserve(cell0D.cells1D.size());
			cells2D.reserve(cell0D.cells2D.size());
			cells3D.reserve(cell0D.cells3D.size());
			for(unsigned int cel1D = 0; cel1D < cell0D.cells1D.size(); cel1D++)
				cells1D.push_back(cell0D.cells1D[cel1D]);
			for(unsigned int cel2D = 0; cel2D < cell0D.cells2D.size(); cel2D++)
				cells2D.push_back(cell0D.cells2D[cel2D]);
			for(unsigned int cel3D = 0; cel3D < cell0D.cells3D.size(); cel3D++)
				cells3D.push_back(cell0D.cells3D[cel3D]);

			cell0D.cells1D.clear();
			cell0D.cells2D.clear();
			cell0D.cells3D.clear();
		}
		return *this;
	}

}


