#ifndef CELL2D_HPP
#define CELL2D_HPP

#include "MeshTreeNode.hpp"
#include "Polygon.hpp"
#include "Cell1D.hpp"
#include "Cell0D.hpp"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  class Cell2D;
  class Cell3D;

  class Cell2D : public MeshTreeNode, public Polygon
  {
    private:
      vector<bool> normalSign;

      vector<const Cell2D*> cells2D;
      vector<const Cell3D*> cells3D;

    public:
      explicit Cell2D(const unsigned int& id) : MeshTreeNode(), Polygon(id) { normalSign.resize(2); }
      Cell2D(const Cell2D& cell2D) : MeshTreeNode(cell2D), Polygon(cell2D) { *this = cell2D; }
      Cell2D(Cell2D&& cell2D) noexcept : MeshTreeNode(move(cell2D)), Polygon(move(cell2D)) { *this = move(cell2D); }
      Cell2D& operator=(const Cell2D& cell2D);
      Cell2D& operator=(Cell2D&& cell2D);
      virtual ~Cell2D() { normalSign.clear(); cells2D.clear(); cells3D.clear(); }

      void AllocateNormalSign() { normalSign.resize(NumberOfEdges()); }
      void SetNormalSign(const bool& sign, const unsigned int& position) { normalSign[position] = sign; }
      void ComputeNormalSign();
      bool NormalSign(const unsigned int& position) const { return normalSign[position]; }

      void ShrinkNeighCells0D() { vertices.shrink_to_fit(); }
      void ShrinkNeighCells1D() { edges.shrink_to_fit(); }
      void ShrinkNeighCells2D() { cells2D.shrink_to_fit(); }
      void ShrinkNeighCells3D() { cells3D.shrink_to_fit(); }

      void ReAllocateNeighCell2D(const size_t& numberOfCells) { cells2D.assign(numberOfCells, NULL); }
      void ReAllocateNeighCell3D(const size_t& numberOfCells) { cells3D.assign(numberOfCells, NULL); }

      void EraseCell0D(const unsigned int& position) {vertices.erase(vertices.begin() + position);}
      void EraseCell1D(const unsigned int& position) {edges.erase(edges.begin() + position);}
      void EraseNeighCell2D(const unsigned int& position) {cells2D.erase(cells2D.begin() +position);}
      void EraseNeighCell3D(const unsigned int& position) {cells3D.erase(cells3D.begin() + position);}

      void AllocateNeighCell2D(const unsigned int& numNeighs) { cells2D.resize(numNeighs, nullptr); }
      void AllocateNeighCell3D(const unsigned int& numNeighs = 2) { cells3D.resize(numNeighs, nullptr);}

      void InitializeNeighCell2D(const unsigned int& numNeighs) { cells2D.reserve(numNeighs); }
      void InitializeNeighCell3D(const unsigned int& numNeighs = 2) { cells3D.reserve(numNeighs); }

      void InsertNeighCell2D(const Cell2D& cell2D, const unsigned int& position) { cells2D[position] = &cell2D; }
      void InsertNeighCell3D(const Cell3D& cell3D, const unsigned int& position) { cells3D[position] = &cell3D; }

      void AddNeighCell2D(const Cell2D& cell2D) { cells2D.push_back(&cell2D); }
      void AddNeighCell3D(const Cell3D& cell3D) { cells3D.push_back(&cell3D); }

      const Cell0D* GetCell0D(const unsigned int& position) const { return static_cast<const Cell0D*>(vertices[position]);}
      const Cell1D* GetCell1D(const unsigned int& position) const { return static_cast<const Cell1D*>(edges[position]);}
      const Cell2D* GetNeighCell2D(const unsigned int& position) const { return cells2D[position];}
      const Cell3D* GetNeighCell3D(const unsigned int& position) const { return cells3D[position];}

      unsigned int NumberOfNormals() const { return normalSign.size(); }
      unsigned int NumberOfNeighCell2D() const { return cells2D.size(); }
      unsigned int NumberOfNeighCell3D() const { return cells3D.size(); }

      virtual Output::ExitCodes ShiftPositionVertices(const unsigned int &positionVertex);
      virtual Output::ExitCodes ChangeOrientation();
      virtual Output::ExitCodes AllignEdgesVertices();
  };
}

#endif //CELL2D_HPP
