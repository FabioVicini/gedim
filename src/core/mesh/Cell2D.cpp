#include "Cell2D.hpp"

namespace GeDiM
{

  Cell2D& Cell2D::operator=(const Cell2D& cell2D)
  {
    this->MeshTreeNode::operator =(cell2D);
    this->Polygon::operator =(cell2D);


    normalSign.reserve(cell2D.normalSign.size());
    for(unsigned int normSign = 0; normSign < cell2D.normalSign.size(); normSign++)
      normalSign.push_back(cell2D.normalSign[normSign]);

    cells2D.reserve(cell2D.cells2D.size());
    for(unsigned int cel2D = 0; cel2D < cell2D.cells2D.size(); cel2D++)
      cells2D.push_back(cell2D.cells2D[cel2D]);

    cells3D.reserve(cell2D.cells3D.size());
    for(unsigned int cel3D = 0; cel3D < cell2D.cells3D.size(); cel3D++)
      cells3D.push_back(cell2D.cells3D[cel3D]);

    return *this;
  }

  void Cell2D::ComputeNormalSign()
  {
    AllocateNormalSign();
    Point barycenter;

    ComputeBarycenter(barycenter);
    for(unsigned int numEdg = 0; numEdg < NumberOfEdges(); numEdg++)
    {
      const Segment& edge = *Edge(numEdg);
      if(edge.PointPosition(barycenter) == Segment::AtTheLeft)
        SetNormalSign(true, numEdg);
      else
        SetNormalSign(false, numEdg);
    }
  }

  Output::ExitCodes Cell2D::ShiftPositionVertices(const unsigned int& positionVertex)
  {
    Polygon::ShiftPositionVertices(positionVertex);
    if(NumberOfNeighCell3D() == 0)
    {
      rotate(normalSign.begin(), normalSign.begin()+positionVertex, normalSign.end());
      rotate(cells2D.begin(), cells2D.begin()+positionVertex, cells2D.end());
    }

    return Output::Success;
  }

  Output::ExitCodes Cell2D::ChangeOrientation()
  {
    Polygon::ChangeOrientation();

    if(NumberOfNeighCell3D() == 0)
    {
      reverse(normalSign.begin(), normalSign.end());
      reverse(cells2D.begin(), cells2D.end());
    }

    return Output::Success;
  }

  Output::ExitCodes Cell2D::AllignEdgesVertices()
  {
    unsigned int firstIdPointFace = Vertex(0)->Id();
    unsigned int secondIdPointFace = Vertex(1)->Id();
    unsigned int shift = 0;
    for(unsigned int edg = 0; edg < NumberOfEdges(); edg++)
    {
      const Segment& edgeTemp = *Edge(edg);
      unsigned int idFirstPoint = edgeTemp.Vertex(0)->Id();
      unsigned int idSecondPoint = edgeTemp.Vertex(1)->Id();
      if((firstIdPointFace == idFirstPoint && secondIdPointFace == idSecondPoint) || (firstIdPointFace == idSecondPoint && secondIdPointFace == idFirstPoint))
      {
        shift = edg;
        break;
      }
    }
    if(shift == 0)
      return Output::Success;

    rotate(edges.begin(), edges.begin()+shift, edges.end());

    if(NumberOfNeighCell3D() == 0)
    {
      rotate(normalSign.begin(), normalSign.begin()+shift, normalSign.end());
      rotate(cells2D.begin(), cells2D.begin()+shift, cells2D.end());
    }

    return Output::Success;
  }

  Cell2D& Cell2D::operator=(Cell2D&& cell2D)
  {
    if(this != &cell2D)
    {
      this->Polygon::operator =(move(cell2D));
      this->MeshTreeNode::operator =(move(cell2D));

      normalSign.clear();
      cells2D.clear();
      cells3D.clear();

      normalSign.reserve(cell2D.normalSign.size());
      for(unsigned int normSign = 0; normSign < cell2D.normalSign.size(); normSign++)
        normalSign.push_back(cell2D.normalSign[normSign]);

      cells2D.reserve(cell2D.cells2D.size());
      for(unsigned int cel2D = 0; cel2D < cell2D.cells2D.size(); cel2D++)
        cells2D.push_back(cell2D.cells2D[cel2D]);

      cells3D.reserve(cell2D.cells3D.size());
      for(unsigned int cel3D = 0; cel3D < cell2D.cells3D.size(); cel3D++)
        cells3D.push_back(cell2D.cells3D[cel3D]);

      cell2D.normalSign.clear();
      cell2D.cells2D.clear();
      cell2D.cells3D.clear();
    }
    return *this;
  }

}
