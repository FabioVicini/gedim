#ifndef __ITREENODE_H
#define __ITREENODE_H

#include "InputPropertySupportedTypes.hpp"

using namespace std;

namespace GeDiM
{
    /// \brief Class representing a node in a multi-level mesh.
    /// \details This is useful to keep track of multiple refinements of a mesh.
    /// \author Andrea Borio, Alessandro D'Auria, Fabio Vicini.
    /// \copyright See top level LICENSE file for details.
    class ITreeNode
    {
        public:
            virtual ~ITreeNode() { }

            /// \return The tree node level
            virtual const unsigned int& Level() const = 0;
            /// \return If node is active
            virtual const bool& IsActive() const = 0;
            /// \return Number of markers
            virtual unsigned int NumberOfMarkers() const = 0;
            /// \return The marker at position
            virtual const unsigned int& Marker(const unsigned int& position = 0) const = 0;

            /// \brief Returns true if required marker corresponds to a Dirichlet boundary condition
            /// \return Grue if required marker corresponds to a Dirichlet boundary condition
            virtual bool IsDirichlet(const unsigned int& position = 0) const = 0;
            /// \brief Returns true if required marker corresponds to a Neumann boundary condition
            /// \return True if required marker corresponds to a Neumann boundary condition
            virtual bool IsNeumann(const unsigned int& position = 0) const = 0;
            /// \return True if a node has a father node
            virtual bool HasFather() const = 0;
            /// \return True if a node has any child nodes.
            virtual bool HasChildren() const = 0;
            /// \return The number of child nodes
            virtual unsigned int NumberOfChildren() const = 0;

            /// \return The father node
            virtual const ITreeNode* Father() const = 0;
            /// \return The required child node
            virtual const ITreeNode* Child(const unsigned int& position) const = 0;

            /// \brief Allocate child nodes
            virtual void AllocateChildren(const unsigned int& numChilds) = 0;
            /// \brief Insert child node at position
            virtual void InsertChild(const ITreeNode* child, const unsigned int& position) = 0;

            /// \brief Reserve space of child nodes.
            virtual void InitializeChildren(const unsigned int& numChilds) = 0;
            /// \brief Insert node at the end of child nodes, whose size is increased by one.
            virtual void AddChild(const ITreeNode* child) = 0;
            /// \brief Inherit properties from father
            virtual Output::ExitCodes InheritPropertiesByFather() = 0;

            /// \brief Initialize a property in the node
            virtual Output::ExitCodes InitializeProperty(const string& key,
                                                         const InputPropertySupportedTypes::SupportedTypes& type) = 0;
            /// \brief Add property with given key to the node
            virtual void AddProperty(const string& key, void* value) = 0;
            /// \brief Get const property with given key
            virtual const void* GetProperty(const string& key) const = 0;
            /// \brief Get non-const property with given key
            virtual void* GetProperty(const string& key) = 0;
            /// \brief Returns true if object has a property with given key
            virtual bool HasProperty(const string& key) const = 0;
            /// \brief  Delete all properties
            virtual void RemoveProperty() = 0;
    };
}

#endif // __ITREENODE_H
