#include "MeshTreeNode.hpp"

namespace GeDiM
{
  MeshTreeNode::MeshTreeNode()
  {
    father = NULL;
    childs = vector<const ITreeNode*>{};
    level = 0;
    isActive = true;
    markers = vector<unsigned int>{0};
  }

  MeshTreeNode& MeshTreeNode::operator=(const MeshTreeNode& MeshTreeNode)
  {
    father = MeshTreeNode.father;
    childs.resize(MeshTreeNode.childs.size(), NULL); // Child nodes in the tree.
    for(unsigned int numChild = 0; numChild < MeshTreeNode.childs.size(); numChild++)
      childs[numChild] = MeshTreeNode.childs[numChild];

    isActive = MeshTreeNode.isActive; // Tells if the node is active for system matrix computation
    level = MeshTreeNode.level;
    markers = MeshTreeNode.markers; // Marker for Dirichlet and Neumann conditions
    properties.insert(MeshTreeNode.properties.begin(), MeshTreeNode.properties.end());
    propertyTypes.insert(MeshTreeNode.propertyTypes.begin(), MeshTreeNode.propertyTypes.end());

    return *this;
  }

  MeshTreeNode& MeshTreeNode::operator=(MeshTreeNode&& MeshTreeNode)
  {
    if(this != &MeshTreeNode)
    {
      delete father;
      childs.clear();
      properties.clear();
      propertyTypes.clear();
      markers.clear();


      father = MeshTreeNode.father;
      childs.resize(MeshTreeNode.childs.size(), NULL); // Child nodes in the tree.
      for(unsigned int numChild = 0; numChild < MeshTreeNode.childs.size(); numChild++)
        childs[numChild] = MeshTreeNode.childs[numChild];

      isActive = MeshTreeNode.isActive; // Tells if the node is active for system matrix computation
      level = MeshTreeNode.level;
      markers = MeshTreeNode.markers; // Marker for Dirichlet and Neumann conditions
      properties.insert(MeshTreeNode.properties.begin(), MeshTreeNode.properties.end());
      propertyTypes.insert(MeshTreeNode.propertyTypes.begin(), MeshTreeNode.propertyTypes.end());

      MeshTreeNode.father = nullptr;
      MeshTreeNode.childs.clear();
      MeshTreeNode.properties.clear();
      MeshTreeNode.propertyTypes.clear();
    }
    return *this;
  }

  MeshTreeNode::~MeshTreeNode()
  {
    RemoveProperty();
    properties.clear();
    childs.clear();
    markers.clear();
    father = NULL;
  }
  // ***************************************************************************
  Output::ExitCodes MeshTreeNode::InheritPropertiesByFather()
  {
    const MeshTreeNode& fatherTree = static_cast<const MeshTreeNode&>(*father);
    const map< string, void* >& propertiesFather = fatherTree.GetAllProperties();
    if(propertiesFather.size() == 0)
      return Output::Success;

    for (auto it = fatherTree.propertyTypes.begin(); it != fatherTree.propertyTypes.end(); it++)
      InitializeProperty(it->first, it->second);

    for(map< string, void* >::const_iterator it = propertiesFather.begin(); it != propertiesFather.end(); it++)
    {
      const string& id = it->first;
      const InputPropertySupportedTypes::SupportedTypes& type = propertyTypes.at(id);

      switch (type)
      {
        case InputPropertySupportedTypes::UInt:
          AddProperty(it->first, new unsigned int(*static_cast<unsigned int*>(it->second)));
        break;
        case InputPropertySupportedTypes::String:
          AddProperty(it->first, new string(*static_cast<string*>(it->second)));
        break;
        case InputPropertySupportedTypes::Double:
          AddProperty(it->first, new double(*static_cast<double*>(it->second)));
        break;
        case InputPropertySupportedTypes::VectorInt:
          AddProperty(it->first, new vector<int>(*static_cast<vector<int>*>(it->second)));
        break;
        case InputPropertySupportedTypes::VectorDouble:
          AddProperty(it->first, new vector<double>(*static_cast<vector<double>*>(it->second)));
        break;
        case InputPropertySupportedTypes::SetUInt:
          AddProperty(it->first, new set<unsigned int>(*static_cast<set<unsigned int>*>(it->second)));
        break;
        default:
        break;
      }
    }
    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes MeshTreeNode::InitializeProperty(const string& key, const InputPropertySupportedTypes::SupportedTypes& type)
  {
    map<string, void* >::iterator finder = properties.find(key);

    if(finder == properties.end())
    {
      propertyTypes.insert(pair<string, InputPropertySupportedTypes::SupportedTypes>(key, type));
      properties.insert(pair<string, void* >(key, NULL));
      return Output::Success;
    }
    else
      return Output::GenericError;
  }

  // ***************************************************************************
  void MeshTreeNode::RemoveProperty()
  {
    for (auto it = propertyTypes.begin(); it != propertyTypes.end();)
    {
      const string& id = it->first;
      const InputPropertySupportedTypes::SupportedTypes& type = it->second;

      switch (type)
      {
        case InputPropertySupportedTypes::UInt:
          delete static_cast<unsigned int*>(properties[id]);
        break;
        case InputPropertySupportedTypes::String:
          delete static_cast<string*>(properties[id]);
        break;
        case InputPropertySupportedTypes::Double:
          delete static_cast<double*>(properties[id]);
        break;
        case InputPropertySupportedTypes::VectorInt:
          delete static_cast<vector<int>*>(properties[id]);
        break;
        case InputPropertySupportedTypes::VectorDouble:
          delete static_cast<vector<double>*>(properties[id]);
        break;
        case InputPropertySupportedTypes::SetUInt:
        {
          set<unsigned int>* property = static_cast<set<unsigned int>*>(properties[id]);
          if(property->size() != 0)
          {
            property->clear();
            delete property;
          }
        }
        break;
        default:
        break;
      }

      properties.erase(id);
      propertyTypes.erase(it++);
    }
  }
}
