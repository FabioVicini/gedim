#include "Cell1D.hpp"

namespace GeDiM
{

	Cell1D& Cell1D::operator=(const Cell1D& cell1D)
	{
    this->MeshTreeNode::operator =(cell1D);
		this->Segment::operator =(cell1D);

		normalSign.reserve(cell1D.normalSign.size());
		for(unsigned int normSign = 0; normSign < cell1D.normalSign.size(); normSign++)
			normalSign.push_back(cell1D.normalSign[normSign]);

		cells1D.reserve(cell1D.cells1D.size());
		for(unsigned int cel1D = 0; cel1D < cell1D.cells1D.size(); cel1D++)
			cells1D.push_back(cell1D.cells1D[cel1D]);

		cells2D.reserve(cell1D.cells2D.size());
		for(unsigned int cel2D = 0; cel2D < cell1D.cells2D.size(); cel2D++)
			cells2D.push_back(cell1D.cells2D[cel2D]);

		cells3D.reserve(cell1D.cells3D.size());
		for(unsigned int cel3D = 0; cel3D < cell1D.cells3D.size(); cel3D++)
			cells3D.push_back(cell1D.cells3D[cel3D]);
    return *this;
  }

  Cell1D& Cell1D::operator=(Cell1D&& cell1D)
	{
		if(this != &cell1D)
		{
			this->Segment::operator =(move(cell1D));
      this->MeshTreeNode::operator =(move(cell1D));
			cells1D.reserve(cell1D.cells1D.size());
			cells2D.reserve(cell1D.cells2D.size());
			cells3D.reserve(cell1D.cells3D.size());
			for(unsigned int cel1D = 0; cel1D < cell1D.cells1D.size(); cel1D++)
				cells1D.push_back(cell1D.cells1D[cel1D]);
			for(unsigned int cel2D = 0; cel2D < cell1D.cells2D.size(); cel2D++)
				cells2D.push_back(cell1D.cells2D[cel2D]);
			for(unsigned int cel3D = 0; cel3D < cell1D.cells3D.size(); cel3D++)
				cells3D.push_back(cell1D.cells3D[cel3D]);

			cell1D.cells1D.clear();
			cell1D.cells2D.clear();
			cell1D.cells3D.clear();
		}
		return *this;
	}

}
