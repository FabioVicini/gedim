#include "Cell3D.hpp"
#include "MeasurerPoint.hpp"

namespace GeDiM
{

	Cell3D& Cell3D::operator=(const Cell3D& cell3D)
	{
    this->MeshTreeNode::operator =(cell3D);
		this->Polyhedron::operator =(cell3D);

		normalSign.reserve(cell3D.normalSign.size());
		for(unsigned int normSign = 0; normSign < cell3D.normalSign.size(); normSign++)
			normalSign.push_back(cell3D.normalSign[normSign]);

		cells3D.reserve(cell3D.cells3D.size());
		for(unsigned int cel3D = 0; cel3D < cell3D.cells3D.size(); cel3D++)
			cells3D.push_back(cell3D.cells3D[cel3D]);
    return *this;
  }

  void Cell3D::ComputeNormalSign()
  {
    AllocateNormalSign();
    Point barycenter;

    ComputeBarycenter(barycenter);
    for(unsigned int numEdg = 0; numEdg < NumberOfEdges(); numEdg++)
    {
      const Polygon& face = *Face(numEdg);
      if(face.PointInPlane(barycenter) == Polygon::Down)
        SetNormalSign(true, numEdg);
      else
        SetNormalSign(false, numEdg);
    }
  }

	Cell3D& Cell3D::operator=(Cell3D&& cell3D)
	{
		if(this != &cell3D)
		{
      this->MeshTreeNode::operator =(move(cell3D));
			this->Polyhedron::operator =(move(cell3D));

			normalSign.reserve(cell3D.normalSign.size());
			for(unsigned int normSign = 0; normSign < cell3D.normalSign.size(); normSign++)
				normalSign.push_back(cell3D.normalSign[normSign]);

			cells3D.reserve(cell3D.cells3D.size());
			for(unsigned int cel3D = 0; cel3D < cell3D.cells3D.size(); cel3D++)
				cells3D.push_back(cell3D.cells3D[cel3D]);

			cell3D.cells3D.clear();
		}
		return *this;
	}

}
