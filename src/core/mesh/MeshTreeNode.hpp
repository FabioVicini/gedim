#ifndef __MESHTREENODE_H
#define __MESHTREENODE_H

#include "ITreeNode.hpp"

using namespace std;

namespace GeDiM
{
    /*!
        \brief Class representing a node in a multi-level mesh.
        \details This is useful to keep track of multiple refinements of a
        mesh.
        \copyright See top level LICENSE file for details.
    */
    class MeshTreeNode : public ITreeNode
    {
        protected:
            const ITreeNode* father; ///< Father node in the tree.
            vector<const ITreeNode*> childs; ///< Child nodes in the tree.
            unsigned int level; ///< Level of the node in the tree.
            bool isActive; ///< Tells if the node is active for system matrix computation.
            /*!
              \brief Markers for Dirichlet and Neumann conditions.
              \details Use odd numbers for Dirichlet BCs, even numbers for Neumann BCs.
            */
            vector<unsigned int> markers;

            map<string, InputPropertySupportedTypes::SupportedTypes> propertyTypes; ///< Type of each property.
            /*!
              \brief User-defined properties.
              \warning Since properties are stored in a map object, accessing
              them has \f$O(\log(n))\f$ cost.
            */
            map<string, void* > properties;

        public:
            explicit MeshTreeNode(); ///< Default constructor.
            MeshTreeNode(const MeshTreeNode& treeNode) { *this = treeNode; } ///< Copy constructor.
            MeshTreeNode(MeshTreeNode&& treeNode) noexcept { *this = move(treeNode);} ///< Move constructor.
            MeshTreeNode& operator=(const MeshTreeNode& treeNode); ///< Assignment operator.
            MeshTreeNode& operator=(MeshTreeNode&& treeNode); ///< Assignment operator.
            virtual ~MeshTreeNode(); ///< Destructor.

            const unsigned int& Level() const { return level; } ///< Get tree node level.
            const bool& IsActive() const { return isActive; } ///< Returns true if node is active.
            unsigned int NumberOfMarkers() const {return markers.size();} ///< Returns number of markers.
            const unsigned int& Marker(const unsigned int& position = 0) const { return markers[position]; } ///< Returns marker at position.
            const vector<unsigned int>& Markers() const { return markers; } ///< Returns vector of markers.

            /*!
              \brief Returns true if required marker corresponds to a Dirichlet boundary condition.
              \details A marker is of Dirichlet type if it is an odd number.
            */
            bool IsDirichlet(const unsigned int& position = 0) const { return (markers[position] % 2 == 1); }
            /*!
              \brief Returns true if required marker corresponds to a Neumann boundary condition.
              \details A marker is of Neumann type if it is an even number.
            */
            bool IsNeumann(const unsigned int& position = 0) const { return (markers[position] > 0 && markers[position] % 2 == 0); }
            bool HasFather() const { return (father != NULL);} ///< Returns true if a node has a father node.
            bool HasChildren() const { return (childs.size() > 0);} ///< Returns true if a node has any child nodes.
            unsigned int NumberOfChildren() const { return childs.size();} ///< Returns the number of child nodes.

            const ITreeNode* Father() const { return father;} ///< Returns father node.
            const ITreeNode* Child(const unsigned int& position) const { return childs[position];} ///< Returns required child node.

            void SetLevel(const unsigned int& _level) { level = _level; } ///< Sets the level of the node in the tree.
            void SetState(bool _isActive = true) { isActive = _isActive; } ///< Sets the value of the isActive property.
            void SetMarker(const unsigned short& _marker, const unsigned int& position = 0) { markers[position] = _marker; } ///< Sets the required marker.
            void SetSizeMarkers(const unsigned int& numberOfMarker) { markers.resize(numberOfMarker); } ///< Sets the number of markers.
            void SetMarkers(const vector<unsigned int>& _markers) { markers = _markers; } ///< Sets the vector of markers.
            void SetFather(const MeshTreeNode* _father) { father = _father; } ///< Sets the father node.

            void AllocateChildren(const unsigned int& numChilds){ childs.resize( numChilds); } ///< Allocate child nodes.
            void InsertChild(const ITreeNode* child, const unsigned int& position) { childs[position] = child; } ///< Insert child node at position.

            void InitializeChildren(const unsigned int& numChilds){ childs.reserve(numChilds); } ///< Reserve space in vector of child nodes.
            void AddChild(const ITreeNode* child) { childs.push_back(child); } ///< Insert node at the end of vector of child nodes, whose size is increased by one.
            Output::ExitCodes InheritPropertiesByFather();

            Output::ExitCodes InitializeProperty(const string& key, const InputPropertySupportedTypes::SupportedTypes& type = InputPropertySupportedTypes::SupportedTypes::Unknown);
            void AddProperty(const string& key, void* value) { properties[key] = value; } ///< Add property with given key to the node.
            const map<string, void* >& GetAllProperties() const { return properties; } ///< Get const map object containing all properties.
            map<string, void* >& GetAllProperties() { return properties; } ///< Get non-const map object containing all properties.
            const void* GetProperty(const string& key) const { return properties.at(key); } ///< Get const property with given key.
            void* GetProperty(const string& key) { return properties.at(key); } ///< Get non-const property with given key.
            bool HasProperty(const string& key) const { return (properties.find(key) != properties.end()); } ///< Returns true if object has a property with given key.
            void RemoveProperty(); ///< Delete all properties
    };
}

#endif // __MESHTREENODE_H
