#ifndef __CONSTANTBOUNDARYCONDITION_H
#define __CONSTANTBOUNDARYCONDITION_H

#include "IBoundaryCondition.hpp"
#include <unordered_map>

namespace GeDiM
{
  /// \brief Constant boundary condition of Differential Equations
  /// \copyright See top level LICENSE file for details.
  class ConstantBoundaryCondition final : public IBoundaryCondition
  {
    protected:
      unordered_map<unsigned int, double> values; ///< Constant Boundary condition values
    public:
      /// \brief Create a constant boundary condition b : X^m -> Y, where m is the number of
      /// markers
      ConstantBoundaryCondition() { }
      ~ConstantBoundaryCondition() { values.clear(); }

      /// \brief Set the constant value of Boundary condition in marker
      void SetValue(const unsigned int& marker,
                    const double& value)
      {
        values[marker] = value;
      }

      Output::ExitCodes Eval(const unsigned int& marker,
                             const MatrixXd& points,
                             VectorXd& results) const
      {
        unordered_map<unsigned int, double>::const_iterator it = values.find(marker);
        if (it == values.end())
          return Output::GenericError;

        results.setConstant(points.cols(), it->second);
        return Output::Success;
      }
  };
}

#endif // __CONSTANTBOUNDARYCONDITION_H
