#ifndef __MESHDOFHANDLER_H
#define __MESHDOFHANDLER_H

#include "ISingleDomainDofHandler.hpp"
#include "IReferenceElement.hpp"
#include "IMesh.hpp"
#include "ITreeNode.hpp"
#include "EigenExtension.hpp"

namespace GeDiM
{
  /// \brief Class used to create dof indices on a single mesh
  /// \copyright See top level LICENSE file for details.
  /// \sa GeDiM::DofHandler, GeDiM::SingleDomainAssembler.
  class MeshDofHandler final : public ISingleDomainDofHandler
  {
    private:
      /// Dof type
      enum DofType
      {
        Dof = 0,
        Dirichlet = 1,
        Neumann = 2
      };

      /// \brief Dimension of the solution.
      /// \details If \ref solutionDimension > 1, the problem is vectorial. Defaults to 1.
      unsigned int solutionDimension;
      /// \brief True if the resulting global matrix is known to be symmetric.
      /// \details Affects \ref numNonZeroContributes. Defaults to false.
      bool symmetricProblem;
      unsigned int numDofs; ///< Total number of degrees of freedom.
      unsigned int numDirichletDofs; ///< Total number of degrees of freedom of the Dirichlet condition.
      vector<unsigned int> numDofsPerComponent; ///< Total number of degrees of freedom for each component of the solution.
      vector<unsigned int> numDirichletDofsPerComponent; ///< Total number of degrees of freedom of the Dirichlet condition for each component of the solution.
      unsigned int numNonZeroContributes; ///< Number of non zero contributes to the global matrix.
      unsigned int numNonZeroDirichletContributes; ///< Number of non zero contributes to the global Dirichlet matrix.
      vector< vector<int> > cell0DDofIndices; ///< Dof indices of internal to each 0D cell
      vector< vector<int> > cell1DDofIndices; ///< Dof indices of internal to each 1D cell
      vector< vector<int> > cell2DDofIndices; ///< Dof indices of internal to each 2D cell
      vector< vector<int> > cell3DDofIndices; ///< Dof indices of internal to each 3D cell
      vector< const vector<unsigned int>* > cell0DDofMarkers; ///< Dof markers of internal to each 0D cell
      vector< const vector<unsigned int>* > cell1DDofMarkers; ///< Dof markers of internal to each 1D cell
      vector< const vector<unsigned int>* > cell2DDofMarkers; ///< Dof markers of internal to each 2D cell
      vector< const vector<unsigned int>* > cell3DDofMarkers; ///< Dof markers of internal to each 3D cell
      vector< vector<DofType> > cell0DDofTypes; ///< Dof markers type of internal to each 0D cell
      vector< vector<DofType> > cell1DDofTypes; ///< Dof markers type of internal to each 1D cell
      vector< vector<DofType> > cell2DDofTypes; ///< Dof markers type of internal to each 2D cell
      vector< vector<DofType> > cell3DDofTypes; ///< Dof markers type of internal to each 3D cell
      vector< vector<int> > maximalCellsGlobalDofs; ///< Vector of all dofs for each maximal cell.
      vector< vector<unsigned int> > maximalCellsGlobalMarkers; ///< Vector of all markers for each maximal cell.
      vector< vector<DofType> > maximalCellsGlobalDofTypes; ///< Vector of all marker type for each maximal cell.
      vector< MatrixXd > maximalCellsDofPositions; ///< Dof positions on the maximal cells
      MatrixXd _dofPositions; ///< Dof positions on the mesh
      MatrixXd _dirichletPositions; ///< Dirichlet positions on the mesh

      vector<int> numNeighbourhoodOccurrences; ///< Number of neighbouring dofs for each dof.
      vector<int> O_nnz; ///< Off-Block-Diagonal nonzeroes per row (see PETSc documentation for further informations).
      vector<int> D_nnz; ///< Block-Diagonal nonzeroes per row.

      /// \brief Initialize auxiliary parameters.
      Output::ExitCodes InitializeDofs();
      /// \brief Auxiliary method used to create a vector of dof indices on a given TreeNode object.
      void CreateTreeNodeDofs(const ITreeNode& treeNode,
                              const unsigned int& numElementDofs,
                              vector<int>& treeNodeDofs,
                              vector<DofType>& treeNodeTypes);
      /// \brief Auxiliary parameter.
      /// \details Used as counter of degrees of freedom for each component by \ref CreateTreeNodeDofs().
      vector<int> nextDofIndex;
      /// \brief Auxiliary parameter.
      /// \details Used as counter of degrees of freedom of the Dirichlet condition for each component by \ref CreateTreeNodeDofs().
      vector<int> nextDirichletDofIndex;
      /// \brief Auxiliary parameter.
      /// \details Used to store temporarily the number of dofs on each 0D cell, as returned by \ref referenceElementPtr.
      vector<unsigned int> num0DElementDofs;
      /// \brief Auxiliary parameter.
      /// \details Used to store temporarily the number of dofs on each 1D cell, as returned by \ref referenceElementPtr.
      vector<unsigned int> num1DElementDofs;
      /// \brief Auxiliary parameter.
      /// \details Used to store temporarily the number of dofs on each 2D cell, as returned by \ref referenceElementPtr.
      vector<unsigned int> num2DElementDofs;
      /// \brief Auxiliary parameter.
      /// \details Used to store temporarily the number of dofs on each 3D cell, as returned by \ref referenceElementPtr.
      vector<unsigned int> num3DElementDofs;
    protected:
      const IReferenceElement* _referenceElement; ///< A pointer to the reference element.
      const IMesh* _mesh; ///< A pointer to the mesh.
    public:
      MeshDofHandler();
      MeshDofHandler(const IReferenceElement& element,
                     const IMesh& mesh);
      ~MeshDofHandler() {}

      void SetReferenceElement(const IReferenceElement& element) { _referenceElement = &element; }
      void SetMesh(const IMesh& mesh) { _mesh = &mesh; }

      void SetSolutionDimension(const unsigned int& value) { solutionDimension = value; }
      void SetSymmetricProblem(const bool& value) { symmetricProblem = value; }

      unsigned int SolutionDimension() const { return solutionDimension; }
      unsigned int NumDofsPerSolution(const unsigned int& solutionIndex) const { return  numDofsPerComponent[solutionIndex]; }
      unsigned int NumDirichletPerSolution(const unsigned int& solutionIndex) const { return  numDirichletDofsPerComponent[solutionIndex]; }
      unsigned int NumDofs() const { return numDofs; }
      unsigned int NumDirichletDofs() const { return numDirichletDofs; }
      unsigned int NumNonZeroContributes() const { return numNonZeroContributes; }
      unsigned int NumNonZeroDirichletContributes() const { return numNonZeroDirichletContributes; }
      inline unsigned int NumMaximalCellDofs(const unsigned int& cellId) const { return maximalCellsGlobalDofs[cellId].size(); }
      inline unsigned int NumMaximalCellDofsPerSolution(const unsigned int& cellId,
                                                        const unsigned int&) const { return maximalCellsGlobalDofs[cellId].size() / solutionDimension; }
      inline unsigned int DofGlobalIndex(const unsigned int& cellId,
                                         const unsigned int& localIndex) const { return maximalCellsGlobalDofs[cellId][localIndex]; }
      inline unsigned int DirichletGlobalIndex(const unsigned int& cellId,
                                               const unsigned int& localIndex) const { return -maximalCellsGlobalDofs[cellId][localIndex] - 1; }
      inline VectorXd DofPosition(const unsigned int& cellId,
                                  const unsigned int& localIndex) const { return maximalCellsDofPositions[cellId].col(localIndex); }
      inline Output::ExitCodes DofPositions(Map<const MatrixXd>& dofPositions) const { return EigenExtension::MatrixToMap(_dofPositions, dofPositions); }
      inline Output::ExitCodes DirichletPositions(Map<const MatrixXd>& dirichletPositions) const { return EigenExtension::MatrixToMap(_dirichletPositions, dirichletPositions); }
      inline unsigned int Marker(const unsigned int& cellId,
                                 const unsigned int& localIndex) const { return maximalCellsGlobalMarkers[cellId][localIndex]; };
      inline bool IsDirichletDof(const unsigned int& cellId,
                                 const unsigned int& localIndex) const { return maximalCellsGlobalDofTypes[cellId][localIndex] == Dirichlet; }
      inline bool IsNeumannDof(const unsigned int& cellId,
                               const unsigned int& localIndex) const { return maximalCellsGlobalDofTypes[cellId][localIndex] == Neumann; }
      const vector<int>& Cell0DDofIndices(const unsigned int& cellId) const { return cell0DDofIndices[cellId]; }
      const vector<int>& Cell1DDofIndices(const unsigned int& cellId) const { return cell1DDofIndices[cellId]; }
      const vector<int>& Cell2DDofIndices(const unsigned int& cellId) const { return cell2DDofIndices[cellId]; }
      const vector<int>& Cell3DDofIndices(const unsigned int& cellId) const { return cell3DDofIndices[cellId]; }
      const vector<unsigned int>& Cell0DDofMarkers(const unsigned int& cellId) const { return *cell0DDofMarkers[cellId]; }
      const vector<unsigned int>& Cell1DDofMarkers(const unsigned int& cellId) const { return *cell1DDofMarkers[cellId]; }
      const vector<unsigned int>& Cell2DDofMarkers(const unsigned int& cellId) const { return *cell2DDofMarkers[cellId]; }
      const vector<unsigned int>& Cell3DDofMarkers(const unsigned int& cellId) const { return *cell3DDofMarkers[cellId]; }
      const vector<int>& D_Nnz() const { return D_nnz; }
      const vector<int>& O_Nnz() const { return O_nnz; }
      const bool& SymmetricProblem() const { return symmetricProblem; }

      Output::ExitCodes ResetDofs();
      Output::ExitCodes CreateDofs();
      Output::ExitCodes CreateDofPositions();
      Output::ExitCodes CreateNeighbourhoodInformation();
  };
}

#endif // __MESHDOFHANDLER_H
