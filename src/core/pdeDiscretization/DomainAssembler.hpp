#ifndef __DOMAINASSEMBLER_H
#define __DOMAINASSEMBLER_H

#include "ISingleDomainAssembler.hpp"

namespace GeDiM
{
  /// \brief Implementation of the DomainAssembler
  /// \copyright See top level LICENSE file for details.
  class DomainAssembler final : public ISingleDomainAssembler
  {
    private:
      const IDifferentialEquation* _equation; ///< The equation to assemble
      const IMesh* _mesh; ///< The mesh of the equation
      const ISingleDomainDofHandler* _dofHandler; ///< The DofHandler used to compute the DOFs
      IVector* _rightHandSide; ///< Right hand side of the equation
      IVector* _solutionDirichlet; ///< Dirichlet solution of the equation
      ISparseMatrix* _globalMatrix; ///< Dirichlet matrix of the equation
      ISparseMatrix* _dirichletMatrix; ///< Dirichlet matrix of the equation

    public:
      DomainAssembler();
      ~DomainAssembler();

      Output::ExitCodes Initialize(const IDifferentialEquation& equation,
                                   const IMesh& mesh,
                                   const ISingleDomainDofHandler& dofHandler,
                                   IVector& rightHandSide,
                                   IVector& solutionDirichlet,
                                   ISparseMatrix& globalMatrix,
                                   ISparseMatrix& dirichletMatrix);

      Output::ExitCodes AssembleDiscreteSystem() const;
  };
}
#endif // __DOMAINASSEMBLER_H
