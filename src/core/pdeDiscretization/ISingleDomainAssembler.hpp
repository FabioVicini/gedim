#ifndef __ISINGLEDOMAINASSEMBLER_H
#define __ISINGLEDOMAINASSEMBLER_H

#include "IAssembler.hpp"
#include "IMesh.hpp"
#include "IDifferentialEquation.hpp"
#include "Output.hpp"
#include "ISingleDomainDofHandler.hpp"
#include "ISparseMatrix.hpp"
#include "IVector.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Class used to assemble a discrete equation A*x = A_D*x_D + rhs on a single domain
  /// \copyright See top level LICENSE file for details.
  class ISingleDomainAssembler : public IAssembler
  {
    public:
      virtual ~ISingleDomainAssembler() {}

      /// \brief Initialize the Assembler
      /// \param equation The equation of the discrete system on the mesh element
      /// \param mesh The mesh of the domain
      /// \param dofHandler The dof handler
      /// \param rightHandSide The right-hand side of the equation
      /// \param solutionDirichlet The dirichlet solution of the equation
      /// \param globalMatrix The stiffness matrix of the equation
      /// \param dirichletMatrix The dirichlet matrix of the equation
      virtual Output::ExitCodes Initialize(const IDifferentialEquation& equation,
                                           const IMesh& mesh,
                                           const ISingleDomainDofHandler& dofHandler,
                                           IVector& rightHandSide,
                                           IVector& solutionDirichlet,
                                           ISparseMatrix& globalMatrix,
                                           ISparseMatrix& dirichletMatrix) = 0;
  };
}

#endif // __ISINGLEDOMAINASSEMBLER_H
