#ifndef _CONSTANTPHYSICALPARAMETER_H
#define _CONSTANTPHYSICALPARAMETER_H

#include "IPhysicalParameter.hpp"

namespace GeDiM
{
  /// \brief Class for the constant Physical Parameter of Differential Equations
  /// \copyright See top level LICENSE file for details.
  class ConstantPhysicalParameter final : public IPhysicalParameter
  {
    private:
      double _value; ///< The constant physical parameter value

    public:
      ConstantPhysicalParameter(const double& value) { _value = value; }
      ~ConstantPhysicalParameter() {}

      Output::ExitCodes Eval(const MatrixXd& points, VectorXd& results) const
      {
        results.setConstant(points.cols(), _value);
        return  Output::Success;
      }
  };
}

#endif // _CONSTANTPHYSICALPARAMETER_H
