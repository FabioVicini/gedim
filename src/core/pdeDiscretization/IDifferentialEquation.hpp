#ifndef __IDIFFERENTIALEQUATION_H
#define __IDIFFERENTIALEQUATION_H

#include "Output.hpp"
#include "IMesh.hpp"
#include "Eigen"

using namespace std;
using namespace Eigen;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Interface used to implement a Differential Equation
  /// \copyright See top level LICENSE file for details.
  class IDifferentialEquation
  {
    public:
      virtual ~IDifferentialEquation() {}

      /// \brief Initialize the values of the equation
      virtual Output::ExitCodes Initialize() = 0;
      /// \brief Build the equation on the single cell
      /// \param mesh The mesh of the problem
      /// \param cellPostionId The cell position id of the mesh
      /// \param cellMatrix The resulting local matrix of the equation
      /// Its size is geometric NumberLocalDofs x NumberLocalDofs
      /// \param cellRightHandSide The resulting local right hand side of the equation
      /// Its size is geometric NumberLocalDofs
      /// \param cellDirichletTermValues The resulting Dirichlet local matrix of the equation
      /// Its size is geometric NumberLocalDofs
      /// \return The result of the function
      virtual Output::ExitCodes BuildLocalSystem(const IMesh& mesh,
                                                 const unsigned int& cellPostionId,
                                                 MatrixXd& cellMatrix,
                                                 VectorXd& cellRightHandSide,
                                                 VectorXd& cellDirichletTermValues) const = 0;
  };
}


#endif // __IDIFFERENTIALEQUATION_H
