#ifndef IREFERENCEELEMENT_H
#define IREFERENCEELEMENT_H

#include "Cell0D.hpp"
#include "Cell1D.hpp"
#include "Cell2D.hpp"
#include "Cell3D.hpp"
#include "Output.hpp"

using namespace MainApplication;

namespace GeDiM
{
  /// \brief Class used to impose a certain number of degrees of freedom on each geometrical object.
  /// \copyright See top level LICENSE file for details.
  class IReferenceElement
  {
    public:
      /// \brief Class destructor.
      virtual ~IReferenceElement() { }
      /// \brief Initialize reference element.
      /// \returns MainApplication::Output::Success if the method was successful.
      virtual Output::ExitCodes Initialize() = 0;

      /// \return The dimension of the element
      virtual unsigned int Dimension() const = 0;
      /// \return The order of the element
      virtual unsigned int Order() const = 0;

      /// \brief Evaluate the DOF positions from the reference element to the cell mapped
      /// \param referenceElementMap The cell reference element
      /// \param dofPositions The point positions evaluated
      /// Its size is geometric dimension x NumberBasisFunctions
      /// \return The computation result
      virtual Output::ExitCodes EvaluateDofPositions(const IMapping& referenceElementMap,
                                                     MatrixXd& dofPositions) const = 0;

      /// \brief Get number of degrees of freedom on a 0D cell.
      /// \param cell0D The cell.
      /// \returns The number of degrees of freedom.
      virtual unsigned int NumDofs0D(const Cell0D& cell0D) const = 0;
      /// Get number of degrees of freedom on a 1D cell.
      /// \param cell1D The cell.
      /// \returns The number of degrees of freedom.
      virtual unsigned int NumDofs1D(const Cell1D& cell1D) const = 0;
      /// Get number of degrees of freedom on a 2D cell.
      /// \param cell2D The cell.
      /// \returns The number of degrees of freedom.
      virtual unsigned int NumDofs2D(const Cell2D& cell2D) const = 0;
      /// Get number of degrees of freedom on a 3D cell.
      /// \param cell3D The cell.
      /// \returns The number of degrees of freedom.
      virtual unsigned int NumDofs3D(const Cell3D& cell3D) const = 0;
  };
}

#endif // IREFERENCEELEMENT_H
