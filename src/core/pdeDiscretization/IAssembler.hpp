#ifndef __IASSEMBLER_H
#define __IASSEMBLER_H

#include "Output.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Interface for the problem Assembler
  /// \copyright See top level LICENSE file for details.
  class IAssembler
  {
    public:
      virtual ~IAssembler() {}

      /// \brief Assemble the Discrete System
      /// \return The result of the assembling action
      virtual Output::ExitCodes AssembleDiscreteSystem() const = 0;
  };
}

#endif // __IASSEMBLER_H
