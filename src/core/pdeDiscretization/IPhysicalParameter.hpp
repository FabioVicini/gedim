#ifndef _IPHYSICALPARAMETER_H
#define _IPHYSICALPARAMETER_H

#include "Output.hpp"
#include "Eigen"

using namespace std;
using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
    /// \brief Interface class for the Physical Parameter of Differential Equations
    /// \copyright See top level LICENSE file for details.
    class IPhysicalParameter
    {
        public:
            virtual ~IPhysicalParameter() {}

            /// \brief Evaluate the value of the parameter on the points
            /// \param points The points on which evaluate the parameter. Size is Dimension x NumPoints
            /// \param resuts The result of the parameter for each point. Size is NumPoints
            virtual Output::ExitCodes Eval(const MatrixXd& points, VectorXd& results) const = 0;
    };
}

#endif // _IPHYSICALPARAMETER_H
