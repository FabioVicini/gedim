#ifndef IDOFHANDLER_H
#define IDOFHANDLER_H

#include "Output.hpp"
#include "Eigen"

using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
  /// \brief Interface used to create Degrees of freedom (Dofs) indices for global matrix assembly.
  /// \copyright See top level LICENSE file for details.
  class IDofHandler
  {
    public:
      virtual ~IDofHandler() {} ///< Class destructor.

      /// Set SolutionDimension value. \param value The new value.
      virtual void SetSolutionDimension(const unsigned int& value) = 0;
      /// Set SymmetricProblem value. \param value The new value.
      virtual void SetSymmetricProblem(const bool& value) = 0;

      /// \brief Get SolutionDimension value
      virtual unsigned int SolutionDimension() const = 0;
      /// \return The total number of degrees of freedom.
      virtual unsigned int NumDofs() const = 0;
      /// \param solutionIndex The index of the solution
      /// \return The total number of Dofs per solution.
      virtual unsigned int NumDofsPerSolution(const unsigned int& solutionIndex) const = 0;
      /// \brief Get the total number of degrees of freedom of the Dirichlet condition.
      /// \returns The total number of Dirichlet Dofs.
      virtual unsigned int NumDirichletDofs() const = 0;
      /// \param solutionIndex The index of the solution
      /// \return The total number of Dirichlet points per solution.
      virtual unsigned int NumDirichletPerSolution(const unsigned int& solutionIndex) const = 0;
      /// \brief Get the number of non zero contributes to the global matrix.
      /// \returns A const-reference to NumNonZeroContributes.
      virtual unsigned int NumNonZeroContributes() const = 0;
      /// \brief Get the number of non zero contributes to the global Dirichlet matrix.
      /// \returns A const-reference to NumNonZeroDirichletContributes.
      virtual unsigned int NumNonZeroDirichletContributes() const = 0;
      /// \param cellId The id of the cell.
      /// \param solutionIndex The index of the solution
      /// \returns The number of Dof per solution on the single maximal cell
      virtual unsigned int NumMaximalCellDofsPerSolution(const unsigned int& cellId,
                                                         const unsigned int& solutionIndex) const = 0;
      /// \brief Get the number of Dof on the single cell
      /// \param cellId The id of the cell.
      /// \returns The number of the single cell DOF
      virtual unsigned int NumMaximalCellDofs(const unsigned int& cellId) const = 0;
      /// \brief Get Dof Global index from cell local index
      /// \param cellId The id of the cell.
      /// \param localIndex The dof local index on the cell
      /// \returns The DOF global position
      virtual unsigned int DofGlobalIndex(const unsigned int& cellId,
                                          const unsigned int& localIndex) const = 0;
      /// \brief Get Dof position from cell local index
      /// \param cellId The id of the cell.
      /// \param localIndex The dof local index on the cell
      /// \returns The DOF position
      virtual VectorXd DofPosition(const unsigned int& cellId,
                                   const unsigned int& localIndex) const = 0;
      /// \brief Get Dof global positions
      /// \param dofPositions the computed dof positions
      virtual Output::ExitCodes DofPositions(Map<const MatrixXd>& dofPositions) const = 0;
      /// \brief Get Dof global positions
      /// \param dirichletPositions the computed dirichlet positions
      virtual Output::ExitCodes DirichletPositions(Map<const MatrixXd>& dirichletPositions) const = 0;
      /// \brief Get Dirichlet Global index from maximal cell local index
      /// \param cellId The id of the cell.
      /// \param position The dof local index on the cell
      /// \returns The Dirichlet global index
      virtual unsigned int DirichletGlobalIndex(const unsigned int& cellId,
                                                const unsigned int& localIndex) const = 0;
      /// \brief Get the Marker from maximal cell local index
      /// \param cellId The id of the cell.
      /// \param position The dof local index on the cell
      /// \returns The marker
      virtual unsigned int Marker(const unsigned int& cellId,
                                  const unsigned int& localIndex) const = 0;
      /// \brief Check if the dof at localIndex corresponds to Dirichlet condition
      /// \param cellId The id of the cell.
      /// \param localIndex The dof local index on the cell
      /// \returns True if dof is Dirichlet
      virtual bool IsDirichletDof(const unsigned int& cellId,
                                  const unsigned int& localIndex) const = 0;
      /// \brief Check if the dof at localIndex corresponds to Neumann condition
      /// \param cellId The id of the cell.
      /// \param localIndex The dof local index on the cell
      /// \returns True if dof is Neumann
      virtual bool IsNeumannDof(const unsigned int& cellId,
                                const unsigned int& localIndex) const = 0;
      /// \brief Get vector of internal dof indices on a given 0D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the dofs.
      virtual const vector<int>& Cell0DDofIndices(const unsigned int& cellId) const = 0;
      /// \brief Get vector of internal dof indices on a given 1D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the dofs.
      virtual const vector<int>& Cell1DDofIndices(const unsigned int& cellId) const = 0;
      /// \brief Get vector of internal dof indices on a given 2D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the dofs.
      virtual const vector<int>& Cell2DDofIndices(const unsigned int& cellId) const = 0;
      /// \brief Get vector of internal dof indices on a given 3D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the dofs.
      virtual const vector<int>& Cell3DDofIndices(const unsigned int& cellId) const = 0;
      /// \brief Get vector of internal dof markers on a given 0D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the marker dofs.
      virtual const vector<unsigned int>& Cell0DDofMarkers(const unsigned int& cellId) const = 0;
      /// \brief Get vector of internal dof markers on a given 1D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the marker dofs.
      virtual const vector<unsigned int>& Cell1DDofMarkers(const unsigned int& cellId) const = 0;
      /// \brief Get vector of internal dof markers on a given 2D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the marker dofs.
      virtual const vector<unsigned int>& Cell2DDofMarkers(const unsigned int& cellId) const = 0;
      /// \brief Get vector of internal dof markers on a given 3D cell.
      /// \param cellId The id of the cell.
      /// \returns A const-reference to the vector containing the marker dofs.
      virtual const vector<unsigned int>& Cell3DDofMarkers(const unsigned int& cellId) const = 0;
      /// \brief Get vector block-diagonal nonzeroes.
      /// \returns A const-reference to D_nnz.
      virtual const vector<int>& D_Nnz() const = 0;
      /// \brief Get vector off-block-diagonal nonzeroes.
      /// \returns A const-reference to O_nnz.
      virtual const vector<int>& O_Nnz() const = 0;
      /// \brief Tell if the resulting matrix is known to be symmetric.
      /// \returns A const-reference to SymmetricProblem.
      virtual const bool& SymmetricProblem() const = 0;

      /// \brief Create dofs structure
      /// \returns A MainApplication::Output::ExitCodes value to tell if the method was successfully executed
      virtual Output::ExitCodes CreateDofs() = 0;
      /// \brief Fill neighbourhood information
      /// \returns A MainApplication::Output::ExitCodes value to tell if the method was successfully executed
      virtual Output::ExitCodes CreateNeighbourhoodInformation() = 0;
      /// \brief Compute the dofs positions on cells and on mesh
      /// \returns A MainApplication::Output::ExitCodes value to tell if the method was successfully executed
      virtual Output::ExitCodes CreateDofPositions() = 0;

      /// \brief Reset DOF structure
      /// \returns A MainApplication::Output::ExitCodes value to tell if the method was successfully executed
      virtual Output::ExitCodes ResetDofs() = 0;
  };
}

#endif // IDOFHANDLER_H
