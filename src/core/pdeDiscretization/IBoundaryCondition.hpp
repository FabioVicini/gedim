#ifndef __IBOUNDARYCONDITION_H
#define __IBOUNDARYCONDITION_H

#include "Output.hpp"
#include "Eigen"

using namespace Eigen;
using namespace MainApplication;
using namespace std;

namespace GeDiM
{
    /// \brief Interface class for the boundary conditions
    /// \copyright See top level LICENSE file for details.
    class IBoundaryCondition
    {
        public:
            virtual ~IBoundaryCondition() {}

            /// \brief Evaluate the value of the boundary condition on the points
            /// \param marker The marker of the boundary condition
            /// \param points The points on which evaluate the parameter
            /// \param resuts The result of the parameter for each point
            /// \note results size is number points
            virtual Output::ExitCodes Eval(const unsigned int& marker,
                                           const MatrixXd& points,
                                           VectorXd& results) const = 0;
    };
}

#endif // __IBOUNDARYCONDITION_H
