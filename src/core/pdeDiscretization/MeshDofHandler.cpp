#include "MeshDofHandler.hpp"
#include "MacroDefinitions.hpp"
#include "MpiParallelEnvironment.hpp"
#include "MpiPartitioner.hpp"

namespace GeDiM
{
  // ***************************************************************************
  MeshDofHandler::MeshDofHandler()
  {
    numDofs = 0;
    numDirichletDofs = 0;
    numNonZeroContributes = 0;
    numNonZeroDirichletContributes = 0;
    solutionDimension = 1;
    symmetricProblem = false;
    _referenceElement = nullptr;
    _mesh = nullptr;
  }

  MeshDofHandler::MeshDofHandler(const IReferenceElement& element, const IMesh& mesh) : MeshDofHandler()
  {
    SetReferenceElement(element);
    SetMesh(mesh);
  }
  // ***************************************************************************
  Output::ExitCodes MeshDofHandler::InitializeDofs()
  {
    if(_mesh == nullptr)
      return Output::GenericError;
    const IMesh& mesh = *_mesh;
    if(_referenceElement == nullptr)
      return Output::GenericError;
    const IReferenceElement& refElement = *_referenceElement;

    // Count dofs for each cell and compute totals.
    numDofsPerComponent.assign(solutionDimension, 0);
    numDirichletDofsPerComponent.assign(solutionDimension, 0);
    numDofs = 0;
    numDirichletDofs = 0;
    num0DElementDofs.resize(mesh.NumberOfCells0D());
    for(unsigned int i = 0; i < mesh.NumberOfCells0D(); ++i)
    {
      const Cell0D& cell = *mesh.GetCell0D(i);
      if(!cell.IsActive())
        continue;
      num0DElementDofs[cell.Id()] = refElement.NumDofs0D(cell);

      if (cell.Markers().size() < solutionDimension)
      {
        Output::PrintErrorMessage("%s: Cell0D %d marker size not correct. Expected %d get %d", false, __func__, cell.Id(), solutionDimension, cell.Markers().size());
        return Output::GenericError;
      }

      for(unsigned int d = 0; d < solutionDimension; ++d)
      {
        if(cell.IsDirichlet(d))
        {
          numDirichletDofsPerComponent[d] += num0DElementDofs[cell.Id()];
          numDirichletDofs += num0DElementDofs[cell.Id()];
        }
        else
        {
          numDofsPerComponent[d] += num0DElementDofs[cell.Id()];
          numDofs += num0DElementDofs[cell.Id()];
        }
      }
    }
    num1DElementDofs.resize(mesh.NumberOfCells1D());
    for(unsigned int i = 0; i < mesh.NumberOfCells1D(); ++i)
    {
      const Cell1D& cell = *mesh.GetCell1D(i);
      if(!cell.IsActive())
        continue;

      num1DElementDofs[cell.Id()] = refElement.NumDofs1D(cell);

      if (cell.Markers().size() < solutionDimension)
      {
        Output::PrintErrorMessage("%s: Cell1D %d marker size not correct. Expected %d get %d", false, __func__, cell.Id(), solutionDimension, cell.Markers().size());
        return Output::GenericError;
      }

      for(unsigned int d = 0; d < solutionDimension; ++d)
      {
        if(cell.IsDirichlet(d))
        {
          numDirichletDofsPerComponent[d] += num1DElementDofs[cell.Id()];
          numDirichletDofs += num1DElementDofs[cell.Id()];
        }
        else
        {
          numDofsPerComponent[d] += num1DElementDofs[cell.Id()];
          numDofs += num1DElementDofs[cell.Id()];
        }
      }
    }
    num2DElementDofs.resize(mesh.NumberOfCells2D());
    for(unsigned int i = 0; i < mesh.NumberOfCells2D(); ++i)
    {
      const Cell2D& cell = *mesh.GetCell2D(i);
      if(!cell.IsActive())
        continue;
      num2DElementDofs[cell.Id()] = refElement.NumDofs2D(cell);

      if (cell.Markers().size() < solutionDimension)
      {
        Output::PrintErrorMessage("%s: Cell2D %d marker size not correct. Expected %d get %d", false, __func__, cell.Id(), solutionDimension, cell.Markers().size());
        return Output::GenericError;
      }

      for(unsigned int d = 0; d < solutionDimension; ++d)
      {
        if(cell.IsDirichlet(d))
        {
          numDirichletDofsPerComponent[d] += num2DElementDofs[cell.Id()];
          numDirichletDofs += num2DElementDofs[cell.Id()];
        }
        else
        {
          numDofsPerComponent[d] += num2DElementDofs[cell.Id()];
          numDofs += num2DElementDofs[cell.Id()];
        }
      }
    }
    num3DElementDofs.resize(mesh.NumberOfCells3D());
    for(unsigned int i = 0; i < mesh.NumberOfCells3D(); ++i)
    {
      const Cell3D& cell = *mesh.GetCell3D(i);
      if(!cell.IsActive())
        continue;
      num3DElementDofs[cell.Id()] = refElement.NumDofs3D(cell);
      for(unsigned int d = 0; d < solutionDimension; ++d)
      {
        if (cell.Markers().size() < solutionDimension)
        {
          Output::PrintErrorMessage("%s: Cell3D %d marker size not correct. Expected %d get %d", false, __func__, cell.Id(), solutionDimension, cell.Markers().size());
          return Output::GenericError;
        }

        if(cell.IsDirichlet(d))
        {
          numDirichletDofsPerComponent[d] += num3DElementDofs[cell.Id()];
          numDirichletDofs += num3DElementDofs[cell.Id()];
        }
        else
        {
          numDofsPerComponent[d] += num3DElementDofs[cell.Id()];
          numDofs += num3DElementDofs[cell.Id()];
        }
      }
    }

    // Initialize auxiliary counters.
    nextDofIndex.resize(solutionDimension);
    nextDirichletDofIndex.resize(solutionDimension);
    nextDofIndex[0] = 0;
    nextDirichletDofIndex[0] = -1;
    for(unsigned int d = 0; d < solutionDimension-1; ++d)
    {
      nextDofIndex[d+1] = nextDofIndex[d] + numDofsPerComponent[d];
      nextDirichletDofIndex[d+1] = nextDirichletDofIndex[d] - numDirichletDofsPerComponent[d];
    }

    // Initialize other properties.
    numNonZeroContributes = 0;
    numNonZeroDirichletContributes = 0;

    return Output::Success;
  }

  // ***************************************************************************
  Output::ExitCodes MeshDofHandler::CreateDofs()
  {
    Output::ExitCodes exitCode = InitializeDofs();
    if(exitCode != Output::Success)
      return exitCode;

    const IMesh& mesh = *_mesh;

    // Fill cellxDDofIndices vectors
    cell0DDofIndices.resize(mesh.NumberOfCells0D());
    cell0DDofMarkers.resize(mesh.NumberOfCells0D(), nullptr);
    cell0DDofTypes.resize(mesh.NumberOfCells0D());
    for (unsigned int i = 0; i < mesh.NumberOfCells0D(); ++i)
    {
      const Cell0D& cell = *mesh.GetCell0D(i);
      if (!cell.IsActive())
        continue;

      const unsigned int& cellId = cell.Id();

      CreateTreeNodeDofs(cell,
                         num0DElementDofs[i],
                         cell0DDofIndices[cellId],
                         cell0DDofTypes[cellId]);
      cell0DDofMarkers[cellId] = &cell.Markers();
    }

    cell1DDofIndices.resize(mesh.NumberOfCells1D());
    cell1DDofMarkers.resize(mesh.NumberOfCells1D(), nullptr);
    cell1DDofTypes.resize(mesh.NumberOfCells1D());
    for (unsigned int i = 0; i < mesh.NumberOfCells1D(); ++i)
    {
      const Cell1D& cell = *mesh.GetCell1D(i);
      if (!cell.IsActive())
        continue;

      const unsigned int& cellId = cell.Id();

      CreateTreeNodeDofs(cell,
                         num1DElementDofs[i],
                         cell1DDofIndices[cellId],
                         cell1DDofTypes[cellId]);
      cell1DDofMarkers[cellId] = &cell.Markers();
    }

    cell2DDofIndices.resize(mesh.NumberOfCells2D());
    cell2DDofMarkers.resize(mesh.NumberOfCells2D(), nullptr);
    cell2DDofTypes.resize(mesh.NumberOfCells2D());
    for (unsigned int i = 0; i < mesh.NumberOfCells2D(); ++i)
    {
      const Cell2D& cell = *mesh.GetCell2D(i);
      if (!cell.IsActive())
        continue;

      const unsigned int& cellId = cell.Id();

      CreateTreeNodeDofs(cell,
                         num2DElementDofs[i],
                         cell2DDofIndices[cellId],
                         cell2DDofTypes[cellId]);
      cell2DDofMarkers[cellId] = &cell.Markers();
    }

    cell3DDofIndices.resize(mesh.NumberOfCells3D());
    cell3DDofMarkers.resize(mesh.NumberOfCells3D(), nullptr);
    cell3DDofTypes.resize(mesh.NumberOfCells3D());
    for(unsigned int i = 0; i < mesh.NumberOfCells3D(); ++i)
    {
      const Cell3D& cell = *mesh.GetCell3D(i);
      if(!cell.IsActive())
        continue;

      const unsigned int& cellId = cell.Id();

      CreateTreeNodeDofs(cell,
                         num3DElementDofs[i],
                         cell3DDofIndices[cellId],
                         cell3DDofTypes[cellId]);
      cell3DDofMarkers[cell.Id()] = &cell.Markers();
    }

    // Fill maximal cells global dofs.
    maximalCellsGlobalDofs.resize(mesh.NumberOfMaximalCells());
    maximalCellsGlobalMarkers.resize(mesh.NumberOfMaximalCells());
    maximalCellsGlobalDofTypes.resize(mesh.NumberOfMaximalCells());

    vector< vector<int> > *maximalCellsDofIndices;
    vector<unsigned int> *numMaximalElementDofs;
    switch(mesh.Dimension())
    {
      case 0:
        maximalCellsDofIndices = &cell0DDofIndices;
        numMaximalElementDofs = &num0DElementDofs;
      break;
      case 1:
        maximalCellsDofIndices = &cell1DDofIndices;
        numMaximalElementDofs = &num1DElementDofs;
      break;
      case 2:
        maximalCellsDofIndices = &cell2DDofIndices;
        numMaximalElementDofs = &num2DElementDofs;
      break;
      case 3:
        maximalCellsDofIndices = &cell3DDofIndices;
        numMaximalElementDofs = &num3DElementDofs;
      break;
      default:
        maximalCellsDofIndices = nullptr;
        numMaximalElementDofs = nullptr;
    }
    for(unsigned int i = 0; i < mesh.NumberOfMaximalCells(); ++i)
    {
      if(!mesh.GetMaximalTreeNode(i)->IsActive())
        continue;
      const IGeometricObject& cell = *mesh.GetMaximalGeometricObject(i);
      const unsigned int& cellId = cell.Id();
      const vector<int>& thisCellDofIndices = (*maximalCellsDofIndices)[cellId];
      unsigned int numGlobalDofs = thisCellDofIndices.size();
      for(unsigned int j = 0; j < cell.NumberOfVertices(); ++j)
        numGlobalDofs += cell0DDofIndices[cell.Vertex(j)->Id()].size();
      for(unsigned int j = 0; j < cell.NumberOfEdges(); ++j)
        numGlobalDofs += cell1DDofIndices[cell.Edge(j)->Id()].size();
      for(unsigned int j = 0; j < cell.NumberOfFaces(); ++j)
        numGlobalDofs += cell2DDofIndices[cell.Face(j)->Id()].size();

      vector<int>& thisCellGlobalDofIndices = maximalCellsGlobalDofs[cellId];
      vector<unsigned int>& thisCellGlobalMarkers = maximalCellsGlobalMarkers[cellId];
      vector<DofType>& thisCellGlobalDofTypes = maximalCellsGlobalDofTypes[cellId];

      thisCellGlobalDofIndices.resize(numGlobalDofs);
      thisCellGlobalMarkers.resize(numGlobalDofs, 0);
      thisCellGlobalDofTypes.resize(numGlobalDofs, MeshDofHandler::Dof);

      unsigned int pos = 0;
      for(unsigned int d = 0; d < solutionDimension; ++d)
      {
        for(unsigned int j = 0; j < cell.NumberOfVertices(); ++j)
        {
          const unsigned int& vertexId = cell.Vertex(j)->Id();
          const unsigned int& numVertElementDofs = num0DElementDofs[vertexId];
          for(unsigned int k = 0; k < numVertElementDofs; ++k)
          {
            thisCellGlobalDofIndices[pos] = cell0DDofIndices[vertexId][d * numVertElementDofs + k];
            thisCellGlobalMarkers[pos] = (*cell0DDofMarkers[vertexId])[d * numVertElementDofs + k];
            thisCellGlobalDofTypes[pos] = cell0DDofTypes[vertexId][d * numVertElementDofs + k];

            pos++;
          }
        }

        for(unsigned int j = 0; j < cell.NumberOfEdges(); ++j)
        {
          const unsigned int& edgeId = cell.Edge(j)->Id();
          const unsigned int& numEdgeElementDofs = num1DElementDofs[edgeId];
          for(unsigned int k = 0; k < numEdgeElementDofs; ++k)
          {
            thisCellGlobalDofIndices[pos] = cell1DDofIndices[edgeId][d * numEdgeElementDofs + k];
            thisCellGlobalMarkers[pos] = (*cell1DDofMarkers[edgeId])[d * numEdgeElementDofs + k];
            thisCellGlobalDofTypes[pos] = cell1DDofTypes[edgeId][d * numEdgeElementDofs + k];

            pos++;
          }
        }
        for(unsigned int j = 0; j < cell.NumberOfFaces(); ++j)
        {
          const unsigned int& faceId = cell.Face(j)->Id();
          const unsigned int& numFaceElementDofs = num2DElementDofs[faceId];
          for(unsigned int k = 0; k < numFaceElementDofs; ++k)
          {
            thisCellGlobalDofIndices[pos] = cell2DDofIndices[faceId][d * numFaceElementDofs + k];
            thisCellGlobalMarkers[pos] = (*cell2DDofMarkers[faceId])[d * numFaceElementDofs + k];
            thisCellGlobalDofTypes[pos] = cell2DDofTypes[faceId][d * numFaceElementDofs + k];

            pos++;
          }
        }
        const unsigned int& numCellElementDofs = (*numMaximalElementDofs)[cellId];
        for(unsigned int k = 0; k < numCellElementDofs; ++k)
          thisCellGlobalDofIndices[pos++] = thisCellDofIndices[d * numCellElementDofs + k];
      }

      // add local contributes to system matrices
      unsigned int numCellNonZeroContributes = 0, numCellNonZeroDirichletContributes = 0;
      for(unsigned int j = 0; j < numGlobalDofs; ++j)
      {
        if(thisCellGlobalDofTypes[j] != Dirichlet)
          numCellNonZeroContributes++ ;
        else
          numCellNonZeroDirichletContributes++;
      }

      if(symmetricProblem)
        numNonZeroContributes += (numCellNonZeroContributes*(numCellNonZeroContributes+1))/2;
      else
        numNonZeroContributes += numCellNonZeroContributes * numCellNonZeroContributes;
      numNonZeroDirichletContributes += numCellNonZeroContributes * numCellNonZeroDirichletContributes;
    } // end cycle on maximal cells

    // Deallocate memory in auxiliary parameters.
    vector<int>().swap(nextDofIndex);
    vector<int>().swap(nextDirichletDofIndex);
    vector<unsigned int>().swap(num0DElementDofs);
    vector<unsigned int>().swap(num1DElementDofs);
    vector<unsigned int>().swap(num2DElementDofs);
    vector<unsigned int>().swap(num3DElementDofs);
    return exitCode;
  }
  // ***************************************************************************
  Output::ExitCodes MeshDofHandler::CreateDofPositions()
  {
    if (_mesh == nullptr || _referenceElement == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return  Output::GenericError;
    }

    const IReferenceElement& referenceElement = *_referenceElement;
    const IMesh& mesh = *_mesh;

    const unsigned int dimension = mesh.Dimension();

    _dofPositions.setZero(dimension, numDofs);
    _dirichletPositions.setZero(dimension, numDirichletDofs);
    maximalCellsDofPositions.resize(mesh.NumberOfMaximalCells());

    for (unsigned int c = 0; c < mesh.NumberOfMaximalCells(); c++)
    {
      const ITreeNode& cell = *mesh.GetMaximalTreeNode(c);
      const IGeometricObject& cellGeometry = *mesh.GetMaximalGeometricObject(c);

      if (!cell.IsActive())
        continue;

      const unsigned int& cellPositionId = cellGeometry.Id();

      IMapping* _referenceElementMap = nullptr;
      Output::ExitCodes result = cellGeometry.ComputeMap(_referenceElementMap);

      if (result != Output::Success)
      {
        Output::PrintErrorMessage("%s: computation of cell map failed", true, __func__);
        return result;
      }

      const IMapping& referenceElementMap = *_referenceElementMap;

      MatrixXd cellDofPositionsSingleSolution;
      MatrixXd& cellDofPositions = maximalCellsDofPositions[cellPositionId];

      referenceElement.EvaluateDofPositions(referenceElementMap,
                                            cellDofPositionsSingleSolution);

      const unsigned int numCellDofs = NumMaximalCellDofs(cellPositionId);
      const unsigned int numReferenceElementDofs = cellDofPositionsSingleSolution.cols();

      cellDofPositions.setZero(dimension, numCellDofs);

      for (unsigned int i = 0; i < solutionDimension; i++)
      {
        cellDofPositions.block(0,
                               i * numReferenceElementDofs,
                               dimension,
                               numReferenceElementDofs) = cellDofPositionsSingleSolution;
      }

      for (unsigned int i = 0; i < numCellDofs; i++)
      {
        if (IsDirichletDof(cellPositionId, i))
        {
          const int globalDofI = DirichletGlobalIndex(cellPositionId, i);
          _dirichletPositions.col(globalDofI)<< cellDofPositions.col(i);
        }
        else
        {
          const int globalDofI = DofGlobalIndex(cellPositionId, i);
          _dofPositions.col(globalDofI)<< cellDofPositions.col(i);
        }
      }

      delete _referenceElementMap;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes MeshDofHandler::ResetDofs()
  {
    numDofs = 0;
    numDirichletDofs = 0;
    numNonZeroContributes = 0;
    numNonZeroDirichletContributes = 0;
    cell0DDofIndices.clear();
    cell1DDofIndices.clear();
    cell2DDofIndices.clear();
    cell3DDofIndices.clear();
    cell0DDofMarkers.clear();
    cell1DDofMarkers.clear();
    cell2DDofMarkers.clear();
    cell3DDofMarkers.clear();
    cell0DDofTypes.clear();
    cell1DDofTypes.clear();
    cell2DDofTypes.clear();
    cell3DDofTypes.clear();
    maximalCellsGlobalDofs.clear();
    maximalCellsGlobalMarkers.clear();
    maximalCellsGlobalDofTypes.clear();
    maximalCellsDofPositions.clear();
    numNeighbourhoodOccurrences.clear();
    O_nnz.clear();
    D_nnz.clear();

    return Output::Success;
  }

  // ***************************************************************************
  void MeshDofHandler::CreateTreeNodeDofs(const ITreeNode& treeNode,
                                          const unsigned int& numElementDofs,
                                          vector<int>& treeNodeDofs,
                                          vector<DofType>& treeNodeTypes)
  {
    if(numElementDofs <= 0)
    {
      treeNodeDofs.resize(0);
      treeNodeTypes.resize(0);
      return;
    }

    treeNodeDofs.resize(solutionDimension*numElementDofs);
    treeNodeTypes.resize(solutionDimension*numElementDofs, MeshDofHandler::Dof);

    unsigned int pos = 0;
    for (unsigned int d = 0; d < solutionDimension; ++d)
    {
      if (treeNode.IsDirichlet(d))
      {
        for (unsigned int i = 0; i < numElementDofs; ++i)
        {
          treeNodeDofs[pos] = nextDirichletDofIndex[d]--;
          treeNodeTypes[pos] = MeshDofHandler::Dirichlet;
          pos++;
        }
      }
      else if (treeNode.IsNeumann(d))
      {
        for (unsigned int i = 0; i < numElementDofs; ++i)
        {
          treeNodeDofs[pos] = nextDofIndex[d]++;
          treeNodeTypes[pos] = MeshDofHandler::Neumann;
          pos++;
        }
      }
      else
      {
        for (unsigned int i = 0; i < numElementDofs; ++i)
        {
          treeNodeDofs[pos] = nextDofIndex[d]++;
          treeNodeTypes[pos] = MeshDofHandler::Dof;
          pos++;
        }
      }
    }
  }
  // ***************************************************************************
  Output::ExitCodes MeshDofHandler::CreateNeighbourhoodInformation()
  {
    // Initialize process management.
    MpiPartitioner partition;

#if USE_MPI == 1
    partition.SetProcess(MpiParallelEnvironment::Process());
#endif
    unsigned int rstart = 0, rend = 0;
#if USE_PETSC == 1
    MpiPartitioner::TypeBalancing type = MpiPartitioner::PETSCParallelismLogic;
#else
    MpiPartitioner::TypeBalancing type = MpiPartitioner::SequentialIndicesBalancing;
#endif
    /// <li> Partitioning data among processes
    partition.IndexPartition(type,numDofs,rstart,rend);
    const int processorRows=rend-rstart;
#if USE_MPI == 1
    int offset = (MpiParallelEnvironment::Process().Rank() == 0) ? 0 : rstart;
#else
    int offset =  0;
#endif
    ///<li> Fill neighbourhood information. Non-Dirichlet dofs are considered neighbourhood. The point itself is neighbourhood as long as its position in the global matrix fill the diagonal.
    numNeighbourhoodOccurrences.assign(numDofs,0);
    O_nnz.assign(processorRows,0);
    D_nnz.assign(processorRows,0);
    const IMesh& mesh = *_mesh;
    unsigned int numCellGlobalDofs=0;
    vector<set<unsigned int>> neighs(numDofs);
    vector<set<unsigned int>> o_neighs(numDofs);
    vector<set<unsigned int>> d_neighs(numDofs);

    ////TODO
    /// Dividere per bande processo e per banda soluzione (if)
    /// NumCellDofsSingleSolution
    unsigned int countedSolutionDofs=0;
    for(unsigned int solutionindex=0; solutionindex<solutionDimension; solutionindex++)
    {
      for(unsigned int i = 0; i < mesh.NumberOfMaximalCells(); ++i)
      {
        if(!mesh.GetMaximalTreeNode(i)->IsActive())
          continue;
        const IGeometricObject& cell = *mesh.GetMaximalGeometricObject(i);
        const unsigned int& cellId = cell.Id();
        numCellGlobalDofs = NumMaximalCellDofs(cellId);
        for(unsigned int j = 0; j < numCellGlobalDofs; ++j)
        {
          unsigned int dofIndex ;
          if (!IsDirichletDof(cellId, j))
          {
            dofIndex=DofGlobalIndex(cellId,j);

            for(unsigned int k = 0; k < numCellGlobalDofs; ++k)
            {
              unsigned int dofIndex2;
              if (!IsDirichletDof(cellId, k))
              {
                dofIndex2=DofGlobalIndex(cellId,k);
                neighs.at(dofIndex).insert(dofIndex2);
                if((dofIndex < rend) && (dofIndex >= rstart) && (dofIndex>=countedSolutionDofs && dofIndex<(countedSolutionDofs+numDofsPerComponent.at(solutionindex))))
                {
                  if((dofIndex2 < rend) && (dofIndex2 >= rstart) && (dofIndex2>=countedSolutionDofs && dofIndex2<(countedSolutionDofs+numDofsPerComponent.at(solutionindex))))
                  {
                    d_neighs.at(dofIndex).insert(dofIndex2);
                  }
                  else if((dofIndex2>=countedSolutionDofs && dofIndex2<(countedSolutionDofs+numDofsPerComponent.at(solutionindex))))
                  {
                    o_neighs.at(dofIndex).insert(dofIndex2);
                  }
                  else
                    continue;
                }
              }

            }
          }

        }
      } // End loop on maximal cells
      countedSolutionDofs+=numDofsPerComponent.at(solutionindex);

    }///End loop on solutions

    for(unsigned int i = 0; i < numDofs; ++i)
    {
      numNeighbourhoodOccurrences[i] = neighs.at(i).size()/solutionDimension;
    }
    for(unsigned int i = rstart; i < rend; ++i)
    {
      int index_local = i-offset;
      D_nnz[index_local] = d_neighs.at(i).size();
      O_nnz[index_local] = o_neighs.at(i).size();
    }

#if VERBOSE == 4
    for(unsigned int i=0; i < MpiParallelEnvironment::Process().NumberProcesses(); ++i)
    {
      MPI_Barrier(MPI_COMM_WORLD);
      if(i == MpiParallelEnvironment::Process().Rank())
      {
        Output::PrintDebugMessage("New",false);
        ostringstream debug;
        debug << numNeighbourhoodOccurrences << endl;
        debug << numNeighbourhoodOccurrences.size() << endl;
        debug << D_nnz << endl;
        debug << O_nnz << endl;
        Output::PrintDebugMessage("%s",false, debug.str().c_str());
      }
    }
#endif

    return Output::Success;
  }
  // ***************************************************************************
}
