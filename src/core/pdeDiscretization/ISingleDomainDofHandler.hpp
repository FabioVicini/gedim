#ifndef __ISINGLEDOMAINDOFHANDLER_H
#define __ISINGLEDOMAINDOFHANDLER_H

#include "Output.hpp"
#include "IDofHandler.hpp"
#include "IReferenceElement.hpp"
#include "IMesh.hpp"

using namespace MainApplication;

namespace GeDiM
{
	/// \brief Interface used to create dof indices for global matrix assembly of a single domain.
	/// \copyright See top level LICENSE file for details.
	class ISingleDomainDofHandler : public IDofHandler
	{
		public:
			virtual ~ISingleDomainDofHandler() {}

			/// \brief Set reference element of the domain
			virtual void SetReferenceElement(const IReferenceElement& element) = 0;
			/// \brief Set mesh of the domain
			virtual void SetMesh(const IMesh& mesh) = 0;
	};
}

#endif // __ISINGLEDOMAINDOFHANDLER_H
