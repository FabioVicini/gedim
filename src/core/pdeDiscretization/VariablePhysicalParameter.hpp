#ifndef _VARIABLEPHYSICALPARAMETER_H
#define _VARIABLEPHYSICALPARAMETER_H

#include "IPhysicalParameter.hpp"

namespace GeDiM
{
  /// \brief Class for the variable Physical Parameter of Differential Equations
  /// \copyright See top level LICENSE file for details.
  class VariablePhysicalParameter final : public IPhysicalParameter
  {
    public:
      typedef Output::ExitCodes (*PhysicalParameterFunction)(const MatrixXd& points,
                                                             VectorXd& results);

    private:
      PhysicalParameterFunction _function; ///< The physical parameter function

    public:
      VariablePhysicalParameter(const PhysicalParameterFunction& function) { _function = function; }
      ~VariablePhysicalParameter() { _function = nullptr; }

      Output::ExitCodes Eval(const MatrixXd& points, VectorXd& results) const
      {
        if (_function == nullptr)
          return Output::GenericError;

        return _function(points, results);
      }
  };
}

#endif // _VARIABLEPHYSICALPARAMETER_H
