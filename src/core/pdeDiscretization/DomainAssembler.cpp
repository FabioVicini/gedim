#include "DomainAssembler.hpp"
#if USE_PETSC
#include "petscvec.h"
#include "petscmat.h"
#endif

namespace GeDiM
{
  // ***************************************************************************
  DomainAssembler::DomainAssembler()

  {
    _equation = nullptr;
    _mesh = nullptr;
    _dofHandler = nullptr;

    _rightHandSide = nullptr;
    _solutionDirichlet = nullptr;
    _globalMatrix = nullptr;
    _dirichletMatrix = nullptr;
  }
  DomainAssembler::~DomainAssembler()
  {
    _equation = nullptr;
    _mesh = nullptr;
    _dofHandler = nullptr;

    _rightHandSide = nullptr;
    _solutionDirichlet = nullptr;
    _globalMatrix = nullptr;
    _dirichletMatrix = nullptr;
  }
  // ***************************************************************************
  Output::ExitCodes DomainAssembler::Initialize(const IDifferentialEquation& equation,
                                                                        const IMesh& mesh,
                                                                        const ISingleDomainDofHandler& dofHandler,
                                                                        IVector& rightHandside,
                                                                        IVector& solutionDirichlet,
                                                                        ISparseMatrix& globalMatrix,
                                                                        ISparseMatrix& dirichletMatrix)
  {
    _equation = &equation;
    _mesh = &mesh;
    _dofHandler = &dofHandler;
    _rightHandSide = &rightHandside;
    _solutionDirichlet = &solutionDirichlet;
    _globalMatrix = &globalMatrix;
    _dirichletMatrix = &dirichletMatrix;

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes DomainAssembler::AssembleDiscreteSystem() const
  {
    /// <ul>

    if (_equation == nullptr ||
        _mesh == nullptr ||
        _dofHandler == nullptr ||
        _rightHandSide == nullptr ||
        _solutionDirichlet == nullptr ||
        _globalMatrix == nullptr ||
        _dirichletMatrix == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return  Output::GenericError;
    }

    const IMesh& mesh = *_mesh;
    const IDifferentialEquation& equation = *_equation;
    const ISingleDomainDofHandler& dofHandler = *_dofHandler;

    IVector& rightHandSide = *_rightHandSide;
    IVector& solutionDirichlet = *_solutionDirichlet;
    ISparseMatrix& globalMatrix = *_globalMatrix;
    ISparseMatrix& dirichletMatrix = *_dirichletMatrix;

    const bool symmetricProblem = dofHandler.SymmetricProblem();
    const unsigned int numDofs = dofHandler.NumDofs();
    const unsigned int numDirichletDofs = dofHandler.NumDirichletDofs();
    const unsigned int numLocalDofs=dofHandler.D_Nnz().size();
    /// Maximal nonzeroes per row
    int  d_nz,o_nz;
    /// <li> Initialize variable dimensions
    rightHandSide.SetSize(numDofs);
    solutionDirichlet.SetSize(numDirichletDofs);

    ///Compute maximal nonzeroes for local stripe and copy informations from
    for(unsigned int offset=0;offset<numLocalDofs;offset++)
    {
      if(o_nz<dofHandler.O_Nnz().at(offset)) o_nz=dofHandler.O_Nnz().at(offset);
      if(d_nz<dofHandler.D_Nnz().at(offset)) d_nz=dofHandler.D_Nnz().at(offset);
    }

    globalMatrix.SetSizes(numDofs, numDofs,numLocalDofs,d_nz,o_nz,dofHandler.D_Nnz(),dofHandler.O_Nnz());
    dirichletMatrix.SetSize(numDofs, numDirichletDofs);

    /// <li> Assemble the equation
    for (unsigned int c = 0; c < mesh.NumberOfMaximalCells(); c++)
    {
      const ITreeNode& cell = *mesh.GetMaximalTreeNode(c);
      const IGeometricObject& cellGeometry = *mesh.GetMaximalGeometricObject(c);

      if (!cell.IsActive())
        continue;

      const unsigned int cellPositionId = cellGeometry.Id();

      MatrixXd cellMatrix;
      VectorXd cellRightHandSide, cellDirichletTermValues;

      Output::ExitCodes result = equation.BuildLocalSystem(mesh,
                                                           cellPositionId,
                                                           cellMatrix,
                                                           cellRightHandSide,
                                                           cellDirichletTermValues);

      if (result != Output::Success)
      {
        Output::PrintErrorMessage("%s: error on build local system of cell %d", true, __func__, cellPositionId);
        return result;
      }

      const unsigned int numCellDofs = dofHandler.NumMaximalCellDofs(cellPositionId);

      for(unsigned int i = 0; i < numCellDofs; i++)
      {
        if (dofHandler.IsDirichletDof(cellPositionId, i))
        {
          const int globalDirichletI = dofHandler.DirichletGlobalIndex(cellPositionId, i);
          solutionDirichlet.SetFillMode(VectorFillMode::Insert);
          solutionDirichlet.SetValue(globalDirichletI,cellDirichletTermValues(i));

          continue;
        }

        const int globalDofI = dofHandler.DofGlobalIndex(cellPositionId, i);
        rightHandSide.SetFillMode(VectorFillMode::Add);
        rightHandSide.SetValue(globalDofI,cellRightHandSide(i));

        for(unsigned int j = 0; j < numCellDofs; j++)
        {
          if (dofHandler.IsDirichletDof(cellPositionId, j))
          {
            const int globalDirichletJ = dofHandler.DirichletGlobalIndex(cellPositionId, j);
            dirichletMatrix.Triplet(globalDofI, globalDirichletJ, cellMatrix(i, j));
          }
          else
          {
            const int globalDofJ = dofHandler.DofGlobalIndex(cellPositionId, j);

            if (!symmetricProblem || (symmetricProblem && (globalDofJ <= globalDofI)) )
              globalMatrix.Triplet(globalDofI, globalDofJ, cellMatrix(i, j));
          }
        }
      }
    }
    rightHandSide.Create();
    solutionDirichlet.Create();
    globalMatrix.Create();
    dirichletMatrix.Create();

    return Output::Success;
  }
  // ***************************************************************************
}
