#ifndef _VARIABLEBOUNDARYCONDITION_H
#define _VARIABLEBOUNDARYCONDITION_H

#include "IBoundaryCondition.hpp"

namespace GeDiM
{
  /// \brief Class for the variable Boundary condition of Differential Equations
  /// \copyright See top level LICENSE file for details.
  class VariableBoundaryCondition final : public IBoundaryCondition
  {
    public:
      typedef Output::ExitCodes (*BoundaryConditionFunction)(const unsigned int& marker,
                                                             const MatrixXd& points,
                                                             VectorXd& results);

    private:
      BoundaryConditionFunction _function; ///< The physical parameter function

    public:
      VariableBoundaryCondition(const BoundaryConditionFunction& function) { _function = function; }
      ~VariableBoundaryCondition() { _function = nullptr; }

      Output::ExitCodes Eval(const unsigned int& marker,
                             const MatrixXd& points,
                             VectorXd& results) const
      {
        if (_function == nullptr)
          return Output::GenericError;

        return _function(marker, points, results);
      }
  };
}

#endif // _VARIABLEBOUNDARYCONDITION_H
