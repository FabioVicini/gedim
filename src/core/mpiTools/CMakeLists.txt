set(gedim_core_mpiTools_source
  ${CMAKE_CURRENT_SOURCE_DIR}/MpiProcess.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/MpiPartitioner.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/MpiParallelEnvironment.cpp
  PARENT_SCOPE)

set(gedim_core_mpiTools_headers
  ${CMAKE_CURRENT_SOURCE_DIR}/IMpiProcess.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/MpiProcess.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/MpiPartitioner.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/MpiParallelEnvironment.hpp
  PARENT_SCOPE)

set(gedim_core_mpiTools_include
	# insert subdirectories to be included here, one per line
  PARENT_SCOPE)
