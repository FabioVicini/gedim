#ifndef __MPIPARTITIONER_H
#define __MPIPARTITIONER_H

#include <vector>
#include <string>

#include "Output.hpp"
#include "Eigen"
using namespace Eigen;

#include "IMpiProcess.hpp"
#include "AdjacencyMatrix.hpp"
#include <algorithm>

#if USE_MPI == 1
#include <mpi.h>
using	namespace MPI;
#endif


#if USE_PETSC == 1
#include "petscvec.h"
#endif

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  class MpiNetworkPartitionOptions
  {
    public:
      enum PartitionTypes
      {
        CutBalancing = 0,
        VolBalancing = 1
      };
      enum GraphTypes
      {
        Base = 0,
        Bipartited = 1,
        Tripartited = 2
      };

    public:
      MpiNetworkPartitionOptions();
      ~MpiNetworkPartitionOptions();

      PartitionTypes PartitionType;
      GraphTypes GraphType;
      unsigned int NumberOfParts;
      unsigned int MasterWeight; /// 0 de-activated; 100 activated totally
      vector<unsigned int> NodeWeights;
      vector<unsigned int> EdgeWeights;
  };

  class MpiPartitioner
  {
    public:
      enum TypeBalancing
      {
        SequentialIndicesBalancing = 0,
        PETSCParallelismLogic = 1
      };

    protected:
      const IMpiProcess *processPointer;


    public:
      MpiPartitioner();
      virtual ~MpiPartitioner();

      void SetProcess(const IMpiProcess& process) { processPointer = &process; }
      Output::ExitCodes VecPartition(const  TypeBalancing &type,
                                     unsigned int &rankStart,
                                     unsigned int &rankEnd,
                                     VectorXd &vector);
      ///\brief split a set of indices sizeToAttribute locally from start to end for each process. Charge and ways are TypeBalancing defined
      /// \returns local start index and local end index
      Output::ExitCodes IndexPartition(const  TypeBalancing &type,
                                       const unsigned int &sizeToAttribute,
                                       unsigned int &rankStart,
                                       unsigned int &rankEnd);

      ///\brief Partition a vector in parallel distributing the charghe following a vector of true and false (objects are not considered, such an example non-Active cells in the vector of cells for a mesh)
      /// \returns the beginning index of local attribution, and the end one
      Output::ExitCodes PartitionWithHoles(const vector<bool>& myVec,
                                           const unsigned int& numTrues,
                                           unsigned int &rankStart,
                                           unsigned int &rankEnd);
      Output::ExitCodes NetworkPartition(const MpiNetworkPartitionOptions& options,
                                         const AdjacencyMatrix& adjacencyMatrix,
                                         vector<unsigned int>& partition);

  };
}


#endif // __MPIPARTITIONER_H
