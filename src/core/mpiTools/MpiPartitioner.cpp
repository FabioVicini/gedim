#include "MpiPartitioner.hpp"
#include "MpiParallelEnvironment.hpp"
#include "Utilities.hpp"


#if USE_MPI == 1
#include "mpi.h"
#endif
#if USE_PETSC == 1
#include "petscvec.h"
#endif
#if ENABLE_METIS
#include "metis.h"
#endif

namespace GeDiM
{
  // ***************************************************************************
  MpiNetworkPartitionOptions::MpiNetworkPartitionOptions()
  {
    PartitionType = PartitionTypes::CutBalancing;
    NumberOfParts = 0;
    MasterWeight = 100;
  }
  MpiNetworkPartitionOptions::~MpiNetworkPartitionOptions()
  {
    NodeWeights.clear();
    EdgeWeights.clear();
  }
  // ***************************************************************************
  MpiPartitioner::MpiPartitioner()
  {
    processPointer = NULL;
  }
  MpiPartitioner::~MpiPartitioner()
  {
  }
  // ***************************************************************************
  Output::ExitCodes MpiPartitioner::VecPartition(const MpiPartitioner::TypeBalancing& type,
                                                 unsigned int& rankStart,
                                                 unsigned int& rankEnd,
                                                 VectorXd &vector)
  {
    return IndexPartition(type, vector.size(), rankStart, rankEnd);
  }
  // ***************************************************************************
  Output::ExitCodes MpiPartitioner::IndexPartition(const GeDiM::MpiPartitioner::TypeBalancing& type,
                                                   const unsigned int& sizeToAttribute,
                                                   unsigned int& rankStart,
                                                   unsigned int& rankEnd)
  {
    Output::ExitCodes result = Output::Success;
    Utilities::Unused(type); //< could be unused if macro is not active


#if USE_MPI == 1
    const IMpiProcess& process = *processPointer;

    switch (type)
    {
      case SequentialIndicesBalancing:
      {
        rankStart = sizeToAttribute*(process.Rank())/process.NumberProcesses() ;
        rankEnd = sizeToAttribute*(process.Rank()+1)/process.NumberProcesses() ;
      }
      break;
      case PETSCParallelismLogic:
      {
#if USE_PETSC == 1
        Vec Vector;
        VecCreate(PETSC_COMM_WORLD,&Vector);
        VecSetType(Vector,VECMPI);
        VecSetSizes(Vector,PETSC_DECIDE,sizeToAttribute);
        VecGetOwnershipRange(Vector,(PetscInt *)(&rankStart),(PetscInt *)(&rankEnd));
        VecDestroy(&Vector);
#else
        Output::PrintErrorMessage("The chosen type would call PETSc functions.",type);
        result= Output::GenericError;
#endif
      }
      break;
      default:
        Output::PrintErrorMessage("The chosen type does not match any implemented logic of parallelism.",type);
        result= Output::GenericError;
      break;
    }
#else
    rankStart=0;
    rankEnd=sizeToAttribute;
#endif
    return result;
  }
  // ***************************************************************************
  Output::ExitCodes MpiPartitioner::NetworkPartition(const MpiNetworkPartitionOptions& options,
                                                     const AdjacencyMatrix& adjacencyMatrix,
                                                     vector<unsigned int>& partition)
  {
    /// <ul>

    const IMpiProcess& process= *processPointer;

    /// <li> Check the number of parts

    const unsigned int& numberElements = adjacencyMatrix.NumberDomains();
    unsigned int numberParts = options.NumberOfParts;
    const unsigned int& masterWeight = options.MasterWeight;
    partition.resize(numberElements, 0);


    if (numberElements == 0)
      return Output::GenericError;

    if (numberParts == 0)
    {
      numberParts = (masterWeight == 0) ? process.NumberProcesses() - 1 : process.NumberProcesses();

    }
    if (numberParts == 0)
    {
      Output::PrintErrorMessage("Number Parts 0 not supported", false);
      return Output::GenericError;
    }

    if (numberParts == 1)
    {
      if (masterWeight == 0 && process.NumberProcesses() > 1)
        partition.resize(numberElements, 1);
      else
        partition.resize(numberElements, 0);

      return Output::Success;
    }

    Output::ExitCodes result = Output::Success;

#if ENABLE_METIS == 1
    unsigned int numberConnections = adjacencyMatrix.NumberConnections();

    /// <li> Initialize METIS Partition
    rstatus_et metisResult = METIS_OK;
    idx_t nParts = numberParts;
    idx_t objval;
    idx_t nElements = numberElements;
    vector<idx_t> xadj(nElements + 1);
    vector<idx_t> adjncy(numberConnections);
    vector<idx_t> metisPartition(numberElements);

    const MpiNetworkPartitionOptions::PartitionTypes& metisDivisionType = options.PartitionType;

    idx_t nCon = 1;
    idx_t* vwgt = NULL, *adjwgt = NULL;
    idx_t* v_size = NULL;
    real_t* tpwgts = NULL, *ubvec = NULL;
    idx_t metisOptions[METIS_NOPTIONS];

    /// <li> Build adjncy and xadj for METIS partition
    memcpy(xadj.data(), adjacencyMatrix.AdjacencyRows().data(), sizeof(idx_t) * (numberElements + 1));
    memcpy(adjncy.data(), adjacencyMatrix.AdjacencyCols().data(), sizeof(idx_t) * numberConnections);

    METIS_SetDefaultOptions(metisOptions);

    switch (metisDivisionType)
    {
      case MpiNetworkPartitionOptions::CutBalancing:
        metisOptions[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;

        if (options.NodeWeights.size() > 0)
        {
          vwgt = new idx_t[numberElements];
          memcpy(vwgt, options.NodeWeights.data(), sizeof(idx_t) * numberElements);
        }

        if (options.EdgeWeights.size() > 0)
        {
          adjwgt = new idx_t[numberConnections];
          memcpy(adjwgt, options.EdgeWeights.data(), sizeof(idx_t)*numberConnections);
        }

      break;
      case MpiNetworkPartitionOptions::VolBalancing:
        metisOptions[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_VOL;

        if (options.NodeWeights.size() > 0)
        {
          v_size = new idx_t[numberElements];
          memcpy(v_size, options.NodeWeights.data(), sizeof(idx_t) * numberElements);
        }
      break;
      default:
        Output::PrintErrorMessage("MetisDivisionType %d not supported", false, metisDivisionType);
      return Output::GenericError;
    }

    if (masterWeight > 0)
    {
      tpwgts = new real_t[numberParts];
      tpwgts[0] = (real_t)masterWeight / (real_t)numberParts / 100.0;

      for (unsigned int p = 1; p < numberParts; p++)
        tpwgts[p] = 1.0 / (numberParts - 1.0) * (1.0 - tpwgts[0]);
    }

    /// <li> Partiton
    metisResult = (rstatus_et)METIS_PartGraphKway(&nElements, &nCon, xadj.data(), adjncy.data(), vwgt, v_size, adjwgt, &nParts, tpwgts, ubvec, metisOptions, &objval, metisPartition.data());

    switch(metisResult)
    {
      case METIS_ERROR_INPUT:
        Output::PrintErrorMessage("METIS failed due to erroneous inputs and/or options", false);
        result = Output::GenericError;
      break;
      case METIS_ERROR_MEMORY:
        Output::PrintErrorMessage("METIS failed due to insufficient memory", false);
        result = Output::GenericError;
      break;
      case METIS_ERROR:
        Output::PrintErrorMessage("METIS failed because of an unknown error", false);
        result = Output::GenericError;
      break;
      default:
      break;
    }

    partition.resize(numberElements);

    // Sometimes it happens METIS partition is not correct because of a METIS implementation error
    /// <li> Correct METIS partition
    if(objval == 0)
    {
      if (masterWeight == 0)
        fill_n(partition.data(), nElements, 1);
      else
        fill_n(partition.data(), nElements, 0);
    }
    else
    {
      if (masterWeight > 0)
        memcpy(partition.data(), metisPartition.data(), numberElements * sizeof(unsigned int));
      else
      {
        for (unsigned int p = 0; p < numberElements; p++)
          partition[p] = metisPartition[p] + 1;
      }
    }
    delete[] tpwgts; tpwgts = NULL;
    delete[] v_size; v_size = NULL;
    delete[] vwgt; vwgt = NULL;
    delete[] adjwgt; adjwgt = NULL;

#endif

    // Reordering partition vector
    unsigned int controlActivation=(masterWeight>0) ? 0 : 1;
    bool control =false;
    unsigned int loop=0;
    while(!control)
    {
      if(find(partition.begin(),partition.end(),controlActivation)==partition.end())
      {
        for (unsigned int p = 0; p < numberElements; p++)
        {
          if(controlActivation<partition[p])
            partition[p]=partition[p]-1;
        }
      }
      else{
        controlActivation++;
      }
      loop++;
      control = (loop==numberParts) ? true : false ;
    }
    return result;

    /// </ul>
  }
  // ***************************************************************************
  Output::ExitCodes MpiPartitioner::PartitionWithHoles(const vector<bool>& myVec, const unsigned int& numTrues, unsigned int &rankStart, unsigned int &rankEnd)
  {
    const unsigned int& myRank = MpiParallelEnvironment::Process().Rank();
    const unsigned int& numberProcesses = MpiParallelEnvironment::Process().NumberProcesses();

    /// compute previous true values and elements in current process
    std::div_t quotientAndRemainder = std::div( (int)numTrues, (int)numberProcesses );
    unsigned int myOffset = 0;
    for(unsigned int i = 0; i < myRank; i++)
    {
      myOffset += quotientAndRemainder.quot;
      if(quotientAndRemainder.rem>0)
      {
        myOffset++;
        quotientAndRemainder.rem--;
      }
    }
    unsigned int numElements = quotientAndRemainder.quot;
    if(quotientAndRemainder.rem>0)
      numElements += 1;

    /// compute rank start
    unsigned int countTrues = 0;
    rankStart = 0;
    while(countTrues < myOffset)
    {
      if(myVec[rankStart])
        countTrues++;
      rankStart++;
    }
    while(!myVec[rankStart])
      rankStart++;

    /// compute past-to-last index of current project
    countTrues = 0;
    rankEnd = rankStart;
    while(countTrues < numElements)
    {
      if(myVec[rankEnd])
        countTrues++;
      rankEnd++;
    }

    return Output::Success;
  }

  //****************************************************************************

}
