#ifndef COMPAREDOUBLE_HPP
#define COMPAREDOUBLE_HPP

namespace GeDiM
{
  struct CompareDouble
  {
      double epsilon;
      CompareDouble() : epsilon(1.0E-7) { }
      CompareDouble(const double& _eps) : epsilon(_eps) { }

      bool operator()(const double& first, const double& second) const
      {
        return first <= second - first * epsilon; //Errore Relativo
      }
  };
}

#endif // COMPAREDOUBLE_HPP
