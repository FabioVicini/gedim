#ifndef __IEIGENLINEARSOLVER_H
#define __IEIGENLINEARSOLVER_H

#include "ISolver.hpp"
#include "Eigen"

using namespace Eigen;

namespace GeDiM
{
  /// \brief Interface used for Eigen linear solvers
  /// \copyright See top level LICENSE file for details.
  class IEigenSolver : ISolver
  {
    public:
      virtual ~IEigenSolver() { }

      /// \brief Initialize the solver
      Output::ExitCodes Initialize() { return Output::UnimplementedMethod; }

      /// \brief Initialize the solver
      /// \param matrix The matrix of the linear system
      /// \param rightHandSide The rightHandSide of the linear system
      /// \param solution The solution of the linear system
      virtual Output::ExitCodes Initialize(const SparseMatrix<double>& matrix,
                                           const VectorXd& rightHandSide,
                                           VectorXd& solution) = 0;

      virtual Output::ExitCodes Solve() const = 0;
  };
}

#endif // __IEIGENLINEARSOLVER_H
