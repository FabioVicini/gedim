set(gedim_core_linearAlgebra_source
	${CMAKE_CURRENT_SOURCE_DIR}/EigenCholeskySolver.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/EigenLUSolver.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/EigenSparseMatrix.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/EigenVector.cpp
	PARENT_SCOPE)

set(gedim_core_linearAlgebra_headers
	${CMAKE_CURRENT_SOURCE_DIR}/ILinearSolver.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/ISparseMatrix.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/IVector.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/EigenLUSolver.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/EigenCholeskySolver.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/EigenSparseMatrix.hpp
	${CMAKE_CURRENT_SOURCE_DIR}/EigenVector.hpp
	PARENT_SCOPE)

set(gedim_core_linearAlgebra_include
	# insert subdirectories to be included here, one per line
  PARENT_SCOPE)
