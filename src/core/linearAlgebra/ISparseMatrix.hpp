#ifndef ISPARSEMATRIX_HPP
#define ISPARSEMATRIX_HPP

#include "Output.hpp"


using namespace std;
using namespace MainApplication;

namespace GeDiM
{

  enum class SparseMatrixFillMode
  {
    Insert = 1,
    Add = 0
  };

  /// \brief Interface used for sparse matrix of double
  /// \copyright See top level LICENSE file for details.
  class ISparseMatrix
  {
    public:
      virtual ~ISparseMatrix() {}

      /// \brief Resize the matrix
      /// \param numRows the number of rows
      /// \param numCols the number of cols
      virtual Output::ExitCodes SetSize(const unsigned int& numRows,
                                        const unsigned int& numCols) = 0;
      /// \brief Resize the matrix
      /// \param numRows the number of rows
      /// \param numCols the number of cols
      /// \param numLocalRows the local size of the banner part of the matrix (rectangular parallelization)
      /// \param d_nz number of nonzeros per row in DIAGONAL portion of local submatrix (same value is used for all local rows)
      /// \param o_nz 	- number of nonzeros per row in the OFF-DIAGONAL portion of local submatrix (same value is used for all local rows).
      /// If the *_nnz parameter is given then the *_nz parameter is ignored
      virtual Output::ExitCodes SetSizes(const unsigned int& numRows,
                                         const unsigned int& numCols,
                                         const unsigned int& numLocalRows,
                                         const unsigned int& d_nz,
                                         const unsigned int& o_nz,
                                         const vector<int>& D_nnz,
                                         const vector<int>& O_nnz) = 0;
      /// \brief  Matrix Allocation is implemented in child classes
      /// \brief Matrix create call. Last call to complete the matrix structure.
      virtual Output::ExitCodes Create() = 0;
      /// \brief Intermediate flush of the matrix during creation.
      virtual Output::ExitCodes Flush() = 0;

      /// \brief Set the fill mode for the matrix
      virtual void SetFillMode(const SparseMatrixFillMode& inputMode) = 0;
      /// \brief Put zero-values in the matrix
      virtual Output::ExitCodes Reset() = 0;
      /// \brief Given the row index i and the column index j the value val is put into the matrix in ADD or INSERT mode
      virtual Output::ExitCodes Triplet(const unsigned int& i,
                                        const unsigned int& j,
                                        const double& val) = 0;
      /// \brief Given the row indices i and the column indices j the values val are put into the matrix in ADD or INSERT mode
      virtual Output::ExitCodes Triplets(const vector<unsigned int>& i,
                                         const vector<unsigned int>& j,
                                         const vector<double>& val) = 0;


  };
}

#endif // ISPARSEMATRIX_HPP
