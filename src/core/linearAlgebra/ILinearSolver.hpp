#ifndef __ILINEARSOLVER_H
#define __ILINEARSOLVER_H

#include "Output.hpp"
#include "ISparseMatrix.hpp"
#include "IVector.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Interface used for algebra linear solvers
  /// \copyright See top level LICENSE file for details.
  class ILinearSolver
  {
    public:
      virtual ~ILinearSolver() { }

      /// \brief Initialize the linear solver Ax = b
      /// \param matrix The matrix A
      /// \param rightHandSide The right-hand side b
      /// \param solution The solution x
      /// \return the result of the initialization
      virtual Output::ExitCodes Initialize(const ISparseMatrix& matrix,
                                           const IVector& rightHandSide,
                                           IVector& solution) = 0;
      /// \brief Compute the solution
      virtual Output::ExitCodes Solve() const = 0;
  };
}

#endif // __ILINEARSOLVER_H
