#include "EigenVector.hpp"

namespace GeDiM
{
  template class EigenVector<VectorXd>;
  // ***************************************************************************
  template<typename EigenVectorType>
  Output::ExitCodes EigenVector<EigenVectorType>::SetValue(const int& i,
                                                           const double& val)
  {
    if (_mode == VectorFillMode::Insert)
      _vector[i]=val;
    else
      _vector[i]+=val;
    return Output::Success;
  }
  // ***************************************************************************
  template<typename EigenVectorType>
  Output::ExitCodes EigenVector<EigenVectorType>::SetValues(const vector<int>&,
                                                            const vector<double>&)
  {
    return Output::UnimplementedMethod;
  }
  // ***************************************************************************
}
