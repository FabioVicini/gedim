#ifndef IVECTOR_HPP
#define IVECTOR_HPP

#include "Output.hpp"


using namespace std;
using namespace MainApplication;

namespace GeDiM
{

  enum class VectorFillMode
  {
    Insert = 1,
    Add = 0
  };
  /// \brief Interface used for column double vector
  /// \copyright See top level LICENSE file for details.
  class IVector
  {
    public:
      virtual ~IVector() {}

      /// \brief Matrix create call. Last call to complete the matrix structure.
      virtual Output::ExitCodes Create() = 0;
      /// \brief Set the size the vector. Resize a vector for Eigen application.
      /// \param numCols the number of cols
      virtual Output::ExitCodes SetSize(const unsigned int& numCols) = 0;

      /// \brief Set the size the vector. Resize a vector for Eigen application.
      /// \param numCols the number of cols
      /// \param numLocalCols the local size for the vector (It can be set for each process. For PETSC applciation when not specified is set to PETSC_DECIDE)
      virtual Output::ExitCodes SetSizes(const unsigned int& numCols,
                                         const unsigned int& numLocalCols) = 0;
      /// \brief Put zero-values in the vector
      virtual Output::ExitCodes Zeros() = 0;
      /// \brief operator []
      /// \param i the i_th position starting from 0
      /// \return the reference to i_th element
      virtual double& operator[] (const int& i) = 0;
      /// \brief const operator []
      /// \param i the i_th position starting from 0
      /// \return the const reference to i_th element
      virtual const double& operator[] (const int& i) const = 0;
      /// \brief Set the Vector Value
      /// \param i the i_th global position starting from 0
      /// \param val element to set
      virtual Output::ExitCodes SetValue(const int& i,
                                         const double& val) = 0;
      /// \brief Set the Vector Values
      /// \param i the i_th global positions starting from 0
      /// \param val the elements to set
      virtual Output::ExitCodes SetValues(const vector<int>& i,
                                          const vector<double>& val) = 0;
      /// \brief Set the fill mode for the matrix
      virtual void SetFillMode(const VectorFillMode& inputMode) = 0;
  };
}

#endif // IVECTOR_HPP
