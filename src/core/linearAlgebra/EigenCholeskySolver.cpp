#include "EigenCholeskySolver.hpp"

namespace GeDiM
{
  // ***************************************************************************
  EigenCholeskySolver::EigenCholeskySolver(const EigenCholeskySolverConfig& configuration)
  {
    _configuration = configuration;
    _rightHandSide = nullptr;
    _solution = nullptr;
  }
  EigenCholeskySolver::~EigenCholeskySolver()
  {
    _rightHandSide = nullptr;
    _solution = nullptr;
  }
  // ***************************************************************************
  Output::ExitCodes EigenCholeskySolver::Initialize(const ISparseMatrix& matrix,
                                                    const IVector& rightHandSide,
                                                    IVector& solution)
  {
    _rightHandSide = &rightHandSide;
    _solution = &solution;

    const SparseMatrix<double>& _matrix = static_cast<const EigenSparseMatrix<SparseMatrix<double>>&>(matrix);

    if (_configuration.SymmetricProblem)
      linearSolver.compute(_matrix.selfadjointView<Lower>());
    else
      linearSolver.compute(_matrix);

    if (linearSolver.info() != Eigen::ComputationInfo::Success)
    {
      Output::PrintErrorMessage("%s: Cholesky Factorization computation failed", true, __func__);
      return Output::GenericError;
    }

    return Output::Success;
  }
  // ***************************************************************************
  Output::ExitCodes EigenCholeskySolver::Solve() const
  {
    if (_rightHandSide == nullptr ||
        _solution == nullptr)
    {
      Output::PrintErrorMessage("%s: no initialization found", false, __func__);
      return  Output::GenericError;
    }

    const VectorXd& rightHandSide = static_cast<const EigenVector<VectorXd>&>(*_rightHandSide);
    VectorXd& solution = static_cast<EigenVector<VectorXd>&>(*_solution);

    solution = linearSolver.solve(rightHandSide);

    if (linearSolver.info() != Eigen::ComputationInfo::Success)
    {
      Output::PrintErrorMessage("%s: Cholesky solver failed", false, __func__);
      return Output::GenericError;
    }

    return Output::Success;
  }
  // ***************************************************************************
}
