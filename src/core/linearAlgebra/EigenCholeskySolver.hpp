#ifndef __EIGENCHOLESKYSOLVER_H
#define __EIGENCHOLESKYSOLVER_H

#include "ILinearSolver.hpp"
#include "EigenSparseMatrix.hpp"
#include "EigenVector.hpp"

using namespace Eigen;

namespace GeDiM
{
  class EigenCholeskySolverConfig
  {
    public:
      bool SymmetricProblem = true; ///<< True if the resulting global matrix is known to be symmetric.
  };

  /// \brief Eigen Cholesky Linear solver
  /// \copyright See top level LICENSE file for details.
  class EigenCholeskySolver final : public ILinearSolver
  {
    private:
      EigenCholeskySolverConfig _configuration;
      SimplicialLLT<SparseMatrix<double>, Lower> linearSolver; ///< The solver
      const IVector* _rightHandSide; ///< The rightHandSide of the linear syste
      IVector* _solution; ///< The solution of the linear syste

    public:
      EigenCholeskySolver(const EigenCholeskySolverConfig& configuration = EigenCholeskySolverConfig());
      ~EigenCholeskySolver();

      Output::ExitCodes Initialize(const ISparseMatrix& matrix,
                                   const IVector& rightHandSide,
                                   IVector& solution);

      Output::ExitCodes Solve() const;
  };
}

#endif // __EIGENCHOLESKYSOLVER_H
