#ifndef EIGENVECTOR_HPP
#define EIGENVECTOR_HPP

#include "IVector.hpp"
#include "Eigen"

using namespace std;
using namespace MainApplication;
using namespace Eigen;

namespace GeDiM
{
  /// \brief Eigen column vector
  /// \copyright See top level LICENSE file for details.
  template<typename EigenVectorType = VectorXd>
  class EigenVector final : public IVector
  {
    private:
      EigenVectorType _vector;
      VectorFillMode _mode;
    public:
      EigenVector() { _mode=VectorFillMode::Insert; }
      EigenVector(EigenVectorType&& vector) {
        _mode = VectorFillMode::Insert;
        _vector = std::move(vector);
      }
      ~EigenVector() { }

      operator EigenVectorType&() { return _vector;  }
      operator const EigenVectorType&() const { return _vector; }
      Output::ExitCodes Create(){ return Output::Success; }
      void SetFillMode(const VectorFillMode& inputMode){ _mode=inputMode; }
      Output::ExitCodes SetSize(const unsigned int& numCols) { _vector.setZero(numCols); return Output::Success; }
      Output::ExitCodes SetSizes(const unsigned int& numCols,
                                 const unsigned int& =0) { return SetSize(numCols); }
      Output::ExitCodes SetValue(const int& i,
                                 const double& val);
      Output::ExitCodes SetValues(const vector<int>&,
                                  const vector<double>&);

      Output::ExitCodes Zeros() { _vector.setZero(); return Output::Success; }
      double& operator[] (const int& i) { return _vector[i]; }
      const double& operator[] (const int& i) const { return _vector[i]; }

      EigenVector<EigenVectorType> operator+(const EigenVector<EigenVectorType>& other) const
      {
        EigenVector<EigenVectorType> sumResult;
        sumResult._vector = _vector + other._vector;
        return sumResult;
      }

      EigenVector<EigenVectorType> operator-(const EigenVector<EigenVectorType>& other) const
      {
        EigenVector<EigenVectorType> sumResult;
        sumResult._vector = _vector - other._vector;
        return sumResult;
      }


      EigenVector<EigenVectorType>& operator=(EigenVectorType&& vector)
      {
        _vector = std::move(vector);
        return *this;
      }
  };
}

#endif // EIGENVECTOR_HPP
