#ifndef __EIGENLUSOLVER_H
#define __EIGENLUSOLVER_H

#include "ILinearSolver.hpp"
#include "EigenSparseMatrix.hpp"
#include "EigenVector.hpp"

using namespace Eigen;

namespace GeDiM
{
  class EigenLUSolverConfig
  {
    public:
      bool SymmetricProblem = true; ///<< True if the resulting global matrix is known to be symmetric.
  };

  /// \brief Eigen LU Linear solver
  /// \copyright See top level LICENSE file for details.
  class EigenLUSolver final : public ILinearSolver
  {
    private:
      EigenLUSolverConfig _configuration;
      SparseLU<SparseMatrix<double>> linearSolver; ///< The solver
      const IVector* _rightHandSide; ///< The rightHandSide of the linear syste
      IVector* _solution; ///< The solution of the linear syste

    public:
      EigenLUSolver(const EigenLUSolverConfig& configuration = EigenLUSolverConfig());
      ~EigenLUSolver();

      Output::ExitCodes Initialize(const ISparseMatrix& matrix,
                                   const IVector& rightHandSide,
                                   IVector& solution);

      Output::ExitCodes Solve() const;
  };
}

#endif // __EIGENLUSOLVER_H
