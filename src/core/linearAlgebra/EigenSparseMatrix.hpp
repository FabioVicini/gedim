#ifndef EIGENSPARSEMATRIX_HPP
#define EIGENSPARSEMATRIX_HPP

#include "ISparseMatrix.hpp"
#include "EigenVector.hpp"
#include "Eigen"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  /// \brief Eigen sparse matrix
  /// \copyright See top level LICENSE file for details.
  template<typename EigenMatrixType = Eigen::SparseMatrix<double>>
  class EigenSparseMatrix final : public ISparseMatrix
  {
    private:
      EigenMatrixType _matrix; ///< internal matrix
      SparseMatrixFillMode _fillMode;
      list<Eigen::Triplet<double>> _triplets;

    public:
      EigenSparseMatrix()
      {

      }
      ~EigenSparseMatrix()
      {
        _triplets.clear();
      }

      operator EigenMatrixType&() { return _matrix; }
      operator const EigenMatrixType&() const { return _matrix; }

      Output::ExitCodes SetSize(const unsigned int& numRows,
                                const unsigned int& numCols)
      {
        _matrix.resize(numRows, numCols);
        return Output::Success;
      }
      Output::ExitCodes SetSizes(const unsigned int& numRows,
                                 const unsigned int& numCols,
                                 const unsigned int&,
                                 const unsigned int& ,
                                 const unsigned int& ,
                                 const vector<int>& ,
                                 const vector<int>& )
      {return SetSize(numRows,numCols);}
      Output::ExitCodes Create()
      {
        _matrix.setFromTriplets(_triplets.begin(), _triplets.end());
        _matrix.makeCompressed();
        _triplets.clear();

        return Output::Success;
      }
      Output::ExitCodes Flush()
      {
        return Output::UnimplementedMethod;
      }
      void SetFillMode(const SparseMatrixFillMode& inputMode) { _fillMode = inputMode; }
      Output::ExitCodes Reset()
      {
        _matrix.prune(0.0);
        return Output::Success;
      }
      Output::ExitCodes Triplet(const unsigned int& i,
                                const unsigned int& j,
                                const double& value)
      {
        _triplets.push_back(Eigen::Triplet<double>(i, j, value));
        return Output::Success;
      }
      Output::ExitCodes Triplets(const vector<unsigned int>& i,
                                 const vector<unsigned int>& j,
                                 const vector<double>& values)
      {
        if (i.size() != j.size() || i.size() != values.size())
        {
          Output::PrintGenericMessage("%s: invalid triplets size", false, __func__);
          return Output::GenericError;
        }

        unsigned int numTriplets = i.size();
        for (unsigned int t = 0; t < numTriplets; t++)
          _triplets.push_back(Eigen::Triplet<double>(i[t], j[t], values[t]));

        return Output::Success;
      }

      template<typename VectorType>
      EigenVector<VectorType> operator*(const EigenVector<VectorType>& other) const
      {
        EigenVector<VectorType> productResult;
        productResult = _matrix * static_cast<const VectorType&>(other);
        return productResult;
      }

  };
}

#endif // EIGENSPARSEMATRIX_HPP
